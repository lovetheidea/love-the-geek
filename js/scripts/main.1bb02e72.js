(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("@ux/button")["default"], require("@ux/checkbox")["default"], require("@ux/checkbox-group")["default"], require("@ux/dropdown")["default"], require("@ux/form")["default"], require("@ux/form-element")["default"], require("@ux/icon")["default"], require("@ux/modal")["default"], require("@ux/quantity-selector")["default"], require("@ux/radio")["default"], require("@ux/tooltip")["default"], require("ux"), require("ReactDOM"), require("react-intl")["default"]);
	else if(typeof define === 'function' && define.amd)
		define(["@ux/button", "@ux/checkbox", "@ux/checkbox-group", "@ux/dropdown", "@ux/form", "@ux/form-element", "@ux/icon", "@ux/modal", "@ux/quantity-selector", "@ux/radio", "@ux/tooltip", "ux", "ReactDOM", "react-intl"], factory);
	else {
		var a = typeof exports === 'object' ? factory(require("@ux/button")["default"], require("@ux/checkbox")["default"], require("@ux/checkbox-group")["default"], require("@ux/dropdown")["default"], require("@ux/form")["default"], require("@ux/form-element")["default"], require("@ux/icon")["default"], require("@ux/modal")["default"], require("@ux/quantity-selector")["default"], require("@ux/radio")["default"], require("@ux/tooltip")["default"], require("ux"), require("ReactDOM"), require("react-intl")["default"]) : factory(root["ux"]["Button"], root["ux"]["Checkbox"], root["ux"]["CheckboxGroup"], root["ux"]["Dropdown"], root["ux"]["Form"], root["ux"]["FormElement"], root["ux"]["Icon"], root["ux"]["Modal"], root["ux"]["QuantitySelector"], root["ux"]["Radio"], root["ux"]["Tooltip"], root["ux"], root["ReactDOM"], root["ux"]["intl"]);
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(typeof self !== 'undefined' ? self : this, function(__WEBPACK_EXTERNAL_MODULE__ux_button__, __WEBPACK_EXTERNAL_MODULE__ux_checkbox__, __WEBPACK_EXTERNAL_MODULE__ux_checkbox_group__, __WEBPACK_EXTERNAL_MODULE__ux_dropdown__, __WEBPACK_EXTERNAL_MODULE__ux_form__, __WEBPACK_EXTERNAL_MODULE__ux_form_element__, __WEBPACK_EXTERNAL_MODULE__ux_icon__, __WEBPACK_EXTERNAL_MODULE__ux_modal__, __WEBPACK_EXTERNAL_MODULE__ux_quantity_selector__, __WEBPACK_EXTERNAL_MODULE__ux_radio__, __WEBPACK_EXTERNAL_MODULE__ux_tooltip__, __WEBPACK_EXTERNAL_MODULE__ux_uxcore2__, __WEBPACK_EXTERNAL_MODULE_react_dom__, __WEBPACK_EXTERNAL_MODULE_react_intl__) {
return webpackJsonp(["main"],{

/***/ "./node_modules/@reseller/domain-iq-ui/dist/actions/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DISABLE_ALL_BUTTONS = exports.FORCE_EXACT_MATCH_SELECT = exports.FLIP_RACE_CONDITION_MODAL_STATE = exports.FLIP_DISCLAIMER_MODAL_STATE = exports.FLIP_DBS_MODAL_STATE = exports.SET_MAX_PRICE = exports.SET_SLD_MAX_SIZE = exports.DOMAIN_SEARCH_SUCCESS = exports.SET_SEARCH_STATUS = exports.REMOVE_FROM_CART = exports.ADD_TO_CART = exports.SET_CART_DOMAINS = exports.SET_CROSS_SELL = exports.SET_BUNDLE_RESULTS = exports.SET_DOMAIN_SEARCH_RESULT = exports.TOGGLE_CCTLD_BUTTON = exports.TOGGLE_PREMIUM_BUTTON = exports.TOGGLE_TLD_BUTTON = exports.SET_SPIN_RESULTS = exports.SET_INIT_SPIN_RESULTS = exports.SET_FILTERS = exports.SET_PRODUCTS = undefined;

var _ActionTypes = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/constants/ActionTypes.js");

var _ActionTypes2 = _interopRequireDefault(_ActionTypes);

var _HttpRequestHelper = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/HttpRequestHelper.js");

var _HttpRequestHelper2 = _interopRequireDefault(_HttpRequestHelper);

var _UrlHelper = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/UrlHelper.js");

var _UrlHelper2 = _interopRequireDefault(_UrlHelper);

var _Logger = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/Logger.js");

var _Logger2 = _interopRequireDefault(_Logger);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var SET_PRODUCTS = exports.SET_PRODUCTS = function SET_PRODUCTS(products) {
  return {
    type: _ActionTypes2.default.SET_PRODUCTS,
    products: products
  };
};

var SET_FILTERS = exports.SET_FILTERS = function SET_FILTERS(tlds, recommendedDomains) {
  return {
    type: _ActionTypes2.default.SET_FILTERS,
    tlds: tlds,
    recommendedDomains: recommendedDomains
  };
};

var SET_INIT_SPIN_RESULTS = exports.SET_INIT_SPIN_RESULTS = function SET_INIT_SPIN_RESULTS(spins) {
  return {
    type: _ActionTypes2.default.SET_INIT_SPIN_RESULTS,
    spins: spins
  };
};

var SET_SPIN_RESULTS = exports.SET_SPIN_RESULTS = function SET_SPIN_RESULTS(spins) {
  return {
    type: _ActionTypes2.default.SET_SPIN_RESULTS,
    spins: spins
  };
};

var fetchSpins = function fetchSpins(hostOverride, privateLabelId) {
  return function (dispatch, getState) {
    var sources = new Set();

    var _getState = getState(),
        filterInfo = _getState.filterInfo,
        results = _getState.results;

    var searchPhrase = results.ExactMatchDomain.Fqdn;
    if (filterInfo.showOnlyCCTLDS) {
      sources.add("cctld");
    }
    if (filterInfo.showOnlyPremiums) {
      sources.add("premium");
    }
    var sourcesArray = [];
    sources.forEach(function (v) {
      return sourcesArray.push(v);
    });
    var activeTldArray = [];
    filterInfo.activeTlds.forEach(function (v) {
      return activeTldArray.push(v);
    });
    // no api key specified
    var spinUrl = _UrlHelper2.default.getSpinUrl(hostOverride, searchPhrase, '', privateLabelId, activeTldArray, sourcesArray);
    var successCallback = function successCallback(result) {
      dispatch(SET_SPIN_RESULTS(result.data || {}));
    };
    var failureCallback = function failureCallback(error) {
      console.log(error);
      return Promise.reject(error);
    };
    var params = {
      httpMethod: "GET",
      needCredentials: true,
      apiUrl: spinUrl,
      logInfo: {
        api: 'spins',
        trigger: 'filter'
      },
      successCallback: successCallback,
      failureCallback: failureCallback
    };
    _HttpRequestHelper2.default.callApi(params);
  };
};

var TOGGLE_TLD_BUTTON = exports.TOGGLE_TLD_BUTTON = function TOGGLE_TLD_BUTTON(tld, hostOverride, privateLabelId) {
  return function (dispatch, getState) {
    var _getState2 = getState(),
        filterInfo = _getState2.filterInfo;

    var oldTlds = filterInfo.activeTlds;
    var tlds = new Set();
    oldTlds.forEach(function (x) {
      return tlds.add(x);
    });
    if (oldTlds.has(tld)) {
      tlds.delete(tld);
    } else {
      tlds.add(tld);
    }
    dispatch({
      type: _ActionTypes2.default.TOGGLE_TLD_BUTTON,
      tlds: tlds,
      tld: tld
    });
    return dispatch(fetchSpins(hostOverride, privateLabelId));
  };
};

var TOGGLE_PREMIUM_BUTTON = exports.TOGGLE_PREMIUM_BUTTON = function TOGGLE_PREMIUM_BUTTON(_ref) {
  var urlOverride = _ref.urlOverride,
      privateLabelId = _ref.privateLabelId;
  return function (dispatch) {
    dispatch({ type: _ActionTypes2.default.TOGGLE_PREMIUM_BUTTON });
    return dispatch(fetchSpins(urlOverride, privateLabelId));
  };
};

var TOGGLE_CCTLD_BUTTON = exports.TOGGLE_CCTLD_BUTTON = function TOGGLE_CCTLD_BUTTON(_ref2) {
  var urlOverride = _ref2.urlOverride,
      privateLabelId = _ref2.privateLabelId;
  return function (dispatch) {
    dispatch({ type: _ActionTypes2.default.TOGGLE_CCTLD_BUTTON });
    return dispatch(fetchSpins(urlOverride, privateLabelId));
  };
};

var SET_DOMAIN_SEARCH_RESULT = exports.SET_DOMAIN_SEARCH_RESULT = function SET_DOMAIN_SEARCH_RESULT(result) {
  var clrBundleAndXSell = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

  return {
    type: _ActionTypes2.default.SET_DOMAIN_SEARCH_RESULT,
    result: result,
    clrBundleAndXSell: clrBundleAndXSell
  };
};

var SET_BUNDLE_RESULTS = exports.SET_BUNDLE_RESULTS = function SET_BUNDLE_RESULTS(bundles) {
  return {
    type: _ActionTypes2.default.SET_BUNDLE_RESULTS,
    bundles: bundles
  };
};

var SET_CROSS_SELL = exports.SET_CROSS_SELL = function SET_CROSS_SELL(crossSell) {
  return {
    type: _ActionTypes2.default.SET_CROSS_SELL,
    crossSell: crossSell
  };
};

var SET_CART_DOMAINS = exports.SET_CART_DOMAINS = function SET_CART_DOMAINS(domains) {
  return {
    type: _ActionTypes2.default.SET_CART_DOMAINS,
    domains: domains
  };
};

var ADD_TO_CART = exports.ADD_TO_CART = function ADD_TO_CART(domains) {
  return {
    type: _ActionTypes2.default.ADD_TO_CART,
    domains: domains
  };
};

var REMOVE_FROM_CART = exports.REMOVE_FROM_CART = function REMOVE_FROM_CART(domains) {
  return {
    type: _ActionTypes2.default.REMOVE_FROM_CART,
    domains: domains
  };
};

var SET_SEARCH_STATUS = exports.SET_SEARCH_STATUS = function SET_SEARCH_STATUS(searchStatus) {
  return {
    type: _ActionTypes2.default.SET_SEARCH_STATUS,
    searchStatus: searchStatus
  };
};

var DOMAIN_SEARCH_SUCCESS = exports.DOMAIN_SEARCH_SUCCESS = function DOMAIN_SEARCH_SUCCESS(result, extraSearches) {
  return function (dispatch) {

    extraSearches.searchStatusPromise.then(function (status) {
      dispatch(SET_SEARCH_STATUS(status));
      dispatch(SET_DOMAIN_SEARCH_RESULT(result));
    }).catch(function (err) {
      console.log(err);
      (0, _Logger2.default)({
        type: 'fos showing domainSearchError',
        error: err
      });
      dispatch(SET_SEARCH_STATUS("error"));
      dispatch(SET_DOMAIN_SEARCH_RESULT({}));
    });

    extraSearches.spinPromise.then(function (val) {
      if (val.status === 200) {
        dispatch(SET_INIT_SPIN_RESULTS(val.data || {}));
        !val.data && (0, _Logger2.default)({
          type: 'response body empty',
          error: null,
          meta: {
            api: 'spins',
            apiUrl: val && val.config && val.config.url
          }
        });
      } else {
        dispatch(SET_INIT_SPIN_RESULTS({}));
        (0, _Logger2.default)({
          type: 'response status not 200',
          error: null,
          meta: {
            api: 'spins',
            apiUrl: val && val.config && val.config.url
          }
        });
      }
    }).catch(function () {
      dispatch(SET_INIT_SPIN_RESULTS({}));
    });

    extraSearches.bundlePromise.then(function (val) {
      if (val.status === 200) {
        dispatch(SET_BUNDLE_RESULTS(val.data[0] || {}));
        (!val.data || val.data.length < 1) && (0, _Logger2.default)({
          type: 'response body empty',
          error: null,
          meta: {
            api: 'bundle',
            apiUrl: val && val.config && val.config.url
          }
        });
      } else {
        dispatch(SET_BUNDLE_RESULTS({}));
        (0, _Logger2.default)({
          type: 'response status not 200',
          error: null,
          meta: {
            api: 'bundle',
            apiUrl: val && val.config && val.config.url
          }
        });
      }
    }).catch(function () {
      dispatch(SET_BUNDLE_RESULTS({}));
    });

    extraSearches.crossSellPromise.then(function (val) {
      if (val.status === 200) {
        dispatch(SET_CROSS_SELL(val.data || {}));
        !val.data && (0, _Logger2.default)({
          type: 'response body empty',
          error: null,
          meta: {
            api: 'crosssell',
            apiUrl: val && val.config && val.config.url
          }
        });
      } else {
        dispatch(SET_CROSS_SELL({}));
        (0, _Logger2.default)({
          type: 'response status not 200',
          error: null,
          meta: {
            api: 'crosssell',
            apiUrl: val && val.config && val.config.url
          }
        });
      }
    }).catch(function () {
      dispatch(SET_CROSS_SELL({}));
    });

    extraSearches.inCartPromise.then(function (val) {
      if (val.status === 200) {
        dispatch(SET_CART_DOMAINS(val.data.PendingDomainNames || []));
        !val.data && (0, _Logger2.default)({
          type: 'response body empty',
          error: null,
          meta: {
            api: 'cart',
            apiUrl: val && val.config && val.config.url
          }
        });
      } else {
        dispatch(SET_CART_DOMAINS([]));
        (0, _Logger2.default)({
          type: 'response status not 200',
          error: null,
          meta: {
            api: 'cart',
            apiUrl: val && val.config && val.config.url
          }
        });
      }
    }).catch(function () {
      dispatch(SET_CART_DOMAINS([]));
    });
  };
};

var SET_SLD_MAX_SIZE = exports.SET_SLD_MAX_SIZE = function SET_SLD_MAX_SIZE(size, defaultSize) {
  return {
    type: _ActionTypes2.default.SET_SLD_MAX_SIZE,
    size: size,
    defaultSize: defaultSize
  };
};

var SET_MAX_PRICE = exports.SET_MAX_PRICE = function SET_MAX_PRICE(price) {
  return {
    type: _ActionTypes2.default.SET_MAX_PRICE,
    price: price
  };
};

var FLIP_DBS_MODAL_STATE = exports.FLIP_DBS_MODAL_STATE = function FLIP_DBS_MODAL_STATE(visibility) {
  return {
    type: _ActionTypes2.default.FLIP_DBS_MODAL_STATE,
    visibility: visibility
  };
};

var FLIP_DISCLAIMER_MODAL_STATE = exports.FLIP_DISCLAIMER_MODAL_STATE = function FLIP_DISCLAIMER_MODAL_STATE(visibility) {
  return {
    type: _ActionTypes2.default.FLIP_DISCLAIMER_MODAL_STATE,
    visibility: visibility
  };
};

/* eslint-disable id-length */
var FLIP_RACE_CONDITION_MODAL_STATE = exports.FLIP_RACE_CONDITION_MODAL_STATE = function FLIP_RACE_CONDITION_MODAL_STATE(visibility) {
  return {
    type: _ActionTypes2.default.FLIP_RACE_CONDITION_MODAL_STATE,
    visibility: visibility
  };
};

var FORCE_EXACT_MATCH_SELECT = exports.FORCE_EXACT_MATCH_SELECT = function FORCE_EXACT_MATCH_SELECT(on, byCartButton) {
  return {
    type: _ActionTypes2.default.FORCE_EXACT_MATCH_SELECT,
    on: on,
    byCartButton: byCartButton
  };
};

var DISABLE_ALL_BUTTONS = exports.DISABLE_ALL_BUTTONS = function DISABLE_ALL_BUTTONS(disable) {
  return {
    type: _ActionTypes2.default.DISABLE_ALL_BUTTONS,
    disable: disable
  };
};

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/components/CheckBoxWidget.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _checkbox = __webpack_require__("@ux/checkbox");

var _checkbox2 = _interopRequireDefault(_checkbox);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var CheckBoxWidget = function CheckBoxWidget(_ref) {
  var label = _ref.label,
      onButtonClick = _ref.onButtonClick,
      isSelected = _ref.isSelected,
      eidApply = _ref.eidApply,
      id = _ref.id;

  return _react2.default.createElement('div', { className: 'filter-container' }, _react2.default.createElement('div', { 'data-eid': eidApply }, _react2.default.createElement(_checkbox2.default, { inline: false, label: label, onChange: onButtonClick, checked: isSelected, id: id })));
};

CheckBoxWidget.propTypes = {
  label: _propTypes2.default.string.isRequired,
  onButtonClick: _propTypes2.default.func.isRequired
};

exports.default = CheckBoxWidget;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/components/DisclaimerWidget.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _modal = __webpack_require__("@ux/modal");

var _modal2 = _interopRequireDefault(_modal);

var _button = __webpack_require__("@ux/button");

var _button2 = _interopRequireDefault(_button);

var _i18nHelper = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/i18nHelper.js");

var _i18nHelper2 = _interopRequireDefault(_i18nHelper);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var DisclaimerWidget = function DisclaimerWidget(_ref) {
  var onOpen = _ref.onOpen,
      onClose = _ref.onClose,
      show = _ref.show,
      icann_fee = _ref.icann_fee,
      language = _ref.language;
  var disclaimer = language.disclaimer,
      limitationModalHeader = language.limitationModalHeader,
      limitationModalButton = language.limitationModalButton,
      productLimitations = language.productLimitations;

  var footer = _react2.default.createElement(_button2.default, { design: 'primary', onClick: onClose }, limitationModalButton);

  var icannFee = icann_fee || '';
  var buyerBrokerFee = '$15';

  var limitations = _i18nHelper2.default.parseString(productLimitations, 1, [icannFee, buyerBrokerFee], true);

  var modal = _react2.default.createElement(_modal2.default, { footer: footer, title: limitationModalHeader, onClose: onClose }, _react2.default.createElement('div', { id: 'disclaimerModal' }, limitations.map(function (limit, i) {
    return _react2.default.createElement('p', { key: i }, limit);
  })));

  return _react2.default.createElement('div', { className: 'container disclaimer' }, _react2.default.createElement('p', { className: 'disclaimerAction', onClick: onOpen }, disclaimer), show && modal);
};

DisclaimerWidget.propTypes = {
  onOpen: _propTypes2.default.func.isRequired,
  onClose: _propTypes2.default.func.isRequired,
  show: _propTypes2.default.bool.isRequired,
  icann_fee: _propTypes2.default.string,
  language: _propTypes2.default.object.isRequired
};

exports.default = DisclaimerWidget;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/components/DomainBuyServiceWidget.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _modal = __webpack_require__("@ux/modal");

var _modal2 = _interopRequireDefault(_modal);

var _button = __webpack_require__("@ux/button");

var _button2 = _interopRequireDefault(_button);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var DomainBuyServiceWidget = function DomainBuyServiceWidget(_ref) {
  var onCloseDBS = _ref.onCloseDBS,
      dbsHeadline1 = _ref.dbsHeadline1,
      dbsHeadline2 = _ref.dbsHeadline2,
      dbsBody = _ref.dbsBody,
      dbsButtonText = _ref.dbsButtonText,
      dbsPrice = _ref.dbsPrice,
      selectDBSDomain = _ref.selectDBSDomain,
      dbsPriceSuffix = _ref.dbsPriceSuffix;

  var footer = _react2.default.createElement('div', null, _react2.default.createElement(_button2.default, { design: 'primary', onClick: function onClick() {
      selectDBSDomain();onCloseDBS();
    } }, dbsButtonText));

  return _react2.default.createElement(_modal2.default, { title: dbsHeadline1, onClose: onCloseDBS }, _react2.default.createElement('h4', { className: 'headline-primary' }, dbsHeadline2), _react2.default.createElement('p', null, dbsBody), _react2.default.createElement('div', null, _react2.default.createElement('h3', { className: 'text-secondary headline-primary', style: { display: "inline-block" } }, dbsPrice), '\xA0', _react2.default.createElement('span', { className: 'text-secondary headline-primary' }, dbsPriceSuffix), footer));
};

exports.default = DomainBuyServiceWidget;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/components/DomainSearchWidget.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _lib = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/index.js");

var _UrlHelper = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/UrlHelper.js");

var _UrlHelper2 = _interopRequireDefault(_UrlHelper);

var _HttpRequestHelper = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/HttpRequestHelper.js");

var _HttpRequestHelper2 = _interopRequireDefault(_HttpRequestHelper);

var _button = __webpack_require__("@ux/button");

var _button2 = _interopRequireDefault(_button);

var _EidCodes = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/constants/EidCodes.js");

var _EidCodes2 = _interopRequireDefault(_EidCodes);

var _SearchStatus = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/constants/SearchStatus.js");

var _SearchStatus2 = _interopRequireDefault(_SearchStatus);

var _CartContinueHelper = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/CartContinueHelper.js");

var _CartContinueHelper2 = _interopRequireDefault(_CartContinueHelper);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var DomainSearchWidget = function DomainSearchWidget(_ref) {
  var initProps = _ref.initProps,
      onDomainSearchSuccess = _ref.onDomainSearchSuccess,
      failureCallback = _ref.failureCallback,
      domainsInCart = _ref.domainsInCart,
      forceExactClickFunc = _ref.forceExactClickFunc,
      searchStatus = _ref.searchStatus,
      allButtonsDisabled = _ref.allButtonsDisabled,
      flipRaceConditionModal = _ref.flipRaceConditionModal,
      Logger = _ref.Logger;
  var language = initProps.language;

  var getPromiseFunc = function getPromiseFunc(urlFunc, extraParams) {
    return function (domainName) {
      return new Promise(function (resolve, reject) {
        var url = urlFunc(initProps.urlOverride, domainName, initProps.apiKey, initProps.privateLabelId);
        var requestParams = _extends({
          httpMethod: "GET",
          apiUrl: url,
          successCallback: resolve,
          headers: { 'Accept': 'application/json' },
          needCredentials: true,
          failureCallback: function failureCallback(err) {
            reject && reject(err);
          }
        }, extraParams);
        _HttpRequestHelper2.default.callApi(requestParams);
      });
    };
  };

  var domainsInCartSize = domainsInCart.size;
  var domainsInCartTxt = domainsInCartSize === 1 ? language.domainInCart : language.domainsInCart;
  var handleCartClick = function handleCartClick() {
    if (domainsInCartSize === 0) {
      forceExactClickFunc(true, true);
    } else {
      (0, _CartContinueHelper2.default)(initProps.urlOverride, initProps.privateLabelId, flipRaceConditionModal, Logger);
    }
  };
  var cartButtonDisabled = domainsInCartSize === 0 && searchStatus !== _SearchStatus2.default.Available || allButtonsDisabled;
  var cartContinue = _react2.default.createElement('div', { className: 'col-xl-3 col-lg-4 offset-lg-0 col-md-12' }, _react2.default.createElement('div', { className: 'clearfix' }, _react2.default.createElement(_button2.default, {
    design: 'purchase',
    onClick: handleCartClick,
    style: { float: 'right' },
    className: 'cart-button',
    disabled: cartButtonDisabled
  }, language.continueToCart)), domainsInCartSize === 0 ? null : _react2.default.createElement('span', { className: 'font-primary-bold float-right' }, domainsInCartTxt.replace('{0}', domainsInCartSize)));

  var spinPromiseFunc = getPromiseFunc(_UrlHelper2.default.getSpinUrl.bind(_UrlHelper2.default), {
    logInfo: {
      api: 'spins',
      trigger: 'search'
    }
  });
  var bundlePromiseFunc = initProps.showBundles ? getPromiseFunc(_UrlHelper2.default.getBundleUrl.bind(_UrlHelper2.default), {
    logInfo: {
      api: 'bundle',
      trigger: 'search'
    }
  }) : function () {
    return Promise.resolve({});
  };
  var crossSellPromiseFunc = initProps.showCrossSell ? getPromiseFunc(_UrlHelper2.default.getCrossSellUrl.bind(_UrlHelper2.default), {
    logInfo: {
      api: 'crosssell',
      trigger: 'search'
    }
  }) : function () {
    return Promise.resolve({});
  };
  var inCartPromiseFunc = getPromiseFunc(_UrlHelper2.default.getAllCart.bind(_UrlHelper2.default), {
    needCredentials: true,
    logInfo: {
      api: 'cart',
      trigger: 'search'
    }
  });
  var tld = initProps.tld || "";
  if (tld && tld.trim().length !== 0) {
    tld = /\..*/.test(tld) ? tld : ".".concat(tld);
  }
  var domainToCheck = /.*\..*/.test(initProps.domainToCheck) ? initProps.domainToCheck : (initProps.domainToCheck || '').concat(tld);

  return _react2.default.createElement('div', { className: 'dpp-results container' }, _react2.default.createElement('div', { className: 'col-xl-9 col-lg-8 col-md-12' }, _react2.default.createElement(_lib.DomainSearch, _extends({}, initProps, {
    getSuccessCallback: onDomainSearchSuccess,
    getFailureCallback: failureCallback,
    showResultsMessage: false,
    showHeadline: false,
    searchTerm: domainToCheck,
    eid: _EidCodes2.default.SearchButton,
    doInitialSearch: !!domainToCheck,
    extraSearchFunctions: {
      spinPromise: spinPromiseFunc,
      bundlePromise: bundlePromiseFunc,
      crossSellPromise: crossSellPromiseFunc,
      inCartPromise: inCartPromiseFunc
    },
    Logger: Logger
  }))), cartContinue);
};

exports.default = DomainSearchWidget;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/components/EmptyResult.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var EmptyResultCard = function EmptyResultCard(_ref) {
  var language = _ref.language;

  var noItemsTextArr = language.EmptyResults.split('{0}');
  return React.createElement("div", { className: "noItems" }, React.createElement("h3", null, noItemsTextArr[0].trim()), React.createElement("p", null, noItemsTextArr[1].trim()), React.createElement("ul", null, React.createElement("li", null, noItemsTextArr[2].trim()), React.createElement("li", null, noItemsTextArr[3].trim()), React.createElement("li", null, noItemsTextArr[4].trim())));
};

exports.default = EmptyResultCard;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/components/ExactMatchWidget.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _lib = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/index.js");

var _HttpRequestHelper = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/HttpRequestHelper.js");

var _HttpRequestHelper2 = _interopRequireDefault(_HttpRequestHelper);

var _UrlHelper = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/UrlHelper.js");

var _UrlHelper2 = _interopRequireDefault(_UrlHelper);

var _DomainBuyServiceWidget = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/components/DomainBuyServiceWidget.js");

var _DomainBuyServiceWidget2 = _interopRequireDefault(_DomainBuyServiceWidget);

var _AuctionInfo = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/AuctionInfo.js");

var _AuctionInfo2 = _interopRequireDefault(_AuctionInfo);

var _EidCodes = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/constants/EidCodes.js");

var _EidCodes2 = _interopRequireDefault(_EidCodes);

var _SearchStatus = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/constants/SearchStatus.js");

var _SearchStatus2 = _interopRequireDefault(_SearchStatus);

var _CartContinueHelper = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/CartContinueHelper.js");

var _CartContinueHelper2 = _interopRequireDefault(_CartContinueHelper);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
  }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var ExactMatchWidget = function (_Component) {
  _inherits(ExactMatchWidget, _Component);

  function ExactMatchWidget() {
    _classCallCheck(this, ExactMatchWidget);

    return _possibleConstructorReturn(this, (ExactMatchWidget.__proto__ || Object.getPrototypeOf(ExactMatchWidget)).apply(this, arguments));
  }

  _createClass(ExactMatchWidget, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          results = _props.results,
          crossSellResult = _props.crossSellResult,
          bundleResult = _props.bundleResult,
          initProps = _props.initProps,
          searchStatus = _props.searchStatus,
          onDomainSelect = _props.onDomainSelect,
          supportInfo = _props.supportInfo,
          dbsPrice = _props.dbsPrice,
          onDomainUnSelect = _props.onDomainUnSelect,
          flipDBSModal = _props.flipDBSModal,
          isDBSOn = _props.isDBSOn,
          forcedExactClick = _props.forcedExactClick,
          forceExactClickFunc = _props.forceExactClickFunc,
          domainsInCart = _props.domainsInCart,
          exactResultProduct = _props.exactResultProduct,
          allButtonsDisabled = _props.allButtonsDisabled,
          setDisableAllButtons = _props.setDisableAllButtons,
          flipRaceConditionModal = _props.flipRaceConditionModal,
          Logger = _props.Logger;

      var exactMatchInCart = domainsInCart.has(results.ExactMatchDomain.Fqdn);
      var exactMatch = _extends({}, results.ExactMatchDomain, { isInCart: exactMatchInCart });
      var auctionInfo = (0, _AuctionInfo2.default)(initProps.language, exactMatch);
      var exactCardClass = "";
      if (exactMatch.Inventory === 'premium' && exactMatch.VendorId !== 11 || auctionInfo && auctionInfo.isPremium) {
        if (initProps && initProps.market === 'en-US') {
          return null;
        }
        exactCardClass = "premium";
      } else if (exactMatch.Inventory === 'registry_premium' || exactMatch.IsPremiumTier) {
        exactCardClass = "premium";
      } else if (auctionInfo) {
        if (initProps && initProps.market === 'en-US') {
          return null;
        }
        exactCardClass = "auction";
      } else if (searchStatus === _SearchStatus2.default.PreReg) {
        exactCardClass = "prereg";
      }

      var isTaken = searchStatus === _SearchStatus2.default.Taken;
      var isUnavailable = searchStatus === _SearchStatus2.default.Unavailable;

      var exactClass = isTaken || isUnavailable ? "exact-block-unavailable" : "exact-block-card";

      function getCallback(urlFunc, logInfo, domain) {
        for (var _len = arguments.length, extraParams = Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {
          extraParams[_key - 3] = arguments[_key];
        }

        return function () {
          var url = urlFunc.apply(undefined, [initProps.urlOverride, domain || exactMatch.Fqdn].concat(extraParams));
          var parameters = {
            httpMethod: 'GET',
            needCredentials: true,
            apiUrl: url,
            logInfo: logInfo,
            successCallback: function successCallback() {
              //console.log(res);
            },
            failureCallback: function failureCallback() {
              //console.log(err);
            }
          };
          _HttpRequestHelper2.default.callApi(parameters);
        };
      }

      var selectPostParams = function selectPostParams(domainName, domain, extraInfo) {
        var domainstatus = domain.Inventory === "premium" ? "premium" : "available";
        var extraProps = _extends({ domainstatus: domainstatus }, extraInfo);
        var url = _UrlHelper2.default.getCartAddUrl(initProps.urlOverride, domainName, initProps.privateLabelId, extraProps);
        var byCartButton = forcedExactClick.byCartButton;

        return {
          httpMethod: 'GET',
          needCredentials: true,
          apiUrl: url,
          logInfo: {
            api: 'cart-add',
            trigger: 'exact'
          },
          successCallback: function successCallback() {
            onDomainSelect([domainName]);
            if (byCartButton) {
              (0, _CartContinueHelper2.default)(initProps.urlOverride, initProps.privateLabelId, flipRaceConditionModal, Logger);
            }
          },
          failureCallback: function failureCallback() {
            flipRaceConditionModal(true);
            //console.log(err);
          }
        };
      };
      var unSelectExactCallback = getCallback(_UrlHelper2.default.getCartRemoveUrl.bind(_UrlHelper2.default), {
        api: 'cart-remove',
        trigger: 'exact'
      });

      var getDBSCallback = function getDBSCallback() {
        getCallback(_UrlHelper2.default.getCartAddUrl.bind(_UrlHelper2.default), {
          api: 'cart-add',
          trigger: 'dbs'
        }, null, { domainstatus: 'dbs' })();
        onDomainSelect([exactMatch.Fqdn]);
      };

      var isDynamicBundle = !!bundleResult.DynamicStackId;
      var bundleDomains = isDynamicBundle ? (bundleResult.Stack || []).map(function (d) {
        return d.Fqdn;
      }) : (bundleResult.DotTypesText || '').split(',');

      var addBundleParams = function addBundleParams() {
        var url = _UrlHelper2.default.getBundleCartAddUrl(initProps.urlOverride, exactMatch.Fqdn, initProps.privateLabelId, isDynamicBundle);

        return {
          httpMethod: 'POST',
          needCredentials: true,
          apiUrl: url,
          logInfo: {
            api: 'cart-add',
            trigger: 'bundle'
          },
          successCallback: function successCallback() {
            return onDomainSelect(bundleDomains.concat([exactMatch.Fqdn]));
          },
          failureCallback: function failureCallback() {
            flipRaceConditionModal(true);
            //console.log(err);
          }
        };
      };

      var unSelectBundleCallback = function unSelectBundleCallback() {
        var url = _UrlHelper2.default.getBundleCartRemoveUrl(initProps.urlOverride, exactMatch.Fqdn, initProps.privateLabelId, isDynamicBundle);

        var parameters = {
          httpMethod: 'GET',
          needCredentials: true,
          apiUrl: url,
          logInfo: {
            api: 'cart-remove',
            trigger: 'bundle'
          },
          successCallback: function successCallback() {
            return onDomainUnSelect(bundleDomains);
          },
          failureCallback: function failureCallback() {
            flipRaceConditionModal(true);
            //console.log(err);
          }
        };

        _HttpRequestHelper2.default.callApi(parameters);
      };

      var bundlePriceInfo = isDynamicBundle ? bundleResult.Pricing && {
        CurrentPrice: bundleResult.Pricing.Subtotal,
        ListPrice: bundleResult.Pricing.List,
        ListPriceDisplay: bundleResult.Pricing.ListDisplay,
        CurrentPriceDisplay: bundleResult.Pricing.SubtotalDisplay,
        Savings: bundleResult.Pricing.Savings
      } : bundleResult.Prices && {
        CurrentPrice: bundleResult.Prices.CurrentPrice,
        ListPrice: bundleResult.Prices.ListPrice,
        ListPriceDisplay: bundleResult.Prices.ListPrice,
        CurrentPriceDisplay: bundleResult.Prices.CurrentPrice
      };

      var savings = bundlePriceInfo && bundlePriceInfo.CurrentPrice && bundlePriceInfo.ListPrice && Math.round(100 * bundlePriceInfo.Savings / bundlePriceInfo.ListPrice) + '%';

      var bundleInfo = {
        bundleDomains: bundleDomains,
        bundlePriceInfo: bundlePriceInfo,
        savingsText: isDynamicBundle ? savings : bundleResult.SavingsText
      };

      var getCrossSellUnSelectCallback = function getCrossSellUnSelectCallback(domain) {
        onDomainUnSelect([domain]);
        getCallback(_UrlHelper2.default.getCartRemoveUrl.bind(_UrlHelper2.default), {
          api: 'cart-remove',
          trigger: 'crosssell'
        }, domain)();
      };
      var getCrossSellAddParamsFunc = function getCrossSellAddParamsFunc(domain) {
        var url = _UrlHelper2.default.getCartAddUrl(initProps.urlOverride, domain, initProps.privateLabelId);
        return {
          httpMethod: 'GET',
          needCredentials: true,
          apiUrl: url,
          logInfo: {
            api: 'cart-add',
            trigger: 'crosssell'
          },
          successCallback: function successCallback() {
            onDomainSelect([domain]);
          },
          failureCallback: function failureCallback() {
            flipRaceConditionModal(true);
            //console.log(err);
          }
        };
      };
      var isCrossSellSelected = function isCrossSellSelected(domain) {
        return domainsInCart.has(domain);
      };

      var extraButtonProps = {
        setDisableAllButtons: setDisableAllButtons,
        exact: {
          buttonDesign: 'primary',
          getUnSelectCallback: function getUnSelectCallback() {
            onDomainUnSelect([exactMatch.Fqdn]);
            unSelectExactCallback();
            if (forcedExactClick.value) {
              forceExactClickFunc(false, false);
            }
          },
          forceExactClickFunc: forceExactClickFunc,
          performSelect: forcedExactClick.value,
          selectPostParams: selectPostParams
        },
        bundle: {
          buttonDesign: 'primary',
          getBundleAddParams: addBundleParams,
          getUnSelectCallback: unSelectBundleCallback,
          eid: _EidCodes2.default.Domain.Bundle,
          showBundles: initProps.showBundles
        },
        crossSell: {
          selected: isCrossSellSelected,
          getUnSelectCallback: getCrossSellUnSelectCallback,
          getCrossSellAddParamsFunc: getCrossSellAddParamsFunc,
          eid: _EidCodes2.default.Domain.ccTLD,
          showCrossSell: initProps.showCrossSell
        }
      };

      var sorryDomainTakenSuffixes = initProps.language.sorryDomainTakenSuffix.split('{0}');
      var domainTaken = isTaken && !initProps.privateLabelId ? _react2.default.createElement('span', { className: 'h6' }, sorryDomainTakenSuffixes[0], '\xA0', _react2.default.createElement('a', { href: 'javascript:void(0);', onClick: function onClick() {
          return flipDBSModal(true);
        } }, sorryDomainTakenSuffixes[1])) : "";

      var initPropsLangChange = _extends({}, initProps, {
        language: _extends({}, initProps.language, {
          sorryDomainTakenSuffix: domainTaken,
          moreInfo: initProps.language.moreInfo.replace('{0}', supportInfo.premiumDomainSupportPhone)
        })
      });

      var exactMatchHeadline = isTaken || isUnavailable ? null : _react2.default.createElement('div', { className: 'exactMatchHeadline' }, _react2.default.createElement('span', { className: 'h2' }, initPropsLangChange.language.domainAvailableText));

      var dbsModalProps = {
        onCloseDBS: function onCloseDBS() {
          return flipDBSModal(false);
        },
        dbsHeadline1: initPropsLangChange.language.DBSHeader1,
        dbsHeadline2: initPropsLangChange.language.DBSHeader2,
        dbsBody: initPropsLangChange.language.DBSBody,
        dbsButtonText: initPropsLangChange.language.DBSButtonText,
        dbsPriceSuffix: initPropsLangChange.language.dbsPriceSuffix,
        dbsPrice: dbsPrice,
        selectDBSDomain: getDBSCallback
      };

      var domainBuyServiceModal = isDBSOn ? _react2.default.createElement(_DomainBuyServiceWidget2.default, dbsModalProps) : null;

      return _react2.default.createElement('div', { className: 'container' }, _react2.default.createElement('div', { className: 'col-md-12' }, exactMatchHeadline, _react2.default.createElement('div', { className: exactClass + ' text-gray-dark clearfix ' + exactCardClass }, _react2.default.createElement(_lib.ExactMatchResult, _extends({}, results, initPropsLangChange, {
        isDisabled: allButtonsDisabled,
        crossSellInfo: isTaken || isUnavailable ? {} : crossSellResult,
        bundleInfo: isTaken || isUnavailable ? {} : bundleInfo,
        exactMatch: exactMatch,
        product: exactResultProduct,
        extraButtonProps: extraButtonProps,
        searchStatus: searchStatus,
        auctionInfo: auctionInfo,
        eid: _EidCodes2.default.Domain.Exact,
        Logger: Logger
      }))), domainBuyServiceModal));
    }
  }]);

  return ExactMatchWidget;
}(_react.Component);

ExactMatchWidget.propTypes = {
  results: _propTypes2.default.object, crossSellResult: _propTypes2.default.object, bundleResult: _propTypes2.default.object,
  initProps: _propTypes2.default.object, searchStatus: _propTypes2.default.string, onDomainSelect: _propTypes2.default.func,
  auction_fee: _propTypes2.default.string, supportInfo: _propTypes2.default.object, dbsPrice: _propTypes2.default.string,
  onDomainUnSelect: _propTypes2.default.func, flipDBSModal: _propTypes2.default.func, isDBSOn: _propTypes2.default.bool,
  forcedExactClick: _propTypes2.default.object, forceExactClickFunc: _propTypes2.default.func, domainsInCart: _propTypes2.default.object,
  exactResultProduct: _propTypes2.default.object, allButtonsDisabled: _propTypes2.default.bool, setDisableAllButtons: _propTypes2.default.func,
  flipRaceConditionModal: _propTypes2.default.func, Logger: _propTypes2.default.func.isRequired
};
exports.default = ExactMatchWidget;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/components/FiltersWidget.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _TldFilterWidget = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/components/TldFilterWidget.js");

var _TldFilterWidget2 = _interopRequireDefault(_TldFilterWidget);

var _CheckBoxWidget = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/components/CheckBoxWidget.js");

var _CheckBoxWidget2 = _interopRequireDefault(_CheckBoxWidget);

var _LengthFilterWidget = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/components/LengthFilterWidget.js");

var _LengthFilterWidget2 = _interopRequireDefault(_LengthFilterWidget);

var _PriceFilterWidget = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/components/PriceFilterWidget.js");

var _PriceFilterWidget2 = _interopRequireDefault(_PriceFilterWidget);

var _form = __webpack_require__("@ux/form");

var _form2 = _interopRequireDefault(_form);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var FilterWidget = function FilterWidget(_ref) {
  var tldFilterInputs = _ref.tldFilterInputs,
      premiumFilterInputs = _ref.premiumFilterInputs,
      cctldFilterInputs = _ref.cctldFilterInputs,
      sizeFilterInputs = _ref.sizeFilterInputs,
      priceFilterInputs = _ref.priceFilterInputs;

  return _react2.default.createElement('div', { className: 'filters text-gray-dark' }, _react2.default.createElement('div', { className: 'card ux-card' }, _react2.default.createElement('div', { className: 'card-block' }, _react2.default.createElement(_form2.default, { className: 'filter-form' }, _react2.default.createElement(_TldFilterWidget2.default, tldFilterInputs), _react2.default.createElement(_LengthFilterWidget2.default, sizeFilterInputs), _react2.default.createElement(_PriceFilterWidget2.default, priceFilterInputs), _react2.default.createElement(_CheckBoxWidget2.default, cctldFilterInputs), _react2.default.createElement(_CheckBoxWidget2.default, premiumFilterInputs)))));
};

FilterWidget.propTypes = {
  priceFilterInputs: _propTypes2.default.object.isRequired,
  tldFilterInputs: _propTypes2.default.object.isRequired,
  premiumFilterInputs: _propTypes2.default.object.isRequired,
  cctldFilterInputs: _propTypes2.default.object.isRequired
};

exports.default = FilterWidget;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/components/LengthFilterWidget.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _quantitySelector = __webpack_require__("@ux/quantity-selector");

var _quantitySelector2 = _interopRequireDefault(_quantitySelector);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var LengthFilterWidget = function LengthFilterWidget(_ref) {
  var label = _ref.label,
      onDomainSizeSet = _ref.onDomainSizeSet,
      defaultMaximum = _ref.defaultMaximum,
      maxSet = _ref.maxSet,
      eidApply = _ref.eidApply,
      errorMessage = _ref.errorMessage;

  return _react2.default.createElement('div', { className: 'filter-container' }, _react2.default.createElement('label', { className: 'custom-control-description text-gray-dark' }, label), _react2.default.createElement('div', { id: 'length-filter' }, _react2.default.createElement('div', { 'data-eid': eidApply }, _react2.default.createElement(_quantitySelector2.default, { max: defaultMaximum, defaultValue: maxSet,
    errorMessage: errorMessage || 'Your input is invalid.', onChange: function onChange(x) {
      onDomainSizeSet(x);
    } }))));
};

LengthFilterWidget.propTypes = {
  label: _propTypes2.default.string.isRequired,
  onDomainSizeSet: _propTypes2.default.func.isRequired,
  defaultMaximum: _propTypes2.default.number.isRequired,
  maxSet: _propTypes2.default.number.isRequired
};

exports.default = LengthFilterWidget;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/components/NewExactMatchWidget.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _lib = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/index.js");

var _DomainBuyServiceWidget = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/components/DomainBuyServiceWidget.js");

var _DomainBuyServiceWidget2 = _interopRequireDefault(_DomainBuyServiceWidget);

var _Notification = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/components/Notification.js");

var _Notification2 = _interopRequireDefault(_Notification);

var _HttpRequestHelper = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/HttpRequestHelper.js");

var _HttpRequestHelper2 = _interopRequireDefault(_HttpRequestHelper);

var _UrlHelper = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/UrlHelper.js");

var _UrlHelper2 = _interopRequireDefault(_UrlHelper);

var _AuctionInfo = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/AuctionInfo.js");

var _AuctionInfo2 = _interopRequireDefault(_AuctionInfo);

var _RtbInfo = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/RtbInfo.js");

var _RtbInfo2 = _interopRequireDefault(_RtbInfo);

var _CartContinueHelper = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/CartContinueHelper.js");

var _CartContinueHelper2 = _interopRequireDefault(_CartContinueHelper);

var _EidCodes = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/constants/EidCodes.js");

var _EidCodes2 = _interopRequireDefault(_EidCodes);

var _AvailablityStatus = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/constants/AvailablityStatus.js");

var _AvailablityStatus2 = _interopRequireDefault(_AvailablityStatus);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var ExactMatchWidget = function ExactMatchWidget(_ref) {
  var results = _ref.results,
      crossSellResult = _ref.crossSellResult,
      bundleResult = _ref.bundleResult,
      initProps = _ref.initProps,
      searchStatus = _ref.searchStatus,
      onDomainSelect = _ref.onDomainSelect,
      supportInfo = _ref.supportInfo,
      dbsPrice = _ref.dbsPrice,
      onDomainUnSelect = _ref.onDomainUnSelect,
      flipDBSModal = _ref.flipDBSModal,
      isDBSOn = _ref.isDBSOn,
      forcedExactClick = _ref.forcedExactClick,
      forceExactClickFunc = _ref.forceExactClickFunc,
      domainsInCart = _ref.domainsInCart,
      exactResultProduct = _ref.exactResultProduct,
      flipRaceConditionModal = _ref.flipRaceConditionModal,
      allButtonsDisabled = _ref.allButtonsDisabled,
      setDisableAllButtons = _ref.setDisableAllButtons,
      Logger = _ref.Logger;

  /* Exact Match */
  var exactMatch = _extends({}, results.ExactMatchDomain, {
    isInCart: domainsInCart.has(results.ExactMatchDomain.Fqdn)
  });
  var auctionInfo = (0, _AuctionInfo2.default)(initProps.language, exactMatch);
  var rtbInfo = (0, _RtbInfo2.default)(initProps.language, exactMatch);

  // tmp logic for valuation UI
  if (!initProps || initProps.market !== 'en-US' || exactMatch.AvailabilityLabel !== _AvailablityStatus2.default.Premium
  //&& exactMatch.AvailabilityLabel !== AVAIL_STATUS.RegPremium
  && exactMatch.AvailabilityLabel !== _AvailablityStatus2.default.Auction) {
    return null;
  }

  function getClassName(label) {
    switch (label) {
      case _AvailablityStatus2.default.Available:
        return 'available';

      // to do: add design for prereg
      case _AvailablityStatus2.default.PreReg:
        return 'available';

      case _AvailablityStatus2.default.Premium:
      case _AvailablityStatus2.default.RegPremium:
        return 'premium';

      case _AvailablityStatus2.default.Auction:
        return 'auction';

      case _AvailablityStatus2.default.Taken:
      case _AvailablityStatus2.default.Excluded:
      case _AvailablityStatus2.default.Reserved:
        return 'taken';

      default:
        return 'error';
    }
  }

  var exactClassname = getClassName(exactMatch.AvailabilityLabel);

  function getCallback(urlFunc, logInfo, domain) {
    for (var _len = arguments.length, extraParams = Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {
      extraParams[_key - 3] = arguments[_key];
    }

    return function () {
      var url = urlFunc.apply(undefined, [initProps.urlOverride, domain || exactMatch.Fqdn].concat(extraParams));
      var parameters = {
        httpMethod: 'GET',
        needCredentials: true,
        apiUrl: url,
        logInfo: logInfo,
        successCallback: function successCallback() {
          //console.log(res);
        },
        failureCallback: function failureCallback() {
          //console.log(err);
        }
      };
      _HttpRequestHelper2.default.callApi(parameters);
    };
  }

  var goToCart = function goToCart() {
    return (0, _CartContinueHelper2.default)(initProps.urlOverride, initProps.privateLabelId, flipRaceConditionModal, Logger);
  };
  var selectPostParams = function selectPostParams(domainName, domain, extraInfo) {
    var domainstatus = domain.Inventory === "premium" ? "premium" : "available";
    var extraProps = _extends({ domainstatus: domainstatus }, extraInfo);
    var url = _UrlHelper2.default.getCartAddUrl(initProps.urlOverride, domainName, initProps.privateLabelId, extraProps);
    var byCartButton = forcedExactClick.byCartButton;

    return {
      httpMethod: 'GET',
      needCredentials: true,
      apiUrl: url,
      logInfo: {
        api: 'cart-add',
        trigger: 'exact'
      },
      successCallback: function successCallback() {
        onDomainSelect([domainName]);
        if (byCartButton) {
          goToCart();
        }
      },
      failureCallback: function failureCallback() {
        flipRaceConditionModal(true);
        //console.log(err);
      }
    };
  };

  var unSelectExactCallback = getCallback(_UrlHelper2.default.getCartRemoveUrl.bind(_UrlHelper2.default), {
    api: 'cart-remove',
    trigger: 'exact'
  });

  var exactButtonProps = {
    buttonDesign: 'purchase',
    getUnSelectCallback: function getUnSelectCallback() {
      onDomainUnSelect([exactMatch.Fqdn]);
      unSelectExactCallback();
      if (forcedExactClick.value) {
        forceExactClickFunc(false, false);
      }
    },
    setDisableAllButtons: setDisableAllButtons,
    forceExactClickFunc: forceExactClickFunc,
    performSelect: forcedExactClick.value,
    selectPostParams: selectPostParams,
    goToCart: goToCart,
    openDBS: function openDBS() {
      return flipDBSModal(true);
    }
  };

  /* Bundle */
  var isDynamicBundle = !!bundleResult.DynamicStackId;
  var bundleDomains = isDynamicBundle ? (bundleResult.Stack || []).map(function (d) {
    return d.Fqdn;
  }) : (bundleResult.DotTypesText || '').split(',');

  var addBundleParams = function addBundleParams() {
    var url = _UrlHelper2.default.getBundleCartAddUrl(initProps.urlOverride, exactMatch.Fqdn, initProps.privateLabelId, isDynamicBundle);

    return {
      httpMethod: 'POST',
      needCredentials: true,
      apiUrl: url,
      logInfo: {
        api: 'cart-add',
        trigger: 'bundle'
      },
      successCallback: function successCallback() {
        return onDomainSelect(bundleDomains.concat([exactMatch.Fqdn]));
      },
      failureCallback: function failureCallback() {
        flipRaceConditionModal(true);
        //console.log(err);
      }
    };
  };

  var unSelectBundleCallback = function unSelectBundleCallback() {
    var url = _UrlHelper2.default.getBundleCartRemoveUrl(initProps.urlOverride, exactMatch.Fqdn, initProps.privateLabelId, isDynamicBundle);

    var parameters = {
      httpMethod: 'GET',
      needCredentials: true,
      apiUrl: url,
      logInfo: {
        api: 'cart-remove',
        trigger: 'bundle'
      },
      successCallback: function successCallback() {
        return onDomainUnSelect(bundleDomains);
      },
      failureCallback: function failureCallback() {
        flipRaceConditionModal(true);
        //console.log(err);
      }
    };

    _HttpRequestHelper2.default.callApi(parameters);
  };

  var bundleButtonProps = {
    buttonDesign: 'defaultPurchase',
    getBundleAddParams: addBundleParams,
    getUnSelectCallback: unSelectBundleCallback,
    setDisableAllButtons: setDisableAllButtons,
    eid: _EidCodes2.default.Domain.Bundle,
    showBundles: initProps.showBundles
  };

  var bundlePriceInfo = isDynamicBundle ? bundleResult.Pricing && {
    CurrentPrice: bundleResult.Pricing.Subtotal,
    ListPrice: bundleResult.Pricing.List,
    ListPriceDisplay: bundleResult.Pricing.ListDisplay,
    CurrentPriceDisplay: bundleResult.Pricing.SubtotalDisplay,
    Savings: bundleResult.Pricing.Savings
  } : bundleResult.Prices && {
    CurrentPrice: bundleResult.Prices.CurrentPrice,
    ListPrice: bundleResult.Prices.ListPrice,
    ListPriceDisplay: bundleResult.Prices.ListPrice,
    CurrentPriceDisplay: bundleResult.Prices.CurrentPrice
  };

  var savings = bundlePriceInfo && bundlePriceInfo.CurrentPrice && bundlePriceInfo.ListPrice && Math.round(100 * bundlePriceInfo.Savings / bundlePriceInfo.ListPrice) + '%';

  var bundleInfo = {
    bundleDomains: bundleDomains,
    bundlePriceInfo: bundlePriceInfo,
    savingsText: isDynamicBundle ? savings : bundleResult.SavingsText
  };

  /* Cross Sell */
  var getCrossSellUnSelectCallback = function getCrossSellUnSelectCallback(domain) {
    onDomainUnSelect([domain]);
    getCallback(_UrlHelper2.default.getCartRemoveUrl.bind(_UrlHelper2.default), {
      api: 'cart-remove',
      trigger: 'crosssell'
    }, domain)();
  };

  var getCrossSellAddParamsFunc = function getCrossSellAddParamsFunc(domain) {
    var url = _UrlHelper2.default.getCartAddUrl(initProps.urlOverride, domain, initProps.privateLabelId);
    return {
      httpMethod: 'GET',
      needCredentials: true,
      apiUrl: url,
      logInfo: {
        api: 'cart-add',
        trigger: 'crosssell'
      },
      successCallback: function successCallback() {
        onDomainSelect([domain]);
      },
      failureCallback: function failureCallback() {
        flipRaceConditionModal(true);
        //console.log(err);
      }
    };
  };

  var isCrossSellSelected = function isCrossSellSelected(domain) {
    return domainsInCart.has(domain);
  };

  var crossSellButtonProps = {
    selected: isCrossSellSelected,
    getUnSelectCallback: getCrossSellUnSelectCallback,
    getCrossSellAddParamsFunc: getCrossSellAddParamsFunc,
    eid: _EidCodes2.default.Domain.ccTLD,
    showCrossSell: initProps.showCrossSell
  };

  /* Domain Buy Service */
  var getDBSCallback = function getDBSCallback() {
    getCallback(_UrlHelper2.default.getCartAddUrl.bind(_UrlHelper2.default), {
      api: 'cart-add',
      trigger: 'dbs'
    }, null, { domainstatus: 'dbs' })();
    onDomainSelect([exactMatch.Fqdn]);
  };

  var dbsModalProps = {
    onCloseDBS: function onCloseDBS() {
      return flipDBSModal(false);
    },
    dbsHeadline1: initProps.language.DBSHeader1,
    dbsHeadline2: initProps.language.DBSHeader2,
    dbsBody: initProps.language.DBSBody,
    dbsButtonText: initProps.language.DBSButtonText,
    dbsPriceSuffix: initProps.language.dbsPriceSuffix,
    dbsPrice: dbsPrice,
    selectDBSDomain: getDBSCallback
  };

  var domainBuyServiceModal = isDBSOn ? _react2.default.createElement(_DomainBuyServiceWidget2.default, dbsModalProps) : null;

  /* Notification Box */
  var notification = null;

  // Restricted
  if (exactResultProduct && exactResultProduct.Content) {
    notification = _react2.default.createElement(_Notification2.default, {
      text: initProps.language.notificationRestricted,
      tooltipText: initProps.language.learnMore,
      tooltipTitle: exactResultProduct.Content.SubHeader,
      tooltipMessage: exactResultProduct.Content.Messages
    });
  }
  // Unsupported
  if (exactMatch.AvailabilityLabel === 'tld_unsupported') {
    notification = _react2.default.createElement(_Notification2.default, {
      text: initProps.language.notificationUnsupported,
      tooltipText: initProps.language.learnMore,
      tooltipTitle: 'Missing tooltipTitle',
      tooltipMessage: 'Missing tooltipMessage'
    });
  }

  // other stuff
  var languagePack = {
    moreInfo: initProps.language.moreInfo.replace('{0}', supportInfo.premiumDomainSupportPhone)
  };
  _extends(initProps.language, languagePack);

  return _react2.default.createElement('div', { className: 'container', style: { marginTop: '20px' } }, _react2.default.createElement('div', { className: 'col-md-12' }, notification, _react2.default.createElement('div', { className: 'new-exact-block-card text-gray-dark clearfix ' + exactClassname }, _react2.default.createElement(_lib.NewExactMatchResult, _extends({}, results, initProps, {
    isDisabled: allButtonsDisabled,
    exactMatch: exactMatch,
    crossSellInfo: crossSellResult,
    bundleInfo: bundleInfo,
    supportInfo: supportInfo,
    product: exactResultProduct,
    exactButtonProps: exactButtonProps,
    bundleButtonProps: bundleButtonProps,
    crossSellButtonProps: crossSellButtonProps,
    searchStatus: searchStatus,
    auctionInfo: auctionInfo,
    rtbInfo: rtbInfo,
    eid: _EidCodes2.default.Domain.Exact,
    Logger: Logger
  }))), domainBuyServiceModal));
};

ExactMatchWidget.propTypes = {
  results: _propTypes2.default.object.isRequired,
  crossSellResult: _propTypes2.default.object,
  bundleResult: _propTypes2.default.object,
  initProps: _propTypes2.default.object.isRequired,
  searchStatus: _propTypes2.default.string.isRequired,
  onDomainSelect: _propTypes2.default.func.isRequired,
  auction_fee: _propTypes2.default.string,
  supportInfo: _propTypes2.default.object,
  dbsPrice: _propTypes2.default.string,
  onDomainUnSelect: _propTypes2.default.func.isRequired,
  flipDBSModal: _propTypes2.default.func.isRequired,
  isDBSOn: _propTypes2.default.bool.isRequired,
  forcedExactClick: _propTypes2.default.object.isRequired,
  forceExactClickFunc: _propTypes2.default.func.isRequired,
  domainsInCart: _propTypes2.default.object.isRequired,
  exactResultProduct: _propTypes2.default.object.isRequired,
  goGetValue: _propTypes2.default.object,
  flipRaceConditionModal: _propTypes2.default.func.isRequired,
  allButtonsDisabled: _propTypes2.default.bool,
  setDisableAllButtons: _propTypes2.default.func.isRequired,
  Logger: _propTypes2.default.func.isRequired
};

exports.default = ExactMatchWidget;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/components/Notification.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _tooltip = __webpack_require__("@ux/tooltip");

var _tooltip2 = _interopRequireDefault(_tooltip);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var Notification = function Notification(_ref) {
  var text = _ref.text,
      tooltipText = _ref.tooltipText,
      tooltipTitle = _ref.tooltipTitle,
      tooltipMessage = _ref.tooltipMessage;

  var htmlMessage = _react2.default.createElement('div', { dangerouslySetInnerHTML: { __html: tooltipMessage } });
  var tooltip = _react2.default.createElement(_tooltip2.default, {
    title: tooltipTitle,
    message: htmlMessage,
    text: tooltipText
  });

  return _react2.default.createElement('div', { className: 'notification clearfix' }, _react2.default.createElement('div', { className: 'icon-container' }, _react2.default.createElement('span', { className: 'uxicon uxicon-alert' })), _react2.default.createElement('div', { className: 'text-container' }, _react2.default.createElement('span', null, text), tooltip));
};

Notification.propTypes = {
  text: _propTypes2.default.string.isRequired,
  tooltipText: _propTypes2.default.string.isRequired,
  tooltipTitle: _propTypes2.default.string.isRequired,
  tooltipMessage: _propTypes2.default.string.isRequired
};

exports.default = Notification;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/components/PriceFilterWidget.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _formElement = __webpack_require__("@ux/form-element");

var _formElement2 = _interopRequireDefault(_formElement);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var PriceFilterWidget = function PriceFilterWidget(_ref) {
  var label = _ref.label,
      onPriceSet = _ref.onPriceSet,
      maximumPrice = _ref.maximumPrice,
      priceMessage = _ref.priceMessage,
      clearText = _ref.clearText,
      eidApply = _ref.eidApply,
      eidClear = _ref.eidClear;

  return _react2.default.createElement('div', { className: 'filter-container' }, _react2.default.createElement('label', { className: 'custom-control-description text-gray-dark' }, label), _react2.default.createElement('div', { id: 'price-filter' }, _react2.default.createElement('div', { 'data-eid': eidApply }, _react2.default.createElement(_formElement2.default, { label: priceMessage, name: 'maxPrice', value: maximumPrice >= 0 ? maximumPrice : "", onChange: function onChange(event) {
      onPriceSet(event.target.value);
    } }))), _react2.default.createElement('a', { href: 'javascript:void(0);', className: 'clearAction', onClick: function onClick() {
      return onPriceSet(-1);
    }, 'data-eid': eidClear }, clearText));
};

PriceFilterWidget.propTypes = {
  label: _propTypes2.default.string.isRequired,
  onPriceSet: _propTypes2.default.func.isRequired
};

exports.default = PriceFilterWidget;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/components/RaceConditionWidget.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _modal = __webpack_require__("@ux/modal");

var _modal2 = _interopRequireDefault(_modal);

var _button = __webpack_require__("@ux/button");

var _button2 = _interopRequireDefault(_button);

var _RaceCondition = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/constants/RaceCondition.js");

var _RaceCondition2 = _interopRequireDefault(_RaceCondition);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var RaceConditionWidget = function RaceConditionWidget(_ref) {
  var onClose = _ref.onClose,
      show = _ref.show,
      phone = _ref.phone,
      language = _ref.language;

  if (!show) {
    return null;
  }

  var raceConditionShortTitle = language.raceConditionShortTitle,
      raceConditionShortBody = language.raceConditionShortBody,
      raceConditionShortButton = language.raceConditionShortButton,
      raceConditionLongTitle = language.raceConditionLongTitle,
      raceConditionLongBody = language.raceConditionLongBody,
      raceConditionLongButton = language.raceConditionLongButton;
  var RESET_TIME_LENGTH = _RaceCondition2.default.RESET_TIME_LENGTH,
      COLLISION_DURATION = _RaceCondition2.default.COLLISION_DURATION,
      MAX_NUM_RETRY = _RaceCondition2.default.MAX_NUM_RETRY,
      LOCAL_STORAGE_TIMESTAMP = _RaceCondition2.default.LOCAL_STORAGE_TIMESTAMP,
      LOCAL_STORAGE_FAILURE_TIMES = _RaceCondition2.default.LOCAL_STORAGE_FAILURE_TIMES;

  var showRetry = true;

  if (typeof Storage !== "undefined") {
    var lastTime = localStorage.getItem(LOCAL_STORAGE_TIMESTAMP);
    var current = new Date().getTime();

    if (lastTime && current - lastTime < RESET_TIME_LENGTH) {
      if (current - lastTime > COLLISION_DURATION) {
        localStorage.setItem(LOCAL_STORAGE_FAILURE_TIMES, Number(localStorage.getItem(LOCAL_STORAGE_FAILURE_TIMES)) + 1);
      }
      if (localStorage.getItem(LOCAL_STORAGE_FAILURE_TIMES) > MAX_NUM_RETRY) {
        showRetry = false;
      }
    } else {
      localStorage.setItem(LOCAL_STORAGE_FAILURE_TIMES, 1);
    }

    localStorage.setItem(LOCAL_STORAGE_TIMESTAMP, current);
  } else {
    showRetry = false;
  }

  var footer = _react2.default.createElement(_button2.default, { design: 'primary', onClick: onClose }, showRetry ? raceConditionShortButton : raceConditionLongButton);

  var modal = _react2.default.createElement(_modal2.default, {
    footer: footer,
    title: showRetry ? raceConditionShortTitle : raceConditionLongTitle,
    onClose: onClose
  }, _react2.default.createElement('div', { id: 'raceCoditionModal' }, showRetry ? raceConditionShortBody : raceConditionLongBody));

  return _react2.default.createElement('div', { className: 'container race-condition' }, modal);
};

RaceConditionWidget.propTypes = {
  temp: _propTypes2.default.func,
  onClose: _propTypes2.default.func.isRequired,
  show: _propTypes2.default.bool.isRequired,
  phone: _propTypes2.default.string,
  language: _propTypes2.default.object.isRequired
};

exports.default = RaceConditionWidget;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/components/SpinResultWidget.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _lib = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/index.js");

var _UrlHelper = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/UrlHelper.js");

var _UrlHelper2 = _interopRequireDefault(_UrlHelper);

var _HttpRequestHelper = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/HttpRequestHelper.js");

var _HttpRequestHelper2 = _interopRequireDefault(_HttpRequestHelper);

var _EidCodes = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/constants/EidCodes.js");

var _EidCodes2 = _interopRequireDefault(_EidCodes);

var _EmptyResult = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/components/EmptyResult.js");

var _EmptyResult2 = _interopRequireDefault(_EmptyResult);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var SpinResultsWidget = function SpinResultsWidget(_ref) {
  var initProps = _ref.initProps,
      results = _ref.results,
      filterInfo = _ref.filterInfo,
      domainsInCart = _ref.domainsInCart,
      onDomainSelect = _ref.onDomainSelect,
      onDomainUnSelect = _ref.onDomainUnSelect,
      spinResult = _ref.spinResult,
      showFilters = _ref.showFilters,
      allButtonsDisabled = _ref.allButtonsDisabled,
      setDisableAllButtons = _ref.setDisableAllButtons,
      flipRaceConditionModal = _ref.flipRaceConditionModal,
      Logger = _ref.Logger;
  var activeTlds = filterInfo.activeTlds,
      showOnlyCCTLDS = filterInfo.showOnlyCCTLDS,
      showOnlyPremiums = filterInfo.showOnlyPremiums,
      maximumLengthSet = filterInfo.maximumLengthSet,
      maximumPrice = filterInfo.maximumPrice;

  var maxPriceValue = parseFloat(maximumPrice) || parseFloat("-1");

  function showDomain(recommendedDomain) {
    var inventory = recommendedDomain.Inventory;
    var showBasedOnTlD = activeTlds.size === 0 || activeTlds.has(recommendedDomain.Extension);
    var showBasedOnCCTLD = !showOnlyCCTLDS || inventory === "ccTLD" || recommendedDomain.DomainSource === "cctld";
    var showBasedOnPremium = !showOnlyPremiums || inventory === "premium" || inventory === "registry_premium";
    var showBasedOnLength = recommendedDomain.NameWithoutExtension.length <= maximumLengthSet;
    var price = recommendedDomain.Price > 0 ? recommendedDomain.Price : spinResult.productMap[recommendedDomain.Extension] && spinResult.productMap[recommendedDomain.Extension][recommendedDomain.TierId]["PriceInfo"]["CurrentPrice"];
    var showBasedOnPrice = !(maxPriceValue > 0) || price <= maxPriceValue;
    return showBasedOnTlD && (showBasedOnCCTLD || showBasedOnPremium) && showBasedOnLength && showBasedOnPrice && price;
  }

  var spinResultsFiltered = null;
  if (results) {

    spinResultsFiltered = spinResult.RecommendedDomains.reduce(function (results, recommendedDomain) {
      var isInCart = domainsInCart.has(recommendedDomain.Fqdn);
      if (showDomain(recommendedDomain)) {
        return results.concat(_extends({}, recommendedDomain, { isInCart: isInCart }));
      } else {
        return results;
      }
    }, []).slice(0, initProps.numberOfSpins);
  }

  var emptyResults = spinResultsFiltered && spinResultsFiltered.length === 0 ? _react2.default.createElement(_EmptyResult2.default, { language: initProps.language }) : null;

  function unSelectCallback(payload, domain) {
    var url = _UrlHelper2.default.getCartRemoveUrl(initProps.urlOverride, domain, initProps.privateLabelId);

    var parameters = {
      httpMethod: 'GET',
      needCredentials: true,
      apiUrl: url,
      logInfo: {
        api: 'cart-remove',
        trigger: 'spins'
      },
      successCallback: function successCallback() {
        onDomainUnSelect([domain]);
      },
      failureCallback: function failureCallback() {
        //console.log(err);
      }
    };
    _HttpRequestHelper2.default.callApi(parameters);
  }

  function selectPostParams(domainName, domain, extraInfo) {
    var domainStatus = domain.Inventory === "premium" ? "premium" : "available";
    var extraProps = _extends({ domainStatus: domainStatus }, extraInfo);
    var url = _UrlHelper2.default.getCartAddUrl(initProps.urlOverride, domainName, initProps.privateLabelId, extraProps);

    return {
      httpMethod: 'POST',
      needCredentials: true,
      apiUrl: url,
      logInfo: {
        api: 'cart-add',
        trigger: 'spins'
      },
      successCallback: function successCallback() {
        onDomainSelect([domainName]);
      },
      failureCallback: function failureCallback() {
        flipRaceConditionModal(true);
        //console.log(err);
      }
    };
  }

  var extraButtonProps = {
    setDisableAllButtons: setDisableAllButtons,
    buttonDesign: 'primary',
    getUnSelectCallback: unSelectCallback,
    selectPostParams: selectPostParams
  };

  var visualOptions = {
    showExact: false,
    showExtraSpinCSS: true,
    showSpinHeader: false,
    showDomainTypeTag: true
  };

  var resultsPayload = results ? _extends({}, results, spinResult, {
    RecommendedDomains: spinResultsFiltered
  }) : results;

  return _react2.default.createElement('div', { className: 'spin-wrap ' + (showFilters ? "" : "no-filter") }, _react2.default.createElement(_lib.SearchResults, _extends({}, initProps, {
    resultsPayload: resultsPayload,
    eid: _EidCodes2.default.Domain.Spins,
    isDisabled: allButtonsDisabled,
    extraButtonProps: extraButtonProps,
    visualOptions: visualOptions,
    Logger: Logger
  })), emptyResults);
};

exports.default = SpinResultsWidget;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/components/TldFilterWidget.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _checkboxGroup = __webpack_require__("@ux/checkbox-group");

var _checkboxGroup2 = _interopRequireDefault(_checkboxGroup);

__webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/styles/base.scss");

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var TldFilterWidget = function TldFilterWidget(_ref) {
  var label = _ref.label,
      inputs = _ref.inputs,
      clearAction = _ref.clearAction,
      clearText = _ref.clearText,
      eidApply = _ref.eidApply,
      eidClear = _ref.eidClear;

  return _react2.default.createElement('div', { className: 'filter-container' }, _react2.default.createElement('p', { className: 'font-primary-bold custom-control-description text-gray-dark' }, label), _react2.default.createElement('div', { className: 'tld-filter-box' }, _react2.default.createElement('div', { 'data-eid': eidApply }, _react2.default.createElement(_checkboxGroup2.default, { inputs: inputs }))), _react2.default.createElement('a', { href: 'javascript:void(0);', className: 'clearAction', onClick: clearAction, 'data-eid': eidClear }, clearText));
};
TldFilterWidget.propTypes = {
  inputs: _propTypes2.default.arrayOf(_propTypes2.default.shape({
    label: _propTypes2.default.string.required,
    name: _propTypes2.default.string,
    value: _propTypes2.default.string.required,
    checked: _propTypes2.default.bool.required,
    onChange: _propTypes2.default.func.required
  })).isRequired
};

exports.default = TldFilterWidget;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/config/config.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _development = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/config/development.json");

var _development2 = _interopRequireDefault(_development);

var _test = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/config/test.json");

var _test2 = _interopRequireDefault(_test);

var _production = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/config/production.json");

var _production2 = _interopRequireDefault(_production);

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

var NODE_ENV = undefined;

var configFile = {};

switch (NODE_ENV) {
    case 'production':
        configFile = _production2.default;
        break;

    case 'test':
        configFile = _test2.default;
        break;

    default:
        configFile = _development2.default;
}

exports.default = configFile;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/config/development.json":
/***/ (function(module, exports) {

module.exports = {"apiURLPaths":{"domainSearch":"/domainsapi/v1/search","availCheckAndAdd":"/domainsapi/v1/check/add","serpUrl":"/domains/searchresults.aspx","spinsUrl":"/domainsapi/v1/search/spins","virtualCart":"/upp/vcart","cartNextStep":"/api/dpp/searchresultscart/3?applyBP=1&ci=84427","cartAdd":"/api/dpp/searchresultscart/13","cartRemove":"/api/dpp/searchresultscart/2","bundleGet":"/domainsapi/v1/dynamicbundles","bundleAdd":"/api/dpp/searchresultscart/16","bundleRemove":"/api/dpp/searchresultscart/17","crossSellGet":"/domainsapi/v1/crosssell/all","cartGet":"/api/dpp/searchresultscart/10","cartContinue":"/domains/domain-configuration.aspx?ci=84427","auctionContinue":"/trpItemListing.aspx","support":"/v1/plid/1/contacts"},"apiURLPrefixes":{"fos":"https://","cart":"https://cart.","auction":"https://auctions.","support":"http://support.api.","find":"https://find."},"apiURLhosts":{"gd":"dev-godaddy.com"}}

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/config/production.json":
/***/ (function(module, exports) {

module.exports = {"apiURLPaths":{"domainSearch":"/domainsapi/v1/search","availCheckAndAdd":"/domainsapi/v1/check/add","serpUrl":"/domains/searchresults.aspx","spinsUrl":"/domainsapi/v1/search/spins","virtualCart":"/upp/vcart","cartNextStep":"/api/dpp/searchresultscart/3?applyBP=1&ci=84427","cartAdd":"/api/dpp/searchresultscart/13","cartRemove":"/api/dpp/searchresultscart/2","bundleGet":"/domainsapi/v1/dynamicbundles","bundleAdd":"/api/dpp/searchresultscart/16","bundleRemove":"/api/dpp/searchresultscart/17","crossSellGet":"/domainsapi/v1/crosssell/all","cartGet":"/api/dpp/searchresultscart/10","cartContinue":"/domains/domain-configuration.aspx?ci=84427","auctionContinue":"/trpItemListing.aspx","support":"/v1/plid/1/contacts"},"apiURLPrefixes":{"fos":"https://","cart":"https://cart.","auction":"https://auctions.","support":"http://support.api.","find":"https://find."},"apiURLhosts":{"gd":"godaddy.com"}}

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/config/test.json":
/***/ (function(module, exports) {

module.exports = {"apiURLPaths":{"domainSearch":"/domainsapi/v1/search","availCheckAndAdd":"/domainsapi/v1/check/add","serpUrl":"/domains/searchresults.aspx","spinsUrl":"/domainsapi/v1/search/spins","virtualCart":"/upp/vcart","cartNextStep":"/api/dpp/searchresultscart/3?applyBP=1&ci=84427","cartAdd":"/api/dpp/searchresultscart/13","cartRemove":"/api/dpp/searchresultscart/2","bundleGet":"/domainsapi/v1/dynamicbundles","bundleAdd":"/api/dpp/searchresultscart/16","bundleRemove":"/api/dpp/searchresultscart/17","crossSellGet":"/domainsapi/v1/crosssell/all","cartGet":"/api/dpp/searchresultscart/10","cartContinue":"/domains/domain-configuration.aspx?ci=84427","auctionContinue":"/trpItemListing.aspx","support":"/v1/plid/1/contacts"},"apiURLPrefixes":{"fos":"https://","cart":"https://cart.","auction":"https://auctions.","support":"http://support.api.","find":"https://find."},"apiURLhosts":{"gd":"test-godaddy.com"}}

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/constants/ActionTypes.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _keymirror = __webpack_require__("./node_modules/keymirror/index.js");

var _keymirror2 = _interopRequireDefault(_keymirror);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

exports.default = (0, _keymirror2.default)({
  ADD_TO_CART: null,
  DOMAIN_SEARCH_SUCCESS: null,
  FLIP_DBS_MODAL_STATE: null,
  FLIP_DISCLAIMER_MODAL_STATE: null,
  /* eslint-disable id-length */
  FLIP_RACE_CONDITION_MODAL_STATE: null,
  FORCE_EXACT_MATCH_SELECT: null,
  REMOVE_FROM_CART: null,
  SET_BUNDLE_RESULTS: null,
  SET_CART_DOMAINS: null,
  SET_CROSS_SELL: null,
  SET_DOMAIN_SEARCH_RESULT: null,
  SET_FILTERS: null,
  SET_MAX_PRICE: null,
  SET_PRODUCTS: null,
  SET_SEARCH_STATUS: null,
  SET_SLD_MAX_SIZE: null,
  SET_INIT_SPIN_RESULTS: null,
  SET_SPIN_RESULTS: null,
  TOGGLE_CCTLD_BUTTON: null,
  TOGGLE_PREMIUM_BUTTON: null,
  TOGGLE_TLD_BUTTON: null,
  DISABLE_ALL_BUTTONS: null
});

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/constants/AvailablityStatus.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  Auction: "auction",
  Available: "available_for_registration",
  PreReg: "pre_reg",
  Premium: "premium",
  RegPremium: "registry_premium",
  Taken: "taken",
  Excluded: "excluded",
  Reserved: "reserved",
  Unsupported: "tld_unsupported",
  // Errors
  CharacterInvalid: "character_invalid",
  IdnError: "us_idn_not_allowed",
  LanguageUnsupported: "language_unsupported",
  PunycodeError: "punycode_conversion_error",
  TldInvalid: "tld_invalid"
};

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/constants/EidCodes.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  AddToCart: 'find.sales.domains.search-results.button.add-to-cart',
  BackorderPopup: 'find.sales.domains.search-results.button.backorder-popup',
  Domain: {
    Exact: 'find.sales.domains.search-results.domain.exact',
    DbsExact: 'find.sales.domains.search-results.domain.dbsexact',
    ccTLD: 'find.sales.domains.search-results.domain.ccTLD',
    Bundle: 'find.sales.domains.search-results.domain.bundle',
    Spins: 'find.sales.domains.search-results.domain.spins'
  },
  Filters: {
    Price: {
      Apply: 'find.sales.domains.search-results.filters.price.apply',
      Clear: 'find.sales.domains.search-results.filters.price.clear',
      Collapse: 'find.sales.domains.search-results.filters.price.collapse',
      Expand: 'find.sales.domains.search-results.filters.price.expand',
      Halt: 'find.sales.domains.search-results.filters.price.halt'
    },
    CharLength: {
      Apply: 'find.sales.domains.search-results.filters.char-length.apply',
      Clear: 0,
      Collapse: 'find.sales.domains.search-results.filters.char-length.collapse',
      Expand: 'find.sales.domains.search-results.filters.char-length.expand',
      Halt: 'find.sales.domains.search-results.filters.char-length.halt'
    },
    TLD: {
      Apply: 'find.sales.domains.search-results.filters.tld.apply',
      Clear: 'find.sales.domains.search-results.filters.tld.clear',
      Collapse: 'find.sales.domains.search-results.filters.tld.collapse',
      Expand: 'find.sales.domains.search-results.filters.tld.expand',
      Halt: 'find.sales.domains.search-results.filters.tld.halt'
    },
    Location: {
      Apply: 'find.sales.domains.search-results.filters.location.apply',
      Clear: 0,
      Collapse: 0,
      Expand: 0,
      Halt: 'find.sales.domains.search-results.filters.location.halt'
    },
    Premium: {
      Apply: 'find.sales.domains.search-results.filters.premium.apply',
      Clear: 0,
      Collapse: 0,
      Expand: 0,
      Halt: 'find.sales.domains.search-results.filters.premium.halt'
    }
  },
  LearnMore: 'find.sales.domains.search-results.button.learn-more',
  PrivateSalePopup: 'find.sales.domains.search-results.button.private-sale-popup',
  RemoveFromCart: 'find.sales.domains.search-results.remove-from-cart',
  SearchButton: 'find.sales.domains.search-results.button.search',
  SelectPhase: 'find.sales.domains.search-results.button.select-phase',
  ShowAll: 'find.sales.domains.search-results.button.show-all',
  ShowMore: 'find.sales.domains.search-results.button.show-more',
  SkipConfig: 'find.sales.domains.search-results.button.skip-config'
};

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/constants/RaceCondition.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  RESET_TIME_LENGTH: 1000 * 60 * 60 * 4, //4 hrs
  //handle the immediate second render
  COLLISION_DURATION: 1000,
  MAX_NUM_RETRY: 1,
  LOCAL_STORAGE_TIMESTAMP: 'SERP_ADD_DOMAIN_FAILED_LAST_TIME',
  LOCAL_STORAGE_FAILURE_TIMES: 'SERP_ADD_DOMAIN_FAILED_TIMES'
};

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/constants/SearchStatus.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var SEARCH_STATUS = {
  Auction: "auction",
  Available: "available",
  Error: "error",
  None: "none",
  PreReg: "prereg",
  Restricted: "restricted",
  SearchInProgress: "searching",
  Taken: "taken",
  Unavailable: "unavailable"
};

exports.default = SEARCH_STATUS;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/containers/DomainSearchPage.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = __webpack_require__("./node_modules/react-redux/es/index.js");

var _index = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/actions/index.js");

var actionCreators = _interopRequireWildcard(_index);

var _DomainSearchWidget = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/components/DomainSearchWidget.js");

var _DomainSearchWidget2 = _interopRequireDefault(_DomainSearchWidget);

var _ExactMatchWidget = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/components/ExactMatchWidget.js");

var _ExactMatchWidget2 = _interopRequireDefault(_ExactMatchWidget);

var _NewExactMatchWidget = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/components/NewExactMatchWidget.js");

var _NewExactMatchWidget2 = _interopRequireDefault(_NewExactMatchWidget);

var _SpinAndFilter = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/containers/SpinAndFilter.js");

var _SpinAndFilter2 = _interopRequireDefault(_SpinAndFilter);

var _DisclaimerWidget = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/components/DisclaimerWidget.js");

var _DisclaimerWidget2 = _interopRequireDefault(_DisclaimerWidget);

var _RaceConditionWidget = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/components/RaceConditionWidget.js");

var _RaceConditionWidget2 = _interopRequireDefault(_RaceConditionWidget);

var _SearchStatus = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/constants/SearchStatus.js");

var _SearchStatus2 = _interopRequireDefault(_SearchStatus);

var _reactSticky = __webpack_require__("./node_modules/react-sticky/lib/index.js");

__webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/styles/base.scss");

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  } else {
    var newObj = {};if (obj != null) {
      for (var key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
      }
    }newObj.default = obj;return newObj;
  }
}

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
  }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var DomainSearchPage = function (_Component) {
  _inherits(DomainSearchPage, _Component);

  function DomainSearchPage() {
    _classCallCheck(this, DomainSearchPage);

    return _possibleConstructorReturn(this, (DomainSearchPage.__proto__ || Object.getPrototypeOf(DomainSearchPage)).apply(this, arguments));
  }

  _createClass(DomainSearchPage, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          results = _props.results,
          products = _props.products,
          filterInfo = _props.filterInfo,
          onDomainSearchSuccess = _props.onDomainSearchSuccess,
          onTldButtonClick = _props.onTldButtonClick,
          _onPremiumButtonClick = _props.onPremiumButtonClick,
          spinResult = _props.spinResult,
          allButtonsDisabled = _props.allButtonsDisabled,
          _onCCTLDButtonClick = _props.onCCTLDButtonClick,
          flipDBSModal = _props.flipDBSModal,
          isDBSOn = _props.isDBSOn,
          flipDisclaimerModal = _props.flipDisclaimerModal,
          isDisclaimerOn = _props.isDisclaimerOn,
          flipRaceConditionModal = _props.flipRaceConditionModal,
          isRaceConditionOn = _props.isRaceConditionOn,
          initProps = _props.initProps,
          onDomainSizeSet = _props.onDomainSizeSet,
          onPriceSet = _props.onPriceSet,
          forcedExactClick = _props.forcedExactClick,
          forceExactClickFunc = _props.forceExactClickFunc,
          crossSellResult = _props.crossSellResult,
          bundleResult = _props.bundleResult,
          domainsInCart = _props.domainsInCart,
          onDomainSelect = _props.onDomainSelect,
          onDomainUnSelect = _props.onDomainUnSelect,
          searchStatus = _props.searchStatus,
          setSearchStatus = _props.setSearchStatus,
          icann_fee = _props.icann_fee,
          auction_fee = _props.auction_fee,
          supportInfo = _props.supportInfo,
          dbsPrice = _props.dbsPrice,
          exactResultProduct = _props.exactResultProduct,
          setDisableAllButtons = _props.setDisableAllButtons,
          Logger = _props.Logger;

      if (supportInfo.premiumDomainSupportPhone) {
        initProps.language.moreInfo = initProps.language.moreInfo.replace('{0}', supportInfo.premiumDomainSupportPhone);
        initProps.language.premiumToolTipText = initProps.language.premiumToolTipText.replace('{1}', '<a href="tel:' + supportInfo.premiumDomainSupportPhone + '">' + supportInfo.premiumDomainSupportPhone + '</a>');
      }
      var language = initProps.language;
      var spinAndFilterProps = {
        results: results, products: products, filterInfo: filterInfo, onTldButtonClick: onTldButtonClick, onPremiumButtonClick: function onPremiumButtonClick() {
          return _onPremiumButtonClick(initProps);
        }, onDomainSelect: onDomainSelect, onDomainUnSelect: onDomainUnSelect, onCCTLDButtonClick: function onCCTLDButtonClick() {
          return _onCCTLDButtonClick(initProps);
        }, onDomainSizeSet: onDomainSizeSet,
        onPriceSet: onPriceSet, domainsInCart: domainsInCart, spinResult: spinResult, searchStatus: searchStatus, allButtonsDisabled: allButtonsDisabled, setDisableAllButtons: setDisableAllButtons, flipRaceConditionModal: flipRaceConditionModal, Logger: Logger
      };
      var failureCallback = function failureCallback(err) {
        Logger({
          type: 'fos showing domainSearchError',
          error: err
        });
        setSearchStatus(_SearchStatus2.default.Error);
      };

      var exactMatchProps = {
        results: results, crossSellResult: crossSellResult, bundleResult: bundleResult, initProps: initProps, onDomainSelect: onDomainSelect, auction_fee: auction_fee, supportInfo: supportInfo, dbsPrice: dbsPrice, allButtonsDisabled: allButtonsDisabled, setDisableAllButtons: setDisableAllButtons,
        onDomainUnSelect: onDomainUnSelect, searchStatus: searchStatus, flipDBSModal: flipDBSModal, isDBSOn: isDBSOn, forcedExactClick: forcedExactClick, forceExactClickFunc: forceExactClickFunc, domainsInCart: domainsInCart, exactResultProduct: exactResultProduct,
        flipRaceConditionModal: flipRaceConditionModal, Logger: Logger
      };

      var domainSearchParams = {
        initProps: initProps, onDomainSearchSuccess: onDomainSearchSuccess, failureCallback: failureCallback, domainsInCart: domainsInCart, forceExactClickFunc: forceExactClickFunc, searchStatus: searchStatus, allButtonsDisabled: allButtonsDisabled,
        flipRaceConditionModal: flipRaceConditionModal, Logger: Logger
      };

      var disclaimerProps = {
        onOpen: function onOpen() {
          return flipDisclaimerModal(true);
        },
        onClose: function onClose() {
          return flipDisclaimerModal(false);
        },
        show: isDisclaimerOn,
        icann_fee: icann_fee,
        language: language
      };

      var raceConditionProps = {
        onClose: function onClose() {
          return flipRaceConditionModal(false);
        },
        show: isRaceConditionOn,
        phone: results && results.SupportPhone,
        language: language
      };

      var errorPage = searchStatus === _SearchStatus2.default.Error ? _react2.default.createElement('div', { className: 'search-error container headline-primary' }, language.domainSearchError) : null;

      var spinAndFilters = Object.keys(spinResult).length > 0 && !errorPage ? _react2.default.createElement('div', { className: 'spin-and-filter-wrap' }, _react2.default.createElement(_SpinAndFilter2.default, _extends({ style: { zIndex: -1000 }, initProps: initProps }, spinAndFilterProps))) : null;

      var exactMatch = results && results.ExactMatchDomain && !errorPage ? _react2.default.createElement('div', { className: 'exact-match-wrap ' + searchStatus }, _react2.default.createElement(_ExactMatchWidget2.default, exactMatchProps), _react2.default.createElement(_NewExactMatchWidget2.default, exactMatchProps)) : null;

      var disclaimer = results && !errorPage ? _react2.default.createElement(_DisclaimerWidget2.default, disclaimerProps) : null;

      var raceCondition = _react2.default.createElement(_RaceConditionWidget2.default, raceConditionProps);

      return _react2.default.createElement(_reactSticky.StickyContainer, null, _react2.default.createElement(_reactSticky.Sticky, { isActive: true, className: 'search-wrap' }, _react2.default.createElement(_DomainSearchWidget2.default, domainSearchParams)), errorPage, exactMatch, spinAndFilters, disclaimer, raceCondition);
    }
  }]);

  return DomainSearchPage;
}(_react.Component);

DomainSearchPage.propTypes = {
  results: _propTypes2.default.object, products: _propTypes2.default.object, filterInfo: _propTypes2.default.object, exactResultProduct: _propTypes2.default.object,
  spinResult: _propTypes2.default.object, crossSellResult: _propTypes2.default.object, bundleResult: _propTypes2.default.object,
  initProps: _propTypes2.default.object, supportInfo: _propTypes2.default.object, isDBSOn: _propTypes2.default.bool, allButtonsDisabled: _propTypes2.default.bool,
  isDisclaimerOn: _propTypes2.default.bool, isRaceConditionOn: _propTypes2.default.bool, OnforcedExactClick: _propTypes2.default.object, domainsInCart: _propTypes2.default.object,
  searchStatus: _propTypes2.default.string, dbsPrice: _propTypes2.default.string, icann_fee: _propTypes2.default.string, auction_fee: _propTypes2.default.string,
  onPriceSet: _propTypes2.default.func, onDomainSearchSuccess: _propTypes2.default.func, onTldButtonClick: _propTypes2.default.func, onPremiumButtonClick: _propTypes2.default.func,
  onDomainSelect: _propTypes2.default.func, onDomainUnSelect: _propTypes2.default.func, onCCTLDButtonClick: _propTypes2.default.func, onDomainSizeSet: _propTypes2.default.func,
  flipDBSModal: _propTypes2.default.func, setDisableAllButtons: _propTypes2.default.func, flipDisclaimerModal: _propTypes2.default.func, flipRaceConditionModal: _propTypes2.default.func,
  forceExactClickFunc: _propTypes2.default.func, setSearchStatus: _propTypes2.default.func, Logger: _propTypes2.default.func.isRequired
};

var mapStateToProps = function mapStateToProps(state) {
  return {
    products: state.products,
    results: state.results,
    exactResult: state.exactResult,
    spinResult: state.spinResult,
    filterInfo: state.filterInfo,
    bundleResult: state.bundleResult,
    crossSellResult: state.crossSellResult,
    exactResultProduct: state.exactResultProduct,
    domainsInCart: state.domainsInCart,
    isDBSOn: state.isDBSOn,
    isDisclaimerOn: state.isDisclaimerOn,
    isRaceConditionOn: state.isRaceConditionOn,
    searchStatus: state.searchStatus,
    forcedExactClick: state.forcedExactClick,
    dbsPrice: state.dbsPrice,
    supportInfo: state.supportInfo,
    icann_fee: state.icann_fee,
    auction_fee: state.auction_fee,
    allButtonsDisabled: state.allButtonsDisabled
  };
};

var mapDispatchToProps = {
  onDomainSearchSuccess: actionCreators.DOMAIN_SEARCH_SUCCESS,
  onPriceSet: actionCreators.SET_MAX_PRICE,
  onTldButtonClick: actionCreators.TOGGLE_TLD_BUTTON,
  onPremiumButtonClick: actionCreators.TOGGLE_PREMIUM_BUTTON,
  onCCTLDButtonClick: actionCreators.TOGGLE_CCTLD_BUTTON,
  onDomainSizeSet: actionCreators.SET_SLD_MAX_SIZE,
  onDomainSelect: actionCreators.ADD_TO_CART,
  onDomainUnSelect: actionCreators.REMOVE_FROM_CART,
  flipDBSModal: actionCreators.FLIP_DBS_MODAL_STATE,
  flipDisclaimerModal: actionCreators.FLIP_DISCLAIMER_MODAL_STATE,
  flipRaceConditionModal: actionCreators.FLIP_RACE_CONDITION_MODAL_STATE,
  forceExactClickFunc: actionCreators.FORCE_EXACT_MATCH_SELECT,
  setSearchStatus: actionCreators.SET_SEARCH_STATUS,
  setDisableAllButtons: actionCreators.DISABLE_ALL_BUTTONS
};

var DomainSearchContainer = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(DomainSearchPage);

exports.default = DomainSearchContainer;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/containers/Filters.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _FiltersWidget = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/components/FiltersWidget.js");

var _FiltersWidget2 = _interopRequireDefault(_FiltersWidget);

var _EidCodes = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/constants/EidCodes.js");

var _EidCodes2 = _interopRequireDefault(_EidCodes);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var Filters = function Filters(_ref) {
  var filters = _ref.filters,
      onTldButtonClick = _ref.onTldButtonClick,
      onPremiumButtonClick = _ref.onPremiumButtonClick,
      onCCTLDButtonClick = _ref.onCCTLDButtonClick,
      onDomainSizeSet = _ref.onDomainSizeSet,
      onPriceSet = _ref.onPriceSet,
      language = _ref.language;

  var inputs = Object.keys(filters.tldInfoMap).map(function (k) {
    return filters.tldInfoMap[k];
  }).map(function (tldInfo) {
    return _extends({}, tldInfo, {
      name: "tldFilter",
      onChange: function onChange() {
        onTldButtonClick(tldInfo.value);
      }
    });
  });
  inputs[0]["label"] = language.allExt;

  var tldClearAction = function tldClearAction() {
    onTldButtonClick('');
  };

  var priceFilterInputs = { label: language.priceLabel, onPriceSet: onPriceSet, maximumPrice: filters.maximumPrice,
    priceMessage: language.enterPrice, clearText: language.clear, eidApply: _EidCodes2.default.Filters.Price.Apply, eidClear: _EidCodes2.default.Filters.Price.Clear };
  var tldFilterInputs = { inputs: inputs, label: language.Extensions, clearAction: tldClearAction, clearText: language.clear,
    eidApply: _EidCodes2.default.Filters.TLD.Apply, eidClear: _EidCodes2.default.Filters.TLD.Clear };
  var premiumFilterInputs = { onButtonClick: onPremiumButtonClick, label: language.premiumTag, isSelected: filters.showOnlyPremiums, eidApply: _EidCodes2.default.Filters.Premium.Apply, id: "premiumFilter" };
  var cctldFilterInputs = { onButtonClick: onCCTLDButtonClick, label: language.CountryLocationLabel, isSelected: filters.showOnlyCCTLDS, eidApply: _EidCodes2.default.Filters.Location.Apply, id: "cctldFilter" };
  var sizeFilterInputs = { onDomainSizeSet: onDomainSizeSet, label: language.CharLengthLabel, errorMessage: language.quantitySelectorError,
    defaultMaximum: filters.maximumLength, maxSet: filters.maximumLengthSet, eidApply: _EidCodes2.default.Filters.CharLength.Apply };

  var filterWidgetInputs = { tldFilterInputs: tldFilterInputs, premiumFilterInputs: premiumFilterInputs, cctldFilterInputs: cctldFilterInputs, sizeFilterInputs: sizeFilterInputs, priceFilterInputs: priceFilterInputs };

  return _react2.default.createElement(_FiltersWidget2.default, filterWidgetInputs);
};

exports.default = Filters;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/containers/SpinAndFilter.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _Filters = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/containers/Filters.js");

var _Filters2 = _interopRequireDefault(_Filters);

var _SpinResultWidget = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/components/SpinResultWidget.js");

var _SpinResultWidget2 = _interopRequireDefault(_SpinResultWidget);

var _SearchStatus = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/constants/SearchStatus.js");

var _SearchStatus2 = _interopRequireDefault(_SearchStatus);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var SpinAndFilterContainer = function SpinAndFilterContainer(_ref) {
  var results = _ref.results,
      filterInfo = _ref.filterInfo,
      onTldButtonClick = _ref.onTldButtonClick,
      onPremiumButtonClick = _ref.onPremiumButtonClick,
      searchStatus = _ref.searchStatus,
      allButtonsDisabled = _ref.allButtonsDisabled,
      setDisableAllButtons = _ref.setDisableAllButtons,
      onCCTLDButtonClick = _ref.onCCTLDButtonClick,
      initProps = _ref.initProps,
      onDomainSizeSet = _ref.onDomainSizeSet,
      onPriceSet = _ref.onPriceSet,
      domainsInCart = _ref.domainsInCart,
      onDomainSelect = _ref.onDomainSelect,
      onDomainUnSelect = _ref.onDomainUnSelect,
      spinResult = _ref.spinResult,
      flipRaceConditionModal = _ref.flipRaceConditionModal,
      Logger = _ref.Logger;

  var showFilters = initProps.showFilters;
  var filterParams = { onTldButtonClick: onTldButtonClick, onPremiumButtonClick: onPremiumButtonClick, onCCTLDButtonClick: onCCTLDButtonClick, onDomainSizeSet: onDomainSizeSet, onPriceSet: onPriceSet, language: initProps.language };
  var spinResultsParams = {
    results: results, filterInfo: filterInfo, initProps: initProps, domainsInCart: domainsInCart, onDomainSelect: onDomainSelect, onDomainUnSelect: onDomainUnSelect, spinResult: spinResult, showFilters: showFilters, allButtonsDisabled: allButtonsDisabled, setDisableAllButtons: setDisableAllButtons,
    flipRaceConditionModal: flipRaceConditionModal, Logger: Logger
  };

  var filterContainerClass = void 0,
      spinContainerClass = "";

  if (showFilters === false) {
    filterContainerClass = "invisible";
    spinContainerClass = "col-md-12";
  } else {
    filterContainerClass = "col-lg-4";
    spinContainerClass = "col-lg-8 offset-lg-0 col-md-12";
  }

  return _react2.default.createElement('div', { className: 'container' }, _react2.default.createElement('div', { className: 'col-md-12' }, _react2.default.createElement('div', { className: 'spin-and-filter-headline-wrap' }, _react2.default.createElement('span', { className: 'font-primary-bold spin-and-filter-headline text-gray-dark ' + searchStatus }, searchStatus === _SearchStatus2.default.Taken ? initProps.language.selectedForYou : initProps.language.protectYourName))), _react2.default.createElement('div', { className: filterContainerClass }, _react2.default.createElement(_Filters2.default, _extends({ filters: filterInfo }, filterParams))), _react2.default.createElement('div', { className: spinContainerClass }, _react2.default.createElement(_SpinResultWidget2.default, spinResultsParams)));
};

exports.default = SpinAndFilterContainer;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _DomainSearchPage = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/containers/DomainSearchPage.js");

var _DomainSearchPage2 = _interopRequireDefault(_DomainSearchPage);

var _Reducer = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/reducers/Reducer.js");

var _Reducer2 = _interopRequireDefault(_Reducer);

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _redux = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/redux/es/index.js");

var _reduxThunk = __webpack_require__("./node_modules/redux-thunk/es/index.js");

var _reduxThunk2 = _interopRequireDefault(_reduxThunk);

var _reactRedux = __webpack_require__("./node_modules/react-redux/es/index.js");

var _index = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/actions/index.js");

var actionCreators = _interopRequireWildcard(_index);

var _SearchStatus = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/SearchStatus.js");

var _SearchStatus2 = _interopRequireDefault(_SearchStatus);

var _PrefetchProvider = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/PrefetchProvider.js");

var _PrefetchProvider2 = _interopRequireDefault(_PrefetchProvider);

var _Logger = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/Logger.js");

var _Logger2 = _interopRequireDefault(_Logger);

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  } else {
    var newObj = {};if (obj != null) {
      for (var key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
      }
    }newObj.default = obj;return newObj;
  }
}

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var middleware = [_reduxThunk2.default];
var store = (0, _redux.createStore)(_Reducer2.default, _redux.applyMiddleware.apply(undefined, middleware));

var SearchPage = function SearchPage(props) {
  if (props.initProps && props.initProps.domainToCheck) {
    //Prefetch work
    var defaultProviderFactory = function defaultProviderFactory() {
      return Promise.reject({});
    };
    _PrefetchProvider2.default.getProvider('exactAPI', defaultProviderFactory, {}).then(function (response) {
      store.dispatch(actionCreators.SET_SEARCH_STATUS((0, _SearchStatus2.default)(response.data)));
      response.data ? store.dispatch(actionCreators.SET_DOMAIN_SEARCH_RESULT(response.data, false)) : store.dispatch(actionCreators.SET_DOMAIN_SEARCH_RESULT({}));
    }).catch(function (err) {
      console.log(err);
      (0, _Logger2.default)({
        type: 'prefetch failure',
        error: err,
        meta: {
          api: 'exact'
        }
      });
    });

    _PrefetchProvider2.default.getProvider('spinsAPI', defaultProviderFactory, {}).then(function (response) {
      store.dispatch(actionCreators.SET_INIT_SPIN_RESULTS(response.data || {}));
    }).catch(function (err) {
      console.log(err);
      (0, _Logger2.default)({
        type: 'prefetch failure',
        error: err,
        meta: {
          api: 'spins'
        }
      });
    });

    _PrefetchProvider2.default.getProvider('bundlesAPI', defaultProviderFactory, {}).then(function (response) {
      store.dispatch(actionCreators.SET_BUNDLE_RESULTS(response.data && response.data[0] || {}));
    }).catch(function (err) {
      // console.log(err);
      (0, _Logger2.default)({
        type: 'prefetch failure',
        error: err,
        meta: {
          api: 'bundle'
        }
      });
    });

    _PrefetchProvider2.default.getProvider('crossSellAPI', defaultProviderFactory, {}).then(function (response) {
      store.dispatch(actionCreators.SET_CROSS_SELL(response.data || {}));
    }).catch(function (err) {
      console.log(err);
      (0, _Logger2.default)({
        type: 'prefetch failure',
        error: err,
        meta: {
          api: 'crosssell'
        }
      });
    });

    _PrefetchProvider2.default.getProvider('cartAPI', defaultProviderFactory, {}).then(function (response) {
      store.dispatch(actionCreators.SET_CART_DOMAINS(response.data.PendingDomainNames || []));
    }).catch(function (err) {
      console.log(err);
      (0, _Logger2.default)({
        type: 'prefetch failure',
        error: err,
        meta: {
          api: 'cart'
        }
      });
    });
  }

  return _react2.default.createElement(_reactRedux.Provider, { store: store }, _react2.default.createElement('div', { className: 'bg-medium' }, _react2.default.createElement(_DomainSearchPage2.default, _extends({}, props, {
    Logger: _Logger2.default
  }))));
};

SearchPage.propTypes = {
  initProps: _propTypes2.default.shape({
    appName: _propTypes2.default.string,
    apiKey: _propTypes2.default.string.isRequired,
    domainToCheck: _propTypes2.default.string,
    numberOfSpins: _propTypes2.default.string,
    trackingCode: _propTypes2.default.string,
    privateLabelId: _propTypes2.default.number,
    urlOverride: _propTypes2.default.string,
    excludePromo: _propTypes2.default.bool,
    eid: _propTypes2.default.string,
    vcartNavigation: _propTypes2.default.object,
    showFilters: _propTypes2.default.bool,
    showBundles: _propTypes2.default.bool,
    showCrossSell: _propTypes2.default.bool
  }).isRequired
};

exports.default = SearchPage;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/reducers/FilterReducers.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setFilters = undefined;

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _ActionTypes = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/constants/ActionTypes.js");

var _ActionTypes2 = _interopRequireDefault(_ActionTypes);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var tldInfoMapInit = {
  "all": {
    value: '',
    checked: true
  }
};

var setFilters = exports.setFilters = function setFilters() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var action = arguments[1];

  var tldInfoMap = void 0,
      activeTlds = void 0;
  switch (action.type) {

    case _ActionTypes2.default.TOGGLE_CCTLD_BUTTON:
      return _extends({}, state, {
        showOnlyCCTLDS: !state.showOnlyCCTLDS
      });

    case _ActionTypes2.default.TOGGLE_PREMIUM_BUTTON:
      return _extends({}, state, {
        showOnlyPremiums: !state.showOnlyPremiums
      });

    case _ActionTypes2.default.SET_SLD_MAX_SIZE:
      return _extends({}, state, {
        maximumLengthSet: action.size,
        maximumLength: action.defaultSize ? action.defaultSize : state.maximumLength
      });

    case _ActionTypes2.default.SET_MAX_PRICE:
      return _extends({}, state, {
        maximumPrice: action.price
      });

    case _ActionTypes2.default.SET_FILTERS:
      tldInfoMap = action.tlds.reduce(function (tMap, tld) {
        tMap[tld] = {
          label: '.' + tld,
          value: tld,
          checked: false
        };
        return tMap;
      }, _extends({}, tldInfoMapInit));

      var maximumLength = action.recommendedDomains.reduce(function (prevMax, domain) {
        return Math.max(prevMax, domain.NameWithoutExtension.length);
      }, 0);

      var maximumLengthSet = maximumLength;
      var maximumPrice = -1;

      activeTlds = new Set();

      return { tldInfoMap: tldInfoMap, activeTlds: activeTlds, showOnlyCCTLDS: false, showOnlyPremiums: false,
        maximumLength: maximumLength, maximumLengthSet: maximumLengthSet, maximumPrice: maximumPrice };

    case _ActionTypes2.default.TOGGLE_TLD_BUTTON:
      tldInfoMap = _extends({}, state.tldInfoMap);
      var tld = action.tld === '' ? "all" : action.tld;
      var tldInfo = tldInfoMap[tld];
      var tldChecked = tldInfo.checked;
      activeTlds = new Set();
      state.activeTlds.forEach(function (v) {
        return activeTlds.add(v);
      });

      if (tldChecked) {
        activeTlds.delete(action.tld);
        if (activeTlds.size === 0) {
          tldInfoMap["all"]["checked"] = true;
        }
      } else {
        if (tld === "all") {
          activeTlds.clear();
          state.activeTlds.forEach(function (activeTld) {
            tldInfoMap[activeTld]["checked"] = false;
          });
          tldInfoMap["all"]["checked"] = true;
        } else {
          tldInfoMap["all"]["checked"] = false;
          activeTlds.add(action.tld);
        }
      }

      if (tld !== "all") {
        tldInfoMap[action.tld] = _extends({}, tldInfo, {
          checked: !tldChecked
        });
      }

      return _extends({}, state, { tldInfoMap: tldInfoMap, activeTlds: activeTlds });

    default:
      return state;
  }
};

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/reducers/Reducer.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _SetStoreReducers = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/reducers/SetStoreReducers.js");

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var reducers = _SetStoreReducers.fullResults;

exports.default = reducers;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/reducers/SetStoreReducers.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.fullResults = undefined;

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _ActionTypes = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/constants/ActionTypes.js");

var _ActionTypes2 = _interopRequireDefault(_ActionTypes);

var _SearchStatus = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/constants/SearchStatus.js");

var _SearchStatus2 = _interopRequireDefault(_SearchStatus);

var _FilterReducers = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/reducers/FilterReducers.js");

var _actions = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/actions/index.js");

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var setProducts = function setProducts() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var action = arguments[1];

  switch (action.type) {
    case _ActionTypes2.default.SET_PRODUCTS:
      return action.products.reduce(function (pMap, product) {
        pMap[product.Tld] = pMap[product.Tld] || {};
        pMap[product.Tld][product.TierId] = product;
        return pMap;
      }, {});

    default:
      return state;
  }
};

var setCart = function setCart() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : new Set();
  var action = arguments[1];

  var set = new Set();
  switch (action.type) {

    case _ActionTypes2.default.SET_CART_DOMAINS:
      action.domains.forEach(function (x) {
        return set.add(x);
      });
      return set;

    case _ActionTypes2.default.ADD_TO_CART:
      state.forEach(function (x) {
        return set.add(x);
      });
      action.domains.forEach(function (domain) {
        set.add(domain);
      });
      return set;

    case _ActionTypes2.default.REMOVE_FROM_CART:
      state.forEach(function (x) {
        return set.add(x);
      });
      action.domains.forEach(function (domain) {
        set.delete(domain);
      });
      return set;

    default:
      return state;
  }
};

var initState = { products: {},
  results: null,
  exactResult: {},
  exactResultProduct: {},
  spinResult: {},
  bundleResult: {},
  crossSellResult: {},
  domainsInCart: new Set(),
  isDBSOn: false,
  isDisclaimerOn: false,
  isRaceConditionOn: false,
  searchStatus: _SearchStatus2.default.None,
  forcedExactClick: {
    value: false,
    byCartButton: false
  },
  dbsPrice: "",
  supportInfo: {
    premiumDomainSupportPhone: null,
    auctionSupportEmail: "Auctions@GoDaddy.com",
    auctionsSupportPhone: null,
    supportPhone: null
  },
  icann_fee: "",
  auction_fee: "",
  filterInfo: {
    tldInfoMap: {},
    activeTlds: new Set(),
    showOnlyCCTLDS: false,
    showOnlyPremiums: false,
    maximumLength: 0,
    maximumLengthSet: 0,
    maximumPrice: -1
  },
  allButtonsDisabled: false
};
var fullResults = exports.fullResults = function fullResults() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initState;
  var action = arguments[1];

  switch (action.type) {
    case _ActionTypes2.default.SET_DOMAIN_SEARCH_RESULT:
      return _extends({}, state, {
        results: action.result,
        exactResult: action.result.ExactMatchDomain,
        exactResultProduct: action.result.Products[0],
        forcedExactClick: {
          value: false,
          byCartButton: false
        },
        bundleResult: action.clrBundleAndXSell ? {} : state.bundleResult,
        crossSellResult: action.clrBundleAndXSell ? {} : state.crossSellResult,
        dbsPrice: action.result.DbsPrice,
        supportInfo: _extends({}, state.supportInfo, {
          premiumDomainSupportPhone: action.result.PremiumPhone,
          auctionsSupportPhone: action.result.AuctionPhone,
          supportPhone: action.result.SupportPhone
        }),
        icann_fee: action.result.IcannFeePrice,
        auction_fee: action.result.AuctionFeePrice
      });

    case _ActionTypes2.default.SET_BUNDLE_RESULTS:
      return _extends({}, state, {
        bundleResult: action.bundles
      });

    case _ActionTypes2.default.SET_CROSS_SELL:
      var crossSell = _extends({}, action.crossSell, {
        CrossSellDomains: action.crossSell.CrossSellDomains ? action.crossSell.CrossSellDomains.slice(0, 1) : []
      });
      return _extends({}, state, {
        crossSellResult: crossSell
      });

    case _ActionTypes2.default.SET_SEARCH_STATUS:
      return _extends({}, state, {
        searchStatus: action.searchStatus
      });

    case _ActionTypes2.default.SET_CART_DOMAINS:
    case _ActionTypes2.default.ADD_TO_CART:
    case _ActionTypes2.default.REMOVE_FROM_CART:
      return _extends({}, state, {
        domainsInCart: setCart(state.domainsInCart, action)
      });

    case _ActionTypes2.default.SET_INIT_SPIN_RESULTS:
      var productMap = setProducts(state.products, (0, _actions.SET_PRODUCTS)(action.spins.Products || []));
      return _extends({}, state, {
        products: productMap,
        spinResult: _extends({}, action.spins, { productMap: productMap }),
        filterInfo: (0, _FilterReducers.setFilters)(state.filterInfo, (0, _actions.SET_FILTERS)(action.spins.Tlds, action.spins.RecommendedDomains))
      });

    case _ActionTypes2.default.SET_SPIN_RESULTS:
      var maximumLength = action.spins.RecommendedDomains.reduce(function (prevMax, domain) {
        return Math.max(prevMax, domain.NameWithoutExtension.length);
      }, 0);
      return _extends({}, state, {
        spinResult: _extends({}, action.spins, { productMap: setProducts(state.products, (0, _actions.SET_PRODUCTS)(action.spins.Products || [])) }),
        filterInfo: (0, _FilterReducers.setFilters)(state.filterInfo, (0, _actions.SET_SLD_MAX_SIZE)(maximumLength, maximumLength))
      });

    case _ActionTypes2.default.SET_SLD_MAX_SIZE:
    case _ActionTypes2.default.TOGGLE_CCTLD_BUTTON:
    case _ActionTypes2.default.TOGGLE_PREMIUM_BUTTON:
    case _ActionTypes2.default.SET_MAX_PRICE:
    case _ActionTypes2.default.TOGGLE_TLD_BUTTON:
      return _extends({}, state, {
        filterInfo: (0, _FilterReducers.setFilters)(state.filterInfo, action)
      });

    case _ActionTypes2.default.FLIP_DBS_MODAL_STATE:
      return _extends({}, state, {
        isDBSOn: action.visibility
      });

    case _ActionTypes2.default.FLIP_DISCLAIMER_MODAL_STATE:
      return _extends({}, state, {
        isDisclaimerOn: action.visibility
      });

    case _ActionTypes2.default.FLIP_RACE_CONDITION_MODAL_STATE:
      return _extends({}, state, {
        isRaceConditionOn: action.visibility
      });

    case _ActionTypes2.default.FORCE_EXACT_MATCH_SELECT:
      var cartAction = action.on ? (0, _actions.ADD_TO_CART)(new Set([state.results.ExactMatchDomain.Fqdn])) : (0, _actions.REMOVE_FROM_CART)(new Set([state.results.ExactMatchDomain.Fqdn]));
      return _extends({}, state, {
        domainsInCart: setCart(state.domainsInCart, cartAction),
        forcedExactClick: {
          value: action.on,
          byCartButton: action.byCartButton === true || action.byCartButton === false ? action.byCartButton : state.forcedExactClick.byCartButton
        }
      });

    case _ActionTypes2.default.DISABLE_ALL_BUTTONS:
      return _extends({}, state, {
        allButtonsDisabled: action.disable
      });

    default:
      return state;
  }
};

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/services/SubDomainService.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

var _subDomains = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/services/subDomains.json");

var _subDomains2 = _interopRequireDefault(_subDomains);

var _jsCookie = __webpack_require__("./node_modules/js-cookie/src/js.cookie.js");

var _jsCookie2 = _interopRequireDefault(_jsCookie);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

var SubDomainService = function () {
  function SubDomainService() {
    _classCallCheck(this, SubDomainService);
  }

  _createClass(SubDomainService, null, [{
    key: "getSubDomainAndLanguage",

    /**
     * Gets the sub domain and language, if available, for the market id
     *
     * @method getSubDomainAndLanguage
     * @returns {JSON} Returns JSON object with subDomain and language keys
     */
    value: function getSubDomainAndLanguage() {
      var marketCookie = _jsCookie2.default.get("market") || "en-US";
      var subDomainData = _subDomains2.default[marketCookie] || _subDomains2.default["en-US"];
      return { "subDomain": subDomainData.subDomain, "language": subDomainData.language };
    }

    /**
     * Gets the sub domain and language, if available, for the market id
     *
     * @method getUrl
     * @param {String} host Example: dev-godaddy.com or test-godaddy.com
     * @param {String} path with or without query string params. Example: /api/selected/domains or ?q=chcekingdomain.com
     * @returns {String} Returns the full URL
     */

  }, {
    key: "getUrl",
    value: function getUrl(host, path) {
      var hostValue = host || "secureserver.net";
      var url = "https://" + hostValue;

      if (path) {
        url = ~path.indexOf("/") ? url.concat(path) : url.concat('/', path);
      }

      return url;
    }
  }]);

  return SubDomainService;
}();

exports.default = SubDomainService;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/services/subDomains.json":
/***/ (function(module, exports) {

module.exports = {"da-DK":{"subDomain":"dk"},"de-AT":{"subDomain":"at"},"de-CH":{"subDomain":"ch"},"de-DE":{"subDomain":"de"},"el-GR":{"subDomain":"gr"},"en-AE":{"subDomain":"ae"},"en-AU":{"subDomain":"au"},"en-CA":{"subDomain":"ca"},"en-GB":{"subDomain":"uk"},"en-HK":{"subDomain":"hk","language":"en"},"en-IE":{"subDomain":"ie"},"en-IL":{"subDomain":"il"},"en-IN":{"subDomain":"in"},"en-MY":{"subDomain":"my"},"en-NZ":{"subDomain":"nz"},"en-PH":{"subDomain":"ph"},"en-PK":{"subDomain":"pk"},"en-SG":{"subDomain":"sg"},"en-US":{"subDomain":"www"},"en-ZA":{"subDomain":"za"},"es-AR":{"subDomain":"ar"},"es-CL":{"subDomain":"cl"},"es-CO":{"subDomain":"co"},"es-ES":{"subDomain":"es"},"es-MX":{"subDomain":"mx"},"es-PE":{"subDomain":"pe"},"es-US":{"subDomain":"www","language":"es"},"es-VE":{"subDomain":"ve"},"fi-FI":{"subDomain":"fi"},"fil-PH":{"subDomain":"ph","language":"fil"},"fr-BE":{"subDomain":"be","language":"fr"},"fr-CA":{"subDomain":"ca","language":"fr"},"fr-CH":{"subDomain":"ch","language":"fr"},"fr-FR":{"subDomain":"fr"},"hi-IN":{"subDomain":"in","language":"hi"},"id-ID":{"subDomain":"id"},"it-CH":{"subDomain":"ch","language":"it"},"it-IT":{"subDomain":"it"},"ja-JP":{"subDomain":"jp"},"ko-KR":{"subDomain":"kr"},"mr-IN":{"subDomain":"in","language":"mr"},"ms-MY":{"subDomain":"my","language":"ms"},"nb-NO":{"subDomain":"no"},"nl-BE":{"subDomain":"be"},"nl-NL":{"subDomain":"nl"},"pl-PL":{"subDomain":"pl"},"pt-BR":{"subDomain":"br"},"pt-PT":{"subDomain":"pt"},"qa-PS":{"subDomain":"www","language":"qa-ps"},"qa-PZ":{"subDomain":"www","language":"qa-pz"},"qa-QA":{"subDomain":"www","language":"qa-qa"},"ru-RU":{"subDomain":"ru"},"sv-SE":{"subDomain":"se"},"ta-IN":{"subDomain":"in","language":"ta"},"th-TH":{"subDomain":"th"},"tr-TR":{"subDomain":"tr"},"uk-UA":{"subDomain":"ua"},"vi-VN":{"subDomain":"vn"},"zh-HK":{"subDomain":"hk"},"zh-SG":{"subDomain":"sg","language":"zh"},"zh-TW":{"subDomain":"tw"}}

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/styles/base.scss":
/***/ (function(module, exports) {

// empty (null-loader)

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/utilities/AuctionInfo.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getAuctionInfo;

var _UrlHelper = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/UrlHelper.js");

var _UrlHelper2 = _interopRequireDefault(_UrlHelper);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function getAuctionInfo(language, exactDomain) {
  var auctionTypeId = exactDomain && exactDomain.AuctionTypeId || 0;
  var goValue = exactDomain && exactDomain.Valuation && exactDomain.Valuation.Prices && exactDomain.Valuation.Prices.Govalue || 0;
  var binPrice = exactDomain && exactDomain.Price || 0;
  var tooltipTitle = language.nemPremiumTag;
  var auctionURL = _UrlHelper2.default.continueToAuction(null, exactDomain.AuctionId);
  //const auctionEmailContact = `<a href="mailto:${supportInfo.auctionSupportEmail}" subject="Questions about GoDaddy Auctions">${supportInfo.auctionSupportEmail}</a>`;
  //const auctionFooter = `<br/><br/>${language.AuctionFooter.replace('{1}', auctionEmailContact).replace('{0}', `<a href="tel:${supportInfo.auctionsSupportPhone}">${supportInfo.auctionsSupportPhone}</a>`)}`;
  //const gdAuctionMemAnchor = `<a href="https://www.godaddy.com/auctions/domain-auctions">${language.gdAuctionMember}</a>`;
  // For all ids, refer to https://confluence.godaddy.com/pages/viewpage.action?spaceKey=AN&title=Aftermarket+Business+Requirements
  var tooltipMessage = "";
  switch (auctionTypeId) {
    /* Biddable Auction */
    /* TOP-PR-08 */
    case 13:
    case 14:
    case 16:
    case 25:
    case 37:
    case 32:
    case 38:
      //tooltipMessage = language.AuctionBidPar1.replace('{1}',gdAuctionMemAnchor).replace('{0}', auction_fee) + `<br/><br/>${language.AuctionBidPar2}` + auctionFooter;
      tooltipMessage = language.nemAuctionTooltipText;
      return {
        isPremium: false,
        preambleText: language.currentBid,
        buttonText: language.bid,
        auctionURL: auctionURL,
        tooltipTitle: tooltipTitle,
        tooltipMessage: tooltipMessage,
        /*
         * 1) est. value > highest offer
        */
        showValuation: goValue > 100 && goValue > binPrice,
        showBuyNow: false,
        showMakeOffer: true,
        makeOfferProps: {
          text: language.placeBid,
          design: 'purchase'
        },
        showRenewalPrice: false,
        showCrosssell: false
      };

    /* Member Offer/Counter Offer */
    /* TOP-PR-07 */
    case 9:
    case 33:
      //tooltipMessage = language.AuctionOfferPar.replace('{1}',gdAuctionMemAnchor).replace('{0}', auction_fee) + auctionFooter;
      tooltipMessage = language.nemPremiumTooltipText;
      return {
        isPremium: true,
        preambleText: language.minOfferAmt,
        buttonText: language.makeOffer,
        auctionURL: auctionURL,
        tooltipTitle: tooltipTitle,
        tooltipMessage: tooltipMessage,
        /*
         * 1) est. value > $100
         * 2) est. value > BIN price
         * 3) est. value > highest offer?
        */
        showValuation: false,
        showBuyNow: false,
        showMakeOffer: true,
        makeOfferProps: {
          text: language.makeOffer,
          design: 'purchase'
        },
        showRenewalPrice: false,
        showCrosssell: false
      };

    /* Buy It Now, Closeout Auction */
    /* TOP-PR-05, TOP-PR-06 */
    case 20:
    case 39:
      //tooltipMessage = language.AuctionBidPar1.replace('{1}',gdAuctionMemAnchor).replace('{0}', auction_fee) + `<br/><br/>${language.AuctionBidPar2}` + auctionFooter;
      // tooltipMessage = language.nemAuctionTooltipText;
      return {
        isPremium: false,
        preambleText: language.buyNowPrice,
        buttonText: language.buyItNow,
        auctionURL: auctionURL,
        tooltipTitle: tooltipTitle,
        tooltipMessage: tooltipMessage,
        /*
         * 1) est. value > $100
         * 2) est. value > BIN price
        */
        showValuation: goValue > 100 && goValue > binPrice,
        showBuyNow: true,
        showMakeOffer: false,
        makeOfferProps: {},
        showRenewalPrice: false,
        showCrosssell: false,
        domainStatusOverride: 'aftermarket'
      };

    /* Buy It Now, Offer/Counter Offer */
    /* TOP-PR-01, TOP-PR-02 */
    case 10:
    case 11:
    case 15:
    case 18:
    case 21:
    case 23:
    case 36:
      //tooltipMessage = language.AuctionBuyNowPar.replace('{1}',gdAuctionMemAnchor).replace('{0}', auction_fee) + auctionFooter;
      // tooltipMessage = language.nemPremiumTooltipText;
      return {
        isPremium: true,
        preambleText: language.buyNowPrice,
        buttonText: language.getIt,
        auctionURL: auctionURL,
        tooltipTitle: tooltipTitle,
        tooltipMessage: tooltipMessage,
        /*
         * 1) est. value > $100
         * 2) est. value > BIN price
        */
        showValuation: goValue > 100 && goValue > binPrice,
        showBuyNow: false,
        showMakeOffer: true,
        makeOfferProps: {
          text: language.getIt,
          design: 'purchase'
        },
        showRenewalPrice: false,
        showCrosssell: false
      };

    /* Not in Use */
    default:
      return null;
  }
};

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/utilities/CartContinueHelper.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _UrlHelper = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/UrlHelper.js");

var _UrlHelper2 = _interopRequireDefault(_UrlHelper);

var _HttpRequestHelper = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/HttpRequestHelper.js");

var _HttpRequestHelper2 = _interopRequireDefault(_HttpRequestHelper);

var _RaceCondition = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/constants/RaceCondition.js");

var _RaceCondition2 = _interopRequireDefault(_RaceCondition);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function proceedToCart(urlOverride, privateLabelId, flipRaceConditionModal, Logger) {
  var LOCAL_STORAGE_FAILURE_TIMES = _RaceCondition2.default.LOCAL_STORAGE_FAILURE_TIMES,
      LOCAL_STORAGE_TIMESTAMP = _RaceCondition2.default.LOCAL_STORAGE_TIMESTAMP;

  var addToCartUrl = _UrlHelper2.default.getNextStepUrl(urlOverride, privateLabelId);
  var showRaceConditionModal = function showRaceConditionModal() {
    localStorage.setItem(LOCAL_STORAGE_FAILURE_TIMES, 0);
    localStorage.setItem(LOCAL_STORAGE_TIMESTAMP, new Date().getTime());
    flipRaceConditionModal(true);
  };
  _HttpRequestHelper2.default.callApiMin({
    httpMethod: "GET",
    apiUrl: addToCartUrl,
    headers: { 'Accept': 'application/json' },
    needCredentials: true
  }).then(function (res) {
    if (res.status === 200) {
      if (res.data) {
        if (res.data.CurrentlyPending > 0) {
          window.location = res.data.NextStepUrl || _UrlHelper2.default.continueToCart(urlOverride, privateLabelId);
        } else {
          showRaceConditionModal();
          Logger({
            type: 'race condition',
            error: {
              message: 'CurrentlyPending missing or equals to 0'
            },
            meta: {
              api: 'cart-continue',
              apiUrl: addToCartUrl
            }
          });
        }
      } else {
        window.location = _UrlHelper2.default.continueToCart(urlOverride, privateLabelId);
        Logger({
          type: 'response body empty',
          error: {
            message: 'NextStepUrl missing'
          },
          meta: {
            api: 'cart-continue',
            apiUrl: addToCartUrl,
            level: 'warning'
          }
        });
      }
    } else {
      showRaceConditionModal();
      Logger({
        type: 'response status not 200',
        error: null,
        meta: {
          api: 'cart-continue',
          apiUrl: addToCartUrl
        }
      });
    }
  }).catch(function (error) {
    console.log(error);
    showRaceConditionModal();
    Logger({
      type: 'request failure',
      error: error,
      meta: {
        api: 'cart-continue',
        apiUrl: addToCartUrl
      }
    });
  });
}

exports.default = proceedToCart;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/utilities/HttpRequestHelper.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}(); // for IE support


var _axios = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/index.js");

var _axios2 = _interopRequireDefault(_axios);

__webpack_require__("./node_modules/es6-promise/auto.js");

var _Logger = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/utilities/Logger.js");

var _Logger2 = _interopRequireDefault(_Logger);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

var HttpRequestHelper = function () {
  function HttpRequestHelper() {
    _classCallCheck(this, HttpRequestHelper);
  }

  _createClass(HttpRequestHelper, null, [{
    key: 'callApi',

    /**
     * Calls the success call back function or failure call back function depending on the response from api
     *
     * @method getDomainSearchBaseUrl
     * @param  {JSON} parameters in json with request parameters
     * eg., const requestParams = {
     *   "httpMethod": "GET", "apiUrl": api, "successCallback": successCallback, "failureCallback":failureCallback
     *   };
     */
    value: function callApi(parameters) {
      var httpMethod = parameters.httpMethod;
      var apiUrl = encodeURI(parameters.apiUrl);
      var logInfo = parameters.logInfo || {};
      var successCallback = parameters.successCallback;
      var failureCallback = parameters.failureCallback;
      var inputData = parameters.data;
      var needCredentials = parameters.needCredentials || false;
      (0, _axios2.default)({
        method: httpMethod,
        url: apiUrl,
        data: inputData,
        withCredentials: needCredentials
      }).then(function (response) {
        successCallback(response);
      }).catch(function (err) {
        console.log(apiUrl);
        console.log(err);
        (0, _Logger2.default)({
          type: 'request failure',
          error: err,
          meta: _extends({
            apiUrl: parameters.apiUrl
          }, logInfo)
        });
        failureCallback(err);
      });
    }

    /**
     * Calls the success call back function or failure call back function depending on the response from api
     *
     * @method callApiMin
     * @param  {JSON} parameters in json with request parameters
     * eg., const requestParams = {
     *   "httpMethod": "GET", "apiUrl": api
     *   };
     *  @returns {Promise} promise result
     */

  }, {
    key: 'callApiMin',
    value: function callApiMin(parameters) {
      var httpMethod = parameters.httpMethod;
      var apiUrl = parameters.apiUrl;
      var inputData = parameters.data;
      return (0, _axios2.default)({
        method: httpMethod,
        url: apiUrl,
        data: inputData
      });
    }
  }]);

  return HttpRequestHelper;
}();

exports.default = HttpRequestHelper;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/utilities/Logger.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

exports.default = postLog;

var _axios = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/index.js");

var _axios2 = _interopRequireDefault(_axios);

var _detectBrowser = __webpack_require__("./node_modules/detect-browser/index.js");

var _queryString = __webpack_require__("./node_modules/query-string/index.js");

var _queryString2 = _interopRequireDefault(_queryString);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var LOG_API_PATH = '/api/v1/search/jserror';
var cookieNameMap = {
  visitor: 'visitor_guid',
  pathway: 'vguid',
  market: 'market',
  currency: 'currency',
  isc: 'isc'
};

function getApiUrl(hostname) {
  var envPrefixRegexMatch = /.*\.(.*)secureserver.net.*/.exec(hostname);
  var envPrefix = envPrefixRegexMatch && envPrefixRegexMatch[1] ? envPrefixRegexMatch[1] : '';
  return 'https://www.' + envPrefix + 'secureserver.net' + LOG_API_PATH;
}

function parseCookie() {
  var cookieArray = document.cookie.split('; ');
  var cookie = {};
  for (var i = 0; i < cookieArray.length; i++) {
    var equalPos = cookieArray[i].indexOf('=');
    var key = cookieArray[i].substring(0, equalPos);
    if (cookieNameMap[key]) {
      var value = cookieArray[i].substr(equalPos + 1);
      // trim "vid=" at the beginning of visitor_guid
      cookie[key] = key === 'visitor' ? value.replace('vid=', '') : value;
    }
  }

  return cookie;
}

function postLog(_ref) {
  var type = _ref.type,
      error = _ref.error,
      meta = _ref.meta;

  error = error || {};
  meta = meta || {};
  /* eslint-disable no-nested-ternary */
  var env = /.*test-godaddy.*/.test(window.location.hostname) ? 'test' : /.*godaddy.*/.test(window.location.hostname) ? 'prod' : 'unknown';
  var browser = (0, _detectBrowser.detect)();
  var cookie = parseCookie();
  var urlParams = _queryString2.default.parse(location.search);

  try {
    var data = _extends({
      level: 'error',
      type: type || 'Generic client error',
      message: error.message || '',
      stack: error.stack,
      env: env,
      browser: JSON.stringify(browser),
      date: new Date().toJSON()
    }, meta, cookie, urlParams);

    (0, _axios2.default)({
      url: getApiUrl(window.location.hostname),
      method: 'post',
      withCredentials: false,
      data: data
    }).catch(function (e) {
      console.log('error while log request: ' + e);
    });
  } catch (e) {
    console.log('error while posting log: ' + e);
  }

  return;
}

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/utilities/PrefetchProvider.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _deepEqual = __webpack_require__("./node_modules/deep-equal/index.js");

var _deepEqual2 = _interopRequireDefault(_deepEqual);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var _expiration = 10 * 60 * 1000;

function invalidated(cacheEntry) {
  return cacheEntry.invalidated;
}

function expired(cacheEntry) {
  return _expiration > 0 && Date.now() > cacheEntry.startTime + _expiration;
}

function invalidateCacheEntry(cacheEntry) {
  cacheEntry.invalidated = true;
}

function shouldInvalidate(cacheEntry, invalidate) {
  var statusIsServerError = cacheEntry && cacheEntry.response && (cacheEntry.response.status === 0 || cacheEntry.response.status >= 500);
  return invalidate || statusIsServerError;
}

var findCacheStateList = function findCacheStateList() {
  return window.apiCache.STATES;
};

var findValidCacheEntry = function findValidCacheEntry(key, tags, expiration) {
  // const CACHE_ROOT = window.apiCache;

  // if (CACHE_ROOT && CACHE_ROOT[key]) {
  //   let cacheEntry = CACHE_ROOT[key];

  //   if (!invalidated(cacheEntry)) {
  //     if (equal(tags, cacheEntry.tags)) {
  //       if (!expired(cacheEntry, expiration)) {
  //         return cacheEntry;
  //       }
  //     }
  //   }
  // }

  return null;
};

function getCacheProvider(key, tags, expiration, invalidate) {
  var cacheEntry = findValidCacheEntry(key, tags, expiration);

  if (cacheEntry) {
    if (shouldInvalidate(cacheEntry, invalidate)) {
      invalidateCacheEntry(cacheEntry);
    }
    var states = findCacheStateList();
    var status = cacheEntry.callStatus;

    // Request still in flight -> return promise to wait until call completes
    if (status === states.APICALL_IN_FLIGHT) {
      return new Promise(function (resolve, reject) {
        cacheEntry.callback = function (response) {
          if (response.status >= 200 && response.status < 300) {
            return resolve(response);
          }
          return reject(response);
        };
      });
    }

    // Cache available and request completed -> return promise resolve if succeeded or reject if failed
    if (status === states.APICALL_COMPLETED) {
      if (cacheEntry.response.status >= 200 && cacheEntry.response.status < 300) {
        return Promise.resolve(cacheEntry.response);
      }
      return Promise.reject(cacheEntry.response);
    }
  }

  return null;
}

/**
 * @method getProvider
 * @param {String} key - Key to identify the cache entry
 * @param {Function} defaultProviderFactory - provider function to use in case no other provider can return the data
 * @param {Object} tags - Tags to ascertain if the cache entry has changed
 * @param {Number} expirationTimeInMS - Time to expire cached data (Optional, default is to invalidateCacheEntry on first read}
 * @param {Boolean} invalidate - If true or missing, content of cache will be invalidated
 * @returns {Object}  updated
 */

function getProvider(key, defaultProviderFactory) {
  var tags = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  var expirationTimeInMS = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;

  var invalidate = true;
  var cacheProvider = getCacheProvider(key, tags, expirationTimeInMS, invalidate);

  if (cacheProvider) {
    return cacheProvider;
  }

  return defaultProviderFactory();
}

module.exports = { getProvider: getProvider };

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/utilities/RtbInfo.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getRtbInfo;
function priceFormattor(price) {
  var str = String(price.toFixed(2)).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
  return (str.charAt(0) === '$' ? '' : '$') + str.substring(0, str.length - 3);
}

function wordWrapper(word) {
  var isStart = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

  var str = isStart ? word.charAt(0).toUpperCase() + word.slice(1) : word;
  return '"' + str + '"';
}

function getRtbInfo(language, exactDomain) {
  var numToShow = 3;
  var sldShortLength = 15;
  var reasons = exactDomain && exactDomain.Valuation && exactDomain.Valuation.Reasons;
  var listPrice = exactDomain && exactDomain.Price || 0;

  if (!reasons || reasons.lengths < 1) {
    /*
     * Fallback logic
     *  1) Great extension (In case of com). 
     *  2) Short (sld < 15).
     *  3) Sold before.
     *  4) Popular keywords. 
     */
    var rtbGreatExtension = language.rtbGreatExtension,
        rtbShort = language.rtbShort,
        rtbFallbackPopularKeywords = language.rtbFallbackPopularKeywords,
        rtbFallbackSold = language.rtbFallbackSold;

    var _reasonsToShow = [];

    if (exactDomain.Extension === 'com') {
      _reasonsToShow.push(rtbGreatExtension);
    }
    if (exactDomain.NameWithoutExtension.length < sldShortLength) {
      _reasonsToShow.push(rtbShort.replace('{0}', exactDomain.NameWithoutExtension));
    }

    _reasonsToShow = _reasonsToShow.concat([rtbFallbackPopularKeywords, rtbFallbackSold]);

    return _reasonsToShow.slice(0, Math.min(_reasonsToShow.length, numToShow));
  }

  var reasonsToShow = reasons.reduce(function (result, reason) {
    var parsedStr = void 0;
    switch (reason.Type) {
      case "valuable_keyword":
        parsedStr = language.rtbValuableKeyword.replace('{0}', wordWrapper(reason.Keyword, true)).replace('{1}', priceFormattor(reason.Avg_sold_price));
        // filter
        if (reason.Avg_sold_price < listPrice) {
          parsedStr = null;
        }
        break;

      case "valuable_keywords":
        parsedStr = language.rtbValuableKeywords.replace('{0}', wordWrapper(reason.Keyword1, true)).replace('{1}', wordWrapper(reason.Keyword2)).replace('{2}', priceFormattor(reason.Keyword1_avg_sold_price)).replace('{3}', priceFormattor(reason.Keyword2_avg_sold_price));
        // filter
        if (reason.Keyword1_avg_sold_price < listPrice && reason.Keyword2_avg_sold_price < listPrice) {
          parsedStr = null;
        }
        break;

      case "popular_keyword":
        parsedStr = language.rtbPopularKeyword.replace('{0}', wordWrapper(reason.Keyword, true));
        break;

      case "popular_keywords":
        parsedStr = language.rtbPopularKeywords.replace('{0}', wordWrapper(reason.Keyword1, true)).replace('{1}', wordWrapper(reason.Keyword2));
        break;

      case "memorable":
        parsedStr = language.rtbMemorable.replace('{0}', wordWrapper(reason.Domain));
        break;

      case "broad_appeal":
        parsedStr = language.rtbBroadAppeal.replace('{0}', wordWrapper(reason.Domain));
        break;

      case "domain_history":
        parsedStr = language.rtbDomainHistory.replace('{0}', wordWrapper(reason.Domain));
        break;

      case "other_extension_sold_high":
        parsedStr = language.rtbOtherExtensionSoldHigh.replace('{0}', wordWrapper(reason.Domain)).replace('{1}', priceFormattor(reason.Price));
        break;

      case "other_extension_sold":
        parsedStr = language.rtbOtherExtensionSold.replace('{0}', wordWrapper(reason.Domain));
        break;

      case "great_extension":
        parsedStr = language.rtbGreatExtension;
        break;

      case "short":
        parsedStr = language.rtbShort.replace('{0}', wordWrapper(reason.Sld));
        break;

      case "extensions_taken":
        parsedStr = language.rtbExtensionsTaken.replace('{0}', reason.Num_of_other_tlds_in_use).replace('{1}', wordWrapper(reason.Sld));
        break;

      case "unique":
        parsedStr = language.rtbUnique.replace('{0}', wordWrapper(reason.Sld));
        break;

      default:
        parsedStr = null;
    }

    if (parsedStr) {
      return result.concat(parsedStr);
    } else {
      return result;
    }
  }, []);

  return reasonsToShow.slice(0, Math.min(reasons.length, numToShow));
};

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/utilities/SearchStatus.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _SearchStatus = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/constants/SearchStatus.js");

var _SearchStatus2 = _interopRequireDefault(_SearchStatus);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function getSearchStatus(exactData) {
  var searchStatus = _SearchStatus2.default.Unavailable;
  if (exactData.ExactMatchDomain) {
    var exactMatchDomain = exactData.ExactMatchDomain;

    var isPremium = exactMatchDomain.Inventory === 'premium' && exactMatchDomain.VendorId !== 11;
    var isAuction = exactMatchDomain.Inventory === 'auction';
    var isPreReg = exactMatchDomain.TldInfo;

    if (!isPremium && exactMatchDomain.AvailabilityStatus === 1001 && !isAuction) {
      searchStatus = _SearchStatus2.default.Taken;
    } else if (exactMatchDomain.IsPurchasable === false && exactMatchDomain.AvailabilityStatus === 1000 || isPremium) {
      searchStatus = _SearchStatus2.default.Restricted;
    } else if (isAuction) {
      searchStatus = _SearchStatus2.default.Auction;
    } else if (isPreReg) {
      searchStatus = _SearchStatus2.default.PreReg;
    } else if (!isPremium && exactMatchDomain.AvailabilityStatus === 1000 && !isAuction) {
      searchStatus = _SearchStatus2.default.Available;
    } else {
      searchStatus = _SearchStatus2.default.Unavailable;
    }
  }
  return searchStatus;
}

exports.default = getSearchStatus;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/utilities/UrlHelper.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();
//import HostConfig from "./HostConfig";


var _SubDomainService = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/services/SubDomainService.js");

var _SubDomainService2 = _interopRequireDefault(_SubDomainService);

var _queryString = __webpack_require__("./node_modules/query-string/index.js");

var _config = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/config/config.js");

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

var hosts = {
  gd: "secureserver.net"
  // gd: "godaddy.com"
};

var prefixes = {
  fos: "https://",
  cart: "https://cart.",
  auction: "https://auctions.",
  support: "http://support.api.",
  find: "https://www."
};

var paths = {
  domainSearch: "/api/v1/search", // storefront API proxy
  availCheckAndAdd: "/domainsapi/v1/check/add",
  serpUrl: "/domains/searchresults.aspx",
  spinsUrl: "/api/v1/search/spins", // storefront API proxy
  virtualCart: "/upp/vcart",
  cartNextStep: "/api/dpp/searchresultscart/3?applyBP=1&ci=84427",
  cartAdd: "/api/dpp/searchresultscart/13",
  cartRemove: "/api/dpp/searchresultscart/2",
  bundleGet: "/api/v1/search/bundles", // storefront API proxy
  dynamicBundleGet: "/api/v1/search/bundles",
  bundleAdd: "/api/dpp/searchresultscart/12",
  dynamicBundleAdd: "/api/dpp/searchresultscart/16",
  bundleRemove: "/api/dpp/searchresultscart/9",
  dynamicBundleRemove: "/api/dpp/searchresultscart/17",
  crossSellGet: "/api/v1/search/crosssell", // storefront API proxy
  cartGet: "/api/dpp/searchresultscart/10",
  cartContinue: "/dpp/domain-config?ci=84427?ci=84427",
  auctionContinue: "/trpItemListing.aspx",
  support: "/v1/plid/1/contacts"
};

var UrlHelper = function () {
  function UrlHelper() {
    _classCallCheck(this, UrlHelper);
  }

  _createClass(UrlHelper, null, [{
    key: "buildUrl",

    /**
     * Concatenates URL pieces together
     *
     * @method buildUrl
     * @param  {String} prefix Example: https://www.
     * @param  {String} host Example: godaddy.com
     * @param  {String} path Example: /domainsapi/v1/search
     * @returns {String} Returns URL
     */
    value: function buildUrl(prefix, host, path) {
      return prefix.concat(host, path);
    }

    // updated in reseller fork

  }, {
    key: "getTestHost",
    value: function getTestHost(urlOverride) {
      return urlOverride || null;
    }

    /**
     * Concatenates URL pieces together
     *
     * @method getBuildUrlParameters
     * @param {String} hostOverride example: dev-godaddy.com
     * @returns {JSON} Returns prefix and host to build URL
     */

  }, {
    key: "getBuildUrlParameters",
    value: function getBuildUrlParameters(hostOverride) {
      var prefix = prefixes.fos.concat('www', ".");
      var host = hostOverride || this.getTestHost() || hosts.gd;

      return { "prefix": prefix, "host": host };
    }

    /**
     * Returns the fully qualified domainBaseUrl Url for correct environment
     *
     * @method getDomainSearchBaseUrl
     * @param  {String} hostOverride override for host. Example: dev-godaddy.com
     * @returns {String} Returns URL
     */

  }, {
    key: "getDomainSearchBaseUrl",
    value: function getDomainSearchBaseUrl(hostOverride) {
      var parameters = this.getBuildUrlParameters(hostOverride);
      return this.buildUrl(parameters.prefix, parameters.host, paths.domainSearch);
    }

    /**
     * Returns the fully qualified search results Url for correct environment
     *
     * @method getSerpUrl
     * @param  {String} domainName is the domain searched
     * @returns {String} Returns URL
     */

  }, {
    key: "getSerpUrl",
    value: function getSerpUrl(domainName) {
      var parameters = this.getBuildUrlParameters();
      var buildSerpUrl = this.buildUrl(parameters.prefix, parameters.host, paths.serpUrl);
      return buildSerpUrl + "?domain=" + domainName + window.urlSuffix;
    }

    /**
     * Returns the fully qualified domain search All Url for correct environment
     *
     * @method getDomainSearchAllUrl
     * @param  {JSON} domainSearchJson in json with domain search parameters
     * @param  {String} hostOverride override for host. Example: dev-godaddy.com
     * @returns {String} Returns URL
     */

  }, {
    key: "getDomainSearchAllUrl",
    value: function getDomainSearchAllUrl(domainSearchJson, hostOverride) {
      var key = domainSearchJson.apiKey || "dpp_search";
      var size = domainSearchJson.pageSize || 4;
      var start = domainSearchJson.pageStart || 0;
      var plid = domainSearchJson.privateLabelId;
      var excludePromo = domainSearchJson.excludePromo || false;
      var credentailsneeded = domainSearchJson.credentailsneeded || false;
      var baseUrl = this.getDomainSearchBaseUrl(hostOverride);

      baseUrl = baseUrl.concat("/all?", "q=", domainSearchJson.domainName, "&plid=", plid, "&pagestart=", start, "&pagesize=", size, "&key=", key);

      if (excludePromo) {
        baseUrl = baseUrl.concat("&excludepromo=", excludePromo);
      }

      if (credentailsneeded) {
        baseUrl = baseUrl.concat("&credentailsneeded=", credentailsneeded);
      }

      return baseUrl;
    }

    /**
     * Returns the fully qualified availcheck and 'add domain to VCart PostData' Url for correct environment
     *
     * @method getCheckAndGetVCartPostDataBaseUrl
     * @param  {String} hostOverride override for host. Example: dev-godaddy.com
     * @returns {String} Returns URL
     */

  }, {
    key: "getCheckAndGetVCartPostDataUrl",
    value: function getCheckAndGetVCartPostDataUrl(hostOverride) {
      var parameters = this.getBuildUrlParameters(hostOverride);
      return this.buildUrl(parameters.prefix, parameters.host, paths.availCheckAndAdd);
    }

    /**
     * Returns URL to add domains to cart
     *
     * @method getCartAddUrl
     * @param  {String} hostOverride override for host. Example: dev-godaddy.com
     * @param {String} domain is the name to add
     * @param {Number} privateLabelId is the identifier of the reseller.
     * @param {object} extraProps specify domainstatus and other info like pre_reg
     * @returns {String} Returns URL
     */

  }, {
    key: "getCartAddUrl",
    value: function getCartAddUrl(hostOverride, domain, privateLabelId, extraProps) {
      var parameters = this.getBuildUrlParameters(hostOverride);
      var baseUrl = this.buildUrl(parameters.prefix, parameters.host, paths.cartAdd);
      var extraParams = '&' + (0, _queryString.stringify)(extraProps || { domainstatus: 'available' });
      return baseUrl.concat("?applyBP=1&domain=", domain, '&plid=', privateLabelId, extraParams);
    }

    /**
     * Returns URL to add domains to cart
     *
     * @method getCartAddUrl
     * @param  {String} hostOverride override for host. Example: dev-godaddy.com
     * @param {Number} privateLabelId is the identifier of the reseller.
     * @returns {String} Returns URL for retrieving next step url
     */

  }, {
    key: "getNextStepUrl",
    value: function getNextStepUrl(hostOverride, privateLabelId) {
      var parameters = this.getBuildUrlParameters(hostOverride);
      var baseUrl = this.buildUrl(parameters.prefix, parameters.host, paths.cartNextStep);
      return baseUrl + "&plid=" + privateLabelId;
    }

    /**
     * Returns URL to remove domains from cart
     *
     * @method getCartRemoveUrl
     * @param  {String} hostOverride override for host. Example: dev-godaddy.com
     * @param {String} domain is the name to remove
     * @param {Number} privateLabelId is the identifier of the reseller
     * @returns {String} Returns URL
     */

  }, {
    key: "getCartRemoveUrl",
    value: function getCartRemoveUrl(hostOverride, domain, privateLabelId) {
      var parameters = this.getBuildUrlParameters(hostOverride);
      var baseUrl = this.buildUrl(parameters.prefix, parameters.host, paths.cartRemove);
      return baseUrl.concat("?applyBP=1&domain=", domain, '&plid=', privateLabelId);
    }

    /**
     * Returns URL for returning bundles for a domain
     *
     * @method getBundleUrl
     * @param  {String} hostOverride override for host. Example: dev-godaddy.com
     * @param {String} domain to get bundles for
     * @param {String} apiKey domain Find api key
     * @param {Number} privateLabelId is the identifier of the reseller
     * @returns {String} Returns URL
     */

  }, {
    key: "getBundleUrl",
    value: function getBundleUrl(hostOverride, domain, apiKey, privateLabelId) {
      var parameters = this.getBuildUrlParameters(hostOverride);
      var baseUrl = this.buildUrl(prefixes.find, parameters.host, paths.dynamicBundleGet);
      return baseUrl.concat("?q=", domain, "&plid=", privateLabelId, "&key=" + (apiKey || 'dpp_search'));
    }

    /**
     * Returns URL for returning bundles for a domain
     *
     * @method getCrossSellUrl
     * @param  {String} hostOverride override for host. Example: dev-godaddy.com
     * @param {String} domain to get cross sell domains for
     * @param {String} apiKey domain Find api key
     * @param {Number} privateLabelId is the identifier of the reseller
     * @returns {String} Returns URL
     */

  }, {
    key: "getCrossSellUrl",
    value: function getCrossSellUrl(hostOverride, domain, apiKey, privateLabelId) {
      var parameters = this.getBuildUrlParameters(hostOverride);
      var baseUrl = this.buildUrl(prefixes.find, parameters.host, paths.crossSellGet);
      return baseUrl.concat("?key=" + (apiKey || 'dpp_search') + "&pc=&ptl=&sld=", domain, "&plid=", privateLabelId);
    }

    /**
     * Returns URL for adding bundles to cart
     *
     * @method getBundleCartAddUrl
     * @param  {String} hostOverride override for host. Example: dev-godaddy.com
     * @param {String} domain who's bundles to add
     * @param {Number} privateLabelId is the identifier of the reseller
     * @param {Boolean} isDynamicBunle if it is dynamic bundle
     * @returns {String} Returns URL
     */

  }, {
    key: "getBundleCartAddUrl",
    value: function getBundleCartAddUrl(hostOverride, domain, privateLabelId, isDynamicBunle) {
      var parameters = this.getBuildUrlParameters(hostOverride);
      var bundlePath = isDynamicBunle ? paths.dynamicBundleAdd : paths.bundleAdd;
      var baseUrl = this.buildUrl(parameters.prefix, parameters.host, bundlePath);
      return baseUrl.concat("?applyBP=1&domainstatus=available&domain=", domain, '&plid=', privateLabelId);
    }

    /**
     * Returns URL for remove bundles from cart
     *
     * @method getBundleCartRemoveUrl
     * @param  {String} hostOverride override for host. Example: dev-godaddy.com
     * @param {String} domain who's bundles to remove
     * @param {Number} privateLabelId is the identifier of the reseller
     * @param {Boolean} isDynamicBunle if it is dynamic bundle
     * @returns {String} Returns URL
     */

  }, {
    key: "getBundleCartRemoveUrl",
    value: function getBundleCartRemoveUrl(hostOverride, domain, privateLabelId, isDynamicBunle) {
      var parameters = this.getBuildUrlParameters(hostOverride);
      var bundlePath = isDynamicBunle ? paths.dynamicBundleRemove : paths.bundleRemove;
      var baseUrl = this.buildUrl(parameters.prefix, parameters.host, bundlePath);
      return baseUrl.concat("?applyBP=1&domain=", domain, '&plid=', privateLabelId);
    }

    /**
     * Returns URL for getting spins
     *
     * @method getSpinUrl
     * @param  {String} hostOverride override for host. Example: dev-godaddy.com
     * @param {String} domain who's bundles to remove
     * @param {String} apiKey domain Find api key
     * @param {Number} privateLabelId is the identifier of the reseller
     * @param {Array} tlds who's spins to get
     * @param {Array} sources for the spins
     * @returns {String} Returns URL
     */
    /* eslint-disable max-params */

  }, {
    key: "getSpinUrl",
    value: function getSpinUrl(hostOverride, domain, apiKey, privateLabelId) {
      var tlds = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : [];
      var sources = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : [];

      var parameters = this.getBuildUrlParameters(hostOverride);
      var baseUrl = this.buildUrl(prefixes.find, parameters.host, paths.spinsUrl);
      return baseUrl.concat("?pagestart=0&pagesize=20&key=" + (apiKey || 'dpp_search') + "&q=" + domain + "&plid=" + privateLabelId + "&tlds=" + tlds.join(',') + "&source=" + sources.join(',') + "&maxsld=&pc=&ptl=", "");
    }

    /**
     * Returns URL grabbing users cart
     *
     * @method getAllCart
     * @param  {String} hostOverride override for host. Example: dev-godaddy.com
     * @param {String} domain who's cart to get
     * @param {String} apiKey domain Find api key
     * @param {Number} privateLabelId is the identifier of the reseller
     * @returns {String} Returns URL
     */

  }, {
    key: "getAllCart",
    value: function getAllCart(hostOverride, domain, apiKey, privateLabelId) {
      var parameters = this.getBuildUrlParameters(hostOverride);
      var baseUrl = this.buildUrl(parameters.prefix, parameters.host, _config2.default.apiURLPaths.cartGet);
      return baseUrl.concat("?plid=" + privateLabelId);
    }

    /**
     * Returns URL navigating to cart
     *
     * @method continueToCart
     * @param  {String} hostOverride override for host. Example: dev-godaddy.com
     * @param {Number} privateLabelId is the identifier of the reseller
     * @returns {String} Returns URL
     */

  }, {
    key: "continueToCart",
    value: function continueToCart(hostOverride, privateLabelId) {
      var parameters = this.getBuildUrlParameters(hostOverride);
      var baseUrl = this.buildUrl(parameters.prefix, parameters.host, paths.cartContinue);
      return baseUrl + "&plid=" + privateLabelId;
    }

    /**
     * @method continueToAuction
     * @param {String} hostOverride override for host. Example: dev-godaddy.com
     * @param {String} auctionID the auction id of the domain currently in auction
     * @returns {String} Returns Url of the next location/step of the auction process
     */

  }, {
    key: "continueToAuction",
    value: function continueToAuction(hostOverride, auctionID) {
      var baseUrl = this.buildUrl(prefixes.auction, hostOverride || this.getTestHost() || hosts.gd, paths.auctionContinue);
      return baseUrl.concat("?src=dpp&miid=", auctionID);
    }

    /**
     * Returns the fully qualified availcheck and 'add domain to VCart PostData' Url for correct environment
     *
     * @method getCheckAndGetVCartPostDataBaseUrl
     * @param  {String} hostOverride override for host. Example: dev-godaddy.com
     * @returns {String} Returns URL
     */

  }, {
    key: "getVirtualCartUrl",
    value: function getVirtualCartUrl(hostOverride) {
      return this.buildUrl(prefixes.cart, hostOverride || this.getTestHost() || hosts.gd, paths.virtualCart);
    }
  }]);

  return UrlHelper;
}();

exports.default = UrlHelper;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/dist/utilities/i18nHelper.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

var i18nHelper = function () {
  function i18nHelper() {
    _classCallCheck(this, i18nHelper);
  }

  _createClass(i18nHelper, null, [{
    key: 'parseString',

    /**
     * Replace markers in a i18n string with array of values
     *
     * @method parseString
     * @param {String} str contains markers that is to be parsed
     * @param {Number} start indicates the start number of marker
     * @param {Array} values contains all the values to replace the markers
     * @param {Boolen} toArray if true, split the string to array by marker {0}
     * @returns {String/Array} Returns parsed string or converted array
     */
    value: function parseString(str) {
      var start = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var values = arguments[2];
      var toArray = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;

      if (typeof str !== 'string') return null;

      var parsedStr = str;

      values.forEach(function (val, index) {
        parsedStr = parsedStr.replace('{' + (start + index) + '}', val);
      });

      if (toArray) {
        return parsedStr.split('{0}');
      }

      return parsedStr;
    }
  }]);

  return i18nHelper;
}();

exports.default = i18nHelper;

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/index.js":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/axios.js");

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/adapters/xhr.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/utils.js");
var settle = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/core/settle.js");
var buildURL = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/helpers/buildURL.js");
var parseHeaders = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/helpers/parseHeaders.js");
var isURLSameOrigin = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/helpers/isURLSameOrigin.js");
var createError = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/core/createError.js");
var btoa = (typeof window !== 'undefined' && window.btoa && window.btoa.bind(window)) || __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/helpers/btoa.js");

module.exports = function xhrAdapter(config) {
  return new Promise(function dispatchXhrRequest(resolve, reject) {
    var requestData = config.data;
    var requestHeaders = config.headers;

    if (utils.isFormData(requestData)) {
      delete requestHeaders['Content-Type']; // Let the browser set it
    }

    var request = new XMLHttpRequest();
    var loadEvent = 'onreadystatechange';
    var xDomain = false;

    // For IE 8/9 CORS support
    // Only supports POST and GET calls and doesn't returns the response headers.
    // DON'T do this for testing b/c XMLHttpRequest is mocked, not XDomainRequest.
    if (undefined !== 'test' &&
        typeof window !== 'undefined' &&
        window.XDomainRequest && !('withCredentials' in request) &&
        !isURLSameOrigin(config.url)) {
      request = new window.XDomainRequest();
      loadEvent = 'onload';
      xDomain = true;
      request.onprogress = function handleProgress() {};
      request.ontimeout = function handleTimeout() {};
    }

    // HTTP basic authentication
    if (config.auth) {
      var username = config.auth.username || '';
      var password = config.auth.password || '';
      requestHeaders.Authorization = 'Basic ' + btoa(username + ':' + password);
    }

    request.open(config.method.toUpperCase(), buildURL(config.url, config.params, config.paramsSerializer), true);

    // Set the request timeout in MS
    request.timeout = config.timeout;

    // Listen for ready state
    request[loadEvent] = function handleLoad() {
      if (!request || (request.readyState !== 4 && !xDomain)) {
        return;
      }

      // The request errored out and we didn't get a response, this will be
      // handled by onerror instead
      // With one exception: request that using file: protocol, most browsers
      // will return status as 0 even though it's a successful request
      if (request.status === 0 && !(request.responseURL && request.responseURL.indexOf('file:') === 0)) {
        return;
      }

      // Prepare the response
      var responseHeaders = 'getAllResponseHeaders' in request ? parseHeaders(request.getAllResponseHeaders()) : null;
      var responseData = !config.responseType || config.responseType === 'text' ? request.responseText : request.response;
      var response = {
        data: responseData,
        // IE sends 1223 instead of 204 (https://github.com/mzabriskie/axios/issues/201)
        status: request.status === 1223 ? 204 : request.status,
        statusText: request.status === 1223 ? 'No Content' : request.statusText,
        headers: responseHeaders,
        config: config,
        request: request
      };

      settle(resolve, reject, response);

      // Clean up request
      request = null;
    };

    // Handle low level network errors
    request.onerror = function handleError() {
      // Real errors are hidden from us by the browser
      // onerror should only fire if it's a network error
      reject(createError('Network Error', config));

      // Clean up request
      request = null;
    };

    // Handle timeout
    request.ontimeout = function handleTimeout() {
      reject(createError('timeout of ' + config.timeout + 'ms exceeded', config, 'ECONNABORTED'));

      // Clean up request
      request = null;
    };

    // Add xsrf header
    // This is only done if running in a standard browser environment.
    // Specifically not if we're in a web worker, or react-native.
    if (utils.isStandardBrowserEnv()) {
      var cookies = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/helpers/cookies.js");

      // Add xsrf header
      var xsrfValue = (config.withCredentials || isURLSameOrigin(config.url)) && config.xsrfCookieName ?
          cookies.read(config.xsrfCookieName) :
          undefined;

      if (xsrfValue) {
        requestHeaders[config.xsrfHeaderName] = xsrfValue;
      }
    }

    // Add headers to the request
    if ('setRequestHeader' in request) {
      utils.forEach(requestHeaders, function setRequestHeader(val, key) {
        if (typeof requestData === 'undefined' && key.toLowerCase() === 'content-type') {
          // Remove Content-Type if data is undefined
          delete requestHeaders[key];
        } else {
          // Otherwise add header to the request
          request.setRequestHeader(key, val);
        }
      });
    }

    // Add withCredentials to request if needed
    if (config.withCredentials) {
      request.withCredentials = true;
    }

    // Add responseType to request if needed
    if (config.responseType) {
      try {
        request.responseType = config.responseType;
      } catch (e) {
        if (request.responseType !== 'json') {
          throw e;
        }
      }
    }

    // Handle progress if needed
    if (typeof config.onDownloadProgress === 'function') {
      request.addEventListener('progress', config.onDownloadProgress);
    }

    // Not all browsers support upload events
    if (typeof config.onUploadProgress === 'function' && request.upload) {
      request.upload.addEventListener('progress', config.onUploadProgress);
    }

    if (config.cancelToken) {
      // Handle cancellation
      config.cancelToken.promise.then(function onCanceled(cancel) {
        if (!request) {
          return;
        }

        request.abort();
        reject(cancel);
        // Clean up request
        request = null;
      });
    }

    if (requestData === undefined) {
      requestData = null;
    }

    // Send the request
    request.send(requestData);
  });
};


/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/axios.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/utils.js");
var bind = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/helpers/bind.js");
var Axios = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/core/Axios.js");
var defaults = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/defaults.js");

/**
 * Create an instance of Axios
 *
 * @param {Object} defaultConfig The default config for the instance
 * @return {Axios} A new instance of Axios
 */
function createInstance(defaultConfig) {
  var context = new Axios(defaultConfig);
  var instance = bind(Axios.prototype.request, context);

  // Copy axios.prototype to instance
  utils.extend(instance, Axios.prototype, context);

  // Copy context to instance
  utils.extend(instance, context);

  return instance;
}

// Create the default instance to be exported
var axios = createInstance(defaults);

// Expose Axios class to allow class inheritance
axios.Axios = Axios;

// Factory for creating new instances
axios.create = function create(instanceConfig) {
  return createInstance(utils.merge(defaults, instanceConfig));
};

// Expose Cancel & CancelToken
axios.Cancel = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/cancel/Cancel.js");
axios.CancelToken = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/cancel/CancelToken.js");
axios.isCancel = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/cancel/isCancel.js");

// Expose all/spread
axios.all = function all(promises) {
  return Promise.all(promises);
};
axios.spread = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/helpers/spread.js");

module.exports = axios;

// Allow use of default import syntax in TypeScript
module.exports.default = axios;


/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/cancel/Cancel.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * A `Cancel` is an object that is thrown when an operation is canceled.
 *
 * @class
 * @param {string=} message The message.
 */
function Cancel(message) {
  this.message = message;
}

Cancel.prototype.toString = function toString() {
  return 'Cancel' + (this.message ? ': ' + this.message : '');
};

Cancel.prototype.__CANCEL__ = true;

module.exports = Cancel;


/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/cancel/CancelToken.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var Cancel = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/cancel/Cancel.js");

/**
 * A `CancelToken` is an object that can be used to request cancellation of an operation.
 *
 * @class
 * @param {Function} executor The executor function.
 */
function CancelToken(executor) {
  if (typeof executor !== 'function') {
    throw new TypeError('executor must be a function.');
  }

  var resolvePromise;
  this.promise = new Promise(function promiseExecutor(resolve) {
    resolvePromise = resolve;
  });

  var token = this;
  executor(function cancel(message) {
    if (token.reason) {
      // Cancellation has already been requested
      return;
    }

    token.reason = new Cancel(message);
    resolvePromise(token.reason);
  });
}

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
CancelToken.prototype.throwIfRequested = function throwIfRequested() {
  if (this.reason) {
    throw this.reason;
  }
};

/**
 * Returns an object that contains a new `CancelToken` and a function that, when called,
 * cancels the `CancelToken`.
 */
CancelToken.source = function source() {
  var cancel;
  var token = new CancelToken(function executor(c) {
    cancel = c;
  });
  return {
    token: token,
    cancel: cancel
  };
};

module.exports = CancelToken;


/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/cancel/isCancel.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function isCancel(value) {
  return !!(value && value.__CANCEL__);
};


/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/core/Axios.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var defaults = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/defaults.js");
var utils = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/utils.js");
var InterceptorManager = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/core/InterceptorManager.js");
var dispatchRequest = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/core/dispatchRequest.js");
var isAbsoluteURL = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/helpers/isAbsoluteURL.js");
var combineURLs = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/helpers/combineURLs.js");

/**
 * Create a new instance of Axios
 *
 * @param {Object} instanceConfig The default config for the instance
 */
function Axios(instanceConfig) {
  this.defaults = instanceConfig;
  this.interceptors = {
    request: new InterceptorManager(),
    response: new InterceptorManager()
  };
}

/**
 * Dispatch a request
 *
 * @param {Object} config The config specific for this request (merged with this.defaults)
 */
Axios.prototype.request = function request(config) {
  /*eslint no-param-reassign:0*/
  // Allow for axios('example/url'[, config]) a la fetch API
  if (typeof config === 'string') {
    config = utils.merge({
      url: arguments[0]
    }, arguments[1]);
  }

  config = utils.merge(defaults, this.defaults, { method: 'get' }, config);

  // Support baseURL config
  if (config.baseURL && !isAbsoluteURL(config.url)) {
    config.url = combineURLs(config.baseURL, config.url);
  }

  // Hook up interceptors middleware
  var chain = [dispatchRequest, undefined];
  var promise = Promise.resolve(config);

  this.interceptors.request.forEach(function unshiftRequestInterceptors(interceptor) {
    chain.unshift(interceptor.fulfilled, interceptor.rejected);
  });

  this.interceptors.response.forEach(function pushResponseInterceptors(interceptor) {
    chain.push(interceptor.fulfilled, interceptor.rejected);
  });

  while (chain.length) {
    promise = promise.then(chain.shift(), chain.shift());
  }

  return promise;
};

// Provide aliases for supported request methods
utils.forEach(['delete', 'get', 'head'], function forEachMethodNoData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url
    }));
  };
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, data, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url,
      data: data
    }));
  };
});

module.exports = Axios;


/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/core/InterceptorManager.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/utils.js");

function InterceptorManager() {
  this.handlers = [];
}

/**
 * Add a new interceptor to the stack
 *
 * @param {Function} fulfilled The function to handle `then` for a `Promise`
 * @param {Function} rejected The function to handle `reject` for a `Promise`
 *
 * @return {Number} An ID used to remove interceptor later
 */
InterceptorManager.prototype.use = function use(fulfilled, rejected) {
  this.handlers.push({
    fulfilled: fulfilled,
    rejected: rejected
  });
  return this.handlers.length - 1;
};

/**
 * Remove an interceptor from the stack
 *
 * @param {Number} id The ID that was returned by `use`
 */
InterceptorManager.prototype.eject = function eject(id) {
  if (this.handlers[id]) {
    this.handlers[id] = null;
  }
};

/**
 * Iterate over all the registered interceptors
 *
 * This method is particularly useful for skipping over any
 * interceptors that may have become `null` calling `eject`.
 *
 * @param {Function} fn The function to call for each interceptor
 */
InterceptorManager.prototype.forEach = function forEach(fn) {
  utils.forEach(this.handlers, function forEachHandler(h) {
    if (h !== null) {
      fn(h);
    }
  });
};

module.exports = InterceptorManager;


/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/core/createError.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var enhanceError = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/core/enhanceError.js");

/**
 * Create an Error with the specified message, config, error code, and response.
 *
 * @param {string} message The error message.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 @ @param {Object} [response] The response.
 * @returns {Error} The created error.
 */
module.exports = function createError(message, config, code, response) {
  var error = new Error(message);
  return enhanceError(error, config, code, response);
};


/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/core/dispatchRequest.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/utils.js");
var transformData = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/core/transformData.js");
var isCancel = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/cancel/isCancel.js");
var defaults = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/defaults.js");

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
function throwIfCancellationRequested(config) {
  if (config.cancelToken) {
    config.cancelToken.throwIfRequested();
  }
}

/**
 * Dispatch a request to the server using the configured adapter.
 *
 * @param {object} config The config that is to be used for the request
 * @returns {Promise} The Promise to be fulfilled
 */
module.exports = function dispatchRequest(config) {
  throwIfCancellationRequested(config);

  // Ensure headers exist
  config.headers = config.headers || {};

  // Transform request data
  config.data = transformData(
    config.data,
    config.headers,
    config.transformRequest
  );

  // Flatten headers
  config.headers = utils.merge(
    config.headers.common || {},
    config.headers[config.method] || {},
    config.headers || {}
  );

  utils.forEach(
    ['delete', 'get', 'head', 'post', 'put', 'patch', 'common'],
    function cleanHeaderConfig(method) {
      delete config.headers[method];
    }
  );

  var adapter = config.adapter || defaults.adapter;

  return adapter(config).then(function onAdapterResolution(response) {
    throwIfCancellationRequested(config);

    // Transform response data
    response.data = transformData(
      response.data,
      response.headers,
      config.transformResponse
    );

    return response;
  }, function onAdapterRejection(reason) {
    if (!isCancel(reason)) {
      throwIfCancellationRequested(config);

      // Transform response data
      if (reason && reason.response) {
        reason.response.data = transformData(
          reason.response.data,
          reason.response.headers,
          config.transformResponse
        );
      }
    }

    return Promise.reject(reason);
  });
};


/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/core/enhanceError.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Update an Error with the specified config, error code, and response.
 *
 * @param {Error} error The error to update.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 @ @param {Object} [response] The response.
 * @returns {Error} The error.
 */
module.exports = function enhanceError(error, config, code, response) {
  error.config = config;
  if (code) {
    error.code = code;
  }
  error.response = response;
  return error;
};


/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/core/settle.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var createError = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/core/createError.js");

/**
 * Resolve or reject a Promise based on response status.
 *
 * @param {Function} resolve A function that resolves the promise.
 * @param {Function} reject A function that rejects the promise.
 * @param {object} response The response.
 */
module.exports = function settle(resolve, reject, response) {
  var validateStatus = response.config.validateStatus;
  // Note: status is not exposed by XDomainRequest
  if (!response.status || !validateStatus || validateStatus(response.status)) {
    resolve(response);
  } else {
    reject(createError(
      'Request failed with status code ' + response.status,
      response.config,
      null,
      response
    ));
  }
};


/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/core/transformData.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/utils.js");

/**
 * Transform the data for a request or a response
 *
 * @param {Object|String} data The data to be transformed
 * @param {Array} headers The headers for the request or response
 * @param {Array|Function} fns A single function or Array of functions
 * @returns {*} The resulting transformed data
 */
module.exports = function transformData(data, headers, fns) {
  /*eslint no-param-reassign:0*/
  utils.forEach(fns, function transform(fn) {
    data = fn(data, headers);
  });

  return data;
};


/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/defaults.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(process) {

var utils = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/utils.js");
var normalizeHeaderName = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/helpers/normalizeHeaderName.js");

var PROTECTION_PREFIX = /^\)\]\}',?\n/;
var DEFAULT_CONTENT_TYPE = {
  'Content-Type': 'application/x-www-form-urlencoded'
};

function setContentTypeIfUnset(headers, value) {
  if (!utils.isUndefined(headers) && utils.isUndefined(headers['Content-Type'])) {
    headers['Content-Type'] = value;
  }
}

function getDefaultAdapter() {
  var adapter;
  if (typeof XMLHttpRequest !== 'undefined') {
    // For browsers use XHR adapter
    adapter = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/adapters/xhr.js");
  } else if (typeof process !== 'undefined') {
    // For node use HTTP adapter
    adapter = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/adapters/xhr.js");
  }
  return adapter;
}

var defaults = {
  adapter: getDefaultAdapter(),

  transformRequest: [function transformRequest(data, headers) {
    normalizeHeaderName(headers, 'Content-Type');
    if (utils.isFormData(data) ||
      utils.isArrayBuffer(data) ||
      utils.isStream(data) ||
      utils.isFile(data) ||
      utils.isBlob(data)
    ) {
      return data;
    }
    if (utils.isArrayBufferView(data)) {
      return data.buffer;
    }
    if (utils.isURLSearchParams(data)) {
      setContentTypeIfUnset(headers, 'application/x-www-form-urlencoded;charset=utf-8');
      return data.toString();
    }
    if (utils.isObject(data)) {
      setContentTypeIfUnset(headers, 'application/json;charset=utf-8');
      return JSON.stringify(data);
    }
    return data;
  }],

  transformResponse: [function transformResponse(data) {
    /*eslint no-param-reassign:0*/
    if (typeof data === 'string') {
      data = data.replace(PROTECTION_PREFIX, '');
      try {
        data = JSON.parse(data);
      } catch (e) { /* Ignore */ }
    }
    return data;
  }],

  timeout: 0,

  xsrfCookieName: 'XSRF-TOKEN',
  xsrfHeaderName: 'X-XSRF-TOKEN',

  maxContentLength: -1,

  validateStatus: function validateStatus(status) {
    return status >= 200 && status < 300;
  }
};

defaults.headers = {
  common: {
    'Accept': 'application/json, text/plain, */*'
  }
};

utils.forEach(['delete', 'get', 'head'], function forEachMehtodNoData(method) {
  defaults.headers[method] = {};
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  defaults.headers[method] = utils.merge(DEFAULT_CONTENT_TYPE);
});

module.exports = defaults;

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/process/browser.js")))

/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/helpers/bind.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function bind(fn, thisArg) {
  return function wrap() {
    var args = new Array(arguments.length);
    for (var i = 0; i < args.length; i++) {
      args[i] = arguments[i];
    }
    return fn.apply(thisArg, args);
  };
};


/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/helpers/btoa.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// btoa polyfill for IE<10 courtesy https://github.com/davidchambers/Base64.js

var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

function E() {
  this.message = 'String contains an invalid character';
}
E.prototype = new Error;
E.prototype.code = 5;
E.prototype.name = 'InvalidCharacterError';

function btoa(input) {
  var str = String(input);
  var output = '';
  for (
    // initialize result and counter
    var block, charCode, idx = 0, map = chars;
    // if the next str index does not exist:
    //   change the mapping table to "="
    //   check if d has no fractional digits
    str.charAt(idx | 0) || (map = '=', idx % 1);
    // "8 - idx % 1 * 8" generates the sequence 2, 4, 6, 8
    output += map.charAt(63 & block >> 8 - idx % 1 * 8)
  ) {
    charCode = str.charCodeAt(idx += 3 / 4);
    if (charCode > 0xFF) {
      throw new E();
    }
    block = block << 8 | charCode;
  }
  return output;
}

module.exports = btoa;


/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/helpers/buildURL.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/utils.js");

function encode(val) {
  return encodeURIComponent(val).
    replace(/%40/gi, '@').
    replace(/%3A/gi, ':').
    replace(/%24/g, '$').
    replace(/%2C/gi, ',').
    replace(/%20/g, '+').
    replace(/%5B/gi, '[').
    replace(/%5D/gi, ']');
}

/**
 * Build a URL by appending params to the end
 *
 * @param {string} url The base of the url (e.g., http://www.google.com)
 * @param {object} [params] The params to be appended
 * @returns {string} The formatted url
 */
module.exports = function buildURL(url, params, paramsSerializer) {
  /*eslint no-param-reassign:0*/
  if (!params) {
    return url;
  }

  var serializedParams;
  if (paramsSerializer) {
    serializedParams = paramsSerializer(params);
  } else if (utils.isURLSearchParams(params)) {
    serializedParams = params.toString();
  } else {
    var parts = [];

    utils.forEach(params, function serialize(val, key) {
      if (val === null || typeof val === 'undefined') {
        return;
      }

      if (utils.isArray(val)) {
        key = key + '[]';
      }

      if (!utils.isArray(val)) {
        val = [val];
      }

      utils.forEach(val, function parseValue(v) {
        if (utils.isDate(v)) {
          v = v.toISOString();
        } else if (utils.isObject(v)) {
          v = JSON.stringify(v);
        }
        parts.push(encode(key) + '=' + encode(v));
      });
    });

    serializedParams = parts.join('&');
  }

  if (serializedParams) {
    url += (url.indexOf('?') === -1 ? '?' : '&') + serializedParams;
  }

  return url;
};


/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/helpers/combineURLs.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Creates a new URL by combining the specified URLs
 *
 * @param {string} baseURL The base URL
 * @param {string} relativeURL The relative URL
 * @returns {string} The combined URL
 */
module.exports = function combineURLs(baseURL, relativeURL) {
  return baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '');
};


/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/helpers/cookies.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/utils.js");

module.exports = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs support document.cookie
  (function standardBrowserEnv() {
    return {
      write: function write(name, value, expires, path, domain, secure) {
        var cookie = [];
        cookie.push(name + '=' + encodeURIComponent(value));

        if (utils.isNumber(expires)) {
          cookie.push('expires=' + new Date(expires).toGMTString());
        }

        if (utils.isString(path)) {
          cookie.push('path=' + path);
        }

        if (utils.isString(domain)) {
          cookie.push('domain=' + domain);
        }

        if (secure === true) {
          cookie.push('secure');
        }

        document.cookie = cookie.join('; ');
      },

      read: function read(name) {
        var match = document.cookie.match(new RegExp('(^|;\\s*)(' + name + ')=([^;]*)'));
        return (match ? decodeURIComponent(match[3]) : null);
      },

      remove: function remove(name) {
        this.write(name, '', Date.now() - 86400000);
      }
    };
  })() :

  // Non standard browser env (web workers, react-native) lack needed support.
  (function nonStandardBrowserEnv() {
    return {
      write: function write() {},
      read: function read() { return null; },
      remove: function remove() {}
    };
  })()
);


/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/helpers/isAbsoluteURL.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Determines whether the specified URL is absolute
 *
 * @param {string} url The URL to test
 * @returns {boolean} True if the specified URL is absolute, otherwise false
 */
module.exports = function isAbsoluteURL(url) {
  // A URL is considered absolute if it begins with "<scheme>://" or "//" (protocol-relative URL).
  // RFC 3986 defines scheme name as a sequence of characters beginning with a letter and followed
  // by any combination of letters, digits, plus, period, or hyphen.
  return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url);
};


/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/helpers/isURLSameOrigin.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/utils.js");

module.exports = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs have full support of the APIs needed to test
  // whether the request URL is of the same origin as current location.
  (function standardBrowserEnv() {
    var msie = /(msie|trident)/i.test(navigator.userAgent);
    var urlParsingNode = document.createElement('a');
    var originURL;

    /**
    * Parse a URL to discover it's components
    *
    * @param {String} url The URL to be parsed
    * @returns {Object}
    */
    function resolveURL(url) {
      var href = url;

      if (msie) {
        // IE needs attribute set twice to normalize properties
        urlParsingNode.setAttribute('href', href);
        href = urlParsingNode.href;
      }

      urlParsingNode.setAttribute('href', href);

      // urlParsingNode provides the UrlUtils interface - http://url.spec.whatwg.org/#urlutils
      return {
        href: urlParsingNode.href,
        protocol: urlParsingNode.protocol ? urlParsingNode.protocol.replace(/:$/, '') : '',
        host: urlParsingNode.host,
        search: urlParsingNode.search ? urlParsingNode.search.replace(/^\?/, '') : '',
        hash: urlParsingNode.hash ? urlParsingNode.hash.replace(/^#/, '') : '',
        hostname: urlParsingNode.hostname,
        port: urlParsingNode.port,
        pathname: (urlParsingNode.pathname.charAt(0) === '/') ?
                  urlParsingNode.pathname :
                  '/' + urlParsingNode.pathname
      };
    }

    originURL = resolveURL(window.location.href);

    /**
    * Determine if a URL shares the same origin as the current location
    *
    * @param {String} requestURL The URL to test
    * @returns {boolean} True if URL shares the same origin, otherwise false
    */
    return function isURLSameOrigin(requestURL) {
      var parsed = (utils.isString(requestURL)) ? resolveURL(requestURL) : requestURL;
      return (parsed.protocol === originURL.protocol &&
            parsed.host === originURL.host);
    };
  })() :

  // Non standard browser envs (web workers, react-native) lack needed support.
  (function nonStandardBrowserEnv() {
    return function isURLSameOrigin() {
      return true;
    };
  })()
);


/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/helpers/normalizeHeaderName.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/utils.js");

module.exports = function normalizeHeaderName(headers, normalizedName) {
  utils.forEach(headers, function processHeader(value, name) {
    if (name !== normalizedName && name.toUpperCase() === normalizedName.toUpperCase()) {
      headers[normalizedName] = value;
      delete headers[name];
    }
  });
};


/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/helpers/parseHeaders.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/utils.js");

/**
 * Parse headers into an object
 *
 * ```
 * Date: Wed, 27 Aug 2014 08:58:49 GMT
 * Content-Type: application/json
 * Connection: keep-alive
 * Transfer-Encoding: chunked
 * ```
 *
 * @param {String} headers Headers needing to be parsed
 * @returns {Object} Headers parsed into an object
 */
module.exports = function parseHeaders(headers) {
  var parsed = {};
  var key;
  var val;
  var i;

  if (!headers) { return parsed; }

  utils.forEach(headers.split('\n'), function parser(line) {
    i = line.indexOf(':');
    key = utils.trim(line.substr(0, i)).toLowerCase();
    val = utils.trim(line.substr(i + 1));

    if (key) {
      parsed[key] = parsed[key] ? parsed[key] + ', ' + val : val;
    }
  });

  return parsed;
};


/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/helpers/spread.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Syntactic sugar for invoking a function and expanding an array for arguments.
 *
 * Common use case would be to use `Function.prototype.apply`.
 *
 *  ```js
 *  function f(x, y, z) {}
 *  var args = [1, 2, 3];
 *  f.apply(null, args);
 *  ```
 *
 * With `spread` this example can be re-written.
 *
 *  ```js
 *  spread(function(x, y, z) {})([1, 2, 3]);
 *  ```
 *
 * @param {Function} callback
 * @returns {Function}
 */
module.exports = function spread(callback) {
  return function wrap(arr) {
    return callback.apply(null, arr);
  };
};


/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/utils.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var bind = __webpack_require__("./node_modules/@reseller/domain-iq-ui/node_modules/axios/lib/helpers/bind.js");

/*global toString:true*/

// utils is a library of generic helper functions non-specific to axios

var toString = Object.prototype.toString;

/**
 * Determine if a value is an Array
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Array, otherwise false
 */
function isArray(val) {
  return toString.call(val) === '[object Array]';
}

/**
 * Determine if a value is an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an ArrayBuffer, otherwise false
 */
function isArrayBuffer(val) {
  return toString.call(val) === '[object ArrayBuffer]';
}

/**
 * Determine if a value is a FormData
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an FormData, otherwise false
 */
function isFormData(val) {
  return (typeof FormData !== 'undefined') && (val instanceof FormData);
}

/**
 * Determine if a value is a view on an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a view on an ArrayBuffer, otherwise false
 */
function isArrayBufferView(val) {
  var result;
  if ((typeof ArrayBuffer !== 'undefined') && (ArrayBuffer.isView)) {
    result = ArrayBuffer.isView(val);
  } else {
    result = (val) && (val.buffer) && (val.buffer instanceof ArrayBuffer);
  }
  return result;
}

/**
 * Determine if a value is a String
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a String, otherwise false
 */
function isString(val) {
  return typeof val === 'string';
}

/**
 * Determine if a value is a Number
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Number, otherwise false
 */
function isNumber(val) {
  return typeof val === 'number';
}

/**
 * Determine if a value is undefined
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if the value is undefined, otherwise false
 */
function isUndefined(val) {
  return typeof val === 'undefined';
}

/**
 * Determine if a value is an Object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Object, otherwise false
 */
function isObject(val) {
  return val !== null && typeof val === 'object';
}

/**
 * Determine if a value is a Date
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Date, otherwise false
 */
function isDate(val) {
  return toString.call(val) === '[object Date]';
}

/**
 * Determine if a value is a File
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a File, otherwise false
 */
function isFile(val) {
  return toString.call(val) === '[object File]';
}

/**
 * Determine if a value is a Blob
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Blob, otherwise false
 */
function isBlob(val) {
  return toString.call(val) === '[object Blob]';
}

/**
 * Determine if a value is a Function
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Function, otherwise false
 */
function isFunction(val) {
  return toString.call(val) === '[object Function]';
}

/**
 * Determine if a value is a Stream
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Stream, otherwise false
 */
function isStream(val) {
  return isObject(val) && isFunction(val.pipe);
}

/**
 * Determine if a value is a URLSearchParams object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a URLSearchParams object, otherwise false
 */
function isURLSearchParams(val) {
  return typeof URLSearchParams !== 'undefined' && val instanceof URLSearchParams;
}

/**
 * Trim excess whitespace off the beginning and end of a string
 *
 * @param {String} str The String to trim
 * @returns {String} The String freed of excess whitespace
 */
function trim(str) {
  return str.replace(/^\s*/, '').replace(/\s*$/, '');
}

/**
 * Determine if we're running in a standard browser environment
 *
 * This allows axios to run in a web worker, and react-native.
 * Both environments support XMLHttpRequest, but not fully standard globals.
 *
 * web workers:
 *  typeof window -> undefined
 *  typeof document -> undefined
 *
 * react-native:
 *  typeof document.createElement -> undefined
 */
function isStandardBrowserEnv() {
  return (
    typeof window !== 'undefined' &&
    typeof document !== 'undefined' &&
    typeof document.createElement === 'function'
  );
}

/**
 * Iterate over an Array or an Object invoking a function for each item.
 *
 * If `obj` is an Array callback will be called passing
 * the value, index, and complete array for each item.
 *
 * If 'obj' is an Object callback will be called passing
 * the value, key, and complete object for each property.
 *
 * @param {Object|Array} obj The object to iterate
 * @param {Function} fn The callback to invoke for each item
 */
function forEach(obj, fn) {
  // Don't bother if no value provided
  if (obj === null || typeof obj === 'undefined') {
    return;
  }

  // Force an array if not already something iterable
  if (typeof obj !== 'object' && !isArray(obj)) {
    /*eslint no-param-reassign:0*/
    obj = [obj];
  }

  if (isArray(obj)) {
    // Iterate over array values
    for (var i = 0, l = obj.length; i < l; i++) {
      fn.call(null, obj[i], i, obj);
    }
  } else {
    // Iterate over object keys
    for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        fn.call(null, obj[key], key, obj);
      }
    }
  }
}

/**
 * Accepts varargs expecting each argument to be an object, then
 * immutably merges the properties of each object and returns result.
 *
 * When multiple objects contain the same key the later object in
 * the arguments list will take precedence.
 *
 * Example:
 *
 * ```js
 * var result = merge({foo: 123}, {foo: 456});
 * console.log(result.foo); // outputs 456
 * ```
 *
 * @param {Object} obj1 Object to merge
 * @returns {Object} Result of all merge properties
 */
function merge(/* obj1, obj2, obj3, ... */) {
  var result = {};
  function assignValue(val, key) {
    if (typeof result[key] === 'object' && typeof val === 'object') {
      result[key] = merge(result[key], val);
    } else {
      result[key] = val;
    }
  }

  for (var i = 0, l = arguments.length; i < l; i++) {
    forEach(arguments[i], assignValue);
  }
  return result;
}

/**
 * Extends object a by mutably adding to it the properties of object b.
 *
 * @param {Object} a The object to be extended
 * @param {Object} b The object to copy properties from
 * @param {Object} thisArg The object to bind function to
 * @return {Object} The resulting value of object a
 */
function extend(a, b, thisArg) {
  forEach(b, function assignValue(val, key) {
    if (thisArg && typeof val === 'function') {
      a[key] = bind(val, thisArg);
    } else {
      a[key] = val;
    }
  });
  return a;
}

module.exports = {
  isArray: isArray,
  isArrayBuffer: isArrayBuffer,
  isFormData: isFormData,
  isArrayBufferView: isArrayBufferView,
  isString: isString,
  isNumber: isNumber,
  isObject: isObject,
  isUndefined: isUndefined,
  isDate: isDate,
  isFile: isFile,
  isBlob: isBlob,
  isFunction: isFunction,
  isStream: isStream,
  isURLSearchParams: isURLSearchParams,
  isStandardBrowserEnv: isStandardBrowserEnv,
  forEach: forEach,
  merge: merge,
  extend: extend,
  trim: trim
};


/***/ }),

/***/ "./node_modules/@reseller/domain-iq-ui/node_modules/redux/es/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

// EXTERNAL MODULE: ./node_modules/lodash-es/isPlainObject.js + 8 modules
var isPlainObject = __webpack_require__("./node_modules/lodash-es/isPlainObject.js");

// EXTERNAL MODULE: ./node_modules/symbol-observable/es/index.js
var es = __webpack_require__("./node_modules/symbol-observable/es/index.js");

// CONCATENATED MODULE: ./node_modules/@reseller/domain-iq-ui/node_modules/redux/es/createStore.js



/**
 * These are private action types reserved by Redux.
 * For any unknown actions, you must return the current state.
 * If the current state is undefined, you must return the initial state.
 * Do not reference these action types directly in your code.
 */
var ActionTypes = {
  INIT: '@@redux/INIT'

  /**
   * Creates a Redux store that holds the state tree.
   * The only way to change the data in the store is to call `dispatch()` on it.
   *
   * There should only be a single store in your app. To specify how different
   * parts of the state tree respond to actions, you may combine several reducers
   * into a single reducer function by using `combineReducers`.
   *
   * @param {Function} reducer A function that returns the next state tree, given
   * the current state tree and the action to handle.
   *
   * @param {any} [preloadedState] The initial state. You may optionally specify it
   * to hydrate the state from the server in universal apps, or to restore a
   * previously serialized user session.
   * If you use `combineReducers` to produce the root reducer function, this must be
   * an object with the same shape as `combineReducers` keys.
   *
   * @param {Function} [enhancer] The store enhancer. You may optionally specify it
   * to enhance the store with third-party capabilities such as middleware,
   * time travel, persistence, etc. The only store enhancer that ships with Redux
   * is `applyMiddleware()`.
   *
   * @returns {Store} A Redux store that lets you read the state, dispatch actions
   * and subscribe to changes.
   */
};function createStore_createStore(reducer, preloadedState, enhancer) {
  var _ref2;

  if (typeof preloadedState === 'function' && typeof enhancer === 'undefined') {
    enhancer = preloadedState;
    preloadedState = undefined;
  }

  if (typeof enhancer !== 'undefined') {
    if (typeof enhancer !== 'function') {
      throw new Error('Expected the enhancer to be a function.');
    }

    return enhancer(createStore_createStore)(reducer, preloadedState);
  }

  if (typeof reducer !== 'function') {
    throw new Error('Expected the reducer to be a function.');
  }

  var currentReducer = reducer;
  var currentState = preloadedState;
  var currentListeners = [];
  var nextListeners = currentListeners;
  var isDispatching = false;

  function ensureCanMutateNextListeners() {
    if (nextListeners === currentListeners) {
      nextListeners = currentListeners.slice();
    }
  }

  /**
   * Reads the state tree managed by the store.
   *
   * @returns {any} The current state tree of your application.
   */
  function getState() {
    return currentState;
  }

  /**
   * Adds a change listener. It will be called any time an action is dispatched,
   * and some part of the state tree may potentially have changed. You may then
   * call `getState()` to read the current state tree inside the callback.
   *
   * You may call `dispatch()` from a change listener, with the following
   * caveats:
   *
   * 1. The subscriptions are snapshotted just before every `dispatch()` call.
   * If you subscribe or unsubscribe while the listeners are being invoked, this
   * will not have any effect on the `dispatch()` that is currently in progress.
   * However, the next `dispatch()` call, whether nested or not, will use a more
   * recent snapshot of the subscription list.
   *
   * 2. The listener should not expect to see all state changes, as the state
   * might have been updated multiple times during a nested `dispatch()` before
   * the listener is called. It is, however, guaranteed that all subscribers
   * registered before the `dispatch()` started will be called with the latest
   * state by the time it exits.
   *
   * @param {Function} listener A callback to be invoked on every dispatch.
   * @returns {Function} A function to remove this change listener.
   */
  function subscribe(listener) {
    if (typeof listener !== 'function') {
      throw new Error('Expected listener to be a function.');
    }

    var isSubscribed = true;

    ensureCanMutateNextListeners();
    nextListeners.push(listener);

    return function unsubscribe() {
      if (!isSubscribed) {
        return;
      }

      isSubscribed = false;

      ensureCanMutateNextListeners();
      var index = nextListeners.indexOf(listener);
      nextListeners.splice(index, 1);
    };
  }

  /**
   * Dispatches an action. It is the only way to trigger a state change.
   *
   * The `reducer` function, used to create the store, will be called with the
   * current state tree and the given `action`. Its return value will
   * be considered the **next** state of the tree, and the change listeners
   * will be notified.
   *
   * The base implementation only supports plain object actions. If you want to
   * dispatch a Promise, an Observable, a thunk, or something else, you need to
   * wrap your store creating function into the corresponding middleware. For
   * example, see the documentation for the `redux-thunk` package. Even the
   * middleware will eventually dispatch plain object actions using this method.
   *
   * @param {Object} action A plain object representing “what changed”. It is
   * a good idea to keep actions serializable so you can record and replay user
   * sessions, or use the time travelling `redux-devtools`. An action must have
   * a `type` property which may not be `undefined`. It is a good idea to use
   * string constants for action types.
   *
   * @returns {Object} For convenience, the same action object you dispatched.
   *
   * Note that, if you use a custom middleware, it may wrap `dispatch()` to
   * return something else (for example, a Promise you can await).
   */
  function dispatch(action) {
    if (!Object(isPlainObject["a" /* default */])(action)) {
      throw new Error('Actions must be plain objects. ' + 'Use custom middleware for async actions.');
    }

    if (typeof action.type === 'undefined') {
      throw new Error('Actions may not have an undefined "type" property. ' + 'Have you misspelled a constant?');
    }

    if (isDispatching) {
      throw new Error('Reducers may not dispatch actions.');
    }

    try {
      isDispatching = true;
      currentState = currentReducer(currentState, action);
    } finally {
      isDispatching = false;
    }

    var listeners = currentListeners = nextListeners;
    for (var i = 0; i < listeners.length; i++) {
      var listener = listeners[i];
      listener();
    }

    return action;
  }

  /**
   * Replaces the reducer currently used by the store to calculate the state.
   *
   * You might need this if your app implements code splitting and you want to
   * load some of the reducers dynamically. You might also need this if you
   * implement a hot reloading mechanism for Redux.
   *
   * @param {Function} nextReducer The reducer for the store to use instead.
   * @returns {void}
   */
  function replaceReducer(nextReducer) {
    if (typeof nextReducer !== 'function') {
      throw new Error('Expected the nextReducer to be a function.');
    }

    currentReducer = nextReducer;
    dispatch({ type: ActionTypes.INIT });
  }

  /**
   * Interoperability point for observable/reactive libraries.
   * @returns {observable} A minimal observable of state changes.
   * For more information, see the observable proposal:
   * https://github.com/tc39/proposal-observable
   */
  function observable() {
    var _ref;

    var outerSubscribe = subscribe;
    return _ref = {
      /**
       * The minimal observable subscription method.
       * @param {Object} observer Any object that can be used as an observer.
       * The observer object should have a `next` method.
       * @returns {subscription} An object with an `unsubscribe` method that can
       * be used to unsubscribe the observable from the store, and prevent further
       * emission of values from the observable.
       */
      subscribe: function subscribe(observer) {
        if (typeof observer !== 'object') {
          throw new TypeError('Expected the observer to be an object.');
        }

        function observeState() {
          if (observer.next) {
            observer.next(getState());
          }
        }

        observeState();
        var unsubscribe = outerSubscribe(observeState);
        return { unsubscribe: unsubscribe };
      }
    }, _ref[es["a" /* default */]] = function () {
      return this;
    }, _ref;
  }

  // When a store is created, an "INIT" action is dispatched so that every
  // reducer returns their initial state. This effectively populates
  // the initial state tree.
  dispatch({ type: ActionTypes.INIT });

  return _ref2 = {
    dispatch: dispatch,
    subscribe: subscribe,
    getState: getState,
    replaceReducer: replaceReducer
  }, _ref2[es["a" /* default */]] = observable, _ref2;
}
// CONCATENATED MODULE: ./node_modules/@reseller/domain-iq-ui/node_modules/redux/es/utils/warning.js
/**
 * Prints a warning in the console if it exists.
 *
 * @param {String} message The warning message.
 * @returns {void}
 */
function warning(message) {
  /* eslint-disable no-console */
  if (typeof console !== 'undefined' && typeof console.error === 'function') {
    console.error(message);
  }
  /* eslint-enable no-console */
  try {
    // This error was thrown as a convenience so that if you enable
    // "break on all exceptions" in your console,
    // it would pause the execution at this line.
    throw new Error(message);
    /* eslint-disable no-empty */
  } catch (e) {}
  /* eslint-enable no-empty */
}
// CONCATENATED MODULE: ./node_modules/@reseller/domain-iq-ui/node_modules/redux/es/combineReducers.js




function getUndefinedStateErrorMessage(key, action) {
  var actionType = action && action.type;
  var actionName = actionType && '"' + actionType.toString() + '"' || 'an action';

  return 'Given action ' + actionName + ', reducer "' + key + '" returned undefined. ' + 'To ignore an action, you must explicitly return the previous state. ' + 'If you want this reducer to hold no value, you can return null instead of undefined.';
}

function getUnexpectedStateShapeWarningMessage(inputState, reducers, action, unexpectedKeyCache) {
  var reducerKeys = Object.keys(reducers);
  var argumentName = action && action.type === ActionTypes.INIT ? 'preloadedState argument passed to createStore' : 'previous state received by the reducer';

  if (reducerKeys.length === 0) {
    return 'Store does not have a valid reducer. Make sure the argument passed ' + 'to combineReducers is an object whose values are reducers.';
  }

  if (!Object(isPlainObject["a" /* default */])(inputState)) {
    return 'The ' + argumentName + ' has unexpected type of "' + {}.toString.call(inputState).match(/\s([a-z|A-Z]+)/)[1] + '". Expected argument to be an object with the following ' + ('keys: "' + reducerKeys.join('", "') + '"');
  }

  var unexpectedKeys = Object.keys(inputState).filter(function (key) {
    return !reducers.hasOwnProperty(key) && !unexpectedKeyCache[key];
  });

  unexpectedKeys.forEach(function (key) {
    unexpectedKeyCache[key] = true;
  });

  if (unexpectedKeys.length > 0) {
    return 'Unexpected ' + (unexpectedKeys.length > 1 ? 'keys' : 'key') + ' ' + ('"' + unexpectedKeys.join('", "') + '" found in ' + argumentName + '. ') + 'Expected to find one of the known reducer keys instead: ' + ('"' + reducerKeys.join('", "') + '". Unexpected keys will be ignored.');
  }
}

function assertReducerShape(reducers) {
  Object.keys(reducers).forEach(function (key) {
    var reducer = reducers[key];
    var initialState = reducer(undefined, { type: ActionTypes.INIT });

    if (typeof initialState === 'undefined') {
      throw new Error('Reducer "' + key + '" returned undefined during initialization. ' + 'If the state passed to the reducer is undefined, you must ' + 'explicitly return the initial state. The initial state may ' + 'not be undefined. If you don\'t want to set a value for this reducer, ' + 'you can use null instead of undefined.');
    }

    var type = '@@redux/PROBE_UNKNOWN_ACTION_' + Math.random().toString(36).substring(7).split('').join('.');
    if (typeof reducer(undefined, { type: type }) === 'undefined') {
      throw new Error('Reducer "' + key + '" returned undefined when probed with a random type. ' + ('Don\'t try to handle ' + ActionTypes.INIT + ' or other actions in "redux/*" ') + 'namespace. They are considered private. Instead, you must return the ' + 'current state for any unknown actions, unless it is undefined, ' + 'in which case you must return the initial state, regardless of the ' + 'action type. The initial state may not be undefined, but can be null.');
    }
  });
}

/**
 * Turns an object whose values are different reducer functions, into a single
 * reducer function. It will call every child reducer, and gather their results
 * into a single state object, whose keys correspond to the keys of the passed
 * reducer functions.
 *
 * @param {Object} reducers An object whose values correspond to different
 * reducer functions that need to be combined into one. One handy way to obtain
 * it is to use ES6 `import * as reducers` syntax. The reducers may never return
 * undefined for any action. Instead, they should return their initial state
 * if the state passed to them was undefined, and the current state for any
 * unrecognized action.
 *
 * @returns {Function} A reducer function that invokes every reducer inside the
 * passed object, and builds a state object with the same shape.
 */
function combineReducers(reducers) {
  var reducerKeys = Object.keys(reducers);
  var finalReducers = {};
  for (var i = 0; i < reducerKeys.length; i++) {
    var key = reducerKeys[i];

    if (undefined !== 'production') {
      if (typeof reducers[key] === 'undefined') {
        warning('No reducer provided for key "' + key + '"');
      }
    }

    if (typeof reducers[key] === 'function') {
      finalReducers[key] = reducers[key];
    }
  }
  var finalReducerKeys = Object.keys(finalReducers);

  var unexpectedKeyCache = void 0;
  if (undefined !== 'production') {
    unexpectedKeyCache = {};
  }

  var shapeAssertionError = void 0;
  try {
    assertReducerShape(finalReducers);
  } catch (e) {
    shapeAssertionError = e;
  }

  return function combination() {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var action = arguments[1];

    if (shapeAssertionError) {
      throw shapeAssertionError;
    }

    if (undefined !== 'production') {
      var warningMessage = getUnexpectedStateShapeWarningMessage(state, finalReducers, action, unexpectedKeyCache);
      if (warningMessage) {
        warning(warningMessage);
      }
    }

    var hasChanged = false;
    var nextState = {};
    for (var _i = 0; _i < finalReducerKeys.length; _i++) {
      var _key = finalReducerKeys[_i];
      var reducer = finalReducers[_key];
      var previousStateForKey = state[_key];
      var nextStateForKey = reducer(previousStateForKey, action);
      if (typeof nextStateForKey === 'undefined') {
        var errorMessage = getUndefinedStateErrorMessage(_key, action);
        throw new Error(errorMessage);
      }
      nextState[_key] = nextStateForKey;
      hasChanged = hasChanged || nextStateForKey !== previousStateForKey;
    }
    return hasChanged ? nextState : state;
  };
}
// CONCATENATED MODULE: ./node_modules/@reseller/domain-iq-ui/node_modules/redux/es/bindActionCreators.js
function bindActionCreator(actionCreator, dispatch) {
  return function () {
    return dispatch(actionCreator.apply(undefined, arguments));
  };
}

/**
 * Turns an object whose values are action creators, into an object with the
 * same keys, but with every function wrapped into a `dispatch` call so they
 * may be invoked directly. This is just a convenience method, as you can call
 * `store.dispatch(MyActionCreators.doSomething())` yourself just fine.
 *
 * For convenience, you can also pass a single function as the first argument,
 * and get a function in return.
 *
 * @param {Function|Object} actionCreators An object whose values are action
 * creator functions. One handy way to obtain it is to use ES6 `import * as`
 * syntax. You may also pass a single function.
 *
 * @param {Function} dispatch The `dispatch` function available on your Redux
 * store.
 *
 * @returns {Function|Object} The object mimicking the original object, but with
 * every action creator wrapped into the `dispatch` call. If you passed a
 * function as `actionCreators`, the return value will also be a single
 * function.
 */
function bindActionCreators(actionCreators, dispatch) {
  if (typeof actionCreators === 'function') {
    return bindActionCreator(actionCreators, dispatch);
  }

  if (typeof actionCreators !== 'object' || actionCreators === null) {
    throw new Error('bindActionCreators expected an object or a function, instead received ' + (actionCreators === null ? 'null' : typeof actionCreators) + '. ' + 'Did you write "import ActionCreators from" instead of "import * as ActionCreators from"?');
  }

  var keys = Object.keys(actionCreators);
  var boundActionCreators = {};
  for (var i = 0; i < keys.length; i++) {
    var key = keys[i];
    var actionCreator = actionCreators[key];
    if (typeof actionCreator === 'function') {
      boundActionCreators[key] = bindActionCreator(actionCreator, dispatch);
    }
  }
  return boundActionCreators;
}
// CONCATENATED MODULE: ./node_modules/@reseller/domain-iq-ui/node_modules/redux/es/compose.js
/**
 * Composes single-argument functions from right to left. The rightmost
 * function can take multiple arguments as it provides the signature for
 * the resulting composite function.
 *
 * @param {...Function} funcs The functions to compose.
 * @returns {Function} A function obtained by composing the argument functions
 * from right to left. For example, compose(f, g, h) is identical to doing
 * (...args) => f(g(h(...args))).
 */

function compose() {
  for (var _len = arguments.length, funcs = Array(_len), _key = 0; _key < _len; _key++) {
    funcs[_key] = arguments[_key];
  }

  if (funcs.length === 0) {
    return function (arg) {
      return arg;
    };
  }

  if (funcs.length === 1) {
    return funcs[0];
  }

  return funcs.reduce(function (a, b) {
    return function () {
      return a(b.apply(undefined, arguments));
    };
  });
}
// CONCATENATED MODULE: ./node_modules/@reseller/domain-iq-ui/node_modules/redux/es/applyMiddleware.js
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };



/**
 * Creates a store enhancer that applies middleware to the dispatch method
 * of the Redux store. This is handy for a variety of tasks, such as expressing
 * asynchronous actions in a concise manner, or logging every action payload.
 *
 * See `redux-thunk` package as an example of the Redux middleware.
 *
 * Because middleware is potentially asynchronous, this should be the first
 * store enhancer in the composition chain.
 *
 * Note that each middleware will be given the `dispatch` and `getState` functions
 * as named arguments.
 *
 * @param {...Function} middlewares The middleware chain to be applied.
 * @returns {Function} A store enhancer applying the middleware.
 */
function applyMiddleware() {
  for (var _len = arguments.length, middlewares = Array(_len), _key = 0; _key < _len; _key++) {
    middlewares[_key] = arguments[_key];
  }

  return function (createStore) {
    return function (reducer, preloadedState, enhancer) {
      var store = createStore(reducer, preloadedState, enhancer);
      var _dispatch = store.dispatch;
      var chain = [];

      var middlewareAPI = {
        getState: store.getState,
        dispatch: function dispatch(action) {
          return _dispatch(action);
        }
      };
      chain = middlewares.map(function (middleware) {
        return middleware(middlewareAPI);
      });
      _dispatch = compose.apply(undefined, chain)(store.dispatch);

      return _extends({}, store, {
        dispatch: _dispatch
      });
    };
  };
}
// CONCATENATED MODULE: ./node_modules/@reseller/domain-iq-ui/node_modules/redux/es/index.js
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "createStore", function() { return createStore_createStore; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "combineReducers", function() { return combineReducers; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "bindActionCreators", function() { return bindActionCreators; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "applyMiddleware", function() { return applyMiddleware; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "compose", function() { return compose; });







/*
* This is a dummy function to check if the function name has been altered by minification.
* If the function has been minified and NODE_ENV !== 'production', warn the user.
*/
function isCrushed() {}

if (undefined !== 'production' && typeof isCrushed.name === 'string' && isCrushed.name !== 'isCrushed') {
  warning('You are currently using minified code outside of NODE_ENV === \'production\'. ' + 'This means that you are running a slower development build of Redux. ' + 'You can use loose-envify (https://github.com/zertosh/loose-envify) for browserify ' + 'or DefinePlugin for webpack (http://stackoverflow.com/questions/30030031) ' + 'to ensure you have the correct code for your production build.');
}



/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/addon/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

__webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/addon/index.scss");

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _button = __webpack_require__("@ux/button");

var _button2 = _interopRequireDefault(_button);

var _tooltip = __webpack_require__("@ux/tooltip");

var _tooltip2 = _interopRequireDefault(_tooltip);

var _vcartPost = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/vcart-post/index.js");

var _vcartPost2 = _interopRequireDefault(_vcartPost);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
  }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var Addon = function (_Component) {
  _inherits(Addon, _Component);

  function Addon(props) {
    _classCallCheck(this, Addon);

    /* istanbul ignore next */
    var _this = _possibleConstructorReturn(this, (Addon.__proto__ || Object.getPrototypeOf(Addon)).call(this, props));

    _this.state = {
      payload: {},
      isReadyForCart: false
    };

    _this.addPrivacy = _this.addPrivacy.bind(_this);
    _this.declinePrivacy = _this.declinePrivacy.bind(_this);
    return _this;
  }

  _createClass(Addon, [{
    key: 'addPrivacy',
    value: function addPrivacy() {
      var _props = this.props,
          addonData = _props.addonData,
          cartPostData = _props.cartPostData,
          selectedDomains = _props.selectedDomains,
          vcartNavigation = _props.vcartNavigation,
          addonSuccessCallback = _props.addonSuccessCallback;

      var cartPayload = JSON.parse(JSON.stringify(cartPostData));

      var postData = {
        cart: cartPayload.cart,
        navigation: vcartNavigation
      };

      var pfid = void 0,
          domain = void 0,
          privacyData = void 0;
      var len = selectedDomains.length;
      for (var i = 0; i < len; i++) {
        pfid = addonData.PfId;
        domain = selectedDomains[i];
        privacyData = {
          pfid: pfid, metadata: { domains: [{ domain: domain }] }
        };
        postData.cart.items.push(privacyData);
      }

      this.setState({
        payload: postData,
        isReadyForCart: true
      });

      addonSuccessCallback && addonSuccessCallback(postData);
    }
  }, {
    key: 'declinePrivacy',
    value: function declinePrivacy() {
      var _props2 = this.props,
          cartPostData = _props2.cartPostData,
          vcartNavigation = _props2.vcartNavigation,
          addonSuccessCallback = _props2.addonSuccessCallback;

      var cartPayload = JSON.parse(JSON.stringify(cartPostData));

      var postData = {
        cart: cartPayload.cart,
        navigation: vcartNavigation
      };

      this.setState({
        payload: postData,
        isReadyForCart: true
      });

      addonSuccessCallback && addonSuccessCallback(postData);
    }
  }, {
    key: 'render',
    value: function render() {
      var _props3 = this.props,
          addonData = _props3.addonData,
          eid = _props3.eid,
          language = _props3.language,
          redirectToVCart = _props3.redirectToVCart;
      var _state = this.state,
          isReadyForCart = _state.isReadyForCart,
          payload = _state.payload;

      if (!addonData) return null;

      var privacyExample = _react2['default'].createElement('div', null, _react2['default'].createElement('div', { className: 'text-primary' }, _react2['default'].createElement('strong', null, language.infoWithoutPrivacy)), _react2['default'].createElement('address', null, 'Jane Smith', _react2['default'].createElement('br', null), 'jane@BusinessExample.com', _react2['default'].createElement('br', null), '1234 Elm Street', _react2['default'].createElement('br', null), 'Hometown, AZ 85000', _react2['default'].createElement('br', null), '(480) 555-5555', _react2['default'].createElement('br', null)), _react2['default'].createElement('div', null, _react2['default'].createElement('strong', null, language.infoWithPrivacy)), _react2['default'].createElement('address', null, 'DomainsByProxy.com', _react2['default'].createElement('br', null), 'ProxiedDomain@DomainsByProxy.com', _react2['default'].createElement('br', null), '14747 N Northsight Blvd', _react2['default'].createElement('br', null), 'Suite 111, PMB 309', _react2['default'].createElement('br', null), 'Scottsdale, AZ 85260', _react2['default'].createElement('br', null), '+1.480.642.2599', _react2['default'].createElement('br', null)));

      var privacyDetails = _react2['default'].createElement('ul', null, _react2['default'].createElement('li', null, language.privacyTipHides), _react2['default'].createElement('li', null, language.privacyTipPrevents), _react2['default'].createElement('li', null, language.privacyTipHijack), _react2['default'].createElement('li', null, language.privacyTipProtect));

      var privacyPricingBlock = _react2['default'].createElement('div', { className: 'privacy-pricing' }, _react2['default'].createElement('div', { className: 'text-secondary font-primary-bold' }, _react2['default'].createElement('span', { className: 'display-price' }, addonData.CurrentPriceDisplay), language.domainPerYear));

      if (addonData.CurrentPrice !== addonData.ListPrice) {
        privacyPricingBlock = _react2['default'].createElement('div', { className: 'privacy-pricing' }, _react2['default'].createElement('div', { className: 'text-secondary font-primary-bold' }, _react2['default'].createElement('span', { className: 'display-price' }, addonData.CurrentPriceDisplay), language.domainPerYear), _react2['default'].createElement('span', { className: 'list-price' }, addonData.ListPriceDisplay));
      }

      return _react2['default'].createElement('div', { className: 'privacy-card' }, _react2['default'].createElement('h2', { className: 'font-primary-bold' }, language.addPrivacy), _react2['default'].createElement('div', null, language.whyPrivacy, _react2['default'].createElement(_tooltip2['default'], { id: 'addonExampleTip', message: privacyExample }, _react2['default'].createElement('span', { className: 'text-primary' }, language.seeExample, ' ', _react2['default'].createElement('span', { className: 'uxicon uxicon-help' })))), _react2['default'].createElement('div', { className: 'recommend-privacy' }, language.recommendPrivacy), _react2['default'].createElement('div', { className: 'price-wrapper' }, _react2['default'].createElement('div', { className: 'privacy-details' }, _react2['default'].createElement('h3', { className: 'font-primary-bold' }, language.privacyProtection), _react2['default'].createElement(_tooltip2['default'], { id: 'addonDetailsTip', message: privacyDetails }, _react2['default'].createElement('span', { className: 'view-details text-primary' }, language.viewDetails, _react2['default'].createElement('span', { className: 'uxicon uxicon-help' })))), privacyPricingBlock), _react2['default'].createElement('div', { className: 'button-wrapper' }, _react2['default'].createElement(_button2['default'], { id: 'addCrossSellAction', key: 'addCrossSellAction', 'data-eid': eid + '.add.click', design: 'purchase', onClick: this.addPrivacy }, language.addPrivacyBtn), _react2['default'].createElement(_button2['default'], { id: 'noThanksCrossSell', key: 'noThanksCrossSell', 'data-eid': eid + '.noThanks.click', design: 'defaultPurchase', onClick: this.declinePrivacy }, language.noThanks), isReadyForCart && redirectToVCart && _react2['default'].createElement(_vcartPost2['default'], _extends({}, this.props, { payload: payload, shouldSubmitForm: true }))));
    }
  }]);

  return Addon;
}(_react.Component);

exports['default'] = Addon;

Addon.propTypes = {
  addonData: _propTypes2['default'].object,
  cartPostData: _propTypes2['default'].object,
  domain: _propTypes2['default'].object,
  eid: _propTypes2['default'].string.isRequired,
  isc: _propTypes2['default'].string,
  language: _propTypes2['default'].object.isRequired,
  redirectToVCart: _propTypes2['default'].bool,
  selectedDomains: _propTypes2['default'].array,
  urlOverride: _propTypes2['default'].string,
  vcartNavigation: _propTypes2['default'].object,
  addonSuccessCallback: _propTypes2['default'].func
};

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/addon/index.scss":
/***/ (function(module, exports) {

// empty (null-loader)

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/assets/icn-godaddy-valuation.svg":
/***/ (function(module, exports) {

module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 24 24\"><circle cx=\"12\" cy=\"12\" r=\"11\" fill=\"#fdbb2c\"/><path d=\"M23.95 12.97C23.47 18.59 18.75 23 13 23 6.92 23 2 18.08 2 12S6.92 1 13 1c5.75 0 10.47 4.41 10.95 10.03-.48-4.77-4.52-8.5-9.42-8.5-5.24 0-9.48 4.24-9.48 9.47s4.24 9.47 9.48 9.47c4.9 0 8.94-3.73 9.42-8.5z\" fill=\"#dc9d36\"/><path d=\"M12 24C5.383 24 0 18.617 0 12S5.383 0 12 0s12 5.383 12 12-5.383 12-12 12zm0-22C6.486 2 2 6.486 2 12s4.486 10 10 10 10-4.486 10-10S17.514 2 12 2z\" fill=\"#bc7d20\"/><path d=\"M14.974 16.134v-.961c-.332.469-.764.822-1.295 1.061s-1.154.357-1.869.357c-1.258 0-2.31-.437-3.155-1.31s-1.269-1.966-1.269-3.278c0-1.332.445-2.431 1.336-3.296s2.012-1.298 3.363-1.298c1.004 0 1.896.241 2.675.724s1.325 1.134 1.638 1.954c.039.086.043.158.012.217s-.098.1-.199.123l-1.26.299c-.078.023-.147.021-.208-.009s-.114-.089-.161-.179c-.242-.465-.577-.822-1.005-1.072s-.921-.375-1.479-.375c-.812 0-1.483.276-2.013.829s-.795 1.255-.795 2.106c0 .836.266 1.526.797 2.071s1.211.817 2.039.817c.445 0 .852-.098 1.219-.293s.641-.451.82-.768h-1.096c-.082 0-.15-.027-.205-.082s-.082-.123-.082-.205v-.936c0-.082.027-.15.082-.205s.123-.082.205-.082h3.252c.086 0 .156.026.211.079s.082.118.082.196v3.516c0 .082-.028.151-.085.208s-.126.085-.208.085h-1.055c-.082 0-.151-.028-.208-.085s-.084-.126-.084-.208zM10.35 21.42a.56.56 0 0 1-.46-.65.57.57 0 0 1 .65-.46.57.57 0 0 1 .46.65c-.05.28-.28.47-.55.47-.04 0-.07-.01-.1-.01zm2.67-.46a.551.551 0 0 1 .45-.65c.311-.06.6.15.65.46.06.3-.15.59-.45.65-.04 0-.07.01-.101.01a.56.56 0 0 1-.549-.47zm-5.79-.67a.571.571 0 0 1-.21-.771c.16-.27.5-.359.77-.21.27.16.36.5.21.771-.11.18-.3.279-.49.279-.1 0-.19-.019-.28-.069zm8.79-.221a.548.548 0 0 1 .2-.76.558.558 0 0 1 .771.2.561.561 0 0 1-.2.771.601.601 0 0 1-.29.079.539.539 0 0 1-.481-.29zM4.68 18.16a.57.57 0 0 1 .07-.801c.24-.199.59-.17.79.07.2.24.17.59-.06.8a.64.64 0 0 1-.37.13.564.564 0 0 1-.43-.199zm13.86.05a.54.54 0 0 1-.07-.79c.2-.24.55-.271.79-.07.23.2.271.561.07.79a.533.533 0 0 1-.431.2.532.532 0 0 1-.359-.13zM3.02 15.29c-.11-.29.04-.62.33-.721.29-.109.62.04.72.33a.56.56 0 0 1-.33.721.448.448 0 0 1-.19.04c-.23 0-.45-.14-.53-.37zm17.25.319a.562.562 0 0 1-.34-.729c.11-.29.43-.44.72-.33.29.1.44.43.341.72a.57.57 0 0 1-.53.37.597.597 0 0 1-.191-.031zM2.44 12.02c0-.31.25-.56.56-.56.31-.01.56.25.56.56a.56.56 0 1 1-1.12 0zM20.439 12v-.03c0-.32.25-.57.551-.57.319 0 .569.25.569.56V12a.56.56 0 1 1-1.12 0zm-16.879.02zm-.22-2.55A.573.573 0 0 1 3 8.75c.11-.29.43-.44.72-.34.3.11.45.43.34.72-.08.23-.3.37-.53.37-.06 0-.13-.01-.19-.03zm16.58-.39c-.11-.29.04-.61.33-.72.29-.11.62.04.72.33h.01c.101.3-.05.62-.34.73-.06.02-.13.03-.189.03a.56.56 0 0 1-.531-.37zM4.73 6.66a.557.557 0 0 1-.07-.79c.2-.24.55-.27.79-.07s.27.55.07.79c-.11.14-.27.2-.43.2a.55.55 0 0 1-.36-.13zm13.709-.11a.56.56 0 0 1 .07-.79c.24-.2.59-.17.79.07.2.23.17.59-.06.79a.572.572 0 0 1-.369.13c-.16 0-.31-.06-.431-.2zM6.99 4.5a.57.57 0 0 1 .21-.77.56.56 0 0 1 .77.2c.15.27.06.62-.21.77-.09.05-.19.08-.28.08-.2 0-.38-.1-.49-.28zm9.199.18h.011a.56.56 0 0 1-.21-.77.556.556 0 0 1 .76-.21h.01c.26.16.36.5.2.77a.555.555 0 0 1-.49.28c-.09 0-.19-.02-.281-.07zM9.86 3.24c-.06-.31.15-.6.45-.65.31-.06.6.15.65.45.06.31-.14.6-.45.65-.03.01-.07.01-.1.01-.27 0-.5-.19-.55-.46zm3.579.45a.568.568 0 0 1-.459-.65c.05-.31.339-.52.65-.46.31.05.51.34.46.65-.05.27-.29.46-.55.46h-.101z\" fill=\"#bc7d20\"/><path d=\"M23.95 12.97c.02-.16.03-.32.04-.48-.01.16-.02.32-.04.48zM23.95 11.03c.02.16.03.31.04.46-.01-.15-.02-.31-.04-.46z\" fill=\"#ca8a23\"/><path d=\"M19.323 12.5a.5.5 0 0 1-.5-.5A6.831 6.831 0 0 0 12 5.177a.5.5 0 0 1 0-1c4.313 0 7.823 3.509 7.823 7.823a.5.5 0 0 1-.5.5z\" fill=\"#fdce72\"/></svg>"

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/constants/AvailablityStatus.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = {
  Auction: "auction",
  Available: "available_for_registration",
  PreReg: "pre_reg",
  Premium: "premium",
  RegPremium: "registry_premium",
  Taken: "taken",
  Excluded: "excluded",
  Reserved: "reserved",
  Unsupported: "tld_unsupported",
  // Errors
  CharacterInvalid: "character_invalid",
  IdnError: "us_idn_not_allowed",
  LanguageUnsupported: "language_unsupported",
  PunycodeError: "punycode_conversion_error",
  TldInvalid: "tld_invalid"
};

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/constants/SearchStatus.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = {
  Auction: "auction",
  Available: "available",
  Error: "error",
  None: "none",
  PreReg: "prereg",
  Restricted: "restricted",
  SearchInProgress: "searching",
  Taken: "taken",
  Unavailable: "unavailable"
};

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/constants/config.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports['default'] = {
  GOVALUE_UPPER_BOUND: 25000,
  CROSS_SELL_WRAP_HEIGHT: 42, //px
  HELP_TEXT_WIDTH: 300, //px
  MOBILE_WINDOW_WIDTH_THRESHOLD: 768, //px
  WHAT_ARE_PREMIUM_DOMAINS_VIDEO: 'uAGNPeAggPU'
};

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/domainsearch/DomainSearchMessage.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DomainSearchMessage = undefined;

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _SearchStatus = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/constants/SearchStatus.js");

var _SearchStatus2 = _interopRequireDefault(_SearchStatus);

var _UrlHelper = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/utilities/UrlHelper.js");

var _UrlHelper2 = _interopRequireDefault(_UrlHelper);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var DomainSearchMessage = exports.DomainSearchMessage = function DomainSearchMessage(_ref) {
  var language = _ref.language,
      searchStatus = _ref.searchStatus,
      domainName = _ref.domainName,
      showRestrictedAsUnavailable = _ref.showRestrictedAsUnavailable;

  function getUnavailableText(message, sorryDomainTakenSuffix) {
    var htmlForMessage = '<span class="unavailable text-primary">' + domainName + '</span>';
    var domainResultMessage = message.replace('{0}', htmlForMessage);
    return _react2['default'].createElement('p', null, _react2['default'].createElement('span', { className: 'h3 headline-primary', dangerouslySetInnerHTML: { __html: domainResultMessage } }), '\xA0', sorryDomainTakenSuffix);
  }

  function getRestrictedText(message) {
    if (showRestrictedAsUnavailable) {
      return getUnavailableText(language.sorryDomainUnavailable);
    }
    var domainResultMessage = getUnavailableText(message);
    var serpLink = _UrlHelper2['default'].getSerpUrl(domainName);
    var regularSearchMessage = _react2['default'].createElement('span', null, _react2['default'].createElement('a', { href: serpLink }, language.restrictedNamesHelpText));
    return _react2['default'].createElement('div', null, domainResultMessage, regularSearchMessage);
  }

  var encloseInWrapper = function encloseInWrapper(result) {
    return _react2['default'].createElement('div', { id: 'searchResults', className: 'default-msg' }, result);
  };

  switch (searchStatus) {
    case _SearchStatus2['default'].None:
      return encloseInWrapper(_react2['default'].createElement('h4', { className: 'headline-primary initial-msg' }, language.initialText));
    case _SearchStatus2['default'].Available:
      return encloseInWrapper(_react2['default'].createElement('h4', { className: 'headline-primary' }, language.domainIsAvailable));
    case _SearchStatus2['default'].Unavailable:
      return encloseInWrapper(getUnavailableText(language.sorryDomainUnavailable, language.sorryDomainTakenSuffix));
    case _SearchStatus2['default'].Taken:
      return encloseInWrapper(getUnavailableText(language.sorryDomainTaken, language.sorryDomainTakenSuffix));
    case _SearchStatus2['default'].PreReg:
      return null; //encloseInWrapper(getRestrictedText(language.preRegText));
    case _SearchStatus2['default'].Auction:
      return null; //getRestrictedText(language.auctionText);
    case _SearchStatus2['default'].Restricted:
      return null; //getRestrictedText(language.restrictedTldText);
    case _SearchStatus2['default'].Error:
      return encloseInWrapper(_react2['default'].createElement('h4', { className: 'headline-primary' }, language.domainSearchError));
    default:
      return null;
  }
};

DomainSearchMessage.propTypes = {
  language: _propTypes2['default'].object.isRequired,
  searchStatus: _propTypes2['default'].string.isRequired,
  domainName: _propTypes2['default'].string.isRequired,
  showRestrictedAsUnavailable: _propTypes2['default'].bool
};

exports['default'] = DomainSearchMessage;

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/domainsearch/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

__webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/domainsearch/index.scss");

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _searchComponent = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/domainsearch/search-component.js");

var _searchComponent2 = _interopRequireDefault(_searchComponent);

var _UrlHelper = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/utilities/UrlHelper.js");

var _UrlHelper2 = _interopRequireDefault(_UrlHelper);

var _HttpRequestHelper = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/utilities/HttpRequestHelper.js");

var _HttpRequestHelper2 = _interopRequireDefault(_HttpRequestHelper);

var _SearchStatus = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/constants/SearchStatus.js");

var _SearchStatus2 = _interopRequireDefault(_SearchStatus);

var _DomainSearchMessage = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/domainsearch/DomainSearchMessage.js");

var _DomainSearchMessage2 = _interopRequireDefault(_DomainSearchMessage);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
  }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var DomainSearch = function (_Component) {
  _inherits(DomainSearch, _Component);

  function DomainSearch(props) {
    _classCallCheck(this, DomainSearch);

    /* istanbul ignore next */
    var _this = _possibleConstructorReturn(this, (DomainSearch.__proto__ || Object.getPrototypeOf(DomainSearch)).call(this, props));

    _this.state = {
      domainName: '',
      searchTerm: props.searchTerm || '',
      prevSearchTerm: props.searchTerm,
      searchStatus: _SearchStatus2['default'].None
    };

    _this.getExactMatchResultSuccess = _this.getExactMatchResultSuccess.bind(_this);
    _this.getExactMatchResultFailure = _this.getExactMatchResultFailure.bind(_this);
    _this.onChange = _this.onChange.bind(_this);

    if (props.searchTerm && props.searchTerm !== '' && props.doInitialSearch !== false) {
      _this.getExactMatchResults();
    }
    return _this;
  }

  _createClass(DomainSearch, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      if (nextProps.reset !== this.props.reset && nextProps.reset) {
        this.setState({
          prevSearchTerm: '',
          searchTerm: ''
        });
      }
    }
  }, {
    key: 'onChange',
    value: function onChange(event) {
      this.setState({ searchTerm: event.target.value });
    }
  }, {
    key: 'onKeyPress',
    value: function onKeyPress(event) {
      var code = event.keyCode || event.which;
      if (code === 13) {
        this.onSearchClick();
      }
    }
  }, {
    key: 'onSearchClick',
    value: function onSearchClick() {
      var _state = this.state,
          prevSearchTerm = _state.prevSearchTerm,
          searchTerm = _state.searchTerm;

      if (prevSearchTerm !== searchTerm) {
        this.setState({
          prevSearchTerm: searchTerm,
          searchStatus: _SearchStatus2['default'].SearchInProgress
        });
        this.getExactMatchResults();
      }
    }
  }, {
    key: 'getExactMatchResults',
    value: function getExactMatchResults() {
      var _props = this.props,
          numberOfSpins = _props.numberOfSpins,
          apiKey = _props.apiKey,
          privateLabelId = _props.privateLabelId,
          urlOverride = _props.urlOverride,
          excludePromo = _props.excludePromo,
          addons = _props.addons,
          extraSearchFunctions = _props.extraSearchFunctions,
          Logger = _props.Logger;
      var _state2 = this.state,
          searchTerm = _state2.searchTerm,
          searchStatus = _state2.searchStatus;

      if (searchStatus !== _SearchStatus2['default'].SearchInProgress) {
        var searchParametersJson = { domainName: searchTerm, apiKey: apiKey, pageSize: numberOfSpins, pageStart: 0, privateLabelId: privateLabelId };
        if (excludePromo) {
          searchParametersJson["excludePromo"] = excludePromo;
        }
        if (addons) {
          searchParametersJson["addons"] = addons;
        }
        var apiUrl = _UrlHelper2['default'].getDomainSearchExactUrl(searchParametersJson, urlOverride);

        var extraSearchPromises = Object.keys(extraSearchFunctions || {}).reduce(function (promiseMap, key) {
          promiseMap[key] = extraSearchFunctions[key](searchTerm);
          return promiseMap;
        }, {});

        var requestParams = {
          httpMethod: "GET",
          apiUrl: apiUrl,
          logInfo: {
            api: 'exact',
            trigger: 'search'
          },
          successCallback: this.getExactMatchResultSuccess,
          failureCallback: this.getExactMatchResultFailure,
          needCredentials: true,
          extraResponseItems: extraSearchPromises
        };
        _HttpRequestHelper2['default'].callApi(requestParams, Logger);
      }
    }
  }, {
    key: 'getExactMatchResultSuccess',
    value: function getExactMatchResultSuccess(response) {
      var extraResponseItems = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var getSuccessCallback = this.props.getSuccessCallback;
      var searchTerm = this.state.searchTerm;

      var domainName = searchTerm;
      var searchStatus = _SearchStatus2['default'].Unavailable;
      if (response.data.ExactMatchDomain) {
        var exactMatchDomain = response.data.ExactMatchDomain;

        if (exactMatchDomain.Fqdn) {
          domainName = exactMatchDomain.Fqdn;
        }

        var isPremium = exactMatchDomain.Inventory === 'premium' && exactMatchDomain.VendorId !== 11;
        var isAuction = exactMatchDomain.Inventory === 'auction';
        var isPreReg = exactMatchDomain.TldInfo;
        if (!isPremium && exactMatchDomain.AvailabilityStatus === 1001 && !isAuction) {
          searchStatus = _SearchStatus2['default'].Taken;
        } else if (exactMatchDomain.IsPurchasable === false && exactMatchDomain.AvailabilityStatus === 1000 || isPremium) {
          searchStatus = _SearchStatus2['default'].Restricted;
        } else if (isAuction) {
          searchStatus = _SearchStatus2['default'].Auction;
        } else if (isPreReg) {
          searchStatus = _SearchStatus2['default'].PreReg;
        } else if (!isPremium && exactMatchDomain.AvailabilityStatus === 1000 && !isAuction) {
          searchStatus = _SearchStatus2['default'].Available;
        } else {
          searchStatus = _SearchStatus2['default'].Unavailable;
        }
      }

      this.setState({
        searchStatus: searchStatus,
        domainName: domainName
      });
      var searchStatusPromise = Promise.resolve(searchStatus);

      getSuccessCallback && getSuccessCallback(response.data, _extends(extraResponseItems, { searchStatusPromise: searchStatusPromise }));
    }
  }, {
    key: 'getExactMatchResultFailure',
    value: function getExactMatchResultFailure(err) {
      var getFailureCallback = this.props.getFailureCallback;

      this.setState({
        searchStatus: _SearchStatus2['default'].Error
      });
      getFailureCallback && getFailureCallback(err);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props2 = this.props,
          eid = _props2.eid,
          language = _props2.language,
          showResultsMessage = _props2.showResultsMessage,
          showRestrictedAsUnavailable = _props2.showRestrictedAsUnavailable,
          showHeadline = _props2.showHeadline;
      var _state3 = this.state,
          searchTerm = _state3.searchTerm,
          searchStatus = _state3.searchStatus,
          domainName = _state3.domainName;

      var domainSearchProps = {
        name: "searchTerm", value: searchTerm, onChange: this.onChange, placeholder: language.placeHolderText, label: ""
      };
      return _react2['default'].createElement('div', { className: 'dpp-search' }, showHeadline === false ? null : _react2['default'].createElement('h3', { className: 'headline-primary label' }, language.findYourDomain), _react2['default'].createElement(_searchComponent2['default'], { domainSearchProps: domainSearchProps,
        eid: eid,
        onKeyPress: function onKeyPress(event) {
          _this2.onKeyPress(event);
        },
        onSearchClick: function onSearchClick() {
          _this2.onSearchClick();
        },
        searchStatus: searchStatus }), showResultsMessage !== false && _react2['default'].createElement(_DomainSearchMessage2['default'], { language: language, searchStatus: searchStatus, domainName: domainName, showRestrictedAsUnavailable: showRestrictedAsUnavailable }));
    }
  }]);

  return DomainSearch;
}(_react.Component);

exports['default'] = DomainSearch;

DomainSearch.propTypes = {
  addons: _propTypes2['default'].array,
  apiKey: _propTypes2['default'].string,
  eid: _propTypes2['default'].string,
  excludePromo: _propTypes2['default'].bool,
  getFailureCallback: _propTypes2['default'].func,
  getSuccessCallback: _propTypes2['default'].func,
  isc: _propTypes2['default'].string,
  language: _propTypes2['default'].object.isRequired,
  numberOfSpins: _propTypes2['default'].string,
  privateLabelId: _propTypes2['default'].number,
  reset: _propTypes2['default'].bool,
  searchTerm: _propTypes2['default'].string,
  showResultsMessage: _propTypes2['default'].bool,
  urlOverride: _propTypes2['default'].string,
  showRestrictedAsUnavailable: _propTypes2['default'].bool,
  extraSearchFunctions: _propTypes2['default'].object,
  showHeadline: _propTypes2['default'].bool,
  doInitialSearch: _propTypes2['default'].bool,
  Logger: _propTypes2['default'].func
};

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/domainsearch/index.scss":
/***/ (function(module, exports) {

// empty (null-loader)

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/domainsearch/search-component.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _icon = __webpack_require__("@ux/icon");

var _icon2 = _interopRequireDefault(_icon);

var _formElement = __webpack_require__("@ux/form-element");

var _formElement2 = _interopRequireDefault(_formElement);

var _button = __webpack_require__("@ux/button");

var _button2 = _interopRequireDefault(_button);

var _SearchStatus = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/constants/SearchStatus.js");

var _SearchStatus2 = _interopRequireDefault(_SearchStatus);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
  }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var styles = {
  searchBox: {
    position: 'relative'
  },
  searchTextInput: {
    paddingRight: 70
  },
  searchButton: {
    fontSize: 20,
    height: 46,
    minWidth: 50,
    padding: 10,

    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,

    position: 'absolute',
    bottom: 0,
    right: 0
  }
};

var SearchComponent = function (_Component) {
  _inherits(SearchComponent, _Component);

  function SearchComponent() {
    _classCallCheck(this, SearchComponent);

    return _possibleConstructorReturn(this, (SearchComponent.__proto__ || Object.getPrototypeOf(SearchComponent)).apply(this, arguments));
  }

  _createClass(SearchComponent, [{
    key: 'render',
    value: function render() {
      return _react2['default'].createElement('div', { style: styles.searchBox }, _react2['default'].createElement(_formElement2['default'], _extends({}, this.props.domainSearchProps, { onKeyPress: this.props.onKeyPress, style: styles.searchTextInput })), _react2['default'].createElement(_button2['default'], { style: styles.searchButton, key: 'searchAction', 'data-eid': this.props.eid + '.search.click', design: 'primary',
        disabled: this.props.searchStatus === _SearchStatus2['default'].SearchInProgress, onClick: this.props.onSearchClick }, _react2['default'].createElement(_icon2['default'], { name: 'magnifying-glass', size: 18, color: 'white' })));
    }
  }]);

  return SearchComponent;
}(_react.Component);

SearchComponent.propTypes = {
  domainSearchProps: _propTypes2['default'].object,
  eid: _propTypes2['default'].string,
  onKeyPress: _propTypes2['default'].func,
  onSearchClick: _propTypes2['default'].func,
  searchStatus: _propTypes2['default'].string
};

exports['default'] = SearchComponent;

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NewExactMatchResult = exports.ExactMatchResult = exports.Addon = exports.SubDomainService = exports.SearchResults = exports.DomainSearchComponent = exports.DomainSearch = undefined;

var _domainsearch = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/domainsearch/index.js");

var _domainsearch2 = _interopRequireDefault(_domainsearch);

var _searchComponent = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/domainsearch/search-component.js");

var _searchComponent2 = _interopRequireDefault(_searchComponent);

var _results = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/results/index.js");

var _results2 = _interopRequireDefault(_results);

var _SubDomainService = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/services/SubDomainService.js");

var _SubDomainService2 = _interopRequireDefault(_SubDomainService);

var _addon = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/addon/index.js");

var _addon2 = _interopRequireDefault(_addon);

var _ExactMatchResult = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/results/ExactMatchResult.js");

var _ExactMatchResult2 = _interopRequireDefault(_ExactMatchResult);

var _NewExactMatchResult = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/results/NewExactMatchResult.js");

var _NewExactMatchResult2 = _interopRequireDefault(_NewExactMatchResult);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

exports.DomainSearch = _domainsearch2['default'];
exports.DomainSearchComponent = _searchComponent2['default'];
exports.SearchResults = _results2['default'];
exports.SubDomainService = _SubDomainService2['default'];
exports.Addon = _addon2['default'];
exports.ExactMatchResult = _ExactMatchResult2['default'];
exports.NewExactMatchResult = _NewExactMatchResult2['default'];

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/results/BundleResult.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _BuyButton = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/results/BuyButton.js");

var _BuyButton2 = _interopRequireDefault(_BuyButton);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var bundleResult = function bundleResult(_ref) {
  var bundleInfo = _ref.bundleInfo,
      bundleButtonProps = _ref.bundleButtonProps,
      language = _ref.language,
      exactProps = _ref.exactProps,
      hasIcannFee = _ref.hasIcannFee,
      nameSearched = _ref.nameSearched;
  var bundleDomains = bundleInfo.bundleDomains,
      bundlePriceInfo = bundleInfo.bundlePriceInfo,
      savingsText = bundleInfo.savingsText;

  var bundleList = bundleDomains.map(function (d) {
    return _react2['default'].createElement('li', { key: d, className: 'font-base' }, d);
  });

  var eBPBundle = bundleButtonProps ? _extends(bundleButtonProps, { isBundle: true }) : {};

  var bundleHeader = _react2['default'].createElement('span', { className: 'font-primary-bold inline' }, bundleList && savingsText ? language.bundleHeaderText.replace('{0}', bundleList.length).replace('{1}', savingsText) : "");

  return bundleList && bundlePriceInfo ? _react2['default'].createElement('div', { className: 'bundle-domain-result' }, _react2['default'].createElement('div', { className: 'domain-name' }, _react2['default'].createElement('ul', { className: 'list-unstyled' }, bundleHeader, bundleList)), _react2['default'].createElement(_BuyButton2['default'], _extends({}, exactProps, {
    domain: {},
    priceInfo: bundlePriceInfo,
    extraButtonProps: eBPBundle,
    getSuccessCallback: null,
    getFailureCallback: null,
    eid: bundleButtonProps.eid,
    hasIcannFee: hasIcannFee,
    nameSearched: nameSearched
  }))) : null;
};

bundleResult.propTypes = {
  bundleInfo: _propTypes2['default'].object,
  bundleButtonProps: _propTypes2['default'].object,
  language: _propTypes2['default'].object,
  exactProps: _propTypes2['default'].object,
  exactResultPresent: _propTypes2['default'].bool,
  hasIcannFee: _propTypes2['default'].bool,
  isPremium: _propTypes2['default'].func,
  nameSearched: _propTypes2['default'].string
};

exports['default'] = bundleResult;

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/results/BuyButton.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _button = __webpack_require__("@ux/button");

var _button2 = _interopRequireDefault(_button);

var _UrlHelper = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/utilities/UrlHelper.js");

var _UrlHelper2 = _interopRequireDefault(_UrlHelper);

var _JsonHelper = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/utilities/JsonHelper.js");

var _JsonHelper2 = _interopRequireDefault(_JsonHelper);

var _HttpRequestHelper = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/utilities/HttpRequestHelper.js");

var _HttpRequestHelper2 = _interopRequireDefault(_HttpRequestHelper);

var _vcartPost = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/vcart-post/index.js");

var _vcartPost2 = _interopRequireDefault(_vcartPost);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
  }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var STATUS = {
  Default: "default",
  Verifying: "verifying",
  Available: "available",
  Unavailable: "unavailable",
  Error: "error",
  Selected: "selected"
};

var BuyButton = function (_Component) {
  _inherits(BuyButton, _Component);

  function BuyButton(props) {
    _classCallCheck(this, BuyButton);

    var _this = _possibleConstructorReturn(this, (BuyButton.__proto__ || Object.getPrototypeOf(BuyButton)).call(this, props));

    var domainInCart = props.domain.isInCart === true;

    _this.getDomainSelectionInfo = _this.getDomainSelectionInfo.bind(_this);
    /* istanbul ignore next */
    _this.state = {
      status: domainInCart ? STATUS.Selected : STATUS.Default,
      payload: {},
      readyForCart: false,
      tdata: _this.getDomainSelectionInfo()
    };

    _this.buyClick = _this.buyClick.bind(_this);
    _this.unSelect = _this.unSelect.bind(_this);
    _this.getPostDataSuccess = _this.getPostDataSuccess.bind(_this);
    _this.getFailureCallback = _this.getFailureCallback.bind(_this);
    _this.getBundleAddPost = _this.getBundleAddPost.bind(_this);
    return _this;
  }

  _createClass(BuyButton, [{
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      var _props = this.props,
          extraButtonProps = _props.extraButtonProps,
          domain = _props.domain;

      var domainInCart = domain.isInCart === true;

      if (prevProps.domain.Fqdn !== this.props.domain.Fqdn || this.props.domain && this.props.domain.isInCart !== prevProps.domain.isInCart && !(this.props.extraButtonProps && this.props.extraButtonProps.isBundle)) {
        this.setState({
          status: domainInCart ? STATUS.Selected : STATUS.Default
        });
      }
      if (extraButtonProps && extraButtonProps.performSelect !== prevProps.extraButtonProps.performSelect && extraButtonProps.performSelect) {
        this.buyClick();
        extraButtonProps.forceExactClickFunc(false);
      }
    }
  }, {
    key: 'getDomainSelectionInfo',
    value: function getDomainSelectionInfo() {
      try {
        var _props2 = this.props,
            domain = _props2.domain,
            apiKey = _props2.apiKey,
            nameSearched = _props2.nameSearched,
            priceInfo = _props2.priceInfo,
            isPreReg = _props2.isPreReg,
            premiumPriceDisplay = _props2.premiumPriceDisplay;

        var availableValue = domain.AvailabilityStatus !== null && domain.AvailabilityStatus > 1000 ? "unavailable" : "available";
        var matchSource = domain.MatchSource ? domain.MatchSource : "";
        var position = domain.isCcTld ? 1001 : domain.Index;
        if (typeof position === "undefined") {
          position = 0;
        }
        var splitInfo = {
          ExperimentID: "undefined",
          BucketNum: "undefined",
          Split: "undefined",
          TrackingID: "undefined",
          NumBuckets: "undefined"
        };
        var priceDisplay = premiumPriceDisplay || (isPreReg && priceInfo.TotalCurrentPriceDisplay ? priceInfo.TotalCurrentPriceDisplay : priceInfo.CurrentPriceDisplay);

        var domainName = void 0,
            savingstext = void 0,
            price = void 0,
            pf_id = void 0,
            source = void 0,
            nameSearchedPair = void 0,
            responseAvail = void 0,
            ad = void 0,
            selectedPhase = void 0,
            splitExperiment = void 0,
            splitBucketNum = void 0,
            splitSide = void 0,
            splitTrackID = void 0,
            splitNumBuckets = void 0,
            key = void 0,
            inventory = void 0;

        if (typeof apiKey === "undefined" || apiKey.length < 1) {
          key = "dpp_search";
        }
        if (domain.DotTypesText) {
          domainName = 'domain,' + domain.DotTypesText;
          position = 'position,0';
          savingstext = 'savingstext,' + domain.SavingsText;
          price = 'price,' + domain.Prices.CurrentPrice;
          nameSearchedPair = 'NameSearched,' + nameSearched;
          key = 'findKey,' + apiKey;
        } else {
          pf_id = 'pf_id,' + domain.ProductId;
          domainName = 'domain,' + domain.Fqdn;
          source = 'source,' + (domain.DomainSource ? domain.DomainSource : "");
          matchSource = 'matchSource,' + matchSource;
          position = 'position,' + position;
          price = 'price,' + priceDisplay;
          responseAvail = 'responseAvail,' + availableValue;
          ad = 'Ad,' + (domain.IsAd ? domain.IsAd : "false");
          nameSearchedPair = 'NameSearched,' + nameSearched;
          key = 'findKey,' + apiKey;
          splitExperiment = 'ExperimentID,' + splitInfo.ExperimentID;
          splitBucketNum = 'BucketNumber,' + splitInfo.BucketNum;
          splitSide = 'SplitSide,' + splitInfo.Split;
          splitTrackID = 'TrackingId,' + splitInfo.TrackingID;
          splitNumBuckets = 'NumberOfBuckets,' + splitInfo.NumBuckets;
          if (domain.TldInfo && domain.TldInfo.SelectedPhase) {
            selectedPhase = 'selectedphase,' + domain.TldInfo.SelectedPhase;
          }
          if (domain.Inventory) {
            inventory = 'Inventory,' + domain.Inventory;
          }
        }
        return [pf_id, domainName, source, matchSource, position, nameSearchedPair, savingstext, price, responseAvail, ad, selectedPhase, inventory, splitExperiment, splitBucketNum, splitSide, splitTrackID, splitNumBuckets, key].filter(function (x) {
          return !!x;
        }).join('^');
      } catch (e) {
        return "";
      }
    }
  }, {
    key: 'unSelect',
    value: function unSelect() {
      var _props3 = this.props,
          extraButtonProps = _props3.extraButtonProps,
          domain = _props3.domain,
          Logger = _props3.Logger;

      this.setState({
        status: STATUS.Default
      });
      Logger && Logger({ type: "A/B test check", error: {}, meta: { watershedInfo: JSON.stringify(window.RunningSplitTestInfo) } });
      extraButtonProps && extraButtonProps.getUnSelectCallback && extraButtonProps.getUnSelectCallback(this.state.payload, domain.Fqdn);
    }
  }, {
    key: 'buyClick',
    value: function buyClick() {
      if (this.props.auctionInfo && this.props.auctionInfo.auctionURL && !this.props.auctionInfo.showBuyNow) {
        window.location = this.props.auctionInfo.auctionURL;
        return;
      }
      this.setState({
        status: STATUS.Verifying
      });
      var _props4 = this.props,
          extraButtonProps = _props4.extraButtonProps,
          Logger = _props4.Logger;

      extraButtonProps && extraButtonProps.setDisableAllButtons && extraButtonProps.setDisableAllButtons(true);
      if (this.props.extraButtonProps && this.props.extraButtonProps.isBundle) {
        this.getBundleAddPost();
        return;
      }
      Logger && Logger({ type: "A/B test check", error: {},
        meta: { watershedInfo: JSON.stringify(window.RunningSplitTestInfo) } });
      this.getPostData();
    }
  }, {
    key: 'getBundleAddPost',
    value: function getBundleAddPost() {
      var _this2 = this;

      var _props5 = this.props,
          extraButtonProps = _props5.extraButtonProps,
          Logger = _props5.Logger;

      var bundleAddParams = extraButtonProps.getBundleAddParams();
      var params = _extends({}, bundleAddParams, {
        successCallback: function successCallback(response, extraResponseItems) {
          bundleAddParams.successCallback && bundleAddParams.successCallback(response, extraResponseItems);
          _this2.getPostDataSuccess(response);
        },
        failureCallback: function failureCallback(err) {
          bundleAddParams.failureCallback && bundleAddParams.failureCallback(err);
          _this2.getFailureCallback(err);
        }
      });
      _HttpRequestHelper2['default'].callApi(params, Logger);
    }
  }, {
    key: 'getPostData',
    value: function getPostData() {
      var _this3 = this;

      var _props6 = this.props,
          domain = _props6.domain,
          apiKey = _props6.apiKey,
          trackingCode = _props6.trackingCode,
          urlOverride = _props6.urlOverride,
          skipAvailCheck = _props6.skipAvailCheck,
          extraButtonProps = _props6.extraButtonProps,
          preRegInfo = _props6.preRegInfo,
          auctionInfo = _props6.auctionInfo,
          Logger = _props6.Logger;

      var tld = domain.Extension;
      var domainName = domain.Fqdn;
      var tierId = domain.TierId;
      var code = trackingCode || "";
      var requestParams = {};
      if (extraButtonProps && extraButtonProps.selectPostParams) {
        var args = [domainName, domain];
        if (preRegInfo) {
          args.push(preRegInfo);
        } else if (auctionInfo && auctionInfo.domainStatusOverride) {
          args.push({
            domainstatus: auctionInfo.domainStatusOverride
          });
        }
        var selectPostParams = extraButtonProps.selectPostParams.apply(null, args);
        requestParams = _extends({}, selectPostParams, {
          successCallback: function successCallback(response, extraResponseItems) {
            var successCallback = function successCallback() {
              return selectPostParams.successCallback && selectPostParams.successCallback(response, extraResponseItems);
            };
            _this3.getPostDataSuccess(response, successCallback);
          },
          failureCallback: function failureCallback(err) {
            selectPostParams.failureCallback && selectPostParams.failureCallback(err);
            _this3.getFailureCallback(err);
          }
        });
      } else {
        var inputJson = {
          apiKey: apiKey,
          domain_name: domainName,
          tld: tld,
          isPremium: domain.Inventory === "premium",
          tier_id: tierId,
          tracking_code: code,
          isavailcheck_needed: !skipAvailCheck
        };
        var apiUrl = _UrlHelper2['default'].getCheckAndGetVCartPostDataUrl(urlOverride);
        requestParams = {
          httpMethod: "POST",
          apiUrl: apiUrl,
          logInfo: {
            api: 'check-add'
          },
          data: JSON.stringify(inputJson),
          successCallback: this.getPostDataSuccess,
          failureCallback: this.getFailureCallback
        };
      }
      _HttpRequestHelper2['default'].callApi(requestParams, Logger);
    }
  }, {
    key: 'getPostDataSuccess',
    value: function getPostDataSuccess(response, successCallback) {
      var _props7 = this.props,
          domain = _props7.domain,
          getSuccessCallback = _props7.getSuccessCallback,
          extraButtonProps = _props7.extraButtonProps;

      if (response.status !== 200 || !response.data.Success || response.data.hasOwnProperty('bidIsValid') && !response.data.bidIsValid) {
        this.setState({
          readyForCart: false,
          status: STATUS.Unavailable,
          payload: {}
        });
      } else {
        successCallback && successCallback();
        this.setState({
          payload: response.data,
          readyForCart: true,
          status: STATUS.Selected
        });
        var selectedDomains = [domain.Fqdn];
        getSuccessCallback && getSuccessCallback(response, selectedDomains, domain);
      }
      extraButtonProps && extraButtonProps.setDisableAllButtons && extraButtonProps.setDisableAllButtons(false);
    }
  }, {
    key: 'getFailureCallback',
    value: function getFailureCallback(err) {
      var _props8 = this.props,
          getFailureCallback = _props8.getFailureCallback,
          extraButtonProps = _props8.extraButtonProps;

      this.setState({
        status: STATUS.Error
      });
      extraButtonProps && extraButtonProps.setDisableAllButtons && extraButtonProps.setDisableAllButtons(false);
      getFailureCallback && getFailureCallback(err);
    }
  }, {
    key: 'render',
    value: function render() {
      var _props9 = this.props,
          eid = _props9.eid,
          priceInfo = _props9.priceInfo,
          premiumPriceDisplay = _props9.premiumPriceDisplay,
          redirectToVCart = _props9.redirectToVCart,
          buttonDesign = _props9.buttonDesign,
          isDisabled = _props9.isDisabled,
          styleOverride = _props9.styleOverride,
          extraButtonProps = _props9.extraButtonProps,
          hasIcannFee = _props9.hasIcannFee,
          auctionInfo = _props9.auctionInfo,
          isRegPremium = _props9.isRegPremium,
          isPreReg = _props9.isPreReg,
          showPriceBlock = _props9.showPriceBlock,
          language = _props9.language;
      var _state = this.state,
          readyForCart = _state.readyForCart,
          status = _state.status,
          payload = _state.payload;
      var buyButtonText = language.buyButtonText,
          renewalText = language.renewalText,
          selectedText = language.selectedText,
          preRegTagText = language.preRegTagText,
          addedToCart = language.addedToCart,
          viewCart = language.viewCart,
          unavailableText = language.domainFailedAvailCheck,
          verifyingText = language.addingToCart,
          removeText = language.remove,
          yearText = language.year;

      var buttonText = this.props.buttonText || buyButtonText;
      var buttonDesignX = extraButtonProps && extraButtonProps.buttonDesign ? extraButtonProps.buttonDesign : buttonDesign;
      var buttonClass = extraButtonProps && extraButtonProps.buttonClass ? extraButtonProps.buttonClass : 'btn-sm';
      var goToCart = extraButtonProps && extraButtonProps.goToCart;

      var renderContent = void 0;
      if (status === STATUS.Verifying) {
        renderContent = _react2['default'].createElement('h6', { className: 'headline-primary' }, verifyingText);
      } else if (status === STATUS.Unavailable) {
        renderContent = _react2['default'].createElement('h6', { className: 'headline-primary unavailable' }, unavailableText);
      } else if (status === STATUS.Selected) {
        renderContent = styleOverride ? _react2['default'].createElement('div', null, _react2['default'].createElement(_button2['default'], {
          'data-tdata': "action,removeFromCart^" + this.state.tdata,
          'data-eid': eid + '.buy.unclick',
          design: 'tertiary',
          className: 'remove',
          onClick: this.unSelect
        }, removeText), _react2['default'].createElement(_button2['default'], {
          design: buttonDesignX === 'purchase' ? 'primary' : 'default',
          className: buttonClass + ' added',
          onClick: goToCart,
          title: viewCart
        }, addedToCart)) : _react2['default'].createElement('div', null, _react2['default'].createElement(_button2['default'], {
          'data-tdata': "action,removeFromCart^" + this.state.tdata,
          'data-eid': eid + '.buy.unclick',
          design: 'tertiary',
          className: 'text-primary added',
          onClick: this.unSelect
        }, _react2['default'].createElement('span', { className: 'uxicon uxicon-checkmark' }), selectedText), _react2['default'].createElement('br', null), _react2['default'].createElement(_button2['default'], {
          'data-tdata': "action,removeFromCart^" + this.state.tdata,
          'data-eid': eid + '.buy.unclick',
          design: 'tertiary',
          className: buttonClass + ' remove',
          onClick: this.unSelect
        }, _react2['default'].createElement('span', { className: 'uxicon uxicon-delete-fill' }), removeText));
      } else {
        var icannFeeStar = hasIcannFee ? _react2['default'].createElement('span', null, '*') : null;
        var currentPriceDisplay = isPreReg && priceInfo.TotalCurrentPriceDisplay ? priceInfo.TotalCurrentPriceDisplay : priceInfo.CurrentPriceDisplay;
        var listPriceDisplay = isPreReg && priceInfo.TotalListPriceDisplay ? priceInfo.TotalListPriceDisplay : priceInfo.ListPriceDisplay;

        var priceBlock = _react2['default'].createElement('span', { className: 'dpp-price text-primary headline-primary' }, currentPriceDisplay, icannFeeStar);
        var renewalInfo = isRegPremium ? _react2['default'].createElement('div', { className: 'renewal-text' }, priceInfo.RenewalPriceDisplay, icannFeeStar, renewalText) : null;
        var strikeThruPrice = priceInfo.CurrentPrice !== priceInfo.ListPrice ? _react2['default'].createElement('span', null, _react2['default'].createElement('small', null, _react2['default'].createElement('strong', null, _react2['default'].createElement('s', null, listPriceDisplay, icannFeeStar)))) : null;
        var preRegTag = isPreReg ? _react2['default'].createElement('span', { className: 'prereg-tag text-gray-dark' }, preRegTagText) : null;

        priceBlock = _react2['default'].createElement('div', { className: 'price-block' }, strikeThruPrice, priceBlock, renewalInfo, preRegTag);

        //Price Display for Premium names
        if (premiumPriceDisplay) {
          priceBlock = _react2['default'].createElement('div', { className: 'price-block' }, _react2['default'].createElement('span', { className: 'dpp-price text-primary headline-primary' }, premiumPriceDisplay, icannFeeStar, _react2['default'].createElement('span', { className: 'renewal-price ' + (auctionInfo ? 'invisible' : '') }, '+ ', priceInfo.RenewalPriceDisplay, icannFeeStar, '/', yearText)), _react2['default'].createElement('span', { className: 'renewal-text ' + (auctionInfo ? 'invisible' : '') }, priceInfo.RenewalPriceDisplay, icannFeeStar, renewalText));
        }

        renderContent = _react2['default'].createElement('div', null, showPriceBlock && priceBlock, _react2['default'].createElement(_button2['default'], {
          'data-eid': eid + '.buy.click',
          'data-tdata': "action,addToCart^" + this.state.tdata,
          design: buttonDesignX,
          className: buttonClass,
          onClick: this.buyClick,
          disabled: isDisabled
        }, buttonText));
      }

      return _react2['default'].createElement('div', { className: 'buy-btn-wrap' }, renderContent, redirectToVCart && readyForCart && _react2['default'].createElement(_vcartPost2['default'], _extends({}, this.props, {
        payload: _JsonHelper2['default'].parseJson(payload),
        shouldSubmitForm: true
      })));
    }
  }]);

  return BuyButton;
}(_react.Component);

exports['default'] = BuyButton;

BuyButton.propTypes = {
  apiKey: _propTypes2['default'].string.isRequired,
  buttonText: _propTypes2['default'].string,
  domain: _propTypes2['default'].object.isRequired,
  eid: _propTypes2['default'].string.isRequired,
  isc: _propTypes2['default'].string,
  hasIcannFee: _propTypes2['default'].bool,
  priceInfo: _propTypes2['default'].object.isRequired,
  premiumPriceDisplay: _propTypes2['default'].node,
  getFailureCallback: _propTypes2['default'].func,
  getSuccessCallback: _propTypes2['default'].func,
  trackingCode: _propTypes2['default'].string,
  vcartNavigation: _propTypes2['default'].object,
  urlOverride: _propTypes2['default'].string,
  redirectToVCart: _propTypes2['default'].bool,
  buttonDesign: _propTypes2['default'].string,
  isDisabled: _propTypes2['default'].bool,
  auctionInfo: _propTypes2['default'].object,
  isPreReg: _propTypes2['default'].bool,
  isRegPremium: _propTypes2['default'].bool,
  preRegInfo: _propTypes2['default'].object,
  extraButtonProps: _propTypes2['default'].shape({
    getUnSelectCallback: _propTypes2['default'].func,
    isBundle: _propTypes2['default'].bool,
    getBundleAddParams: _propTypes2['default'].func,
    performSelect: _propTypes2['default'].bool,
    performExactSelect: _propTypes2['default'].func,
    buttonClass: _propTypes2['default'].string,
    forceExactClickFunc: _propTypes2['default'].func,
    setDisableAllButtons: _propTypes2['default'].func,
    selectPostParams: _propTypes2['default'].func,
    goToCart: _propTypes2['default'].func
  }),
  showPriceBlock: _propTypes2['default'].bool,
  language: _propTypes2['default'].object.isRequired,
  styleOverride: _propTypes2['default'].bool,
  nameSearched: _propTypes2['default'].string,
  Logger: _propTypes2['default'].func.isRequired
};

BuyButton.defaultProps = {
  buttonDesign: 'primary',
  isDisabled: false,
  isPreReg: false,
  showPriceBlock: true,
  styleOverride: false
};

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/results/CrossSellResult.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _checkbox = __webpack_require__("@ux/checkbox");

var _checkbox2 = _interopRequireDefault(_checkbox);

var _HttpRequestHelper = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/utilities/HttpRequestHelper.js");

var _HttpRequestHelper2 = _interopRequireDefault(_HttpRequestHelper);

var _config = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/constants/config.js");

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var crossSellResult = function crossSellResult(_ref) {
  var crossSellInfo = _ref.crossSellInfo,
      pInfoMap = _ref.pInfoMap,
      crossSellButtonProps = _ref.crossSellButtonProps,
      language = _ref.language,
      scale = _ref.scale,
      Logger = _ref.Logger;

  var scaleRatio = scale || 1;
  var crossSellArray = (crossSellInfo.CrossSellDomains || []).reduce(function (result, crossSellDomain) {
    if (pInfoMap[crossSellDomain.Extension]) {
      var fqdn = crossSellDomain.Fqdn;
      var pInfo = pInfoMap[crossSellDomain.Extension][crossSellDomain.TierId];
      var checked = crossSellButtonProps && crossSellButtonProps.selected(fqdn);
      var renewText = pInfo.IsPromoDiscount && pInfo.PromoRegLengthFlag > 1 && pInfo.RenewalPriceDisplay ? language.crossSellRenewal.replace('{0}', pInfo.CurrentPriceDisplay).replace('{1}', pInfo.RenewalPriceDisplay) : null;
      var onChange = function onChange(event) {
        event.persist();
        if (event.target.checked && crossSellButtonProps) {
          var params = _extends({}, crossSellButtonProps.getCrossSellAddParamsFunc(fqdn), {
            failureCallback: function failureCallback(err) {
              event.target.checked = false;
              console.log(err);
            }
          });
          _HttpRequestHelper2['default'].callApi(params, Logger);
        } else if (crossSellButtonProps) {
          crossSellButtonProps.getUnSelectCallback(fqdn);
        }
      };
      result.push(_react2['default'].createElement('span', {
        key: fqdn,
        className: 'cross-sell-wrap font-base text-gray-dark',
        style: {
          transform: 'scale(' + scaleRatio + ',' + scaleRatio + ')'
        }
      }, _react2['default'].createElement(_checkbox2['default'], {
        label: fqdn + ' ' + language.addThis + ' ' + pInfo.CurrentPriceDisplay,
        value: fqdn,
        onChange: onChange,
        checked: checked ? true : null
      }), _react2['default'].createElement('span', { className: 'cross-sell-renew' }, renewText)));
    }

    return result;
  }, []);

  var style = scale ? {
    height: crossSellArray.length * _config2['default'].CROSS_SELL_WRAP_HEIGHT * scaleRatio + 'px',
    position: 'relative'
  } : {};

  return _react2['default'].createElement('div', {
    className: 'cross-sell-container',
    'data-eid': crossSellButtonProps.eid,
    style: style
  }, crossSellArray);
};

crossSellResult.propTypes = {
  crossSellInfo: _propTypes2['default'].object,
  pInfoMap: _propTypes2['default'].object,
  crossSellButtonProps: _propTypes2['default'].object,
  language: _propTypes2['default'].object,
  scale: _propTypes2['default'].number,
  Logger: _propTypes2['default'].func.isRequired
};

exports['default'] = crossSellResult;

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/results/DomainResult.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _tooltip = __webpack_require__("@ux/tooltip");

var _tooltip2 = _interopRequireDefault(_tooltip);

var _icon = __webpack_require__("@ux/icon");

var _icon2 = _interopRequireDefault(_icon);

var _BuyButton = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/results/BuyButton.js");

var _BuyButton2 = _interopRequireDefault(_BuyButton);

var _TooltipWrapper = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/results/TooltipWrapper.js");

var _TooltipWrapper2 = _interopRequireDefault(_TooltipWrapper);

__webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/results/index.scss");

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var DomainResult = function DomainResult(props) {
  var language = props.language,
      priceInfo = props.priceInfo,
      hasPromo = props.hasPromo,
      isPremium = props.isPremium,
      isRegPremium = props.isRegPremium,
      isAuction = props.isAuction,
      cssClasses = props.cssClasses,
      showDomainTypeTag = props.showDomainTypeTag,
      content = props.content,
      auctionInfo = props.auctionInfo;

  var domainName = _react2['default'].createElement('span', { className: 'font-primary-bold domain-name-text' }, props.domain.Fqdn);
  var isAd = props.domain.IsAd;
  var allowedExtensions = ['app'];

  var messages = {
    title: language.sslCertificateTooltipTitle,
    line1: language.sslCertificateTooltipLine1,
    line2: language.sslCertificateTooltipLine2,
    line3: language.sslCertificateTooltipLine3,
    learnMore: language.learnMoreAboutSsl,
    certificateIncluded: language.sslCertificateIncluded
  };

  var extension = props.domain.Extension.toUpperCase();
  var promoDiscount = void 0,
      contentToolTip = void 0,
      auctionTooltip = void 0,
      premium = "";

  //TMS Promo Text
  // && priceInfo.PromoRegLengthFlag > 1 is removed
  // since hasPromo=true provides priceInfo.PromoRegLengthFlag = 2
  if (hasPromo && !isPremium && !isAuction && priceInfo.RenewalPriceDisplay) {
    var promoText = language.promoDiscount.replace('{0}', priceInfo.CurrentPriceDisplay).replace('{1}', priceInfo.ListPriceDisplay);
    promoDiscount = _react2['default'].createElement('div', { className: 'promo-discount font-base' }, promoText);
  }

  //Premium ToolTip Display
  if (isPremium || isRegPremium) {
    var message = language.premiumToolTipText.split('{0}').map(function (s) {
      return '<p>' + s + '</p>';
    }).join('');
    premium = _react2['default'].createElement(_TooltipWrapper2['default'], { title: language.premiumToolTipTitle, htmlMessage: message, preamble: language.moreInfo, tooltipText: language.learnMore });
  }

  if (content) {
    contentToolTip = _react2['default'].createElement(_TooltipWrapper2['default'], { title: content.SubHeader, htmlMessage: content.Messages, tooltipText: language.restrictionsApply });
  }

  if (auctionInfo) {
    auctionTooltip = _react2['default'].createElement(_TooltipWrapper2['default'], { title: auctionInfo.tooltipTitle, htmlMessage: auctionInfo.tooltipMessage, tooltipText: language.learnMore });
  }

  //domain type tag
  var tag = null;
  if (showDomainTypeTag && isAd) {
    tag = _react2['default'].createElement('span', { className: 'is-ad font-primary-bold' }, language.adTag || "ad");
  }
  if (showDomainTypeTag && (isPremium || isRegPremium || auctionInfo && auctionInfo.isPremium)) {
    tag = _react2['default'].createElement('span', { className: 'is-premium font-primary-bold bg-primary' }, language.premiumTag || "premium");
  }
  if (showDomainTypeTag && auctionInfo && !auctionInfo.isPremium) {
    tag = _react2['default'].createElement('span', { className: 'is-auction font-primary-bold' }, language.auctionTag || "auction");
  }

  //Go GetValue
  var goGetValue = props.goGetValue ? _react2['default'].cloneElement(props.goGetValue, { domainName: props.domain.Fqdn }) : null;

  var classes = "domain-result";

  if (cssClasses) {
    classes = classes.concat(" ", cssClasses);
  }

  return _react2['default'].createElement('div', { className: classes.concat(props.goGetValue ? ' row' : '') }, _react2['default'].createElement('div', { className: props.goGetValue ? 'domain-name col-xs-3' : 'domain-name' }, domainName, tag, premium, contentToolTip, auctionTooltip), goGetValue, _react2['default'].createElement(_BuyButton2['default'], _extends({}, props, {
    className: props.goGetValue ? 'col-xs-7' : '',
    buttonText: auctionInfo ? auctionInfo.buttonText : language.buyButtonText
  })), allowedExtensions.indexOf(extension.toLowerCase()) !== -1 && _react2['default'].createElement('span', { className: 'd-block title small m-b-0' }, _react2['default'].createElement(_tooltip2['default'], {
    title: messages.title,
    message: _react2['default'].createElement('div', null, _react2['default'].createElement('p', null, messages.line1.replace('{0}', extension)), _react2['default'].createElement('p', null, messages.line2.replace('{0}', extension)), _react2['default'].createElement('p', null, messages.line3.replace('{0}', extension)), _react2['default'].createElement('a', { href: '/help/topic/1000006?pl_id=' + props.privateLabelId }, messages.learnMore)),
    text: _react2['default'].createElement('span', null, messages.certificateIncluded, '\xA0', _react2['default'].createElement('span', { className: 'text-default' }, _react2['default'].createElement(_icon2['default'], { name: 'help', size: 14 })))
  })), promoDiscount);
};

DomainResult.propTypes = {
  apiKey: _propTypes2['default'].string.isRequired,
  cssClasses: _propTypes2['default'].string,
  vcartNavigation: _propTypes2['default'].object,
  domain: _propTypes2['default'].object.isRequired,
  eid: _propTypes2['default'].string.isRequired,
  hasPromo: _propTypes2['default'].bool,
  isc: _propTypes2['default'].string,
  isPremium: _propTypes2['default'].bool,
  isAuction: _propTypes2['default'].bool,
  language: _propTypes2['default'].object.isRequired,
  hasIcannFee: _propTypes2['default'].bool,
  priceInfo: _propTypes2['default'].object.isRequired,
  content: _propTypes2['default'].object,
  premiumPriceDisplay: _propTypes2['default'].node,
  trackingCode: _propTypes2['default'].string,
  urlOverride: _propTypes2['default'].string,
  redirectToVCart: _propTypes2['default'].bool,
  getSuccessCallback: _propTypes2['default'].func,
  getFailureCallback: _propTypes2['default'].func,
  buttonDesign: _propTypes2['default'].string,
  isDisabled: _propTypes2['default'].bool,
  skipAvailCheck: _propTypes2['default'].bool,
  crossSell: _propTypes2['default'].node,
  showDomainTypeTag: _propTypes2['default'].bool,
  auctionInfo: _propTypes2['default'].object,
  isRegPremium: _propTypes2['default'].bool,
  goGetValue: _propTypes2['default'].node,
  extraButtonProps: _propTypes2['default'].shape({
    buttonDesign: _propTypes2['default'].string,
    getUnSelectCallback: _propTypes2['default'].func,
    isBundle: _propTypes2['default'].bool,
    getBundleAddParams: _propTypes2['default'].func,
    forceExactClickFunc: _propTypes2['default'].func,
    setDisableAllButtons: _propTypes2['default'].func,
    selectPostParams: _propTypes2['default'].func
  }),
  nameSearched: _propTypes2['default'].string,
  Logger: _propTypes2['default'].func
};

DomainResult.defaultProps = {
  goGetValue: null
};

exports['default'] = DomainResult;

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/results/DynamicBundleResult.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _BuyButton = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/results/BuyButton.js");

var _BuyButton2 = _interopRequireDefault(_BuyButton);

var _tooltip = __webpack_require__("@ux/tooltip");

var _tooltip2 = _interopRequireDefault(_tooltip);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var DynamicBundleResult = function DynamicBundleResult(_ref) {
  var bundleInfo = _ref.bundleInfo,
      bundleButtonProps = _ref.bundleButtonProps,
      language = _ref.language,
      exactProps = _ref.exactProps,
      nameSearched = _ref.nameSearched;
  var dynamicBundleHeaderText = language.dynamicBundleHeaderText,
      dynamicBundleBodyText = language.dynamicBundleBodyText,
      learnMore = language.learnMore;

  var hasIcannFee = !exactProps.IsVatCountry && exactProps.product.HasIcannFee;
  var icannFeeStar = hasIcannFee ? _react2['default'].createElement('span', null, '*') : null;
  var bundleDomains = bundleInfo.bundleDomains,
      bundlePriceInfo = bundleInfo.bundlePriceInfo,
      savingsText = bundleInfo.savingsText;

  var bundleList = bundleDomains.split(',').map(function (d) {
    return _react2['default'].createElement('li', { key: d, className: 'font-base' }, d);
  });

  var eBPBundle = bundleButtonProps ? _extends(bundleButtonProps, { isBundle: true }) : {};

  var priceBlock = bundlePriceInfo && _react2['default'].createElement('span', { className: 'dpp-price text-primary headline-primary' }, bundlePriceInfo.CurrentPriceDisplay, icannFeeStar);
  var strikeThruPrice = bundlePriceInfo && bundlePriceInfo.CurrentPrice !== bundlePriceInfo.ListPrice ? _react2['default'].createElement('span', null, _react2['default'].createElement('small', null, _react2['default'].createElement('strong', null, _react2['default'].createElement('s', null, bundlePriceInfo.ListPriceDisplay, icannFeeStar)))) : null;

  var bundleHeader = _react2['default'].createElement('p', { className: 'font-primary-bold' }, bundleList && savingsText ? dynamicBundleHeaderText.replace('{0}', bundleList.length).replace('{1}', savingsText) : "");
  var bundleBody = _react2['default'].createElement(_tooltip2['default'], {
    title: '',
    text: learnMore,
    message: ''
  }, dynamicBundleBodyText);

  return bundleList && bundlePriceInfo ? _react2['default'].createElement('div', { className: 'bundle-domain-result dynamic' }, _react2['default'].createElement('div', { className: 'domain-name' }, _react2['default'].createElement('div', { className: 'price-block' }, priceBlock, strikeThruPrice), _react2['default'].createElement('ul', { className: 'list-unstyled' }, bundleList)), _react2['default'].createElement('div', { className: 'bundle-description text-gray-dark' }, bundleHeader, bundleBody), _react2['default'].createElement(_BuyButton2['default'], _extends({}, exactProps, {
    domain: {},
    priceInfo: bundlePriceInfo,
    extraButtonProps: eBPBundle,
    getSuccessCallback: null,
    getFailureCallback: null,
    eid: bundleButtonProps.eid,
    showPriceBlock: false,
    styleOverride: true,
    nameSearched: nameSearched
  }))) : null;
};

DynamicBundleResult.propTypes = {
  bundleInfo: _propTypes2['default'].object,
  bundleButtonProps: _propTypes2['default'].object,
  language: _propTypes2['default'].object,
  exactProps: _propTypes2['default'].object,
  exactResultPresent: _propTypes2['default'].bool,
  isPremium: _propTypes2['default'].bool,
  nameSearched: _propTypes2['default'].string
};

exports['default'] = DynamicBundleResult;

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/results/ExactMatchResult.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _DomainResult = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/results/DomainResult.js");

var _DomainResult2 = _interopRequireDefault(_DomainResult);

var _DomainSearchMessage = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/domainsearch/DomainSearchMessage.js");

var _DomainSearchMessage2 = _interopRequireDefault(_DomainSearchMessage);

var _CrossSellResult = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/results/CrossSellResult.js");

var _CrossSellResult2 = _interopRequireDefault(_CrossSellResult);

var _BundleResult = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/results/BundleResult.js");

var _BundleResult2 = _interopRequireDefault(_BundleResult);

var _PreRegResult = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/results/PreRegResult.js");

var _PreRegResult2 = _interopRequireDefault(_PreRegResult);

var _SearchStatus = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/constants/SearchStatus.js");

var _SearchStatus2 = _interopRequireDefault(_SearchStatus);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var ExactMatchResult = function ExactMatchResult(props) {
  var exactMatch = props.exactMatch,
      Privacy = props.Privacy,
      extraButtonProps = props.extraButtonProps,
      bundleInfo = props.bundleInfo,
      language = props.language,
      product = props.product,
      crossSellInfo = props.crossSellInfo,
      searchStatus = props.searchStatus,
      auctionInfo = props.auctionInfo,
      IsVatCountry = props.IsVatCountry,
      isDisabled = props.isDisabled,
      Products = props.Products,
      eid = props.eid,
      Logger = props.Logger;

  var priceInfo = product.PriceInfo;
  var hasIcannFee = !IsVatCountry && product.HasIcannFee;
  var domainSearchMessage = searchStatus === _SearchStatus2['default'].Available ? null : _react2['default'].createElement(_DomainSearchMessage2['default'], {
    language: language,
    searchStatus: searchStatus,
    domainName: exactMatch && exactMatch.Fqdn
  });

  var isPremium = function isPremium() {
    return exactMatch.Inventory === 'premium' && exactMatch.VendorId !== 11 || exactMatch.IsPremiumTier;
  };
  var isRegPremium = function isRegPremium() {
    return exactMatch.Inventory === 'registry_premium' || exactMatch.IsPremiumTier;
  };
  var isAuction = function isAuction() {
    return exactMatch.Inventory === 'auction';
  };
  var isPreReg = searchStatus === _SearchStatus2['default'].PreReg;

  var normalExactShouldNotBeViewable = function normalExactShouldNotBeViewable() {
    return !exactMatch || isPreReg || searchStatus === _SearchStatus2['default'].Taken || searchStatus === _SearchStatus2['default'].Unavailable || exactMatch.AvailabilityStatus === 1001 && exactMatch.IsPurchasable === false;
  };

  var pInfoMap = (crossSellInfo.TldProducts || []).reduce(function (pMap, product) {
    pMap[product.Tld] = pMap[product.Tld] || {};
    pMap[product.Tld][product.TierId] = product.PriceInfo;
    return pMap;
  }, {});

  var crossSellButtonProps = extraButtonProps.crossSell;
  var crossSellNotViewable = normalExactShouldNotBeViewable() || auctionInfo || crossSellButtonProps && crossSellButtonProps.showCrossSell === false;
  var crossSellResult = crossSellNotViewable ? null : _react2['default'].createElement(_CrossSellResult2['default'], {
    crossSellButtonProps: crossSellButtonProps,
    crossSellInfo: crossSellInfo,
    pInfoMap: pInfoMap,
    language: language,
    Logger: Logger
  });
  var bundleButtonProps = _extends(extraButtonProps.bundle, { setDisableAllButtons: extraButtonProps.setDisableAllButtons });
  var bundleNotViewable = normalExactShouldNotBeViewable() || auctionInfo || isPremium() || bundleButtonProps.showBundles === false;
  var bundleResult = bundleNotViewable ? null : _react2['default'].createElement(_BundleResult2['default'], {
    exactProps: props,
    bundleButtonProps: bundleButtonProps,
    isPremium: isPremium,
    hasIcannFee: hasIcannFee,
    bundleInfo: bundleInfo,
    language: language,
    exactResultPresent: !normalExactShouldNotBeViewable(),
    isDisabled: isDisabled,
    nameSearched: exactMatch && exactMatch.Fqdn
  });

  var preRegResult = isPreReg ? _react2['default'].createElement(_PreRegResult2['default'], {
    domain: exactMatch,
    Products: Products,
    hasIcannFee: hasIcannFee,
    language: language,
    eid: eid,
    extraButtonProps: _extends(extraButtonProps.exact, {
      setDisableAllButtons: extraButtonProps.setDisableAllButtons
    })
  }) : null;

  //functions because of lazy value
  var hasPromo = function hasPromo() {
    return priceInfo.IsPromoDiscount && priceInfo.PromoRegLengthFlag === 2;
  };

  var premiumPriceDisplay = function premiumPriceDisplay() {
    if (isRegPremium()) {
      return "";
    } else if (isPremium()) {
      return exactMatch.PriceDisplay;
    } else if (auctionInfo) {
      return _react2['default'].createElement('span', null, _react2['default'].createElement('span', { id: 'auction-preamble' }, auctionInfo.preambleText), '\xA0', _react2['default'].createElement('span', null, exactMatch.PriceDisplay));
    }
    return "";
  };

  var content = product.Content || null;

  var exactResult = !normalExactShouldNotBeViewable() ? _react2['default'].createElement(_DomainResult2['default'], _extends({}, props, {
    domain: exactMatch,
    hasPromo: hasPromo(),
    isPremium: isPremium(),
    isAuction: isAuction(),
    premiumPriceDisplay: premiumPriceDisplay(),
    priceInfo: priceInfo,
    privacyInfo: Privacy,
    hasIcannFee: hasIcannFee,
    content: content,
    isRegPremium: isRegPremium(),
    showDomainTypeTag: true,
    extraButtonProps: _extends(props.extraButtonProps.exact, {
      buttonClass: 'btn-md',
      setDisableAllButtons: extraButtonProps.setDisableAllButtons
    }),
    cssClasses: "exact-domain-result",
    nameSearched: exactMatch && exactMatch.Fqdn
  })) : null;

  return _react2['default'].createElement('div', { className: 'dpp-results' }, domainSearchMessage, exactResult, crossSellResult, bundleResult, preRegResult);
};

ExactMatchResult.propTypes = {
  Products: _propTypes2['default'].array.isRequired,
  apiKey: _propTypes2['default'].string.isRequired,
  eid: _propTypes2['default'].string.isRequired,
  getSuccessCallback: _propTypes2['default'].func,
  getFailureCallback: _propTypes2['default'].func,
  language: _propTypes2['default'].object.isRequired,
  exactMatch: _propTypes2['default'].object,
  Privacy: _propTypes2['default'].object,
  redirectToVCart: _propTypes2['default'].bool,
  addOnSuccessCallBack: _propTypes2['default'].func,
  trackingCode: _propTypes2['default'].string,
  addOns: _propTypes2['default'].string,
  urlOverride: _propTypes2['default'].string,
  product: _propTypes2['default'].object,
  isDisabled: _propTypes2['default'].bool,
  vcartNavigation: _propTypes2['default'].object,
  crossSellInfo: _propTypes2['default'].shape({
    CrossSellDomains: _propTypes2['default'].array,
    TldProducts: _propTypes2['default'].array
  }).isRequired,
  bundleInfo: _propTypes2['default'].shape({
    SavingsText: _propTypes2['default'].string,
    DotTypesText: _propTypes2['default'].string,
    Prices: _propTypes2['default'].object
  }).isRequired,
  extraButtonProps: _propTypes2['default'].shape({
    exact: _propTypes2['default'].shape({
      getUnSelectCallback: _propTypes2['default'].func.isRequired,
      buttonDesign: _propTypes2['default'].string,
      performSelect: _propTypes2['default'].bool,
      forceExactClickFunc: _propTypes2['default'].func
    }).isRequired,
    bundle: _propTypes2['default'].shape({
      eid: _propTypes2['default'].string,
      getUnSelectCallback: _propTypes2['default'].func.isRequired,
      getBundleAddParams: _propTypes2['default'].func.isRequired,
      buttonDesign: _propTypes2['default'].string,
      showBundles: _propTypes2['default'].bool
    }),
    crossSell: _propTypes2['default'].shape({
      eid: _propTypes2['default'].string,
      selected: _propTypes2['default'].func.isRequired,
      getUnSelectCallback: _propTypes2['default'].func.isRequired,
      getCrossSellAddParamsFunc: _propTypes2['default'].func.isRequired,
      showCrossSell: _propTypes2['default'].bool
    }),
    setDisableAllButtons: _propTypes2['default'].func
  }),
  auctionInfo: _propTypes2['default'].shape({
    isPremium: _propTypes2['default'].bool.isRequired,
    preambleText: _propTypes2['default'].string.isRequired,
    buttonText: _propTypes2['default'].string.isRequired,
    auctionURL: _propTypes2['default'].string.isRequired,
    tooltipTitle: _propTypes2['default'].string.isRequired,
    tooltipMessage: _propTypes2['default'].string.isRequired
  }),
  resultsPayload: _propTypes2['default'].object,
  searchStatus: _propTypes2['default'].string.isRequired,
  IsVatCountry: _propTypes2['default'].bool,
  Logger: _propTypes2['default'].func.isRequired
};

exports['default'] = ExactMatchResult;

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/results/NewExactMatchResult.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _CrossSellResult = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/results/CrossSellResult.js");

var _CrossSellResult2 = _interopRequireDefault(_CrossSellResult);

var _DynamicBundleResult = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/results/DynamicBundleResult.js");

var _DynamicBundleResult2 = _interopRequireDefault(_DynamicBundleResult);

var _BuyButton = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/results/BuyButton.js");

var _BuyButton2 = _interopRequireDefault(_BuyButton);

var _tooltip = __webpack_require__("@ux/tooltip");

var _tooltip2 = _interopRequireDefault(_tooltip);

var _button = __webpack_require__("@ux/button");

var _button2 = _interopRequireDefault(_button);

var _AvailablityStatus = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/constants/AvailablityStatus.js");

var _AvailablityStatus2 = _interopRequireDefault(_AvailablityStatus);

var _config = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/constants/config.js");

var _config2 = _interopRequireDefault(_config);

var _icnGodaddyValuation = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/assets/icn-godaddy-valuation.svg");

var _icnGodaddyValuation2 = _interopRequireDefault(_icnGodaddyValuation);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
  }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

function getIsMobile() {
  return window.innerWidth < _config2['default'].MOBILE_WINDOW_WIDTH_THRESHOLD;
}

var GoValueTooltip = function (_Component) {
  _inherits(GoValueTooltip, _Component);

  function GoValueTooltip(props) {
    _classCallCheck(this, GoValueTooltip);

    var _this = _possibleConstructorReturn(this, (GoValueTooltip.__proto__ || Object.getPrototypeOf(GoValueTooltip)).call(this, props));

    _this.state = {
      isOpen: false,
      isDisclaimer: false
    };

    _this.handleClick = _this.handleClick.bind(_this);
    _this.handleDismiss = _this.handleDismiss.bind(_this);
    _this.flipPage = _this.flipPage.bind(_this);
    return _this;
  }

  _createClass(GoValueTooltip, [{
    key: 'handleClick',
    value: function handleClick() {
      if (this.state.isOpen) {
        this.setState({
          isOpen: false,
          isDisclaimer: false
        });
      } else {
        this.setState({ isOpen: true });
      }
    }
  }, {
    key: 'handleDismiss',
    value: function handleDismiss() {
      this.setState({
        isOpen: false,
        isDisclaimer: false
      });
    }
  }, {
    key: 'flipPage',
    value: function flipPage() {
      this.setState(function (_ref) {
        var isDisclaimer = _ref.isDisclaimer;
        return { isDisclaimer: !isDisclaimer };
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          preamble = _props.preamble,
          govalueTitle = _props.govalueTitle,
          govalueText = _props.govalueText,
          govalueButton = _props.govalueButton,
          disclaimerTitle = _props.disclaimerTitle,
          disclaimerText = _props.disclaimerText,
          disclaimerButton = _props.disclaimerButton;
      var _state = this.state,
          isOpen = _state.isOpen,
          isDisclaimer = _state.isDisclaimer;

      var title = isDisclaimer ? disclaimerTitle : govalueTitle;
      var text = isDisclaimer ? disclaimerText : govalueText;
      var button = _react2['default'].createElement('p', {
        onClick: this.flipPage,
        style: {
          cursor: 'pointer',
          userSelect: 'none',
          textDecoration: 'underline'
        }
      }, isDisclaimer ? disclaimerButton : govalueButton);
      var message = text.concat(button);

      return _react2['default'].createElement('div', {
        className: 'exact-body-govalue',
        onClick: this.handleClick
      }, preamble, _react2['default'].createElement(_tooltip2['default'], {
        key: isDisclaimer ? 1 : 0,
        title: title,
        message: message,
        open: isOpen,
        persistent: true,
        onDismiss: this.handleDismiss
      }));
    }
  }]);

  return GoValueTooltip;
}(_react.Component);

GoValueTooltip.propTypes = {
  preamble: _propTypes2['default'].array.isRequired,
  govalueTitle: _propTypes2['default'].string.isRequired,
  govalueText: _propTypes2['default'].array.isRequired,
  govalueButton: _propTypes2['default'].string.isRequired,
  disclaimerTitle: _propTypes2['default'].string.isRequired,
  disclaimerText: _propTypes2['default'].array.isRequired,
  disclaimerButton: _propTypes2['default'].string.isRequired
};

var ExactMatchResult = function (_Component2) {
  _inherits(ExactMatchResult, _Component2);

  function ExactMatchResult(props) {
    _classCallCheck(this, ExactMatchResult);

    var _this2 = _possibleConstructorReturn(this, (ExactMatchResult.__proto__ || Object.getPrototypeOf(ExactMatchResult)).call(this, props));

    _this2.state = {
      isMobile: getIsMobile(),
      scale: {
        domainText: 1
      }
    };

    _this2.handleWindowSizeChange = _this2.handleWindowSizeChange.bind(_this2);
    _this2.calculateScale = _this2.calculateScale.bind(_this2);
    _this2.getExactTag = _this2.getExactTag.bind(_this2);
    _this2.getCtaText = _this2.getCtaText.bind(_this2);
    _this2.renderHeader = _this2.renderHeader.bind(_this2);
    _this2.renderPriceBlock = _this2.renderPriceBlock.bind(_this2);
    _this2.renderGoValue = _this2.renderGoValue.bind(_this2);
    _this2.renderDomainName = _this2.renderDomainName.bind(_this2);
    _this2.renderBody = _this2.renderBody.bind(_this2);
    _this2.renderButtons = _this2.renderButtons.bind(_this2);
    _this2.renderFooter = _this2.renderFooter.bind(_this2);
    return _this2;
  }

  _createClass(ExactMatchResult, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      window.addEventListener('resize', this.handleWindowSizeChange);
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      window.removeEventListener('resize', this.handleWindowSizeChange);
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      var scale = this.state.scale;

      var newScale = this.calculateScale();

      if (newScale.domainText !== scale.domainText) {
        this.setState({
          scale: newScale
        });
      }
    }
  }, {
    key: 'handleWindowSizeChange',
    value: function handleWindowSizeChange() {
      var isMobile = getIsMobile();
      if (this.state.isMobile !== isMobile) {
        this.setState({
          isMobile: isMobile,
          scale: this.calculateScale()
        });
      } else {
        this.setState({
          scale: this.calculateScale()
        });
      }
    }
  }, {
    key: 'calculateScale',
    value: function calculateScale() {
      var isMobile = getIsMobile();
      var parent = document.getElementsByClassName('exact-body')[0];
      var domain = document.getElementsByClassName('domain-name-text')[0];
      if (parent && domain) {
        var padding = 40;
        var margin = 20;
        var availableWidth = parent.clientWidth - padding - (1 - isMobile) * (margin + _config2['default'].HELP_TEXT_WIDTH);
        var actualScale = (availableWidth / domain.clientWidth).toFixed(2);

        return {
          domainText: actualScale > 1 ? 1 : actualScale
        };
      }

      return {
        domainText: 1
      };
    }
  }, {
    key: 'getExactTag',
    value: function getExactTag() {
      var _props2 = this.props,
          exactMatch = _props2.exactMatch,
          product = _props2.product,
          auctionInfo = _props2.auctionInfo,
          language = _props2.language;
      var nemRestrictedTag = language.nemRestrictedTag,
          premiumToolTipTitle = language.premiumToolTipTitle,
          premiumToolTipText = language.premiumToolTipText;

      var headerTagName = void 0;
      switch (exactMatch.AvailabilityLabel) {
        case _AvailablityStatus2['default'].Available:
          headerTagName = 'Available';
          break;

        case _AvailablityStatus2['default'].Taken:
        case _AvailablityStatus2['default'].Excluded:
        case _AvailablityStatus2['default'].Reserved:
          headerTagName = 'Taken';
          break;

        case _AvailablityStatus2['default'].Premium:
        case _AvailablityStatus2['default'].RegPremium:
        case _AvailablityStatus2['default'].Auction:
          headerTagName = 'Premium';
          break;

        default:
          //missing error handler
          headerTagName = 'Taken';
      }

      var showTooltip = this.isTaken || this.isPremium || this.isAuction || this.isRegPremium || this.isRestricted;
      var tooltipTitle = void 0,
          tooltipMessage = void 0,
          headerTag = language['nem' + headerTagName + 'Tag'];

      if (this.isTaken) {
        tooltipTitle = '';
        tooltipMessage = '';
      } else if (this.isPremium || this.isRegPremium) {
        tooltipTitle = premiumToolTipTitle;
        tooltipMessage = premiumToolTipText.split('{0}').map(function (s) {
          return '<p>' + s + '</p>';
        }).join('');
      } else if (this.isAuction) {
        tooltipTitle = auctionInfo && auctionInfo.tooltipTitle;
        tooltipMessage = auctionInfo && auctionInfo.tooltipMessage;
      } else if (this.isRestricted) {
        tooltipTitle = product.Content && product.Content.SubHeader;
        tooltipMessage = product.Content && product.Content.Messages;
        headerTag = '' + nemRestrictedTag;
      }

      var tooltip = showTooltip && _react2['default'].createElement(_tooltip2['default'], {
        title: tooltipTitle,
        message: _react2['default'].createElement('div', { dangerouslySetInnerHTML: { __html: tooltipMessage } })
      });

      return _react2['default'].createElement('span', { className: 'exact-header-tag' }, tooltip, headerTag);
    }
  }, {
    key: 'getCtaText',
    value: function getCtaText() {
      var _props3 = this.props,
          supportInfo = _props3.supportInfo,
          language = _props3.language;

      var phone = supportInfo.supportPhone;
      if (this.isAuction) {
        phone = supportInfo.auctionsSupportPhone;
      } else if (this.isPremium) {
        phone = supportInfo.premiumDomainSupportPhone;
      }

      return phone && language.nemCallForHelp.replace('{0}', phone);
    }
  }, {
    key: 'renderHeader',
    value: function renderHeader() {
      var isMobile = this.state.isMobile;

      var ctaText = !isMobile && _react2['default'].createElement('span', { className: 'exact-header-call' }, this.getCtaText());

      return _react2['default'].createElement('div', { className: 'exact-header bg-primary text-white' }, this.getExactTag(), ctaText);
    }
  }, {
    key: 'renderPriceBlock',
    value: function renderPriceBlock() {
      var _props4 = this.props,
          exactMatch = _props4.exactMatch,
          product = _props4.product,
          auctionInfo = _props4.auctionInfo,
          language = _props4.language,
          IsVatCountry = _props4.IsVatCountry;

      var priceInfo = product.PriceInfo || {};
      var hasIcannFee = !IsVatCountry && product.HasIcannFee;

      var icannFeeStar = hasIcannFee && _react2['default'].createElement('span', null, '*');
      var renewalInfo = this.isRegPremium && _react2['default'].createElement('div', { className: 'renewal-text' }, priceInfo.RenewalPriceDisplay, icannFeeStar, language.renewalText);
      var strikeThruPrice = priceInfo.CurrentPrice !== priceInfo.ListPrice && _react2['default'].createElement('span', null, _react2['default'].createElement('small', null, _react2['default'].createElement('strong', null, _react2['default'].createElement('s', null, priceInfo.ListPriceDisplay, icannFeeStar))));

      var priceBlock = _react2['default'].createElement('div', { className: 'price-block' }, _react2['default'].createElement('span', { className: 'dpp-price text-primary headline-primary' }, priceInfo.CurrentPriceDisplay, icannFeeStar, strikeThruPrice), renewalInfo);

      //Price Display for Premium names
      var premiumPriceDisplay = "";
      if (this.isPremium) {
        premiumPriceDisplay = exactMatch.PriceDisplay;
      } else if (this.isAuction) {
        premiumPriceDisplay = _react2['default'].createElement('span', { className: 'premium-price' }, _react2['default'].createElement('p', { id: 'auction-preamble' }, auctionInfo.preambleText), _react2['default'].createElement('span', null, exactMatch.PriceDisplay), icannFeeStar);
      }

      if (premiumPriceDisplay) {
        priceBlock = _react2['default'].createElement('div', { className: 'price-block' }, _react2['default'].createElement('span', { className: 'dpp-price text-primary headline-primary' }, premiumPriceDisplay, _react2['default'].createElement('span', { className: 'renewal-price ' + (auctionInfo ? "invisible" : "") }, '+ ', priceInfo.ListPriceDisplay, icannFeeStar, '/', language.year)), _react2['default'].createElement('span', { className: 'renewal-text ' + (auctionInfo && !auctionInfo.showRenewalPrice ? "invisible" : "") }, priceInfo.RenewalPriceDisplay, icannFeeStar, language.renewalText));
      }

      return priceBlock;
    }
  }, {
    key: 'renderGoValue',
    value: function renderGoValue() {
      var _props5 = this.props,
          exactMatch = _props5.exactMatch,
          auctionInfo = _props5.auctionInfo,
          product = _props5.product,
          language = _props5.language;

      var goValue = exactMatch && exactMatch.Valuation && exactMatch.Valuation.Prices && exactMatch.Valuation.Prices.Govalue || 0;
      var goValueDisplay = exactMatch && exactMatch.Valuation && exactMatch.Valuation.Prices && exactMatch.Valuation.Prices.GovalueDisplay || '';
      // round up
      goValueDisplay = goValueDisplay.substring(0, goValueDisplay.indexOf('.'));
      var price = exactMatch.Price;
      var priceInfo = product.PriceInfo || {};
      var listPrice = priceInfo.ListPrice || 0;
      var showValue = this.isPremium && goValue > 100 && goValue > price || // govalue > $100 AND govalue > BIN price
      this.isRegPremium && goValue > listPrice // govalue > list price
      || this.isAuction && auctionInfo && auctionInfo.showValuation;

      if (!showValue) return null;

      var govaluePrefix = language.govaluePrefix,
          govalueTooltipTitle = language.govalueTooltipTitle,
          govalueTooltipText = language.govalueTooltipText,
          govalueTooltipButton = language.govalueTooltipButton,
          govalueDisclaimerTooltipTitle = language.govalueDisclaimerTooltipTitle,
          govalueDisclaimerTooltipText = language.govalueDisclaimerTooltipText,
          govalueDisclaimerTooltipButton = language.govalueDisclaimerTooltipButton,
          govalueExceedUpperbound = language.govalueExceedUpperbound;

      var preamble = [_react2['default'].createElement('span', {
        className: 'uxicon gd-valuation',
        dangerouslySetInnerHTML: { __html: _icnGodaddyValuation2['default'] }
      }), _react2['default'].createElement('span', { className: 'exact-body-govalue-prefix' }, govaluePrefix), _react2['default'].createElement('span', { className: 'exact-body-govalue-price' }, goValue >= _config2['default'].GOVALUE_UPPER_BOUND ? govalueExceedUpperbound : goValueDisplay)];

      var text = govalueTooltipText.split('{0}').map(function (s) {
        return _react2['default'].createElement('p', null, s);
      });
      var disclaimerText = govalueDisclaimerTooltipText.split('{0}').map(function (s) {
        return _react2['default'].createElement('p', null, s);
      });

      return _react2['default'].createElement(GoValueTooltip, {
        preamble: preamble,
        govalueTitle: govalueTooltipTitle,
        govalueText: text,
        govalueButton: govalueTooltipButton,
        disclaimerTitle: govalueDisclaimerTooltipTitle,
        disclaimerText: disclaimerText,
        disclaimerButton: govalueDisclaimerTooltipButton
      });
    }
  }, {
    key: 'renderDomainName',
    value: function renderDomainName() {
      var _props6 = this.props,
          exactMatch = _props6.exactMatch,
          language = _props6.language;
      var scale = this.state.scale;
      var nemDomainTaken = language.nemDomainTaken,
          nemDomainAvailable = language.nemDomainAvailable;

      var domain = exactMatch.Fqdn;
      var domainSearchMessage = this.isTaken ? nemDomainTaken : nemDomainAvailable;

      return _react2['default'].createElement('div', { className: 'domain-name' }, _react2['default'].createElement('span', {
        className: 'font-primary-bold domain-name-text',
        style: {
          transform: 'scale(' + scale.domainText + ',' + scale.domainText + ')'
        }
      }, domain + ' ' + domainSearchMessage));
    }
  }, {
    key: 'renderBody',
    value: function renderBody(crossSellResult) {
      var _props7 = this.props,
          product = _props7.product,
          rtbInfo = _props7.rtbInfo,
          language = _props7.language;
      var isMobile = this.state.isMobile;
      var nemDBSHelp = language.nemDBSHelp,
          promoDiscount = language.promoDiscount,
          nemWhatCanIDo = language.nemWhatCanIDo,
          nemWhyItsGreat = language.nemWhyItsGreat;

      var priceInfo = product.PriceInfo || {};
      var hasPromo = priceInfo.IsPromoDiscount && priceInfo.PromoRegLengthFlag === 2;

      var dbsHelp = _react2['default'].createElement('a', { className: 'exact-body-dbs-help', href: '#' }, nemDBSHelp);

      //TMS Promo Text
      var promo = void 0;
      if (!this.isTaken && hasPromo && !this.isPremium && !this.isAuction && priceInfo.RenewalPriceDisplay) {
        var promoText = promoDiscount.replace('{0}', priceInfo.CurrentPriceDisplay).replace('{1}', priceInfo.RenewalPriceDisplay);
        promo = _react2['default'].createElement('div', { className: 'promo-discount font-base' }, promoText);
      }

      var helpTextTitle = _react2['default'].createElement('p', { className: 'exact-body-help-title' }, this.isTaken ? nemWhatCanIDo.split(' {0} ')[0] : nemWhyItsGreat);
      var helpTextMessage = (this.isTaken ? nemWhatCanIDo.split(' {0} ').slice(1) : rtbInfo).map(function (line) {
        return _react2['default'].createElement('p', { className: 'exact-body-help-line' }, _react2['default'].createElement('span', { className: 'uxicon uxicon-checkmark' }), _react2['default'].createElement('span', { className: 'reason' }, line));
      });
      var helpText = !isMobile && _react2['default'].createElement('div', { className: 'exact-body-help' }, helpTextTitle, helpTextMessage);

      return _react2['default'].createElement('div', { className: 'exact-body' }, _react2['default'].createElement('div', { className: 'exact-body-result' }, this.renderDomainName(), this.isTaken ? dbsHelp : this.renderPriceBlock(), promo, this.renderGoValue(), crossSellResult), helpText);
    }
  }, {
    key: 'renderButtons',
    value: function renderButtons() {
      var _props8 = this.props,
          exactMatch = _props8.exactMatch,
          Privacy = _props8.Privacy,
          product = _props8.product,
          exactButtonProps = _props8.exactButtonProps,
          auctionInfo = _props8.auctionInfo,
          eid = _props8.eid,
          language = _props8.language;
      var makeOffer = language.makeOffer,
          buyItNow = language.buyItNow;

      if (this.isTaken) {
        return [_react2['default'].createElement(_button2['default'], {
          'data-eid': null,
          design: 'default',
          className: 'btn-sm',
          onClick: function onClick() {}
        }, "Find Similar"), _react2['default'].createElement(_button2['default'], {
          'data-eid': null,
          design: 'purchase',
          className: 'btn-sm',
          onClick: function onClick() {}
        }, "Refine")];
      }

      var showBuyNow = true,
          showMakeOffer = false,
          makeOfferDesign = 'default',
          makeOfferText = makeOffer,
          buyButtonText = language.buyButtonText;

      if (this.isPremium) {
        buyButtonText = buyItNow;
      } else if (this.isRegPremium) {
        buyButtonText = buyItNow;
      } else if (this.isAuction) {
        buyButtonText = buyItNow;
        if (auctionInfo) {
          showBuyNow = auctionInfo.showBuyNow;
          showMakeOffer = auctionInfo.showMakeOffer;
          buyButtonText = auctionInfo.buttonText;
          if (auctionInfo.makeOfferProps) {
            makeOfferText = auctionInfo.makeOfferProps.text;
            makeOfferDesign = auctionInfo.makeOfferProps.design;
          }
        }
      }

      return [showMakeOffer && _react2['default'].createElement(_button2['default'], {
        'data-eid': eid + '.goToAuction.click',
        design: makeOfferDesign,
        className: 'btn-sm',
        onClick: function onClick() {
          window.location = auctionInfo ? auctionInfo.auctionURL : '#';
        }
      }, makeOfferText), showBuyNow && _react2['default'].createElement(_BuyButton2['default'], _extends({}, this.props, {
        domain: exactMatch,
        buttonText: buyButtonText,
        isPremium: this.isPremium,
        isAuction: this.isAuction,
        isRegPremium: this.isRegPremium,
        premiumPriceDisplay: null,
        priceInfo: product.PriceInfo || {},
        privacyInfo: Privacy,
        content: product.Content || null,
        extraButtonProps: exactButtonProps,
        showPriceBlock: false,
        styleOverride: true
      }))];
    }
  }, {
    key: 'renderFooter',
    value: function renderFooter() {
      var _props9 = this.props,
          rtbInfo = _props9.rtbInfo,
          language = _props9.language;
      var isMobile = this.state.isMobile;
      var nemWhyItsGreat = language.nemWhyItsGreat,
          newWhyItsGreatLink = language.newWhyItsGreatLink,
          nemVideoPremium = language.nemVideoPremium,
          nemVideoHowToPick = language.nemVideoHowToPick,
          nemVideoWhatMakesGreat = language.nemVideoWhatMakesGreat,
          nemTakenTitle = language.nemTakenTitle;

      var videoId = '#',
          videoLinkText = nemVideoWhatMakesGreat;
      if (this.isTaken) {
        videoId = '#';
        videoLinkText = nemVideoHowToPick;
      } else if (this.isPremium || this.isRegPremium || this.isAuction) {
        videoId = _config2['default'].WHAT_ARE_PREMIUM_DOMAINS_VIDEO;
        videoLinkText = nemVideoPremium;
      }

      var helpTooltipTitle = nemWhyItsGreat;
      var helpTooltipMessage = rtbInfo.map(function (line) {
        return _react2['default'].createElement('p', null, line);
      });
      var helpText = isMobile && (this.isPremium || this.isAuction || this.isRegPremium) && _react2['default'].createElement(_tooltip2['default'], {
        title: helpTooltipTitle,
        message: helpTooltipMessage
      }, _react2['default'].createElement('a', { className: 'exact-footer-help', href: '#' }, newWhyItsGreatLink));

      var ctaText = isMobile && _react2['default'].createElement('span', { className: 'exact-footer-call' }, this.getCtaText());

      var title = isMobile && this.isTaken && _react2['default'].createElement('span', { className: 'exact-footer-title' }, nemTakenTitle);

      return _react2['default'].createElement('div', { className: 'exact-footer bg-faint' }, ctaText, helpText, _react2['default'].createElement('div', { className: 'exact-footer-buttons' }, this.renderButtons()), title);
    }
  }, {
    key: 'render',
    value: function render() {
      var _props10 = this.props,
          exactMatch = _props10.exactMatch,
          isDisabled = _props10.isDisabled,
          language = _props10.language,
          product = _props10.product,
          Logger = _props10.Logger,
          bundleButtonProps = _props10.bundleButtonProps,
          bundleInfo = _props10.bundleInfo,
          crossSellButtonProps = _props10.crossSellButtonProps,
          crossSellInfo = _props10.crossSellInfo,
          auctionInfo = _props10.auctionInfo;

      var availLabel = exactMatch.AvailabilityLabel;
      var isError = [_AvailablityStatus2['default'].CharacterInvalid, _AvailablityStatus2['default'].IdnError, _AvailablityStatus2['default'].LanguageUnsupported, _AvailablityStatus2['default'].PunycodeError, _AvailablityStatus2['default'].TldInvalid].indexOf(availLabel) !== -1;

      // Error
      if (isError) {
        return _react2['default'].createElement('span', null, 'Search ', exactMatch.Fqdn, ' Error: ', exactMatch.SyntaxMessage);
      }
      // PreReg
      if (availLabel === _AvailablityStatus2['default'].PreReg) {
        return _react2['default'].createElement('span', null, 'PreReg, no design yet');
      }
      // Unsupported
      if (availLabel === _AvailablityStatus2['default'].Unsupported) {
        return _react2['default'].createElement('span', null, exactMatch.Fqdn, ' is Unsupported, no exact block design for it');
      }
      // Product is available
      if (!product) {
        return _react2['default'].createElement('span', null, 'Error, product is not available in exact return');
      }

      this.isPremium = availLabel === _AvailablityStatus2['default'].Premium;
      this.isRegPremium = availLabel === _AvailablityStatus2['default'].RegPremium;
      this.isAuction = availLabel === _AvailablityStatus2['default'].Auction;
      this.isTaken = availLabel === _AvailablityStatus2['default'].Taken || availLabel === _AvailablityStatus2['default'].Excluded || availLabel === _AvailablityStatus2['default'].Reserved;
      this.isRestricted = !!product.Content;

      var isCrossSellViewable = this.isAuction ? auctionInfo && auctionInfo.showCrosssell : !this.isTaken;
      var isBundleViewable = !this.isTaken && !this.isAuction && !this.isPremium && !this.isRegPremium;

      var pInfoMap = (crossSellInfo.TldProducts || []).reduce(function (pMap, product) {
        pMap[product.Tld] = pMap[product.Tld] || {};
        pMap[product.Tld][product.TierId] = product.PriceInfo;
        return pMap;
      }, {});

      var crossSellResult = isCrossSellViewable ? _react2['default'].createElement(_CrossSellResult2['default'], {
        scale: this.state.scale.domainText,
        crossSellButtonProps: crossSellButtonProps,
        crossSellInfo: crossSellInfo,
        pInfoMap: pInfoMap,
        language: language,
        Logger: Logger
      }) : null;

      var bundleResult = isBundleViewable ? _react2['default'].createElement(_DynamicBundleResult2['default'], {
        exactProps: this.props,
        bundleButtonProps: bundleButtonProps,
        isPremium: this.isPremium,
        bundleInfo: bundleInfo,
        language: language,
        exactResultPresent: !this.isTaken,
        isDisabled: isDisabled,
        nameSearched: exactMatch && exactMatch.Fqdn
      }) : null;

      return _react2['default'].createElement('div', { className: 'dpp-results' }, this.renderHeader(), this.renderBody(crossSellResult), this.renderFooter(), bundleResult);
    }
  }]);

  return ExactMatchResult;
}(_react.Component);

;

ExactMatchResult.propTypes = {
  DbsPrice: _propTypes2['default'].string,
  IcannFeePrice: _propTypes2['default'].string,
  IsVatCountry: _propTypes2['default'].bool,
  Privacy: _propTypes2['default'].object,
  apiKey: _propTypes2['default'].string.isRequired,
  auctionInfo: _propTypes2['default'].shape({
    isPremium: _propTypes2['default'].bool.isRequired,
    preambleText: _propTypes2['default'].string.isRequired,
    buttonText: _propTypes2['default'].string.isRequired,
    auctionURL: _propTypes2['default'].string.isRequired,
    tooltipTitle: _propTypes2['default'].string.isRequired,
    tooltipMessage: _propTypes2['default'].string.isRequired,
    showValuation: _propTypes2['default'].bool.isRequired,
    showMakeOffer: _propTypes2['default'].bool.isRequired,
    makeOfferProps: _propTypes2['default'].object,
    showRenewalPrice: _propTypes2['default'].bool.isRequired,
    showCrosssell: _propTypes2['default'].bool.isRequired
  }),
  bundleButtonProps: _propTypes2['default'].shape({
    eid: _propTypes2['default'].string,
    getUnSelectCallback: _propTypes2['default'].func.isRequired,
    getBundleAddParams: _propTypes2['default'].func.isRequired,
    buttonDesign: _propTypes2['default'].string
  }),
  bundleInfo: _propTypes2['default'].shape({
    SavingsText: _propTypes2['default'].string,
    DotTypesText: _propTypes2['default'].string,
    Prices: _propTypes2['default'].object
  }).isRequired,
  crossSellButtonProps: _propTypes2['default'].shape({
    eid: _propTypes2['default'].string,
    selected: _propTypes2['default'].func.isRequired,
    getUnSelectCallback: _propTypes2['default'].func.isRequired,
    getCrossSellAddParamsFunc: _propTypes2['default'].func.isRequired
  }),
  crossSellInfo: _propTypes2['default'].shape({
    CrossSellDomains: _propTypes2['default'].array,
    TldProducts: _propTypes2['default'].array
  }).isRequired,
  eid: _propTypes2['default'].string.isRequired,
  exactButtonProps: _propTypes2['default'].shape({
    buttonDesign: _propTypes2['default'].string,
    getUnSelectCallback: _propTypes2['default'].func.isRequired,
    setDisableAllButtons: _propTypes2['default'].func.isRequired,
    forceExactClickFunc: _propTypes2['default'].func,
    performSelect: _propTypes2['default'].bool,
    selectPostParams: _propTypes2['default'].func.isRequired,
    goToCart: _propTypes2['default'].func.isRequired,
    openDBS: _propTypes2['default'].func.isRequired
  }).isRequired,
  exactMatch: _propTypes2['default'].object.isRequired,
  getSuccessCallback: _propTypes2['default'].func,
  getFailureCallback: _propTypes2['default'].func,
  isDisabled: _propTypes2['default'].bool,
  language: _propTypes2['default'].object.isRequired,
  product: _propTypes2['default'].object.isRequired,
  redirectToVCart: _propTypes2['default'].bool,
  rtbInfo: _propTypes2['default'].array,
  supportInfo: _propTypes2['default'].shape({
    auctionSupportEmail: _propTypes2['default'].string.isRequired,
    auctionsSupportPhone: _propTypes2['default'].string.isRequired,
    premiumDomainSupportPhone: _propTypes2['default'].string.isRequired
  }),
  trackingCode: _propTypes2['default'].string,
  Logger: _propTypes2['default'].func.isRequired
};

exports['default'] = ExactMatchResult;

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/results/PreRegResult.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _BuyButton = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/results/BuyButton.js");

var _BuyButton2 = _interopRequireDefault(_BuyButton);

var _TooltipWrapper = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/results/TooltipWrapper.js");

var _TooltipWrapper2 = _interopRequireDefault(_TooltipWrapper);

var _dropdown = __webpack_require__("@ux/dropdown");

var _dropdown2 = _interopRequireDefault(_dropdown);

var _radio = __webpack_require__("@ux/radio");

var _radio2 = _interopRequireDefault(_radio);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
  }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var PreRegResult = function (_Component) {
  _inherits(PreRegResult, _Component);

  function PreRegResult(props) {
    _classCallCheck(this, PreRegResult);

    var _this = _possibleConstructorReturn(this, (PreRegResult.__proto__ || Object.getPrototypeOf(PreRegResult)).call(this, props));

    var domain = props.domain,
        Products = props.Products;

    _this.defaultPhase = Products[Products.length - 1].PhaseCode;
    _this.state = {
      selectedPhase: _this.defaultPhase,
      selectedPriority: Products[Products.length - 2].PhaseCode
    };

    _this.phases = Products.reduce(function (phases, product, index) {
      phases[product.PhaseCode] = _extends({}, product, domain.TldInfo.Phases[index]);
      return phases;
    }, {});

    _this.udpatePhase = _this.udpatePhase.bind(_this);
    _this.renderPriceBlock = _this.renderPriceBlock.bind(_this);
    _this.renderPriorityDropdown = _this.renderPriorityDropdown.bind(_this);
    _this.renderPhaseOptions = _this.renderPhaseOptions.bind(_this);
    return _this;
  }

  _createClass(PreRegResult, [{
    key: 'udpatePhase',
    value: function udpatePhase(newPhase) {
      this.setState({
        selectedPhase: newPhase,
        selectedPriority: newPhase === this.defaultPhase ? this.state.selectedPriority : newPhase
      });
    }
  }, {
    key: 'renderPriceBlock',
    value: function renderPriceBlock(phase, textTemplate, style) {
      var _props = this.props,
          Products = _props.Products,
          language = _props.language;

      var product = phase === this.defaultPhase ? Products[Products.length - 1] : Products[Products.length - 2];
      var PriceInfo = product.PriceInfo,
          HasIcannFee = product.HasIcannFee;
      var ListPriceDisplay = PriceInfo.ListPriceDisplay,
          CurrentPriceDisplay = PriceInfo.CurrentPriceDisplay,
          RenewalPriceDisplay = PriceInfo.RenewalPriceDisplay,
          TotalListPriceDisplay = PriceInfo.TotalListPriceDisplay,
          TotalCurrentPriceDisplay = PriceInfo.TotalCurrentPriceDisplay;

      var strikePrice = phase === this.defaultPhase ? ListPriceDisplay : TotalListPriceDisplay;
      var currentPrice = phase === this.defaultPhase ? CurrentPriceDisplay : TotalCurrentPriceDisplay;
      var icannFeeStar = HasIcannFee ? _react2['default'].createElement('span', null, '*') : null;
      var tooltipMessage = textTemplate.replace('{0}', currentPrice).replace('{1}', RenewalPriceDisplay);
      var toolTip = _react2['default'].createElement(_TooltipWrapper2['default'], { title: '', htmlMessage: tooltipMessage, tooltipText: 'Pricing details' });

      return _react2['default'].createElement('div', {
        className: 'price-and-action',
        style: style || {}
      }, _react2['default'].createElement('span', { className: 'price-description' }, language.preRegPricePrefix), _react2['default'].createElement('span', null, _react2['default'].createElement('small', null, _react2['default'].createElement('strong', null, _react2['default'].createElement('s', null, strikePrice, icannFeeStar)))), _react2['default'].createElement('span', { className: 'dpp-price text-primary headline-primary' }, currentPrice, icannFeeStar), toolTip);
    }
  }, {
    key: 'renderPriorityDropdown',
    value: function renderPriorityDropdown() {
      var _this2 = this;

      var _props$language = this.props.language,
          priorityPreRegDropdownLabel = _props$language.priorityPreRegDropdownLabel,
          priorityPreRegLabel = _props$language.priorityPreRegLabel,
          priorityPreRegDropdownDatePrefix = _props$language.priorityPreRegDropdownDatePrefix;

      return _react2['default'].createElement(_dropdown2['default'], {
        type: 'select',
        name: 'priorityPreReg',
        label: priorityPreRegDropdownLabel,
        defaultSelected: this.props.Products.length - 2,
        onChange: function onChange(props) {
          _this2.udpatePhase(props.value);
        }
      }, this.props.Products.slice(0, -1).map(function (phase) {
        return _react2['default'].createElement(_dropdown.DropdownItem, {
          value: phase.PhaseCode,
          onClick: function onClick() {
            _this2.udpatePhase(phase.PhaseCode);
          }
        }, _react2['default'].createElement('span', null, _react2['default'].createElement('strong', null, 'Phase ' + phase.PhaseCode.slice(-1) + ' - '), _react2['default'].createElement('span', { className: 'dpp-price' }, phase.PriceInfo.TotalCurrentPriceDisplay)), _react2['default'].createElement('span', null, priorityPreRegLabel), _react2['default'].createElement('span', null, priorityPreRegDropdownDatePrefix, _react2['default'].createElement('strong', null, _this2.phases[phase.PhaseCode].StartDate)));
      }));
    }
  }, {
    key: 'renderPhaseOptions',
    value: function renderPhaseOptions() {
      var _this3 = this;

      var _state = this.state,
          selectedPhase = _state.selectedPhase,
          selectedPriority = _state.selectedPriority;
      var _props$language2 = this.props.language,
          preRegLabel = _props$language2.preRegLabel,
          preRegSubTitle = _props$language2.preRegSubTitle,
          preRegDescription = _props$language2.preRegDescription,
          preRegToolTipText = _props$language2.preRegToolTipText,
          priorityPreRegLabel = _props$language2.priorityPreRegLabel,
          priorityPreRegSubTitle = _props$language2.priorityPreRegSubTitle,
          priorityPreRegDescription = _props$language2.priorityPreRegDescription,
          priorityPreRegToolTipText = _props$language2.priorityPreRegToolTipText;

      return _react2['default'].createElement('ul', null, _react2['default'].createElement('li', null, _react2['default'].createElement(_radio2['default'], {
        label: preRegLabel,
        name: 'phase',
        value: '',
        checked: selectedPhase === this.defaultPhase,
        onClick: function onClick() {
          _this3.udpatePhase(_this3.defaultPhase);
        }
      }), _react2['default'].createElement('div', { className: 'font-base' }, _react2['default'].createElement('h6', null, preRegSubTitle), _react2['default'].createElement('p', { className: 'description' }, preRegDescription, _react2['default'].createElement('b', null, this.phases[this.defaultPhase].StartDate), '.'), this.renderPriceBlock(this.defaultPhase, preRegToolTipText), _react2['default'].createElement('div', { style: { clear: 'both' } }))), _react2['default'].createElement('li', null, _react2['default'].createElement(_radio2['default'], {
        label: priorityPreRegLabel,
        name: 'phase',
        value: '',
        checked: selectedPhase !== this.defaultPhase,
        onClick: function onClick() {
          _this3.udpatePhase(selectedPriority);
        }
      }), _react2['default'].createElement('div', { className: 'font-base' }, _react2['default'].createElement('h6', null, priorityPreRegSubTitle), _react2['default'].createElement('p', { className: 'description' }, priorityPreRegDescription), this.renderPriceBlock(selectedPriority, priorityPreRegToolTipText, { marginTop: '90px' }), this.renderPriorityDropdown(), _react2['default'].createElement('div', { style: { clear: 'both' } }))));
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          domain = _props2.domain,
          hasIcannFee = _props2.hasIcannFee,
          language = _props2.language,
          eid = _props2.eid,
          extraButtonProps = _props2.extraButtonProps;
      var selectedPhase = this.state.selectedPhase;
      var preRegAd = language.preRegAd;

      var domainName = _react2['default'].createElement('span', { className: 'font-primary-bold domain-name-text' }, domain.Fqdn);
      var priceInfo = this.phases[selectedPhase].PriceInfo;
      var preRegInfo = {
        domainstatus: 'preregistration',
        displayarea: 'dppsr_extnd_avail',
        bundle: 'domain',
        launchphase: selectedPhase
      };

      return _react2['default'].createElement('div', { className: 'domain-result exact-domain-result' }, _react2['default'].createElement('div', { className: 'domain-name' }, domainName), _react2['default'].createElement(_BuyButton2['default'], {
        eid: eid,
        domain: domain,
        hasIcannFee: hasIcannFee,
        priceInfo: priceInfo,
        getSuccessCallback: null,
        getFailureCallback: null,
        isPreReg: true,
        preRegInfo: preRegInfo,
        extraButtonProps: extraButtonProps
      }), _react2['default'].createElement('div', { className: 'phase-options text-gray-dark' }, _react2['default'].createElement('p', { className: 'ad' }, preRegAd), this.renderPhaseOptions()));
    }
  }]);

  return PreRegResult;
}(_react.Component);

;

PreRegResult.propTypes = {
  domain: _propTypes2['default'].object.isRequired,
  Products: _propTypes2['default'].array.isRequired,
  hasIcannFee: _propTypes2['default'].bool,
  language: _propTypes2['default'].object.isRequired,
  eid: _propTypes2['default'].string.isRequired,
  extraButtonProps: _propTypes2['default'].shape({
    getUnSelectCallback: _propTypes2['default'].func.isRequired,
    buttonDesign: _propTypes2['default'].string,
    performSelect: _propTypes2['default'].bool,
    forceExactClickFunc: _propTypes2['default'].func,
    setDisableAllButtons: _propTypes2['default'].func.isRequired
  }).isRequired
};

exports['default'] = PreRegResult;

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/results/TooltipWrapper.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _tooltip = __webpack_require__("@ux/tooltip");

var _tooltip2 = _interopRequireDefault(_tooltip);

__webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/results/index.scss");

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var ToolTipWrapper = function ToolTipWrapper(_ref) {
  var title = _ref.title,
      htmlMessage = _ref.htmlMessage,
      preamble = _ref.preamble,
      tooltipText = _ref.tooltipText;

  var message = _react2['default'].createElement('div', { dangerouslySetInnerHTML: { __html: htmlMessage } });
  return _react2['default'].createElement('div', { className: 'tooltip-wrap' }, preamble && _react2['default'].createElement('span', { id: 'tooltip-preamble' }, preamble), '\xA0', _react2['default'].createElement(_tooltip2['default'], { id: 'tooltip-clickable', title: title, message: message }, _react2['default'].createElement('span', { className: 'text-primary' }, tooltipText, ' ', _react2['default'].createElement('span', { className: 'uxicon uxicon-help' }))));
};

ToolTipWrapper.propTypes = {
  title: _propTypes2['default'].string,
  htmlMessage: _propTypes2['default'].string,
  preamble: _propTypes2['default'].string,
  tooltipText: _propTypes2['default'].string
};

exports['default'] = ToolTipWrapper;

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/results/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _DomainResult = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/results/DomainResult.js");

var _DomainResult2 = _interopRequireDefault(_DomainResult);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
  }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var SearchResults = function (_Component) {
  _inherits(SearchResults, _Component);

  function SearchResults(props) {
    _classCallCheck(this, SearchResults);

    return _possibleConstructorReturn(this, (SearchResults.__proto__ || Object.getPrototypeOf(SearchResults)).call(this, props));
  }

  _createClass(SearchResults, [{
    key: 'getPricing',
    value: function getPricing(products, tld, tierId) {
      for (var i = products.length; i--;) {
        if (products[i].Tld === tld && products[i].TierId === tierId) {
          return products[i].PriceInfo;
        }
      }
    }
  }, {
    key: 'hasExactMatch',
    value: function hasExactMatch(exactMatch) {
      if (exactMatch) {
        var isAuction = exactMatch.Inventory === 'auction';
        var isPreReg = exactMatch.TldInfo;
        var isRestricted = exactMatch.IsPurchasable === false && exactMatch.AvailabilityStatus === 1000; //Available but not for offer
        return exactMatch.AvailabilityStatus === 1000 && !isAuction && !isPreReg && !isRestricted || exactMatch.Inventory === 'premium';
      }
      return false;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          resultsPayload = _props.resultsPayload,
          visualOptions = _props.visualOptions;

      var showExact = void 0,
          addExtraSpinCSS = void 0,
          showSpinHeader = void 0,
          showDomainTypeTag = void 0;
      if (visualOptions) {
        showExact = visualOptions.showExact;
        addExtraSpinCSS = visualOptions.showExtraSpinCSS;
        showSpinHeader = visualOptions.showSpinHeader;
        showDomainTypeTag = visualOptions.showDomainTypeTag;
      }

      if (resultsPayload) {
        var exactMatch = resultsPayload.ExactMatchDomain;
        var IsVatCountry = resultsPayload.IsVatCountry;
        var spins = resultsPayload.RecommendedDomains;
        var hasExactMatch = this.hasExactMatch(exactMatch); //Show exact match
        var exactResult = void 0,
            spinResults = void 0,
            spinsHeader = void 0;
        var productInfoMap = SearchResults.createProductMap(resultsPayload.Products);

        //Exact Match Display
        if (hasExactMatch && showExact !== false) {
          var priceInfo = productInfoMap[exactMatch.Extension][exactMatch.TierId].PriceInfo;
          var hasPromo = priceInfo && priceInfo.IsPromoDiscount && priceInfo.PromoRegLengthFlag === 2;
          var isPremium = exactMatch.Inventory === 'premium' && exactMatch.VendorId !== 11 || exactMatch.IsPremiumTier;
          var isRegPremium = exactMatch.Inventory === 'registry_premium' || exactMatch.IsPremiumTier;
          var premiumPriceDisplay = isPremium ? exactMatch.PriceDisplay : null;
          var content = productInfoMap[exactMatch.Extension][exactMatch.TierId].Content || null;
          exactResult = !isPremium && _react2['default'].createElement(_DomainResult2['default'], _extends({}, this.props, {
            cssClasses: 'exact-match',
            domain: exactMatch,
            hasPromo: hasPromo,
            isPremium: isPremium,
            content: content,
            premiumPriceDisplay: premiumPriceDisplay,
            isRegPremium: isRegPremium,
            priceInfo: priceInfo,
            showDomainTypeTag: showDomainTypeTag,
            nameSearched: exactMatch && exactMatch.Fqdn
          }));
        }

        //Spins Headline
        if (spins && spins.length > 0) {
          spinsHeader = showSpinHeader === false ? null : _react2['default'].createElement('h4', { className: 'headline-primary spins-header' }, hasExactMatch ? this.props.language.getOneOfThese : this.props.language.howAboutThese);
          // Spins Display
          spinResults = spins.map(function (item, index) {
            var product = productInfoMap[item.Extension][item.TierId];
            var priceInfo = product && product.PriceInfo;
            var hasPromo = priceInfo && priceInfo.IsPromoDiscount && priceInfo.PromoRegLengthFlag === 2;
            var isPremium = item.Inventory === 'premium' || item.IsPremiumTier;
            var isRegPremium = item.Inventory === 'registry_premium' || item.IsPremiumTier;
            var premiumPriceDisplay = isPremium ? item.PriceDisplay : null;
            var content = product.Content || null;
            return _react2['default'].createElement(_DomainResult2['default'], _extends({}, _this2.props, {
              domain: item,
              hasPromo: hasPromo,
              isPremium: isPremium,
              key: index,
              hasIcannFee: !IsVatCountry && product.HasIcannFee,
              priceInfo: priceInfo,
              content: content,
              isRegPremium: isRegPremium,
              premiumPriceDisplay: premiumPriceDisplay,
              cssClasses: addExtraSpinCSS ? "spin-domain-result" : "",
              showDomainTypeTag: showDomainTypeTag,
              nameSearched: exactMatch && exactMatch.Fqdn
            }));
          });
          spinResults = _react2['default'].createElement('div', { className: 'spin-results-wrap' }, spinResults);
        }

        return _react2['default'].createElement('div', { className: 'dpp-results' }, exactResult, spinsHeader, spinResults);
      }
      return null;
    }
  }], [{
    key: 'createProductMap',
    value: function createProductMap(products) {
      return products.reduce(function (pMap, product) {
        pMap[product.Tld] = pMap[product.Tld] || {};
        pMap[product.Tld][product.TierId] = product;
        return pMap;
      }, {});
    }
  }]);

  return SearchResults;
}(_react.Component);

exports['default'] = SearchResults;

SearchResults.propTypes = {
  apiKey: _propTypes2['default'].string.isRequired,
  eid: _propTypes2['default'].string.isRequired,
  getSuccessCallback: _propTypes2['default'].func,
  getFailureCallback: _propTypes2['default'].func,
  isc: _propTypes2['default'].string,
  language: _propTypes2['default'].object.isRequired,
  resultsPayload: _propTypes2['default'].object,
  redirectToVCart: _propTypes2['default'].bool,
  trackingCode: _propTypes2['default'].string,
  urlOverride: _propTypes2['default'].string,
  vcartNavigation: _propTypes2['default'].object,
  buttonDesign: _propTypes2['default'].string,
  isDisabled: _propTypes2['default'].bool,
  skipAvailCheck: _propTypes2['default'].bool,
  goGetValue: _propTypes2['default'].node,
  visualOptions: _propTypes2['default'].shape({
    showExact: _propTypes2['default'].bool,
    showExtraSpinCSS: _propTypes2['default'].bool,
    showSpinHeader: _propTypes2['default'].bool,
    showDomainTypeTag: _propTypes2['default'].bool
  }),
  extraButtonProps: _propTypes2['default'].shape({
    buttonDesign: _propTypes2['default'].string,
    getUnSelectCallback: _propTypes2['default'].func,
    isBundle: _propTypes2['default'].bool,
    getBundleAddParams: _propTypes2['default'].func,
    selectPostParams: _propTypes2['default'].func
  }),
  Logger: _propTypes2['default'].func
};

SearchResults.defaultProps = {
  redirectToVCart: true
};

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/results/index.scss":
/***/ (function(module, exports) {

// empty (null-loader)

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/services/SubDomainService.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

var _subDomains = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/services/subDomains.json");

var _subDomains2 = _interopRequireDefault(_subDomains);

var _jsCookie = __webpack_require__("./node_modules/js-cookie/src/js.cookie.js");

var _jsCookie2 = _interopRequireDefault(_jsCookie);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

var SubDomainService = function () {
  function SubDomainService() {
    _classCallCheck(this, SubDomainService);
  }

  _createClass(SubDomainService, null, [{
    key: "getSubDomainAndLanguage",

    /**
     * Gets the sub domain and language, if available, for the market id
     *
     * @method getSubDomainAndLanguage
     * @returns {JSON} Returns JSON object with subDomain and language keys
     */
    value: function getSubDomainAndLanguage() {
      var marketCookie = _jsCookie2["default"].get("market") || "en-US";
      var subDomainData = _subDomains2["default"][marketCookie] || _subDomains2["default"]["en-US"];
      return { "subDomain": subDomainData.subDomain, "language": subDomainData.language };
    }

    /**
     * Gets the sub domain and language, if available, for the market id
     *
     * @method getUrl
     * @param {String} host Example: dev-godaddy.com or test-godaddy.com
     * @param {String} path with or without query string params. Example: /api/selected/domains or ?q=chcekingdomain.com
     * @returns {String} Returns the full URL
     */

  }, {
    key: "getUrl",
    value: function getUrl(host, path) {
      var hostValue = host || "godaddy.com";
      var subDomainData = this.getSubDomainAndLanguage();
      var url = "https://" + subDomainData.subDomain + "." + hostValue;

      if (subDomainData.language) {
        url = url.concat("/", subDomainData.language);
      }

      if (path) {
        url = ~path.indexOf("/") ? url.concat(path) : url.concat('/', path);
      }

      return url;
    }
  }]);

  return SubDomainService;
}();

exports["default"] = SubDomainService;

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/services/subDomains.json":
/***/ (function(module, exports) {

module.exports = {"da-DK":{"subDomain":"dk"},"de-AT":{"subDomain":"at"},"de-CH":{"subDomain":"ch"},"de-DE":{"subDomain":"de"},"el-GR":{"subDomain":"gr"},"en-AU":{"subDomain":"au"},"en-CA":{"subDomain":"ca"},"en-GB":{"subDomain":"uk"},"en-HK":{"subDomain":"hk","language":"en"},"en-IE":{"subDomain":"ie"},"en-IN":{"subDomain":"in"},"en-MY":{"subDomain":"my"},"en-NZ":{"subDomain":"nz"},"en-PH":{"subDomain":"ph"},"en-PK":{"subDomain":"pk"},"en-SG":{"subDomain":"sg"},"en-US":{"subDomain":"www"},"en-ZA":{"subDomain":"za"},"es-AR":{"subDomain":"ar"},"es-CL":{"subDomain":"cl"},"es-CO":{"subDomain":"co"},"es-ES":{"subDomain":"es"},"es-MX":{"subDomain":"mx"},"es-PE":{"subDomain":"pe"},"es-US":{"subDomain":"www","language":"es"},"es-VE":{"subDomain":"ve"},"fi-FI":{"subDomain":"fi"},"fil-PH":{"subDomain":"ph","language":"fil"},"fr-BE":{"subDomain":"be","language":"fr"},"fr-CA":{"subDomain":"ca","language":"fr"},"fr-CH":{"subDomain":"ch","language":"fr"},"fr-FR":{"subDomain":"fr"},"hi-IN":{"subDomain":"in","language":"hi"},"id-ID":{"subDomain":"id"},"it-CH":{"subDomain":"ch","language":"it"},"it-IT":{"subDomain":"it"},"ja-JP":{"subDomain":"jp"},"ko-KR":{"subDomain":"kr"},"mr-IN":{"subDomain":"in","language":"mr"},"ms-MY":{"subDomain":"my","language":"ms"},"nb-NO":{"subDomain":"no"},"nl-BE":{"subDomain":"be"},"nl-NL":{"subDomain":"nl"},"pl-PL":{"subDomain":"pl"},"pt-BR":{"subDomain":"br"},"pt-PT":{"subDomain":"pt"},"qa-PS":{"subDomain":"www","language":"qa-ps"},"qa-PZ":{"subDomain":"www","language":"qa-pz"},"qa-QA":{"subDomain":"www","language":"qa-qa"},"ru-RU":{"subDomain":"ru"},"sv-SE":{"subDomain":"se"},"ta-IN":{"subDomain":"in","language":"ta"},"th-TH":{"subDomain":"th"},"tr-TR":{"subDomain":"tr"},"uk-UA":{"subDomain":"ua"},"vi-VN":{"subDomain":"vn"},"zh-HK":{"subDomain":"hk"},"zh-SG":{"subDomain":"sg","language":"zh"},"zh-TW":{"subDomain":"tw"},"en-AE":{"subDomain":"ae"},"en-IL":{"subDomain":"il","language":"en"}}

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/utilities/HttpRequestHelper.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

var _axios = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/index.js");

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

var HttpRequestHelper = function () {
  function HttpRequestHelper() {
    _classCallCheck(this, HttpRequestHelper);
  }

  _createClass(HttpRequestHelper, null, [{
    key: 'callApi',

    /**
     * Calls the success call back function or failure call back function depending on the response from api
     *
     * @method callApi
     * @param  {JSON} parameters in json with request parameters
     * eg., const requestParams = {
     *   "httpMethod": "GET", "apiUrl": api, "successCallback": successCallback, "failureCallback":failureCallback
     *   };
     * @param  {function} Logger 
     */
    value: function callApi(parameters, Logger) {
      var httpMethod = parameters.httpMethod;
      var apiUrl = parameters.apiUrl;
      var logInfo = parameters.logInfo || {};
      var successCallback = parameters.successCallback;
      var failureCallback = parameters.failureCallback;
      var inputData = parameters.data;
      var needCredentials = parameters.needCredentials || false;
      var extraResponseItems = parameters.extraResponseItems || {};

      (0, _axios2['default'])({
        method: httpMethod,
        url: encodeURI(apiUrl),
        data: inputData,
        withCredentials: needCredentials
      }).then(function (response) {
        successCallback(response, extraResponseItems);
      })['catch'](function (err) {
        console.log(apiUrl);
        console.log(err);
        Logger && Logger({
          type: 'request failure',
          error: err,
          meta: _extends({
            apiUrl: parameters.apiUrl
          }, logInfo)
        });
        failureCallback(err);
      });
    }
  }]);

  return HttpRequestHelper;
}();

exports['default'] = HttpRequestHelper;

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/utilities/JsonHelper.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof2 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
  return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
};

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

var JsonHelper = function () {
  function JsonHelper() {
    _classCallCheck(this, JsonHelper);
  }

  _createClass(JsonHelper, null, [{
    key: 'parseJson',

    /**
     * Parses a string or an object to json
     *
     * @method parseJson
     * @param {string} json to be parsed can also be an object
     * @returns {object} Returns JSON object
     */
    value: function parseJson(json) {
      var jsonObject = null;
      if (typeof json === 'string') {
        try {
          jsonObject = JSON.parse(json);
        } catch (e) {
          jsonObject = null;
        }
      } else if ((typeof json === 'undefined' ? 'undefined' : _typeof(json)) === 'object') {
        jsonObject = json;
      }

      return jsonObject && (typeof jsonObject === 'undefined' ? 'undefined' : _typeof(jsonObject)) === 'object' ? jsonObject : null;
    }
  }]);

  return JsonHelper;
}();

exports['default'] = JsonHelper;

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/utilities/UrlHelper.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

var _SubDomainService = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/services/SubDomainService.js");

var _SubDomainService2 = _interopRequireDefault(_SubDomainService);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

var hosts = {
  gd: "secureserver.net"
};

var prefixes = {
  fos: "https://",
  cart: "https://cart.",
  find: "https://www."
};

var paths = {
  domainSearch: "/api/v1/search",
  availCheckAndAdd: "/domainsapi/v1/check/add",
  serpUrl: "/domains/searchresults.aspx",
  virtualCart: "/upp/vcart"
};

var UrlHelper = function () {
  function UrlHelper() {
    _classCallCheck(this, UrlHelper);
  }

  _createClass(UrlHelper, null, [{
    key: "buildUrl",

    /**
     * Concatenates URL pieces together
     *
     * @method buildUrl
     * @param  {String} prefix Example: https://www.
     * @param  {String} host Example: godaddy.com
     * @param  {String} path Example: /domainsapi/v1/search
     * @returns {String} Returns URL
     */
    value: function buildUrl(prefix, host, path) {
      return prefix.concat(host, path);
    }
  }, {
    key: "getTestHost",
    value: function getTestHost(urlOverride) {
      return urlOverride || null;
    }

    /**
     * Concatenates URL pieces together
     *
     * @method getBuildUrlParameters
     * @param {String} hostOverride example: dev-godaddy.com
     * @returns {JSON} Returns prefix and host to build URL
     */

  }, {
    key: "getBuildUrlParameters",
    value: function getBuildUrlParameters(hostOverride) {
      var prefix = prefixes.fos.concat('www', ".");
      var host = hostOverride || this.getTestHost() || hosts.gd;

      return { "prefix": prefix, "host": host };
    }

    /**
     * Returns the fully qualified domainBaseUrl Url for correct environment
     *
     * @method getDomainSearchBaseUrl
     * @param  {String} hostOverride override for host. Example: dev-godaddy.com
     * @returns {String} Returns URL
     */

  }, {
    key: "getDomainSearchBaseUrl",
    value: function getDomainSearchBaseUrl(hostOverride) {
      var useFind = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      var parameters = this.getBuildUrlParameters(hostOverride);
      return this.buildUrl(useFind ? prefixes.find : parameters.prefix, parameters.host, paths.domainSearch);
    }

    /**
     * Returns the fully qualified search results Url for correct environment
     *
     * @method getSerpUrl
     * @param  {String} domainName is the domain searched
     * @returns {String} Returns URL
     */

  }, {
    key: "getSerpUrl",
    value: function getSerpUrl(domainName) {
      var parameters = this.getBuildUrlParameters();
      var buildSerpUrl = this.buildUrl(parameters.prefix, parameters.host, paths.serpUrl);
      return buildSerpUrl + "?domain=" + domainName;
    }

    /**
     * Returns the fully qualified domain search All Url for correct environment
     *
     * @method getDomainSearchAllUrl
     * @param  {JSON} domainSearchJson in json with domain search parameters
     * @param  {String} hostOverride override for host. Example: dev-godaddy.com
     * @returns {String} Returns URL
     */

  }, {
    key: "getDomainSearchAllUrl",
    value: function getDomainSearchAllUrl(domainSearchJson, hostOverride) {
      var key = domainSearchJson.apiKey || "dpp_search";
      var size = domainSearchJson.pageSize || 4;
      var start = domainSearchJson.pageStart || 0;
      var excludePromo = domainSearchJson.excludePromo || false;
      var credentailsneeded = domainSearchJson.credentailsneeded || false;
      var addons = domainSearchJson.addons;
      var baseUrl = this.getDomainSearchBaseUrl(hostOverride);

      baseUrl = baseUrl.concat("/all?", "q=", domainSearchJson.domainName, "&pagestart=", start, "&pagesize=", size, "&key=", key);

      if (excludePromo) {
        baseUrl = baseUrl.concat("&excludepromo=", excludePromo);
      }

      if (credentailsneeded) {
        baseUrl = baseUrl.concat("&credentailsneeded=", credentailsneeded);
      }

      if (addons && addons.length > 0) {
        baseUrl = baseUrl.concat("&addons=", addons.join(','));
      }

      return baseUrl;
    }

    /**
     * Returns the fully qualified domain search EXACT Url for correct environment
     *
     * @method getDomainSearchExactUrl
     * @param  {JSON} domainSearchJson in json with domain search parameters
     * @param  {String} hostOverride override for host. Example: dev-godaddy.com
     * @returns {String} Returns URL
     */

  }, {
    key: "getDomainSearchExactUrl",
    value: function getDomainSearchExactUrl(domainSearchJson, hostOverride) {
      var key = domainSearchJson.apiKey || "dpp_search";
      var credentailsneeded = domainSearchJson.credentailsneeded || false;
      var addons = domainSearchJson.addons;
      var baseUrl = this.getDomainSearchBaseUrl(hostOverride, true); //use entourage

      baseUrl = baseUrl.concat("/exact?", "q=", domainSearchJson.domainName, "&plid=", domainSearchJson.privateLabelId, "&key=", key, "&pc=&ptl=");
      // baseUrl = `www.secureserver.net/api/v1/domains/1592/${domainSearchJson.domainName}`;

      if (credentailsneeded) {
        baseUrl = baseUrl.concat("&credentailsneeded=", credentailsneeded);
      }

      if (addons && addons.length > 0) {
        baseUrl = baseUrl.concat("&addons=", addons.join(','));
      }

      return baseUrl;
    }

    /**
     * Returns the fully qualified availcheck and 'add domain to VCart PostData' Url for correct environment
     *
     * @method getCheckAndGetVCartPostDataBaseUrl
     * @param  {String} hostOverride override for host. Example: dev-godaddy.com
     * @returns {String} Returns URL
     */

  }, {
    key: "getCheckAndGetVCartPostDataUrl",
    value: function getCheckAndGetVCartPostDataUrl(hostOverride) {
      var parameters = this.getBuildUrlParameters(hostOverride);
      return this.buildUrl(parameters.prefix, parameters.host, paths.availCheckAndAdd);
    }

    /**
     * Returns the fully qualified availcheck and 'add domain to VCart PostData' Url for correct environment
     *
     * @method getCheckAndGetVCartPostDataBaseUrl
     * @param  {String} hostOverride override for host. Example: dev-godaddy.com
     * @returns {String} Returns URL
     */

  }, {
    key: "getVirtualCartUrl",
    value: function getVirtualCartUrl(hostOverride) {
      return this.buildUrl(prefixes.cart, hostOverride || hosts.gd, paths.virtualCart);
    }
  }]);

  return UrlHelper;
}();

exports["default"] = UrlHelper;

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/lib/vcart-post/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.VirtualCartFormPost = undefined;

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _UrlHelper = __webpack_require__("./node_modules/@reseller/dpp-shared-components/lib/utilities/UrlHelper.js");

var _UrlHelper2 = _interopRequireDefault(_UrlHelper);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
  }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var VirtualCartFormPost = exports.VirtualCartFormPost = function (_Component) {
  _inherits(VirtualCartFormPost, _Component);

  function VirtualCartFormPost(props) {
    _classCallCheck(this, VirtualCartFormPost);

    /* istanbul ignore next */
    var _this = _possibleConstructorReturn(this, (VirtualCartFormPost.__proto__ || Object.getPrototypeOf(VirtualCartFormPost)).call(this, props));

    _this.submitForm = _this.submitForm.bind(_this);
    _this.processNavigation = _this.processNavigation.bind(_this);
    return _this;
  }

  _createClass(VirtualCartFormPost, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.submitForm(this.props);
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      this.submitForm(nextProps);
    }
  }, {
    key: 'submitForm',
    value: function submitForm(props) {
      if (props.shouldSubmitForm) {
        document.getElementById('virtualPostForm').submit();
      }
    }
  }, {
    key: 'processNavigation',
    value: function processNavigation(navigationObj) {
      var domain = this.props.domain;

      var navObj = JSON.parse(JSON.stringify(navigationObj));

      if (domain) {
        for (var key in navObj) {
          if (navObj.hasOwnProperty(key) && navObj[key].pathParams) {
            var params = '';
            for (var i = 0, len = navObj[key].pathParams.length; i < len; i++) {
              var param = navObj[key].pathParams[i];
              params += i === 0 ? '?' : '&';
              params += param.name + "=" + domain[param.type];
            }

            if (navObj[key].path.indexOf('?') !== -1) {
              params = params.replace('?', '&');
            }

            navObj[key].path += params;
          }
        }
      }

      return navObj;
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          payload = _props.payload,
          vcartNavigation = _props.vcartNavigation;

      var vcartPayload = JSON.parse(JSON.stringify(payload));

      if (vcartNavigation) {
        vcartPayload.navigation = this.processNavigation(vcartNavigation);
      }

      var payloadString = encodeURIComponent(JSON.stringify(vcartPayload));
      var url = _UrlHelper2['default'].getVirtualCartUrl(this.props.urlOverride);

      return _react2['default'].createElement('div', null, _react2['default'].createElement('form', { method: 'post', action: url, id: 'virtualPostForm', ref: 'virtualPostForm' }, _react2['default'].createElement('input', { type: 'hidden', name: 'postData', value: payloadString })));
    }
  }]);

  return VirtualCartFormPost;
}(_react.Component);

VirtualCartFormPost.propTypes = {
  domain: _propTypes2['default'].object,
  payload: _propTypes2['default'].object,
  shouldSubmitForm: _propTypes2['default'].bool,
  urlOverride: _propTypes2['default'].string,
  vcartNavigation: _propTypes2['default'].object
};

exports['default'] = VirtualCartFormPost;

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/index.js":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/axios.js");

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/adapters/xhr.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/utils.js");
var settle = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/core/settle.js");
var buildURL = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/helpers/buildURL.js");
var parseHeaders = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/helpers/parseHeaders.js");
var isURLSameOrigin = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/helpers/isURLSameOrigin.js");
var createError = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/core/createError.js");
var btoa = (typeof window !== 'undefined' && window.btoa && window.btoa.bind(window)) || __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/helpers/btoa.js");

module.exports = function xhrAdapter(config) {
  return new Promise(function dispatchXhrRequest(resolve, reject) {
    var requestData = config.data;
    var requestHeaders = config.headers;

    if (utils.isFormData(requestData)) {
      delete requestHeaders['Content-Type']; // Let the browser set it
    }

    var request = new XMLHttpRequest();
    var loadEvent = 'onreadystatechange';
    var xDomain = false;

    // For IE 8/9 CORS support
    // Only supports POST and GET calls and doesn't returns the response headers.
    // DON'T do this for testing b/c XMLHttpRequest is mocked, not XDomainRequest.
    if (undefined !== 'test' &&
        typeof window !== 'undefined' &&
        window.XDomainRequest && !('withCredentials' in request) &&
        !isURLSameOrigin(config.url)) {
      request = new window.XDomainRequest();
      loadEvent = 'onload';
      xDomain = true;
      request.onprogress = function handleProgress() {};
      request.ontimeout = function handleTimeout() {};
    }

    // HTTP basic authentication
    if (config.auth) {
      var username = config.auth.username || '';
      var password = config.auth.password || '';
      requestHeaders.Authorization = 'Basic ' + btoa(username + ':' + password);
    }

    request.open(config.method.toUpperCase(), buildURL(config.url, config.params, config.paramsSerializer), true);

    // Set the request timeout in MS
    request.timeout = config.timeout;

    // Listen for ready state
    request[loadEvent] = function handleLoad() {
      if (!request || (request.readyState !== 4 && !xDomain)) {
        return;
      }

      // The request errored out and we didn't get a response, this will be
      // handled by onerror instead
      // With one exception: request that using file: protocol, most browsers
      // will return status as 0 even though it's a successful request
      if (request.status === 0 && !(request.responseURL && request.responseURL.indexOf('file:') === 0)) {
        return;
      }

      // Prepare the response
      var responseHeaders = 'getAllResponseHeaders' in request ? parseHeaders(request.getAllResponseHeaders()) : null;
      var responseData = !config.responseType || config.responseType === 'text' ? request.responseText : request.response;
      var response = {
        data: responseData,
        // IE sends 1223 instead of 204 (https://github.com/mzabriskie/axios/issues/201)
        status: request.status === 1223 ? 204 : request.status,
        statusText: request.status === 1223 ? 'No Content' : request.statusText,
        headers: responseHeaders,
        config: config,
        request: request
      };

      settle(resolve, reject, response);

      // Clean up request
      request = null;
    };

    // Handle low level network errors
    request.onerror = function handleError() {
      // Real errors are hidden from us by the browser
      // onerror should only fire if it's a network error
      reject(createError('Network Error', config));

      // Clean up request
      request = null;
    };

    // Handle timeout
    request.ontimeout = function handleTimeout() {
      reject(createError('timeout of ' + config.timeout + 'ms exceeded', config, 'ECONNABORTED'));

      // Clean up request
      request = null;
    };

    // Add xsrf header
    // This is only done if running in a standard browser environment.
    // Specifically not if we're in a web worker, or react-native.
    if (utils.isStandardBrowserEnv()) {
      var cookies = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/helpers/cookies.js");

      // Add xsrf header
      var xsrfValue = (config.withCredentials || isURLSameOrigin(config.url)) && config.xsrfCookieName ?
          cookies.read(config.xsrfCookieName) :
          undefined;

      if (xsrfValue) {
        requestHeaders[config.xsrfHeaderName] = xsrfValue;
      }
    }

    // Add headers to the request
    if ('setRequestHeader' in request) {
      utils.forEach(requestHeaders, function setRequestHeader(val, key) {
        if (typeof requestData === 'undefined' && key.toLowerCase() === 'content-type') {
          // Remove Content-Type if data is undefined
          delete requestHeaders[key];
        } else {
          // Otherwise add header to the request
          request.setRequestHeader(key, val);
        }
      });
    }

    // Add withCredentials to request if needed
    if (config.withCredentials) {
      request.withCredentials = true;
    }

    // Add responseType to request if needed
    if (config.responseType) {
      try {
        request.responseType = config.responseType;
      } catch (e) {
        if (request.responseType !== 'json') {
          throw e;
        }
      }
    }

    // Handle progress if needed
    if (typeof config.onDownloadProgress === 'function') {
      request.addEventListener('progress', config.onDownloadProgress);
    }

    // Not all browsers support upload events
    if (typeof config.onUploadProgress === 'function' && request.upload) {
      request.upload.addEventListener('progress', config.onUploadProgress);
    }

    if (config.cancelToken) {
      // Handle cancellation
      config.cancelToken.promise.then(function onCanceled(cancel) {
        if (!request) {
          return;
        }

        request.abort();
        reject(cancel);
        // Clean up request
        request = null;
      });
    }

    if (requestData === undefined) {
      requestData = null;
    }

    // Send the request
    request.send(requestData);
  });
};


/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/axios.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/utils.js");
var bind = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/helpers/bind.js");
var Axios = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/core/Axios.js");
var defaults = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/defaults.js");

/**
 * Create an instance of Axios
 *
 * @param {Object} defaultConfig The default config for the instance
 * @return {Axios} A new instance of Axios
 */
function createInstance(defaultConfig) {
  var context = new Axios(defaultConfig);
  var instance = bind(Axios.prototype.request, context);

  // Copy axios.prototype to instance
  utils.extend(instance, Axios.prototype, context);

  // Copy context to instance
  utils.extend(instance, context);

  return instance;
}

// Create the default instance to be exported
var axios = createInstance(defaults);

// Expose Axios class to allow class inheritance
axios.Axios = Axios;

// Factory for creating new instances
axios.create = function create(instanceConfig) {
  return createInstance(utils.merge(defaults, instanceConfig));
};

// Expose Cancel & CancelToken
axios.Cancel = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/cancel/Cancel.js");
axios.CancelToken = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/cancel/CancelToken.js");
axios.isCancel = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/cancel/isCancel.js");

// Expose all/spread
axios.all = function all(promises) {
  return Promise.all(promises);
};
axios.spread = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/helpers/spread.js");

module.exports = axios;

// Allow use of default import syntax in TypeScript
module.exports.default = axios;


/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/cancel/Cancel.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * A `Cancel` is an object that is thrown when an operation is canceled.
 *
 * @class
 * @param {string=} message The message.
 */
function Cancel(message) {
  this.message = message;
}

Cancel.prototype.toString = function toString() {
  return 'Cancel' + (this.message ? ': ' + this.message : '');
};

Cancel.prototype.__CANCEL__ = true;

module.exports = Cancel;


/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/cancel/CancelToken.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var Cancel = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/cancel/Cancel.js");

/**
 * A `CancelToken` is an object that can be used to request cancellation of an operation.
 *
 * @class
 * @param {Function} executor The executor function.
 */
function CancelToken(executor) {
  if (typeof executor !== 'function') {
    throw new TypeError('executor must be a function.');
  }

  var resolvePromise;
  this.promise = new Promise(function promiseExecutor(resolve) {
    resolvePromise = resolve;
  });

  var token = this;
  executor(function cancel(message) {
    if (token.reason) {
      // Cancellation has already been requested
      return;
    }

    token.reason = new Cancel(message);
    resolvePromise(token.reason);
  });
}

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
CancelToken.prototype.throwIfRequested = function throwIfRequested() {
  if (this.reason) {
    throw this.reason;
  }
};

/**
 * Returns an object that contains a new `CancelToken` and a function that, when called,
 * cancels the `CancelToken`.
 */
CancelToken.source = function source() {
  var cancel;
  var token = new CancelToken(function executor(c) {
    cancel = c;
  });
  return {
    token: token,
    cancel: cancel
  };
};

module.exports = CancelToken;


/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/cancel/isCancel.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function isCancel(value) {
  return !!(value && value.__CANCEL__);
};


/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/core/Axios.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var defaults = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/defaults.js");
var utils = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/utils.js");
var InterceptorManager = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/core/InterceptorManager.js");
var dispatchRequest = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/core/dispatchRequest.js");
var isAbsoluteURL = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/helpers/isAbsoluteURL.js");
var combineURLs = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/helpers/combineURLs.js");

/**
 * Create a new instance of Axios
 *
 * @param {Object} instanceConfig The default config for the instance
 */
function Axios(instanceConfig) {
  this.defaults = instanceConfig;
  this.interceptors = {
    request: new InterceptorManager(),
    response: new InterceptorManager()
  };
}

/**
 * Dispatch a request
 *
 * @param {Object} config The config specific for this request (merged with this.defaults)
 */
Axios.prototype.request = function request(config) {
  /*eslint no-param-reassign:0*/
  // Allow for axios('example/url'[, config]) a la fetch API
  if (typeof config === 'string') {
    config = utils.merge({
      url: arguments[0]
    }, arguments[1]);
  }

  config = utils.merge(defaults, this.defaults, { method: 'get' }, config);

  // Support baseURL config
  if (config.baseURL && !isAbsoluteURL(config.url)) {
    config.url = combineURLs(config.baseURL, config.url);
  }

  // Hook up interceptors middleware
  var chain = [dispatchRequest, undefined];
  var promise = Promise.resolve(config);

  this.interceptors.request.forEach(function unshiftRequestInterceptors(interceptor) {
    chain.unshift(interceptor.fulfilled, interceptor.rejected);
  });

  this.interceptors.response.forEach(function pushResponseInterceptors(interceptor) {
    chain.push(interceptor.fulfilled, interceptor.rejected);
  });

  while (chain.length) {
    promise = promise.then(chain.shift(), chain.shift());
  }

  return promise;
};

// Provide aliases for supported request methods
utils.forEach(['delete', 'get', 'head'], function forEachMethodNoData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url
    }));
  };
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, data, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url,
      data: data
    }));
  };
});

module.exports = Axios;


/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/core/InterceptorManager.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/utils.js");

function InterceptorManager() {
  this.handlers = [];
}

/**
 * Add a new interceptor to the stack
 *
 * @param {Function} fulfilled The function to handle `then` for a `Promise`
 * @param {Function} rejected The function to handle `reject` for a `Promise`
 *
 * @return {Number} An ID used to remove interceptor later
 */
InterceptorManager.prototype.use = function use(fulfilled, rejected) {
  this.handlers.push({
    fulfilled: fulfilled,
    rejected: rejected
  });
  return this.handlers.length - 1;
};

/**
 * Remove an interceptor from the stack
 *
 * @param {Number} id The ID that was returned by `use`
 */
InterceptorManager.prototype.eject = function eject(id) {
  if (this.handlers[id]) {
    this.handlers[id] = null;
  }
};

/**
 * Iterate over all the registered interceptors
 *
 * This method is particularly useful for skipping over any
 * interceptors that may have become `null` calling `eject`.
 *
 * @param {Function} fn The function to call for each interceptor
 */
InterceptorManager.prototype.forEach = function forEach(fn) {
  utils.forEach(this.handlers, function forEachHandler(h) {
    if (h !== null) {
      fn(h);
    }
  });
};

module.exports = InterceptorManager;


/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/core/createError.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var enhanceError = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/core/enhanceError.js");

/**
 * Create an Error with the specified message, config, error code, and response.
 *
 * @param {string} message The error message.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 @ @param {Object} [response] The response.
 * @returns {Error} The created error.
 */
module.exports = function createError(message, config, code, response) {
  var error = new Error(message);
  return enhanceError(error, config, code, response);
};


/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/core/dispatchRequest.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/utils.js");
var transformData = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/core/transformData.js");
var isCancel = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/cancel/isCancel.js");
var defaults = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/defaults.js");

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
function throwIfCancellationRequested(config) {
  if (config.cancelToken) {
    config.cancelToken.throwIfRequested();
  }
}

/**
 * Dispatch a request to the server using the configured adapter.
 *
 * @param {object} config The config that is to be used for the request
 * @returns {Promise} The Promise to be fulfilled
 */
module.exports = function dispatchRequest(config) {
  throwIfCancellationRequested(config);

  // Ensure headers exist
  config.headers = config.headers || {};

  // Transform request data
  config.data = transformData(
    config.data,
    config.headers,
    config.transformRequest
  );

  // Flatten headers
  config.headers = utils.merge(
    config.headers.common || {},
    config.headers[config.method] || {},
    config.headers || {}
  );

  utils.forEach(
    ['delete', 'get', 'head', 'post', 'put', 'patch', 'common'],
    function cleanHeaderConfig(method) {
      delete config.headers[method];
    }
  );

  var adapter = config.adapter || defaults.adapter;

  return adapter(config).then(function onAdapterResolution(response) {
    throwIfCancellationRequested(config);

    // Transform response data
    response.data = transformData(
      response.data,
      response.headers,
      config.transformResponse
    );

    return response;
  }, function onAdapterRejection(reason) {
    if (!isCancel(reason)) {
      throwIfCancellationRequested(config);

      // Transform response data
      if (reason && reason.response) {
        reason.response.data = transformData(
          reason.response.data,
          reason.response.headers,
          config.transformResponse
        );
      }
    }

    return Promise.reject(reason);
  });
};


/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/core/enhanceError.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Update an Error with the specified config, error code, and response.
 *
 * @param {Error} error The error to update.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 @ @param {Object} [response] The response.
 * @returns {Error} The error.
 */
module.exports = function enhanceError(error, config, code, response) {
  error.config = config;
  if (code) {
    error.code = code;
  }
  error.response = response;
  return error;
};


/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/core/settle.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var createError = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/core/createError.js");

/**
 * Resolve or reject a Promise based on response status.
 *
 * @param {Function} resolve A function that resolves the promise.
 * @param {Function} reject A function that rejects the promise.
 * @param {object} response The response.
 */
module.exports = function settle(resolve, reject, response) {
  var validateStatus = response.config.validateStatus;
  // Note: status is not exposed by XDomainRequest
  if (!response.status || !validateStatus || validateStatus(response.status)) {
    resolve(response);
  } else {
    reject(createError(
      'Request failed with status code ' + response.status,
      response.config,
      null,
      response
    ));
  }
};


/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/core/transformData.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/utils.js");

/**
 * Transform the data for a request or a response
 *
 * @param {Object|String} data The data to be transformed
 * @param {Array} headers The headers for the request or response
 * @param {Array|Function} fns A single function or Array of functions
 * @returns {*} The resulting transformed data
 */
module.exports = function transformData(data, headers, fns) {
  /*eslint no-param-reassign:0*/
  utils.forEach(fns, function transform(fn) {
    data = fn(data, headers);
  });

  return data;
};


/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/defaults.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(process) {

var utils = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/utils.js");
var normalizeHeaderName = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/helpers/normalizeHeaderName.js");

var PROTECTION_PREFIX = /^\)\]\}',?\n/;
var DEFAULT_CONTENT_TYPE = {
  'Content-Type': 'application/x-www-form-urlencoded'
};

function setContentTypeIfUnset(headers, value) {
  if (!utils.isUndefined(headers) && utils.isUndefined(headers['Content-Type'])) {
    headers['Content-Type'] = value;
  }
}

function getDefaultAdapter() {
  var adapter;
  if (typeof XMLHttpRequest !== 'undefined') {
    // For browsers use XHR adapter
    adapter = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/adapters/xhr.js");
  } else if (typeof process !== 'undefined') {
    // For node use HTTP adapter
    adapter = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/adapters/xhr.js");
  }
  return adapter;
}

var defaults = {
  adapter: getDefaultAdapter(),

  transformRequest: [function transformRequest(data, headers) {
    normalizeHeaderName(headers, 'Content-Type');
    if (utils.isFormData(data) ||
      utils.isArrayBuffer(data) ||
      utils.isStream(data) ||
      utils.isFile(data) ||
      utils.isBlob(data)
    ) {
      return data;
    }
    if (utils.isArrayBufferView(data)) {
      return data.buffer;
    }
    if (utils.isURLSearchParams(data)) {
      setContentTypeIfUnset(headers, 'application/x-www-form-urlencoded;charset=utf-8');
      return data.toString();
    }
    if (utils.isObject(data)) {
      setContentTypeIfUnset(headers, 'application/json;charset=utf-8');
      return JSON.stringify(data);
    }
    return data;
  }],

  transformResponse: [function transformResponse(data) {
    /*eslint no-param-reassign:0*/
    if (typeof data === 'string') {
      data = data.replace(PROTECTION_PREFIX, '');
      try {
        data = JSON.parse(data);
      } catch (e) { /* Ignore */ }
    }
    return data;
  }],

  timeout: 0,

  xsrfCookieName: 'XSRF-TOKEN',
  xsrfHeaderName: 'X-XSRF-TOKEN',

  maxContentLength: -1,

  validateStatus: function validateStatus(status) {
    return status >= 200 && status < 300;
  }
};

defaults.headers = {
  common: {
    'Accept': 'application/json, text/plain, */*'
  }
};

utils.forEach(['delete', 'get', 'head'], function forEachMehtodNoData(method) {
  defaults.headers[method] = {};
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  defaults.headers[method] = utils.merge(DEFAULT_CONTENT_TYPE);
});

module.exports = defaults;

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/process/browser.js")))

/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/helpers/bind.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function bind(fn, thisArg) {
  return function wrap() {
    var args = new Array(arguments.length);
    for (var i = 0; i < args.length; i++) {
      args[i] = arguments[i];
    }
    return fn.apply(thisArg, args);
  };
};


/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/helpers/btoa.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// btoa polyfill for IE<10 courtesy https://github.com/davidchambers/Base64.js

var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

function E() {
  this.message = 'String contains an invalid character';
}
E.prototype = new Error;
E.prototype.code = 5;
E.prototype.name = 'InvalidCharacterError';

function btoa(input) {
  var str = String(input);
  var output = '';
  for (
    // initialize result and counter
    var block, charCode, idx = 0, map = chars;
    // if the next str index does not exist:
    //   change the mapping table to "="
    //   check if d has no fractional digits
    str.charAt(idx | 0) || (map = '=', idx % 1);
    // "8 - idx % 1 * 8" generates the sequence 2, 4, 6, 8
    output += map.charAt(63 & block >> 8 - idx % 1 * 8)
  ) {
    charCode = str.charCodeAt(idx += 3 / 4);
    if (charCode > 0xFF) {
      throw new E();
    }
    block = block << 8 | charCode;
  }
  return output;
}

module.exports = btoa;


/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/helpers/buildURL.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/utils.js");

function encode(val) {
  return encodeURIComponent(val).
    replace(/%40/gi, '@').
    replace(/%3A/gi, ':').
    replace(/%24/g, '$').
    replace(/%2C/gi, ',').
    replace(/%20/g, '+').
    replace(/%5B/gi, '[').
    replace(/%5D/gi, ']');
}

/**
 * Build a URL by appending params to the end
 *
 * @param {string} url The base of the url (e.g., http://www.google.com)
 * @param {object} [params] The params to be appended
 * @returns {string} The formatted url
 */
module.exports = function buildURL(url, params, paramsSerializer) {
  /*eslint no-param-reassign:0*/
  if (!params) {
    return url;
  }

  var serializedParams;
  if (paramsSerializer) {
    serializedParams = paramsSerializer(params);
  } else if (utils.isURLSearchParams(params)) {
    serializedParams = params.toString();
  } else {
    var parts = [];

    utils.forEach(params, function serialize(val, key) {
      if (val === null || typeof val === 'undefined') {
        return;
      }

      if (utils.isArray(val)) {
        key = key + '[]';
      }

      if (!utils.isArray(val)) {
        val = [val];
      }

      utils.forEach(val, function parseValue(v) {
        if (utils.isDate(v)) {
          v = v.toISOString();
        } else if (utils.isObject(v)) {
          v = JSON.stringify(v);
        }
        parts.push(encode(key) + '=' + encode(v));
      });
    });

    serializedParams = parts.join('&');
  }

  if (serializedParams) {
    url += (url.indexOf('?') === -1 ? '?' : '&') + serializedParams;
  }

  return url;
};


/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/helpers/combineURLs.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Creates a new URL by combining the specified URLs
 *
 * @param {string} baseURL The base URL
 * @param {string} relativeURL The relative URL
 * @returns {string} The combined URL
 */
module.exports = function combineURLs(baseURL, relativeURL) {
  return baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '');
};


/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/helpers/cookies.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/utils.js");

module.exports = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs support document.cookie
  (function standardBrowserEnv() {
    return {
      write: function write(name, value, expires, path, domain, secure) {
        var cookie = [];
        cookie.push(name + '=' + encodeURIComponent(value));

        if (utils.isNumber(expires)) {
          cookie.push('expires=' + new Date(expires).toGMTString());
        }

        if (utils.isString(path)) {
          cookie.push('path=' + path);
        }

        if (utils.isString(domain)) {
          cookie.push('domain=' + domain);
        }

        if (secure === true) {
          cookie.push('secure');
        }

        document.cookie = cookie.join('; ');
      },

      read: function read(name) {
        var match = document.cookie.match(new RegExp('(^|;\\s*)(' + name + ')=([^;]*)'));
        return (match ? decodeURIComponent(match[3]) : null);
      },

      remove: function remove(name) {
        this.write(name, '', Date.now() - 86400000);
      }
    };
  })() :

  // Non standard browser env (web workers, react-native) lack needed support.
  (function nonStandardBrowserEnv() {
    return {
      write: function write() {},
      read: function read() { return null; },
      remove: function remove() {}
    };
  })()
);


/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/helpers/isAbsoluteURL.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Determines whether the specified URL is absolute
 *
 * @param {string} url The URL to test
 * @returns {boolean} True if the specified URL is absolute, otherwise false
 */
module.exports = function isAbsoluteURL(url) {
  // A URL is considered absolute if it begins with "<scheme>://" or "//" (protocol-relative URL).
  // RFC 3986 defines scheme name as a sequence of characters beginning with a letter and followed
  // by any combination of letters, digits, plus, period, or hyphen.
  return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url);
};


/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/helpers/isURLSameOrigin.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/utils.js");

module.exports = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs have full support of the APIs needed to test
  // whether the request URL is of the same origin as current location.
  (function standardBrowserEnv() {
    var msie = /(msie|trident)/i.test(navigator.userAgent);
    var urlParsingNode = document.createElement('a');
    var originURL;

    /**
    * Parse a URL to discover it's components
    *
    * @param {String} url The URL to be parsed
    * @returns {Object}
    */
    function resolveURL(url) {
      var href = url;

      if (msie) {
        // IE needs attribute set twice to normalize properties
        urlParsingNode.setAttribute('href', href);
        href = urlParsingNode.href;
      }

      urlParsingNode.setAttribute('href', href);

      // urlParsingNode provides the UrlUtils interface - http://url.spec.whatwg.org/#urlutils
      return {
        href: urlParsingNode.href,
        protocol: urlParsingNode.protocol ? urlParsingNode.protocol.replace(/:$/, '') : '',
        host: urlParsingNode.host,
        search: urlParsingNode.search ? urlParsingNode.search.replace(/^\?/, '') : '',
        hash: urlParsingNode.hash ? urlParsingNode.hash.replace(/^#/, '') : '',
        hostname: urlParsingNode.hostname,
        port: urlParsingNode.port,
        pathname: (urlParsingNode.pathname.charAt(0) === '/') ?
                  urlParsingNode.pathname :
                  '/' + urlParsingNode.pathname
      };
    }

    originURL = resolveURL(window.location.href);

    /**
    * Determine if a URL shares the same origin as the current location
    *
    * @param {String} requestURL The URL to test
    * @returns {boolean} True if URL shares the same origin, otherwise false
    */
    return function isURLSameOrigin(requestURL) {
      var parsed = (utils.isString(requestURL)) ? resolveURL(requestURL) : requestURL;
      return (parsed.protocol === originURL.protocol &&
            parsed.host === originURL.host);
    };
  })() :

  // Non standard browser envs (web workers, react-native) lack needed support.
  (function nonStandardBrowserEnv() {
    return function isURLSameOrigin() {
      return true;
    };
  })()
);


/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/helpers/normalizeHeaderName.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/utils.js");

module.exports = function normalizeHeaderName(headers, normalizedName) {
  utils.forEach(headers, function processHeader(value, name) {
    if (name !== normalizedName && name.toUpperCase() === normalizedName.toUpperCase()) {
      headers[normalizedName] = value;
      delete headers[name];
    }
  });
};


/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/helpers/parseHeaders.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/utils.js");

/**
 * Parse headers into an object
 *
 * ```
 * Date: Wed, 27 Aug 2014 08:58:49 GMT
 * Content-Type: application/json
 * Connection: keep-alive
 * Transfer-Encoding: chunked
 * ```
 *
 * @param {String} headers Headers needing to be parsed
 * @returns {Object} Headers parsed into an object
 */
module.exports = function parseHeaders(headers) {
  var parsed = {};
  var key;
  var val;
  var i;

  if (!headers) { return parsed; }

  utils.forEach(headers.split('\n'), function parser(line) {
    i = line.indexOf(':');
    key = utils.trim(line.substr(0, i)).toLowerCase();
    val = utils.trim(line.substr(i + 1));

    if (key) {
      parsed[key] = parsed[key] ? parsed[key] + ', ' + val : val;
    }
  });

  return parsed;
};


/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/helpers/spread.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Syntactic sugar for invoking a function and expanding an array for arguments.
 *
 * Common use case would be to use `Function.prototype.apply`.
 *
 *  ```js
 *  function f(x, y, z) {}
 *  var args = [1, 2, 3];
 *  f.apply(null, args);
 *  ```
 *
 * With `spread` this example can be re-written.
 *
 *  ```js
 *  spread(function(x, y, z) {})([1, 2, 3]);
 *  ```
 *
 * @param {Function} callback
 * @returns {Function}
 */
module.exports = function spread(callback) {
  return function wrap(arr) {
    return callback.apply(null, arr);
  };
};


/***/ }),

/***/ "./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/utils.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var bind = __webpack_require__("./node_modules/@reseller/dpp-shared-components/node_modules/axios/lib/helpers/bind.js");

/*global toString:true*/

// utils is a library of generic helper functions non-specific to axios

var toString = Object.prototype.toString;

/**
 * Determine if a value is an Array
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Array, otherwise false
 */
function isArray(val) {
  return toString.call(val) === '[object Array]';
}

/**
 * Determine if a value is an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an ArrayBuffer, otherwise false
 */
function isArrayBuffer(val) {
  return toString.call(val) === '[object ArrayBuffer]';
}

/**
 * Determine if a value is a FormData
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an FormData, otherwise false
 */
function isFormData(val) {
  return (typeof FormData !== 'undefined') && (val instanceof FormData);
}

/**
 * Determine if a value is a view on an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a view on an ArrayBuffer, otherwise false
 */
function isArrayBufferView(val) {
  var result;
  if ((typeof ArrayBuffer !== 'undefined') && (ArrayBuffer.isView)) {
    result = ArrayBuffer.isView(val);
  } else {
    result = (val) && (val.buffer) && (val.buffer instanceof ArrayBuffer);
  }
  return result;
}

/**
 * Determine if a value is a String
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a String, otherwise false
 */
function isString(val) {
  return typeof val === 'string';
}

/**
 * Determine if a value is a Number
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Number, otherwise false
 */
function isNumber(val) {
  return typeof val === 'number';
}

/**
 * Determine if a value is undefined
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if the value is undefined, otherwise false
 */
function isUndefined(val) {
  return typeof val === 'undefined';
}

/**
 * Determine if a value is an Object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Object, otherwise false
 */
function isObject(val) {
  return val !== null && typeof val === 'object';
}

/**
 * Determine if a value is a Date
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Date, otherwise false
 */
function isDate(val) {
  return toString.call(val) === '[object Date]';
}

/**
 * Determine if a value is a File
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a File, otherwise false
 */
function isFile(val) {
  return toString.call(val) === '[object File]';
}

/**
 * Determine if a value is a Blob
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Blob, otherwise false
 */
function isBlob(val) {
  return toString.call(val) === '[object Blob]';
}

/**
 * Determine if a value is a Function
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Function, otherwise false
 */
function isFunction(val) {
  return toString.call(val) === '[object Function]';
}

/**
 * Determine if a value is a Stream
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Stream, otherwise false
 */
function isStream(val) {
  return isObject(val) && isFunction(val.pipe);
}

/**
 * Determine if a value is a URLSearchParams object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a URLSearchParams object, otherwise false
 */
function isURLSearchParams(val) {
  return typeof URLSearchParams !== 'undefined' && val instanceof URLSearchParams;
}

/**
 * Trim excess whitespace off the beginning and end of a string
 *
 * @param {String} str The String to trim
 * @returns {String} The String freed of excess whitespace
 */
function trim(str) {
  return str.replace(/^\s*/, '').replace(/\s*$/, '');
}

/**
 * Determine if we're running in a standard browser environment
 *
 * This allows axios to run in a web worker, and react-native.
 * Both environments support XMLHttpRequest, but not fully standard globals.
 *
 * web workers:
 *  typeof window -> undefined
 *  typeof document -> undefined
 *
 * react-native:
 *  typeof document.createElement -> undefined
 */
function isStandardBrowserEnv() {
  return (
    typeof window !== 'undefined' &&
    typeof document !== 'undefined' &&
    typeof document.createElement === 'function'
  );
}

/**
 * Iterate over an Array or an Object invoking a function for each item.
 *
 * If `obj` is an Array callback will be called passing
 * the value, index, and complete array for each item.
 *
 * If 'obj' is an Object callback will be called passing
 * the value, key, and complete object for each property.
 *
 * @param {Object|Array} obj The object to iterate
 * @param {Function} fn The callback to invoke for each item
 */
function forEach(obj, fn) {
  // Don't bother if no value provided
  if (obj === null || typeof obj === 'undefined') {
    return;
  }

  // Force an array if not already something iterable
  if (typeof obj !== 'object' && !isArray(obj)) {
    /*eslint no-param-reassign:0*/
    obj = [obj];
  }

  if (isArray(obj)) {
    // Iterate over array values
    for (var i = 0, l = obj.length; i < l; i++) {
      fn.call(null, obj[i], i, obj);
    }
  } else {
    // Iterate over object keys
    for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        fn.call(null, obj[key], key, obj);
      }
    }
  }
}

/**
 * Accepts varargs expecting each argument to be an object, then
 * immutably merges the properties of each object and returns result.
 *
 * When multiple objects contain the same key the later object in
 * the arguments list will take precedence.
 *
 * Example:
 *
 * ```js
 * var result = merge({foo: 123}, {foo: 456});
 * console.log(result.foo); // outputs 456
 * ```
 *
 * @param {Object} obj1 Object to merge
 * @returns {Object} Result of all merge properties
 */
function merge(/* obj1, obj2, obj3, ... */) {
  var result = {};
  function assignValue(val, key) {
    if (typeof result[key] === 'object' && typeof val === 'object') {
      result[key] = merge(result[key], val);
    } else {
      result[key] = val;
    }
  }

  for (var i = 0, l = arguments.length; i < l; i++) {
    forEach(arguments[i], assignValue);
  }
  return result;
}

/**
 * Extends object a by mutably adding to it the properties of object b.
 *
 * @param {Object} a The object to be extended
 * @param {Object} b The object to copy properties from
 * @param {Object} thisArg The object to bind function to
 * @return {Object} The resulting value of object a
 */
function extend(a, b, thisArg) {
  forEach(b, function assignValue(val, key) {
    if (thisArg && typeof val === 'function') {
      a[key] = bind(val, thisArg);
    } else {
      a[key] = val;
    }
  });
  return a;
}

module.exports = {
  isArray: isArray,
  isArrayBuffer: isArrayBuffer,
  isFormData: isFormData,
  isArrayBufferView: isArrayBufferView,
  isString: isString,
  isNumber: isNumber,
  isObject: isObject,
  isUndefined: isUndefined,
  isDate: isDate,
  isFile: isFile,
  isBlob: isBlob,
  isFunction: isFunction,
  isStream: isStream,
  isURLSearchParams: isURLSearchParams,
  isStandardBrowserEnv: isStandardBrowserEnv,
  forEach: forEach,
  merge: merge,
  extend: extend,
  trim: trim
};


/***/ }),

/***/ "./node_modules/deep-equal/index.js":
/***/ (function(module, exports, __webpack_require__) {

var pSlice = Array.prototype.slice;
var objectKeys = __webpack_require__("./node_modules/deep-equal/lib/keys.js");
var isArguments = __webpack_require__("./node_modules/deep-equal/lib/is_arguments.js");

var deepEqual = module.exports = function (actual, expected, opts) {
  if (!opts) opts = {};
  // 7.1. All identical values are equivalent, as determined by ===.
  if (actual === expected) {
    return true;

  } else if (actual instanceof Date && expected instanceof Date) {
    return actual.getTime() === expected.getTime();

  // 7.3. Other pairs that do not both pass typeof value == 'object',
  // equivalence is determined by ==.
  } else if (!actual || !expected || typeof actual != 'object' && typeof expected != 'object') {
    return opts.strict ? actual === expected : actual == expected;

  // 7.4. For all other Object pairs, including Array objects, equivalence is
  // determined by having the same number of owned properties (as verified
  // with Object.prototype.hasOwnProperty.call), the same set of keys
  // (although not necessarily the same order), equivalent values for every
  // corresponding key, and an identical 'prototype' property. Note: this
  // accounts for both named and indexed properties on Arrays.
  } else {
    return objEquiv(actual, expected, opts);
  }
}

function isUndefinedOrNull(value) {
  return value === null || value === undefined;
}

function isBuffer (x) {
  if (!x || typeof x !== 'object' || typeof x.length !== 'number') return false;
  if (typeof x.copy !== 'function' || typeof x.slice !== 'function') {
    return false;
  }
  if (x.length > 0 && typeof x[0] !== 'number') return false;
  return true;
}

function objEquiv(a, b, opts) {
  var i, key;
  if (isUndefinedOrNull(a) || isUndefinedOrNull(b))
    return false;
  // an identical 'prototype' property.
  if (a.prototype !== b.prototype) return false;
  //~~~I've managed to break Object.keys through screwy arguments passing.
  //   Converting to array solves the problem.
  if (isArguments(a)) {
    if (!isArguments(b)) {
      return false;
    }
    a = pSlice.call(a);
    b = pSlice.call(b);
    return deepEqual(a, b, opts);
  }
  if (isBuffer(a)) {
    if (!isBuffer(b)) {
      return false;
    }
    if (a.length !== b.length) return false;
    for (i = 0; i < a.length; i++) {
      if (a[i] !== b[i]) return false;
    }
    return true;
  }
  try {
    var ka = objectKeys(a),
        kb = objectKeys(b);
  } catch (e) {//happens when one is a string literal and the other isn't
    return false;
  }
  // having the same number of owned properties (keys incorporates
  // hasOwnProperty)
  if (ka.length != kb.length)
    return false;
  //the same set of keys (although not necessarily the same order),
  ka.sort();
  kb.sort();
  //~~~cheap key test
  for (i = ka.length - 1; i >= 0; i--) {
    if (ka[i] != kb[i])
      return false;
  }
  //equivalent values for every corresponding key, and
  //~~~possibly expensive deep test
  for (i = ka.length - 1; i >= 0; i--) {
    key = ka[i];
    if (!deepEqual(a[key], b[key], opts)) return false;
  }
  return typeof a === typeof b;
}


/***/ }),

/***/ "./node_modules/deep-equal/lib/is_arguments.js":
/***/ (function(module, exports) {

var supportsArgumentsClass = (function(){
  return Object.prototype.toString.call(arguments)
})() == '[object Arguments]';

exports = module.exports = supportsArgumentsClass ? supported : unsupported;

exports.supported = supported;
function supported(object) {
  return Object.prototype.toString.call(object) == '[object Arguments]';
};

exports.unsupported = unsupported;
function unsupported(object){
  return object &&
    typeof object == 'object' &&
    typeof object.length == 'number' &&
    Object.prototype.hasOwnProperty.call(object, 'callee') &&
    !Object.prototype.propertyIsEnumerable.call(object, 'callee') ||
    false;
};


/***/ }),

/***/ "./node_modules/deep-equal/lib/keys.js":
/***/ (function(module, exports) {

exports = module.exports = typeof Object.keys === 'function'
  ? Object.keys : shim;

exports.shim = shim;
function shim (obj) {
  var keys = [];
  for (var key in obj) keys.push(key);
  return keys;
}


/***/ }),

/***/ "./node_modules/detect-browser/index.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(process) {function detect() {
  var nodeVersion = getNodeVersion();
  if (nodeVersion) {
    return nodeVersion;
  } else if (typeof navigator !== 'undefined') {
    return parseUserAgent(navigator.userAgent);
  }

  return null;
}

function detectOS(userAgentString) {
  var rules = getOperatingSystemRules();
  var detected = rules.filter(function (os) {
    return os.rule && os.rule.test(userAgentString);
  })[0];

  return detected ? detected.name : null;
}

function getNodeVersion() {
  var isNode = typeof navigator === 'undefined' && typeof process !== 'undefined';
  return isNode ? {
    name: 'node',
    version: process.version.slice(1),
    os: __webpack_require__("./node_modules/os-browserify/browser.js").type().toLowerCase()
  } : null;
}

function parseUserAgent(userAgentString) {
  var browsers = getBrowserRules();
  if (!userAgentString) {
    return null;
  }

  var detected = browsers.map(function(browser) {
    var match = browser.rule.exec(userAgentString);
    var version = match && match[1].split(/[._]/).slice(0,3);

    if (version && version.length < 3) {
      version = version.concat(version.length == 1 ? [0, 0] : [0]);
    }

    return match && {
      name: browser.name,
      version: version.join('.')
    };
  }).filter(Boolean)[0] || null;

  if (detected) {
    detected.os = detectOS(userAgentString);
  }

  if (/alexa|bot|crawl(er|ing)|facebookexternalhit|feedburner|google web preview|nagios|postrank|pingdom|slurp|spider|yahoo!|yandex/i.test(userAgentString)) {
    detected = detected || {};
    detected.bot = true;
  }

  return detected;
}

function getBrowserRules() {
  return buildRules([
    [ 'aol', /AOLShield\/([0-9\._]+)/ ],
    [ 'edge', /Edge\/([0-9\._]+)/ ],
    [ 'yandexbrowser', /YaBrowser\/([0-9\._]+)/ ],
    [ 'vivaldi', /Vivaldi\/([0-9\.]+)/ ],
    [ 'kakaotalk', /KAKAOTALK\s([0-9\.]+)/ ],
    [ 'samsung', /SamsungBrowser\/([0-9\.]+)/ ],
    [ 'chrome', /(?!Chrom.*OPR)Chrom(?:e|ium)\/([0-9\.]+)(:?\s|$)/ ],
    [ 'phantomjs', /PhantomJS\/([0-9\.]+)(:?\s|$)/ ],
    [ 'crios', /CriOS\/([0-9\.]+)(:?\s|$)/ ],
    [ 'firefox', /Firefox\/([0-9\.]+)(?:\s|$)/ ],
    [ 'fxios', /FxiOS\/([0-9\.]+)/ ],
    [ 'opera', /Opera\/([0-9\.]+)(?:\s|$)/ ],
    [ 'opera', /OPR\/([0-9\.]+)(:?\s|$)$/ ],
    [ 'ie', /Trident\/7\.0.*rv\:([0-9\.]+).*\).*Gecko$/ ],
    [ 'ie', /MSIE\s([0-9\.]+);.*Trident\/[4-7].0/ ],
    [ 'ie', /MSIE\s(7\.0)/ ],
    [ 'bb10', /BB10;\sTouch.*Version\/([0-9\.]+)/ ],
    [ 'android', /Android\s([0-9\.]+)/ ],
    [ 'ios', /Version\/([0-9\._]+).*Mobile.*Safari.*/ ],
    [ 'safari', /Version\/([0-9\._]+).*Safari/ ],
    [ 'facebook', /FBAV\/([0-9\.]+)/],
    [ 'instagram', /Instagram\ ([0-9\.]+)/],
    [ 'ios-webview', /AppleWebKit\/([0-9\.]+).*Mobile/]
  ]);
}

function getOperatingSystemRules() {
  return buildRules([
    [ 'iOS', /iP(hone|od|ad)/ ],
    [ 'Android OS', /Android/ ],
    [ 'BlackBerry OS', /BlackBerry|BB10/ ],
    [ 'Windows Mobile', /IEMobile/ ],
    [ 'Amazon OS', /Kindle/ ],
    [ 'Windows 3.11', /Win16/ ],
    [ 'Windows 95', /(Windows 95)|(Win95)|(Windows_95)/ ],
    [ 'Windows 98', /(Windows 98)|(Win98)/ ],
    [ 'Windows 2000', /(Windows NT 5.0)|(Windows 2000)/ ],
    [ 'Windows XP', /(Windows NT 5.1)|(Windows XP)/ ],
    [ 'Windows Server 2003', /(Windows NT 5.2)/ ],
    [ 'Windows Vista', /(Windows NT 6.0)/ ],
    [ 'Windows 7', /(Windows NT 6.1)/ ],
    [ 'Windows 8', /(Windows NT 6.2)/ ],
    [ 'Windows 8.1', /(Windows NT 6.3)/ ],
    [ 'Windows 10', /(Windows NT 10.0)/ ],
    [ 'Windows ME', /Windows ME/ ],
    [ 'Open BSD', /OpenBSD/ ],
    [ 'Sun OS', /SunOS/ ],
    [ 'Linux', /(Linux)|(X11)/ ],
    [ 'Mac OS', /(Mac_PowerPC)|(Macintosh)/ ],
    [ 'QNX', /QNX/ ],
    [ 'BeOS', /BeOS/ ],
    [ 'OS/2', /OS\/2/ ],
    [ 'Search Bot', /(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp)|(MSNBot)|(Ask Jeeves\/Teoma)|(ia_archiver)/ ]
  ]);
}

function buildRules(ruleTuples) {
  return ruleTuples.map(function(tuple) {
    return {
      name: tuple[0],
      rule: tuple[1]
    };
  });
}

module.exports = {
  detect: detect,
  detectOS: detectOS,
  getNodeVersion: getNodeVersion,
  parseUserAgent: parseUserAgent
};

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/process/browser.js")))

/***/ }),

/***/ "./node_modules/es6-promise/auto.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// This file can be required in Browserify and Node.js for automatic polyfill
// To use it:  require('es6-promise/auto');

module.exports = __webpack_require__("./node_modules/es6-promise/dist/es6-promise.js").polyfill();


/***/ }),

/***/ "./node_modules/es6-promise/dist/es6-promise.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(process, global) {/*!
 * @overview es6-promise - a tiny implementation of Promises/A+.
 * @copyright Copyright (c) 2014 Yehuda Katz, Tom Dale, Stefan Penner and contributors (Conversion to ES6 API by Jake Archibald)
 * @license   Licensed under MIT license
 *            See https://raw.githubusercontent.com/stefanpenner/es6-promise/master/LICENSE
 * @version   v4.2.4+314e4831
 */

(function (global, factory) {
	 true ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(global.ES6Promise = factory());
}(this, (function () { 'use strict';

function objectOrFunction(x) {
  var type = typeof x;
  return x !== null && (type === 'object' || type === 'function');
}

function isFunction(x) {
  return typeof x === 'function';
}



var _isArray = void 0;
if (Array.isArray) {
  _isArray = Array.isArray;
} else {
  _isArray = function (x) {
    return Object.prototype.toString.call(x) === '[object Array]';
  };
}

var isArray = _isArray;

var len = 0;
var vertxNext = void 0;
var customSchedulerFn = void 0;

var asap = function asap(callback, arg) {
  queue[len] = callback;
  queue[len + 1] = arg;
  len += 2;
  if (len === 2) {
    // If len is 2, that means that we need to schedule an async flush.
    // If additional callbacks are queued before the queue is flushed, they
    // will be processed by this flush that we are scheduling.
    if (customSchedulerFn) {
      customSchedulerFn(flush);
    } else {
      scheduleFlush();
    }
  }
};

function setScheduler(scheduleFn) {
  customSchedulerFn = scheduleFn;
}

function setAsap(asapFn) {
  asap = asapFn;
}

var browserWindow = typeof window !== 'undefined' ? window : undefined;
var browserGlobal = browserWindow || {};
var BrowserMutationObserver = browserGlobal.MutationObserver || browserGlobal.WebKitMutationObserver;
var isNode = typeof self === 'undefined' && typeof process !== 'undefined' && {}.toString.call(process) === '[object process]';

// test for web worker but not in IE10
var isWorker = typeof Uint8ClampedArray !== 'undefined' && typeof importScripts !== 'undefined' && typeof MessageChannel !== 'undefined';

// node
function useNextTick() {
  // node version 0.10.x displays a deprecation warning when nextTick is used recursively
  // see https://github.com/cujojs/when/issues/410 for details
  return function () {
    return process.nextTick(flush);
  };
}

// vertx
function useVertxTimer() {
  if (typeof vertxNext !== 'undefined') {
    return function () {
      vertxNext(flush);
    };
  }

  return useSetTimeout();
}

function useMutationObserver() {
  var iterations = 0;
  var observer = new BrowserMutationObserver(flush);
  var node = document.createTextNode('');
  observer.observe(node, { characterData: true });

  return function () {
    node.data = iterations = ++iterations % 2;
  };
}

// web worker
function useMessageChannel() {
  var channel = new MessageChannel();
  channel.port1.onmessage = flush;
  return function () {
    return channel.port2.postMessage(0);
  };
}

function useSetTimeout() {
  // Store setTimeout reference so es6-promise will be unaffected by
  // other code modifying setTimeout (like sinon.useFakeTimers())
  var globalSetTimeout = setTimeout;
  return function () {
    return globalSetTimeout(flush, 1);
  };
}

var queue = new Array(1000);
function flush() {
  for (var i = 0; i < len; i += 2) {
    var callback = queue[i];
    var arg = queue[i + 1];

    callback(arg);

    queue[i] = undefined;
    queue[i + 1] = undefined;
  }

  len = 0;
}

function attemptVertx() {
  try {
    var vertx = Function('return this')().require('vertx');
    vertxNext = vertx.runOnLoop || vertx.runOnContext;
    return useVertxTimer();
  } catch (e) {
    return useSetTimeout();
  }
}

var scheduleFlush = void 0;
// Decide what async method to use to triggering processing of queued callbacks:
if (isNode) {
  scheduleFlush = useNextTick();
} else if (BrowserMutationObserver) {
  scheduleFlush = useMutationObserver();
} else if (isWorker) {
  scheduleFlush = useMessageChannel();
} else if (browserWindow === undefined && "function" === 'function') {
  scheduleFlush = attemptVertx();
} else {
  scheduleFlush = useSetTimeout();
}

function then(onFulfillment, onRejection) {
  var parent = this;

  var child = new this.constructor(noop);

  if (child[PROMISE_ID] === undefined) {
    makePromise(child);
  }

  var _state = parent._state;


  if (_state) {
    var callback = arguments[_state - 1];
    asap(function () {
      return invokeCallback(_state, child, callback, parent._result);
    });
  } else {
    subscribe(parent, child, onFulfillment, onRejection);
  }

  return child;
}

/**
  `Promise.resolve` returns a promise that will become resolved with the
  passed `value`. It is shorthand for the following:

  ```javascript
  let promise = new Promise(function(resolve, reject){
    resolve(1);
  });

  promise.then(function(value){
    // value === 1
  });
  ```

  Instead of writing the above, your code now simply becomes the following:

  ```javascript
  let promise = Promise.resolve(1);

  promise.then(function(value){
    // value === 1
  });
  ```

  @method resolve
  @static
  @param {Any} value value that the returned promise will be resolved with
  Useful for tooling.
  @return {Promise} a promise that will become fulfilled with the given
  `value`
*/
function resolve$1(object) {
  /*jshint validthis:true */
  var Constructor = this;

  if (object && typeof object === 'object' && object.constructor === Constructor) {
    return object;
  }

  var promise = new Constructor(noop);
  resolve(promise, object);
  return promise;
}

var PROMISE_ID = Math.random().toString(36).substring(2);

function noop() {}

var PENDING = void 0;
var FULFILLED = 1;
var REJECTED = 2;

var TRY_CATCH_ERROR = { error: null };

function selfFulfillment() {
  return new TypeError("You cannot resolve a promise with itself");
}

function cannotReturnOwn() {
  return new TypeError('A promises callback cannot return that same promise.');
}

function getThen(promise) {
  try {
    return promise.then;
  } catch (error) {
    TRY_CATCH_ERROR.error = error;
    return TRY_CATCH_ERROR;
  }
}

function tryThen(then$$1, value, fulfillmentHandler, rejectionHandler) {
  try {
    then$$1.call(value, fulfillmentHandler, rejectionHandler);
  } catch (e) {
    return e;
  }
}

function handleForeignThenable(promise, thenable, then$$1) {
  asap(function (promise) {
    var sealed = false;
    var error = tryThen(then$$1, thenable, function (value) {
      if (sealed) {
        return;
      }
      sealed = true;
      if (thenable !== value) {
        resolve(promise, value);
      } else {
        fulfill(promise, value);
      }
    }, function (reason) {
      if (sealed) {
        return;
      }
      sealed = true;

      reject(promise, reason);
    }, 'Settle: ' + (promise._label || ' unknown promise'));

    if (!sealed && error) {
      sealed = true;
      reject(promise, error);
    }
  }, promise);
}

function handleOwnThenable(promise, thenable) {
  if (thenable._state === FULFILLED) {
    fulfill(promise, thenable._result);
  } else if (thenable._state === REJECTED) {
    reject(promise, thenable._result);
  } else {
    subscribe(thenable, undefined, function (value) {
      return resolve(promise, value);
    }, function (reason) {
      return reject(promise, reason);
    });
  }
}

function handleMaybeThenable(promise, maybeThenable, then$$1) {
  if (maybeThenable.constructor === promise.constructor && then$$1 === then && maybeThenable.constructor.resolve === resolve$1) {
    handleOwnThenable(promise, maybeThenable);
  } else {
    if (then$$1 === TRY_CATCH_ERROR) {
      reject(promise, TRY_CATCH_ERROR.error);
      TRY_CATCH_ERROR.error = null;
    } else if (then$$1 === undefined) {
      fulfill(promise, maybeThenable);
    } else if (isFunction(then$$1)) {
      handleForeignThenable(promise, maybeThenable, then$$1);
    } else {
      fulfill(promise, maybeThenable);
    }
  }
}

function resolve(promise, value) {
  if (promise === value) {
    reject(promise, selfFulfillment());
  } else if (objectOrFunction(value)) {
    handleMaybeThenable(promise, value, getThen(value));
  } else {
    fulfill(promise, value);
  }
}

function publishRejection(promise) {
  if (promise._onerror) {
    promise._onerror(promise._result);
  }

  publish(promise);
}

function fulfill(promise, value) {
  if (promise._state !== PENDING) {
    return;
  }

  promise._result = value;
  promise._state = FULFILLED;

  if (promise._subscribers.length !== 0) {
    asap(publish, promise);
  }
}

function reject(promise, reason) {
  if (promise._state !== PENDING) {
    return;
  }
  promise._state = REJECTED;
  promise._result = reason;

  asap(publishRejection, promise);
}

function subscribe(parent, child, onFulfillment, onRejection) {
  var _subscribers = parent._subscribers;
  var length = _subscribers.length;


  parent._onerror = null;

  _subscribers[length] = child;
  _subscribers[length + FULFILLED] = onFulfillment;
  _subscribers[length + REJECTED] = onRejection;

  if (length === 0 && parent._state) {
    asap(publish, parent);
  }
}

function publish(promise) {
  var subscribers = promise._subscribers;
  var settled = promise._state;

  if (subscribers.length === 0) {
    return;
  }

  var child = void 0,
      callback = void 0,
      detail = promise._result;

  for (var i = 0; i < subscribers.length; i += 3) {
    child = subscribers[i];
    callback = subscribers[i + settled];

    if (child) {
      invokeCallback(settled, child, callback, detail);
    } else {
      callback(detail);
    }
  }

  promise._subscribers.length = 0;
}

function tryCatch(callback, detail) {
  try {
    return callback(detail);
  } catch (e) {
    TRY_CATCH_ERROR.error = e;
    return TRY_CATCH_ERROR;
  }
}

function invokeCallback(settled, promise, callback, detail) {
  var hasCallback = isFunction(callback),
      value = void 0,
      error = void 0,
      succeeded = void 0,
      failed = void 0;

  if (hasCallback) {
    value = tryCatch(callback, detail);

    if (value === TRY_CATCH_ERROR) {
      failed = true;
      error = value.error;
      value.error = null;
    } else {
      succeeded = true;
    }

    if (promise === value) {
      reject(promise, cannotReturnOwn());
      return;
    }
  } else {
    value = detail;
    succeeded = true;
  }

  if (promise._state !== PENDING) {
    // noop
  } else if (hasCallback && succeeded) {
    resolve(promise, value);
  } else if (failed) {
    reject(promise, error);
  } else if (settled === FULFILLED) {
    fulfill(promise, value);
  } else if (settled === REJECTED) {
    reject(promise, value);
  }
}

function initializePromise(promise, resolver) {
  try {
    resolver(function resolvePromise(value) {
      resolve(promise, value);
    }, function rejectPromise(reason) {
      reject(promise, reason);
    });
  } catch (e) {
    reject(promise, e);
  }
}

var id = 0;
function nextId() {
  return id++;
}

function makePromise(promise) {
  promise[PROMISE_ID] = id++;
  promise._state = undefined;
  promise._result = undefined;
  promise._subscribers = [];
}

function validationError() {
  return new Error('Array Methods must be provided an Array');
}

var Enumerator = function () {
  function Enumerator(Constructor, input) {
    this._instanceConstructor = Constructor;
    this.promise = new Constructor(noop);

    if (!this.promise[PROMISE_ID]) {
      makePromise(this.promise);
    }

    if (isArray(input)) {
      this.length = input.length;
      this._remaining = input.length;

      this._result = new Array(this.length);

      if (this.length === 0) {
        fulfill(this.promise, this._result);
      } else {
        this.length = this.length || 0;
        this._enumerate(input);
        if (this._remaining === 0) {
          fulfill(this.promise, this._result);
        }
      }
    } else {
      reject(this.promise, validationError());
    }
  }

  Enumerator.prototype._enumerate = function _enumerate(input) {
    for (var i = 0; this._state === PENDING && i < input.length; i++) {
      this._eachEntry(input[i], i);
    }
  };

  Enumerator.prototype._eachEntry = function _eachEntry(entry, i) {
    var c = this._instanceConstructor;
    var resolve$$1 = c.resolve;


    if (resolve$$1 === resolve$1) {
      var _then = getThen(entry);

      if (_then === then && entry._state !== PENDING) {
        this._settledAt(entry._state, i, entry._result);
      } else if (typeof _then !== 'function') {
        this._remaining--;
        this._result[i] = entry;
      } else if (c === Promise$1) {
        var promise = new c(noop);
        handleMaybeThenable(promise, entry, _then);
        this._willSettleAt(promise, i);
      } else {
        this._willSettleAt(new c(function (resolve$$1) {
          return resolve$$1(entry);
        }), i);
      }
    } else {
      this._willSettleAt(resolve$$1(entry), i);
    }
  };

  Enumerator.prototype._settledAt = function _settledAt(state, i, value) {
    var promise = this.promise;


    if (promise._state === PENDING) {
      this._remaining--;

      if (state === REJECTED) {
        reject(promise, value);
      } else {
        this._result[i] = value;
      }
    }

    if (this._remaining === 0) {
      fulfill(promise, this._result);
    }
  };

  Enumerator.prototype._willSettleAt = function _willSettleAt(promise, i) {
    var enumerator = this;

    subscribe(promise, undefined, function (value) {
      return enumerator._settledAt(FULFILLED, i, value);
    }, function (reason) {
      return enumerator._settledAt(REJECTED, i, reason);
    });
  };

  return Enumerator;
}();

/**
  `Promise.all` accepts an array of promises, and returns a new promise which
  is fulfilled with an array of fulfillment values for the passed promises, or
  rejected with the reason of the first passed promise to be rejected. It casts all
  elements of the passed iterable to promises as it runs this algorithm.

  Example:

  ```javascript
  let promise1 = resolve(1);
  let promise2 = resolve(2);
  let promise3 = resolve(3);
  let promises = [ promise1, promise2, promise3 ];

  Promise.all(promises).then(function(array){
    // The array here would be [ 1, 2, 3 ];
  });
  ```

  If any of the `promises` given to `all` are rejected, the first promise
  that is rejected will be given as an argument to the returned promises's
  rejection handler. For example:

  Example:

  ```javascript
  let promise1 = resolve(1);
  let promise2 = reject(new Error("2"));
  let promise3 = reject(new Error("3"));
  let promises = [ promise1, promise2, promise3 ];

  Promise.all(promises).then(function(array){
    // Code here never runs because there are rejected promises!
  }, function(error) {
    // error.message === "2"
  });
  ```

  @method all
  @static
  @param {Array} entries array of promises
  @param {String} label optional string for labeling the promise.
  Useful for tooling.
  @return {Promise} promise that is fulfilled when all `promises` have been
  fulfilled, or rejected if any of them become rejected.
  @static
*/
function all(entries) {
  return new Enumerator(this, entries).promise;
}

/**
  `Promise.race` returns a new promise which is settled in the same way as the
  first passed promise to settle.

  Example:

  ```javascript
  let promise1 = new Promise(function(resolve, reject){
    setTimeout(function(){
      resolve('promise 1');
    }, 200);
  });

  let promise2 = new Promise(function(resolve, reject){
    setTimeout(function(){
      resolve('promise 2');
    }, 100);
  });

  Promise.race([promise1, promise2]).then(function(result){
    // result === 'promise 2' because it was resolved before promise1
    // was resolved.
  });
  ```

  `Promise.race` is deterministic in that only the state of the first
  settled promise matters. For example, even if other promises given to the
  `promises` array argument are resolved, but the first settled promise has
  become rejected before the other promises became fulfilled, the returned
  promise will become rejected:

  ```javascript
  let promise1 = new Promise(function(resolve, reject){
    setTimeout(function(){
      resolve('promise 1');
    }, 200);
  });

  let promise2 = new Promise(function(resolve, reject){
    setTimeout(function(){
      reject(new Error('promise 2'));
    }, 100);
  });

  Promise.race([promise1, promise2]).then(function(result){
    // Code here never runs
  }, function(reason){
    // reason.message === 'promise 2' because promise 2 became rejected before
    // promise 1 became fulfilled
  });
  ```

  An example real-world use case is implementing timeouts:

  ```javascript
  Promise.race([ajax('foo.json'), timeout(5000)])
  ```

  @method race
  @static
  @param {Array} promises array of promises to observe
  Useful for tooling.
  @return {Promise} a promise which settles in the same way as the first passed
  promise to settle.
*/
function race(entries) {
  /*jshint validthis:true */
  var Constructor = this;

  if (!isArray(entries)) {
    return new Constructor(function (_, reject) {
      return reject(new TypeError('You must pass an array to race.'));
    });
  } else {
    return new Constructor(function (resolve, reject) {
      var length = entries.length;
      for (var i = 0; i < length; i++) {
        Constructor.resolve(entries[i]).then(resolve, reject);
      }
    });
  }
}

/**
  `Promise.reject` returns a promise rejected with the passed `reason`.
  It is shorthand for the following:

  ```javascript
  let promise = new Promise(function(resolve, reject){
    reject(new Error('WHOOPS'));
  });

  promise.then(function(value){
    // Code here doesn't run because the promise is rejected!
  }, function(reason){
    // reason.message === 'WHOOPS'
  });
  ```

  Instead of writing the above, your code now simply becomes the following:

  ```javascript
  let promise = Promise.reject(new Error('WHOOPS'));

  promise.then(function(value){
    // Code here doesn't run because the promise is rejected!
  }, function(reason){
    // reason.message === 'WHOOPS'
  });
  ```

  @method reject
  @static
  @param {Any} reason value that the returned promise will be rejected with.
  Useful for tooling.
  @return {Promise} a promise rejected with the given `reason`.
*/
function reject$1(reason) {
  /*jshint validthis:true */
  var Constructor = this;
  var promise = new Constructor(noop);
  reject(promise, reason);
  return promise;
}

function needsResolver() {
  throw new TypeError('You must pass a resolver function as the first argument to the promise constructor');
}

function needsNew() {
  throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.");
}

/**
  Promise objects represent the eventual result of an asynchronous operation. The
  primary way of interacting with a promise is through its `then` method, which
  registers callbacks to receive either a promise's eventual value or the reason
  why the promise cannot be fulfilled.

  Terminology
  -----------

  - `promise` is an object or function with a `then` method whose behavior conforms to this specification.
  - `thenable` is an object or function that defines a `then` method.
  - `value` is any legal JavaScript value (including undefined, a thenable, or a promise).
  - `exception` is a value that is thrown using the throw statement.
  - `reason` is a value that indicates why a promise was rejected.
  - `settled` the final resting state of a promise, fulfilled or rejected.

  A promise can be in one of three states: pending, fulfilled, or rejected.

  Promises that are fulfilled have a fulfillment value and are in the fulfilled
  state.  Promises that are rejected have a rejection reason and are in the
  rejected state.  A fulfillment value is never a thenable.

  Promises can also be said to *resolve* a value.  If this value is also a
  promise, then the original promise's settled state will match the value's
  settled state.  So a promise that *resolves* a promise that rejects will
  itself reject, and a promise that *resolves* a promise that fulfills will
  itself fulfill.


  Basic Usage:
  ------------

  ```js
  let promise = new Promise(function(resolve, reject) {
    // on success
    resolve(value);

    // on failure
    reject(reason);
  });

  promise.then(function(value) {
    // on fulfillment
  }, function(reason) {
    // on rejection
  });
  ```

  Advanced Usage:
  ---------------

  Promises shine when abstracting away asynchronous interactions such as
  `XMLHttpRequest`s.

  ```js
  function getJSON(url) {
    return new Promise(function(resolve, reject){
      let xhr = new XMLHttpRequest();

      xhr.open('GET', url);
      xhr.onreadystatechange = handler;
      xhr.responseType = 'json';
      xhr.setRequestHeader('Accept', 'application/json');
      xhr.send();

      function handler() {
        if (this.readyState === this.DONE) {
          if (this.status === 200) {
            resolve(this.response);
          } else {
            reject(new Error('getJSON: `' + url + '` failed with status: [' + this.status + ']'));
          }
        }
      };
    });
  }

  getJSON('/posts.json').then(function(json) {
    // on fulfillment
  }, function(reason) {
    // on rejection
  });
  ```

  Unlike callbacks, promises are great composable primitives.

  ```js
  Promise.all([
    getJSON('/posts'),
    getJSON('/comments')
  ]).then(function(values){
    values[0] // => postsJSON
    values[1] // => commentsJSON

    return values;
  });
  ```

  @class Promise
  @param {Function} resolver
  Useful for tooling.
  @constructor
*/

var Promise$1 = function () {
  function Promise(resolver) {
    this[PROMISE_ID] = nextId();
    this._result = this._state = undefined;
    this._subscribers = [];

    if (noop !== resolver) {
      typeof resolver !== 'function' && needsResolver();
      this instanceof Promise ? initializePromise(this, resolver) : needsNew();
    }
  }

  /**
  The primary way of interacting with a promise is through its `then` method,
  which registers callbacks to receive either a promise's eventual value or the
  reason why the promise cannot be fulfilled.
   ```js
  findUser().then(function(user){
    // user is available
  }, function(reason){
    // user is unavailable, and you are given the reason why
  });
  ```
   Chaining
  --------
   The return value of `then` is itself a promise.  This second, 'downstream'
  promise is resolved with the return value of the first promise's fulfillment
  or rejection handler, or rejected if the handler throws an exception.
   ```js
  findUser().then(function (user) {
    return user.name;
  }, function (reason) {
    return 'default name';
  }).then(function (userName) {
    // If `findUser` fulfilled, `userName` will be the user's name, otherwise it
    // will be `'default name'`
  });
   findUser().then(function (user) {
    throw new Error('Found user, but still unhappy');
  }, function (reason) {
    throw new Error('`findUser` rejected and we're unhappy');
  }).then(function (value) {
    // never reached
  }, function (reason) {
    // if `findUser` fulfilled, `reason` will be 'Found user, but still unhappy'.
    // If `findUser` rejected, `reason` will be '`findUser` rejected and we're unhappy'.
  });
  ```
  If the downstream promise does not specify a rejection handler, rejection reasons will be propagated further downstream.
   ```js
  findUser().then(function (user) {
    throw new PedagogicalException('Upstream error');
  }).then(function (value) {
    // never reached
  }).then(function (value) {
    // never reached
  }, function (reason) {
    // The `PedgagocialException` is propagated all the way down to here
  });
  ```
   Assimilation
  ------------
   Sometimes the value you want to propagate to a downstream promise can only be
  retrieved asynchronously. This can be achieved by returning a promise in the
  fulfillment or rejection handler. The downstream promise will then be pending
  until the returned promise is settled. This is called *assimilation*.
   ```js
  findUser().then(function (user) {
    return findCommentsByAuthor(user);
  }).then(function (comments) {
    // The user's comments are now available
  });
  ```
   If the assimliated promise rejects, then the downstream promise will also reject.
   ```js
  findUser().then(function (user) {
    return findCommentsByAuthor(user);
  }).then(function (comments) {
    // If `findCommentsByAuthor` fulfills, we'll have the value here
  }, function (reason) {
    // If `findCommentsByAuthor` rejects, we'll have the reason here
  });
  ```
   Simple Example
  --------------
   Synchronous Example
   ```javascript
  let result;
   try {
    result = findResult();
    // success
  } catch(reason) {
    // failure
  }
  ```
   Errback Example
   ```js
  findResult(function(result, err){
    if (err) {
      // failure
    } else {
      // success
    }
  });
  ```
   Promise Example;
   ```javascript
  findResult().then(function(result){
    // success
  }, function(reason){
    // failure
  });
  ```
   Advanced Example
  --------------
   Synchronous Example
   ```javascript
  let author, books;
   try {
    author = findAuthor();
    books  = findBooksByAuthor(author);
    // success
  } catch(reason) {
    // failure
  }
  ```
   Errback Example
   ```js
   function foundBooks(books) {
   }
   function failure(reason) {
   }
   findAuthor(function(author, err){
    if (err) {
      failure(err);
      // failure
    } else {
      try {
        findBoooksByAuthor(author, function(books, err) {
          if (err) {
            failure(err);
          } else {
            try {
              foundBooks(books);
            } catch(reason) {
              failure(reason);
            }
          }
        });
      } catch(error) {
        failure(err);
      }
      // success
    }
  });
  ```
   Promise Example;
   ```javascript
  findAuthor().
    then(findBooksByAuthor).
    then(function(books){
      // found books
  }).catch(function(reason){
    // something went wrong
  });
  ```
   @method then
  @param {Function} onFulfilled
  @param {Function} onRejected
  Useful for tooling.
  @return {Promise}
  */

  /**
  `catch` is simply sugar for `then(undefined, onRejection)` which makes it the same
  as the catch block of a try/catch statement.
  ```js
  function findAuthor(){
  throw new Error('couldn't find that author');
  }
  // synchronous
  try {
  findAuthor();
  } catch(reason) {
  // something went wrong
  }
  // async with promises
  findAuthor().catch(function(reason){
  // something went wrong
  });
  ```
  @method catch
  @param {Function} onRejection
  Useful for tooling.
  @return {Promise}
  */


  Promise.prototype.catch = function _catch(onRejection) {
    return this.then(null, onRejection);
  };

  /**
    `finally` will be invoked regardless of the promise's fate just as native
    try/catch/finally behaves
  
    Synchronous example:
  
    ```js
    findAuthor() {
      if (Math.random() > 0.5) {
        throw new Error();
      }
      return new Author();
    }
  
    try {
      return findAuthor(); // succeed or fail
    } catch(error) {
      return findOtherAuther();
    } finally {
      // always runs
      // doesn't affect the return value
    }
    ```
  
    Asynchronous example:
  
    ```js
    findAuthor().catch(function(reason){
      return findOtherAuther();
    }).finally(function(){
      // author was either found, or not
    });
    ```
  
    @method finally
    @param {Function} callback
    @return {Promise}
  */


  Promise.prototype.finally = function _finally(callback) {
    var promise = this;
    var constructor = promise.constructor;

    return promise.then(function (value) {
      return constructor.resolve(callback()).then(function () {
        return value;
      });
    }, function (reason) {
      return constructor.resolve(callback()).then(function () {
        throw reason;
      });
    });
  };

  return Promise;
}();

Promise$1.prototype.then = then;
Promise$1.all = all;
Promise$1.race = race;
Promise$1.resolve = resolve$1;
Promise$1.reject = reject$1;
Promise$1._setScheduler = setScheduler;
Promise$1._setAsap = setAsap;
Promise$1._asap = asap;

/*global self*/
function polyfill() {
  var local = void 0;

  if (typeof global !== 'undefined') {
    local = global;
  } else if (typeof self !== 'undefined') {
    local = self;
  } else {
    try {
      local = Function('return this')();
    } catch (e) {
      throw new Error('polyfill failed because global object is unavailable in this environment');
    }
  }

  var P = local.Promise;

  if (P) {
    var promiseToString = null;
    try {
      promiseToString = Object.prototype.toString.call(P.resolve());
    } catch (e) {
      // silently ignored
    }

    if (promiseToString === '[object Promise]' && !P.cast) {
      return;
    }
  }

  local.Promise = Promise$1;
}

// Strange compat..
Promise$1.polyfill = polyfill;
Promise$1.Promise = Promise$1;

return Promise$1;

})));



//# sourceMappingURL=es6-promise.map

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/process/browser.js"), __webpack_require__("./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/js-cookie/src/js.cookie.js":
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
 * JavaScript Cookie v2.2.0
 * https://github.com/js-cookie/js-cookie
 *
 * Copyright 2006, 2015 Klaus Hartl & Fagner Brack
 * Released under the MIT license
 */
;(function (factory) {
	var registeredInModuleLoader = false;
	if (true) {
		!(__WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
		registeredInModuleLoader = true;
	}
	if (true) {
		module.exports = factory();
		registeredInModuleLoader = true;
	}
	if (!registeredInModuleLoader) {
		var OldCookies = window.Cookies;
		var api = window.Cookies = factory();
		api.noConflict = function () {
			window.Cookies = OldCookies;
			return api;
		};
	}
}(function () {
	function extend () {
		var i = 0;
		var result = {};
		for (; i < arguments.length; i++) {
			var attributes = arguments[ i ];
			for (var key in attributes) {
				result[key] = attributes[key];
			}
		}
		return result;
	}

	function init (converter) {
		function api (key, value, attributes) {
			var result;
			if (typeof document === 'undefined') {
				return;
			}

			// Write

			if (arguments.length > 1) {
				attributes = extend({
					path: '/'
				}, api.defaults, attributes);

				if (typeof attributes.expires === 'number') {
					var expires = new Date();
					expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
					attributes.expires = expires;
				}

				// We're using "expires" because "max-age" is not supported by IE
				attributes.expires = attributes.expires ? attributes.expires.toUTCString() : '';

				try {
					result = JSON.stringify(value);
					if (/^[\{\[]/.test(result)) {
						value = result;
					}
				} catch (e) {}

				if (!converter.write) {
					value = encodeURIComponent(String(value))
						.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);
				} else {
					value = converter.write(value, key);
				}

				key = encodeURIComponent(String(key));
				key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
				key = key.replace(/[\(\)]/g, escape);

				var stringifiedAttributes = '';

				for (var attributeName in attributes) {
					if (!attributes[attributeName]) {
						continue;
					}
					stringifiedAttributes += '; ' + attributeName;
					if (attributes[attributeName] === true) {
						continue;
					}
					stringifiedAttributes += '=' + attributes[attributeName];
				}
				return (document.cookie = key + '=' + value + stringifiedAttributes);
			}

			// Read

			if (!key) {
				result = {};
			}

			// To prevent the for loop in the first place assign an empty array
			// in case there are no cookies at all. Also prevents odd result when
			// calling "get()"
			var cookies = document.cookie ? document.cookie.split('; ') : [];
			var rdecode = /(%[0-9A-Z]{2})+/g;
			var i = 0;

			for (; i < cookies.length; i++) {
				var parts = cookies[i].split('=');
				var cookie = parts.slice(1).join('=');

				if (!this.json && cookie.charAt(0) === '"') {
					cookie = cookie.slice(1, -1);
				}

				try {
					var name = parts[0].replace(rdecode, decodeURIComponent);
					cookie = converter.read ?
						converter.read(cookie, name) : converter(cookie, name) ||
						cookie.replace(rdecode, decodeURIComponent);

					if (this.json) {
						try {
							cookie = JSON.parse(cookie);
						} catch (e) {}
					}

					if (key === name) {
						result = cookie;
						break;
					}

					if (!key) {
						result[name] = cookie;
					}
				} catch (e) {}
			}

			return result;
		}

		api.set = api;
		api.get = function (key) {
			return api.call(api, key);
		};
		api.getJSON = function () {
			return api.apply({
				json: true
			}, [].slice.call(arguments));
		};
		api.defaults = {};

		api.remove = function (key, attributes) {
			api(key, '', extend(attributes, {
				expires: -1
			}));
		};

		api.withConverter = init;

		return api;
	}

	return init(function () {});
}));


/***/ }),

/***/ "./node_modules/keymirror/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright 2013-2014 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



/**
 * Constructs an enumeration with keys equal to their value.
 *
 * For example:
 *
 *   var COLORS = keyMirror({blue: null, red: null});
 *   var myColor = COLORS.blue;
 *   var isColorValid = !!COLORS[myColor];
 *
 * The last line could not be performed if the values of the generated enum were
 * not equal to their keys.
 *
 *   Input:  {key1: val1, key2: val2}
 *   Output: {key1: key1, key2: key2}
 *
 * @param {object} obj
 * @return {object}
 */
var keyMirror = function(obj) {
  var ret = {};
  var key;
  if (!(obj instanceof Object && !Array.isArray(obj))) {
    throw new Error('keyMirror(...): Argument must be an object.');
  }
  for (key in obj) {
    if (!obj.hasOwnProperty(key)) {
      continue;
    }
    ret[key] = key;
  }
  return ret;
};

module.exports = keyMirror;


/***/ }),

/***/ "./node_modules/object-assign/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/*
object-assign
(c) Sindre Sorhus
@license MIT
*/


/* eslint-disable no-unused-vars */
var getOwnPropertySymbols = Object.getOwnPropertySymbols;
var hasOwnProperty = Object.prototype.hasOwnProperty;
var propIsEnumerable = Object.prototype.propertyIsEnumerable;

function toObject(val) {
	if (val === null || val === undefined) {
		throw new TypeError('Object.assign cannot be called with null or undefined');
	}

	return Object(val);
}

function shouldUseNative() {
	try {
		if (!Object.assign) {
			return false;
		}

		// Detect buggy property enumeration order in older V8 versions.

		// https://bugs.chromium.org/p/v8/issues/detail?id=4118
		var test1 = new String('abc');  // eslint-disable-line no-new-wrappers
		test1[5] = 'de';
		if (Object.getOwnPropertyNames(test1)[0] === '5') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test2 = {};
		for (var i = 0; i < 10; i++) {
			test2['_' + String.fromCharCode(i)] = i;
		}
		var order2 = Object.getOwnPropertyNames(test2).map(function (n) {
			return test2[n];
		});
		if (order2.join('') !== '0123456789') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test3 = {};
		'abcdefghijklmnopqrst'.split('').forEach(function (letter) {
			test3[letter] = letter;
		});
		if (Object.keys(Object.assign({}, test3)).join('') !==
				'abcdefghijklmnopqrst') {
			return false;
		}

		return true;
	} catch (err) {
		// We don't expect any of the above to throw, but better to be safe.
		return false;
	}
}

module.exports = shouldUseNative() ? Object.assign : function (target, source) {
	var from;
	var to = toObject(target);
	var symbols;

	for (var s = 1; s < arguments.length; s++) {
		from = Object(arguments[s]);

		for (var key in from) {
			if (hasOwnProperty.call(from, key)) {
				to[key] = from[key];
			}
		}

		if (getOwnPropertySymbols) {
			symbols = getOwnPropertySymbols(from);
			for (var i = 0; i < symbols.length; i++) {
				if (propIsEnumerable.call(from, symbols[i])) {
					to[symbols[i]] = from[symbols[i]];
				}
			}
		}
	}

	return to;
};


/***/ }),

/***/ "./node_modules/os-browserify/browser.js":
/***/ (function(module, exports) {

exports.endianness = function () { return 'LE' };

exports.hostname = function () {
    if (typeof location !== 'undefined') {
        return location.hostname
    }
    else return '';
};

exports.loadavg = function () { return [] };

exports.uptime = function () { return 0 };

exports.freemem = function () {
    return Number.MAX_VALUE;
};

exports.totalmem = function () {
    return Number.MAX_VALUE;
};

exports.cpus = function () { return [] };

exports.type = function () { return 'Browser' };

exports.release = function () {
    if (typeof navigator !== 'undefined') {
        return navigator.appVersion;
    }
    return '';
};

exports.networkInterfaces
= exports.getNetworkInterfaces
= function () { return {} };

exports.arch = function () { return 'javascript' };

exports.platform = function () { return 'browser' };

exports.tmpdir = exports.tmpDir = function () {
    return '/tmp';
};

exports.EOL = '\n';

exports.homedir = function () {
	return '/'
};


/***/ }),

/***/ "./node_modules/process/browser.js":
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),

/***/ "./node_modules/query-string/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var strictUriEncode = __webpack_require__("./node_modules/strict-uri-encode/index.js");
var objectAssign = __webpack_require__("./node_modules/object-assign/index.js");

function encoderForArrayFormat(opts) {
	switch (opts.arrayFormat) {
		case 'index':
			return function (key, value, index) {
				return value === null ? [
					encode(key, opts),
					'[',
					index,
					']'
				].join('') : [
					encode(key, opts),
					'[',
					encode(index, opts),
					']=',
					encode(value, opts)
				].join('');
			};

		case 'bracket':
			return function (key, value) {
				return value === null ? encode(key, opts) : [
					encode(key, opts),
					'[]=',
					encode(value, opts)
				].join('');
			};

		default:
			return function (key, value) {
				return value === null ? encode(key, opts) : [
					encode(key, opts),
					'=',
					encode(value, opts)
				].join('');
			};
	}
}

function parserForArrayFormat(opts) {
	var result;

	switch (opts.arrayFormat) {
		case 'index':
			return function (key, value, accumulator) {
				result = /\[(\d*)\]$/.exec(key);

				key = key.replace(/\[\d*\]$/, '');

				if (!result) {
					accumulator[key] = value;
					return;
				}

				if (accumulator[key] === undefined) {
					accumulator[key] = {};
				}

				accumulator[key][result[1]] = value;
			};

		case 'bracket':
			return function (key, value, accumulator) {
				result = /(\[\])$/.exec(key);
				key = key.replace(/\[\]$/, '');

				if (!result) {
					accumulator[key] = value;
					return;
				} else if (accumulator[key] === undefined) {
					accumulator[key] = [value];
					return;
				}

				accumulator[key] = [].concat(accumulator[key], value);
			};

		default:
			return function (key, value, accumulator) {
				if (accumulator[key] === undefined) {
					accumulator[key] = value;
					return;
				}

				accumulator[key] = [].concat(accumulator[key], value);
			};
	}
}

function encode(value, opts) {
	if (opts.encode) {
		return opts.strict ? strictUriEncode(value) : encodeURIComponent(value);
	}

	return value;
}

function keysSorter(input) {
	if (Array.isArray(input)) {
		return input.sort();
	} else if (typeof input === 'object') {
		return keysSorter(Object.keys(input)).sort(function (a, b) {
			return Number(a) - Number(b);
		}).map(function (key) {
			return input[key];
		});
	}

	return input;
}

exports.extract = function (str) {
	return str.split('?')[1] || '';
};

exports.parse = function (str, opts) {
	opts = objectAssign({arrayFormat: 'none'}, opts);

	var formatter = parserForArrayFormat(opts);

	// Create an object with no prototype
	// https://github.com/sindresorhus/query-string/issues/47
	var ret = Object.create(null);

	if (typeof str !== 'string') {
		return ret;
	}

	str = str.trim().replace(/^(\?|#|&)/, '');

	if (!str) {
		return ret;
	}

	str.split('&').forEach(function (param) {
		var parts = param.replace(/\+/g, ' ').split('=');
		// Firefox (pre 40) decodes `%3D` to `=`
		// https://github.com/sindresorhus/query-string/pull/37
		var key = parts.shift();
		var val = parts.length > 0 ? parts.join('=') : undefined;

		// missing `=` should be `null`:
		// http://w3.org/TR/2012/WD-url-20120524/#collect-url-parameters
		val = val === undefined ? null : decodeURIComponent(val);

		formatter(decodeURIComponent(key), val, ret);
	});

	return Object.keys(ret).sort().reduce(function (result, key) {
		var val = ret[key];
		if (Boolean(val) && typeof val === 'object' && !Array.isArray(val)) {
			// Sort object keys, not values
			result[key] = keysSorter(val);
		} else {
			result[key] = val;
		}

		return result;
	}, Object.create(null));
};

exports.stringify = function (obj, opts) {
	var defaults = {
		encode: true,
		strict: true,
		arrayFormat: 'none'
	};

	opts = objectAssign(defaults, opts);

	var formatter = encoderForArrayFormat(opts);

	return obj ? Object.keys(obj).sort().map(function (key) {
		var val = obj[key];

		if (val === undefined) {
			return '';
		}

		if (val === null) {
			return encode(key, opts);
		}

		if (Array.isArray(val)) {
			var result = [];

			val.slice().forEach(function (val2) {
				if (val2 === undefined) {
					return;
				}

				result.push(formatter(key, val2, result.length));
			});

			return result.join('&');
		}

		return encode(key, opts) + '=' + encode(val, opts);
	}).filter(function (x) {
		return x.length > 0;
	}).join('&') : '';
};


/***/ }),

/***/ "./node_modules/querystring-es3/decode.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.



// If obj.hasOwnProperty has been overridden, then calling
// obj.hasOwnProperty(prop) will break.
// See: https://github.com/joyent/node/issues/1707
function hasOwnProperty(obj, prop) {
  return Object.prototype.hasOwnProperty.call(obj, prop);
}

module.exports = function(qs, sep, eq, options) {
  sep = sep || '&';
  eq = eq || '=';
  var obj = {};

  if (typeof qs !== 'string' || qs.length === 0) {
    return obj;
  }

  var regexp = /\+/g;
  qs = qs.split(sep);

  var maxKeys = 1000;
  if (options && typeof options.maxKeys === 'number') {
    maxKeys = options.maxKeys;
  }

  var len = qs.length;
  // maxKeys <= 0 means that we should not limit keys count
  if (maxKeys > 0 && len > maxKeys) {
    len = maxKeys;
  }

  for (var i = 0; i < len; ++i) {
    var x = qs[i].replace(regexp, '%20'),
        idx = x.indexOf(eq),
        kstr, vstr, k, v;

    if (idx >= 0) {
      kstr = x.substr(0, idx);
      vstr = x.substr(idx + 1);
    } else {
      kstr = x;
      vstr = '';
    }

    k = decodeURIComponent(kstr);
    v = decodeURIComponent(vstr);

    if (!hasOwnProperty(obj, k)) {
      obj[k] = v;
    } else if (isArray(obj[k])) {
      obj[k].push(v);
    } else {
      obj[k] = [obj[k], v];
    }
  }

  return obj;
};

var isArray = Array.isArray || function (xs) {
  return Object.prototype.toString.call(xs) === '[object Array]';
};


/***/ }),

/***/ "./node_modules/querystring-es3/encode.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.



var stringifyPrimitive = function(v) {
  switch (typeof v) {
    case 'string':
      return v;

    case 'boolean':
      return v ? 'true' : 'false';

    case 'number':
      return isFinite(v) ? v : '';

    default:
      return '';
  }
};

module.exports = function(obj, sep, eq, name) {
  sep = sep || '&';
  eq = eq || '=';
  if (obj === null) {
    obj = undefined;
  }

  if (typeof obj === 'object') {
    return map(objectKeys(obj), function(k) {
      var ks = encodeURIComponent(stringifyPrimitive(k)) + eq;
      if (isArray(obj[k])) {
        return map(obj[k], function(v) {
          return ks + encodeURIComponent(stringifyPrimitive(v));
        }).join(sep);
      } else {
        return ks + encodeURIComponent(stringifyPrimitive(obj[k]));
      }
    }).join(sep);

  }

  if (!name) return '';
  return encodeURIComponent(stringifyPrimitive(name)) + eq +
         encodeURIComponent(stringifyPrimitive(obj));
};

var isArray = Array.isArray || function (xs) {
  return Object.prototype.toString.call(xs) === '[object Array]';
};

function map (xs, f) {
  if (xs.map) return xs.map(f);
  var res = [];
  for (var i = 0; i < xs.length; i++) {
    res.push(f(xs[i], i));
  }
  return res;
}

var objectKeys = Object.keys || function (obj) {
  var res = [];
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) res.push(key);
  }
  return res;
};


/***/ }),

/***/ "./node_modules/querystring-es3/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.decode = exports.parse = __webpack_require__("./node_modules/querystring-es3/decode.js");
exports.encode = exports.stringify = __webpack_require__("./node_modules/querystring-es3/encode.js");


/***/ }),

/***/ "./node_modules/react-sticky/lib/channel.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Channel = function Channel(data) {
  _classCallCheck(this, Channel);

  var listeners = [];
  data = data || {};

  this.subscribe = function (fn) {
    listeners.push(fn);
  };

  this.unsubscribe = function (fn) {
    var idx = listeners.indexOf(fn);
    if (idx !== -1) listeners.splice(idx, 1);
  };

  this.update = function (fn) {
    if (fn) fn(data);
    listeners.forEach(function (l) {
      return l(data);
    });
  };
};

exports.default = Channel;
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/react-sticky/lib/container.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactDom = __webpack_require__("react-dom");

var _reactDom2 = _interopRequireDefault(_reactDom);

var _channel = __webpack_require__("./node_modules/react-sticky/lib/channel.js");

var _channel2 = _interopRequireDefault(_channel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Container = function (_React$Component) {
  _inherits(Container, _React$Component);

  function Container(props) {
    _classCallCheck(this, Container);

    var _this = _possibleConstructorReturn(this, (Container.__proto__ || Object.getPrototypeOf(Container)).call(this, props));

    _this.updateOffset = function (_ref) {
      var inherited = _ref.inherited,
          offset = _ref.offset;

      _this.channel.update(function (data) {
        data.inherited = inherited + offset;
      });
    };

    _this.channel = new _channel2.default({ inherited: 0, offset: 0, node: null });
    return _this;
  }

  _createClass(Container, [{
    key: 'getChildContext',
    value: function getChildContext() {
      return { 'sticky-channel': this.channel };
    }
  }, {
    key: 'componentWillMount',
    value: function componentWillMount() {
      var parentChannel = this.context['sticky-channel'];
      if (parentChannel) parentChannel.subscribe(this.updateOffset);
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      var node = _reactDom2.default.findDOMNode(this);
      this.channel.update(function (data) {
        data.node = node;
      });
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.channel.update(function (data) {
        data.node = null;
      });

      var parentChannel = this.context['sticky-channel'];
      if (parentChannel) parentChannel.unsubscribe(this.updateOffset);
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        this.props,
        this.props.children
      );
    }
  }]);

  return Container;
}(_react2.default.Component);

Container.contextTypes = {
  'sticky-channel': _propTypes2.default.any
};
Container.childContextTypes = {
  'sticky-channel': _propTypes2.default.any
};
exports.default = Container;
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/react-sticky/lib/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Channel = exports.StickyContainer = exports.Sticky = undefined;

var _sticky = __webpack_require__("./node_modules/react-sticky/lib/sticky.js");

var _sticky2 = _interopRequireDefault(_sticky);

var _container = __webpack_require__("./node_modules/react-sticky/lib/container.js");

var _container2 = _interopRequireDefault(_container);

var _channel = __webpack_require__("./node_modules/react-sticky/lib/channel.js");

var _channel2 = _interopRequireDefault(_channel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.Sticky = _sticky2.default;
exports.StickyContainer = _container2.default;
exports.Channel = _channel2.default;
exports.default = _sticky2.default;

/***/ }),

/***/ "./node_modules/react-sticky/lib/sticky.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactDom = __webpack_require__("react-dom");

var _reactDom2 = _interopRequireDefault(_reactDom);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Sticky = function (_React$Component) {
  _inherits(Sticky, _React$Component);

  function Sticky(props) {
    _classCallCheck(this, Sticky);

    var _this = _possibleConstructorReturn(this, (Sticky.__proto__ || Object.getPrototypeOf(Sticky)).call(this, props));

    _this.updateContext = function (_ref) {
      var inherited = _ref.inherited,
          node = _ref.node;

      _this.containerNode = node;
      _this.setState({
        containerOffset: inherited,
        distanceFromBottom: _this.getDistanceFromBottom()
      });
    };

    _this.recomputeState = function () {
      var isSticky = _this.isSticky();
      var height = _this.getHeight();
      var width = _this.getWidth();
      var xOffset = _this.getXOffset();
      var distanceFromBottom = _this.getDistanceFromBottom();
      var hasChanged = _this.state.isSticky !== isSticky;

      _this.setState({ isSticky: isSticky, height: height, width: width, xOffset: xOffset, distanceFromBottom: distanceFromBottom });

      if (hasChanged) {
        if (_this.channel) {
          _this.channel.update(function (data) {
            data.offset = isSticky ? _this.state.height : 0;
          });
        }

        _this.props.onStickyStateChange(isSticky);
      }
    };

    _this.state = {};
    return _this;
  }

  _createClass(Sticky, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      this.channel = this.context['sticky-channel'];
      this.channel.subscribe(this.updateContext);
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.on(['resize', 'scroll', 'touchstart', 'touchmove', 'touchend', 'pageshow', 'load'], this.recomputeState);
      this.recomputeState();
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps() {
      this.recomputeState();
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.off(['resize', 'scroll', 'touchstart', 'touchmove', 'touchend', 'pageshow', 'load'], this.recomputeState);
      this.channel.unsubscribe(this.updateContext);
    }
  }, {
    key: 'getXOffset',
    value: function getXOffset() {
      return this.refs.placeholder.getBoundingClientRect().left;
    }
  }, {
    key: 'getWidth',
    value: function getWidth() {
      return this.refs.placeholder.getBoundingClientRect().width;
    }
  }, {
    key: 'getHeight',
    value: function getHeight() {
      return _reactDom2.default.findDOMNode(this.refs.children).getBoundingClientRect().height;
    }
  }, {
    key: 'getDistanceFromTop',
    value: function getDistanceFromTop() {
      return this.refs.placeholder.getBoundingClientRect().top;
    }
  }, {
    key: 'getDistanceFromBottom',
    value: function getDistanceFromBottom() {
      if (!this.containerNode) return 0;
      return this.containerNode.getBoundingClientRect().bottom;
    }
  }, {
    key: 'isSticky',
    value: function isSticky() {
      if (!this.props.isActive) return false;

      var fromTop = this.getDistanceFromTop();
      var fromBottom = this.getDistanceFromBottom();

      var topBreakpoint = this.state.containerOffset - this.props.topOffset;
      var bottomBreakpoint = this.state.containerOffset + this.props.bottomOffset;

      return fromTop <= topBreakpoint && fromBottom >= bottomBreakpoint;
    }
  }, {
    key: 'on',
    value: function on(events, callback) {
      events.forEach(function (evt) {
        window.addEventListener(evt, callback);
      });
    }
  }, {
    key: 'off',
    value: function off(events, callback) {
      events.forEach(function (evt) {
        window.removeEventListener(evt, callback);
      });
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(newProps, newState) {
      var _this2 = this;

      // Have we changed the number of props?
      var propNames = Object.keys(this.props);
      if (Object.keys(newProps).length != propNames.length) return true;

      // Have we changed any prop values?
      var valuesMatch = propNames.every(function (key) {
        return newProps.hasOwnProperty(key) && newProps[key] === _this2.props[key];
      });
      if (!valuesMatch) return true;

      // Have we changed any state that will always impact rendering?
      var state = this.state;
      if (newState.isSticky !== state.isSticky) return true;

      // If we are sticky, have we changed any state that will impact rendering?
      if (state.isSticky) {
        if (newState.height !== state.height) return true;
        if (newState.width !== state.width) return true;
        if (newState.xOffset !== state.xOffset) return true;
        if (newState.containerOffset !== state.containerOffset) return true;
        if (newState.distanceFromBottom !== state.distanceFromBottom) return true;
      }

      return false;
    }

    /*
     * The special sauce.
     */

  }, {
    key: 'render',
    value: function render() {
      var placeholderStyle = { paddingBottom: 0 };
      var className = this.props.className;

      // To ensure that this component becomes sticky immediately on mobile devices instead
      // of disappearing until the scroll event completes, we add `transform: translateZ(0)`
      // to 'kick' rendering of this element to the GPU
      // @see http://stackoverflow.com/questions/32875046
      var style = _extends({}, { transform: 'translateZ(0)' }, this.props.style);

      if (this.state.isSticky) {
        var _stickyStyle = {
          position: 'fixed',
          top: this.state.containerOffset,
          left: this.state.xOffset,
          width: this.state.width
        };

        var bottomLimit = this.state.distanceFromBottom - this.state.height - this.props.bottomOffset;
        if (this.state.containerOffset > bottomLimit) {
          _stickyStyle.top = bottomLimit;
        }

        placeholderStyle.paddingBottom = this.state.height;

        className += ' ' + this.props.stickyClassName;
        style = _extends({}, style, _stickyStyle, this.props.stickyStyle);
      }

      var _props = this.props,
          topOffset = _props.topOffset,
          isActive = _props.isActive,
          stickyClassName = _props.stickyClassName,
          stickyStyle = _props.stickyStyle,
          bottomOffset = _props.bottomOffset,
          onStickyStateChange = _props.onStickyStateChange,
          props = _objectWithoutProperties(_props, ['topOffset', 'isActive', 'stickyClassName', 'stickyStyle', 'bottomOffset', 'onStickyStateChange']);

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement('div', { ref: 'placeholder', style: placeholderStyle }),
        _react2.default.createElement(
          'div',
          _extends({}, props, { ref: 'children', className: className, style: style }),
          this.props.children
        )
      );
    }
  }]);

  return Sticky;
}(_react2.default.Component);

Sticky.propTypes = {
  isActive: _propTypes2.default.bool,
  className: _propTypes2.default.string,
  style: _propTypes2.default.object,
  stickyClassName: _propTypes2.default.string,
  stickyStyle: _propTypes2.default.object,
  topOffset: _propTypes2.default.number,
  bottomOffset: _propTypes2.default.number,
  onStickyStateChange: _propTypes2.default.func
};
Sticky.defaultProps = {
  isActive: true,
  className: '',
  style: {},
  stickyClassName: 'sticky',
  stickyStyle: {},
  topOffset: 0,
  bottomOffset: 0,
  onStickyStateChange: function onStickyStateChange() {}
};
Sticky.contextTypes = {
  'sticky-channel': _propTypes2.default.any
};
exports.default = Sticky;
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/redux-thunk/es/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
function createThunkMiddleware(extraArgument) {
  return function (_ref) {
    var dispatch = _ref.dispatch,
        getState = _ref.getState;
    return function (next) {
      return function (action) {
        if (typeof action === 'function') {
          return action(dispatch, getState, extraArgument);
        }

        return next(action);
      };
    };
  };
}

var thunk = createThunkMiddleware();
thunk.withExtraArgument = createThunkMiddleware;

/* harmony default export */ __webpack_exports__["default"] = (thunk);

/***/ }),

/***/ "./node_modules/strict-uri-encode/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

module.exports = function (str) {
	return encodeURIComponent(str).replace(/[!'()*]/g, function (c) {
		return '%' + c.charCodeAt(0).toString(16).toUpperCase();
	});
};


/***/ }),

/***/ "./src/components/Captcha/fallback.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__("./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _uxcore = __webpack_require__("@ux/uxcore2");

var _reactIntl = __webpack_require__("react-intl");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Fallback = function (_Component) {
  _inherits(Fallback, _Component);

  function Fallback(props) {
    _classCallCheck(this, Fallback);

    var _this = _possibleConstructorReturn(this, (Fallback.__proto__ || Object.getPrototypeOf(Fallback)).call(this, props));

    _this.state = {
      isRefreshing: true,
      isRefreshed: false
    };

    _this.refresh = _this.refresh.bind(_this);
    _this.handleChange = _this.handleChange.bind(_this);
    _this.handlePlayClick = _this.handlePlayClick.bind(_this);
    _this.handleRefreshClick = _this.handleRefreshClick.bind(_this);
    return _this;
  }

  _createClass(Fallback, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.refresh();
    }

    // Enzyme doesn’t support load/play on media elements.
    /* istanbul ignore next */

  }, {
    key: 'handlePlayClick',
    value: function handlePlayClick() {
      var _props = this.props,
          siteKey = _props.siteKey,
          envPrefix = _props.envPrefix;
      var _state = this.state,
          challenge = _state.challenge,
          refresh = _state.refresh;


      this.audio.src = 'https://captcha.' + envPrefix + 'secureserver.net/api/audio?c=' + siteKey + '&ch=' + challenge + '&refresh=' + refresh;

      this.audio.load();
      this.audio.play();
    }
  }, {
    key: 'handleRefreshClick',
    value: function handleRefreshClick() {
      this.refresh();
    }
  }, {
    key: 'refresh',
    value: function refresh() {
      var _this2 = this;

      var _props2 = this.props,
          siteKey = _props2.siteKey,
          market = _props2.market,
          envPrefix = _props2.envPrefix;


      var url = 'https://captcha.' + envPrefix + 'secureserver.net/api/challenge?c=' + siteKey + '&bootstrap=false&marketId=' + market;

      var options = {
        headers: {
          'Content-Type': 'application/json'
        },
        // Disable CORS so that credentials are not included. Otherwise, captcha
        // service will reject. (Just passing in any string other than 'cors'.)
        mode: ''
      };

      this.setState({
        isRefreshing: true,
        isRefreshed: false,
        refresh: null,
        challenge: null
      }, function () {
        _uxcore.utils.request.get(url, options, function (error, response) {
          if (error || response && response.code !== 1) {
            return void _this2.setState({
              isRefreshing: false
            });
          }

          if (_this2.input) {
            _this2.input.value = '';
          }

          _this2.setState({
            isRefreshed: true,
            isRefreshing: false,
            refresh: new Date().getTime(),
            challenge: response.data.challengeId
          });
        });
      });
    }
  }, {
    key: 'handleChange',
    value: function handleChange(e) {
      var onChange = this.props.onChange;
      var challenge = this.state.challenge;


      onChange({ challenge: challenge, response: e.target.value });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      var _props3 = this.props,
          siteKey = _props3.siteKey,
          className = _props3.className,
          envPrefix = _props3.envPrefix;
      var _state2 = this.state,
          refresh = _state2.refresh,
          challenge = _state2.challenge,
          isRefreshed = _state2.isRefreshed,
          isRefreshing = _state2.isRefreshing;


      var src = '';

      if (isRefreshed) {
        src = 'https://captcha.' + envPrefix + 'secureserver.net/api/image?c=' + siteKey + '&ch=' + challenge + '&refresh=' + refresh;
      }

      var playTitle = _react2.default.createElement(
        _reactIntl.FormattedMessage,
        { id: 'captcha.play' },
        function (txt) {
          return txt;
        }
      );

      var refreshTitle = _react2.default.createElement(
        _reactIntl.FormattedMessage,
        { id: 'captcha.refresh' },
        function (txt) {
          return txt;
        }
      );

      return _react2.default.createElement(
        'div',
        { className: (0, _classnames2.default)('captcha-fallback', className) },
        _react2.default.createElement(
          'div',
          { className: 'captcha-fallback-wrapper', style: { backgroundImage: 'url(' + src + ')' } },
          _react2.default.createElement('audio', {
            ref: function ref(audio) {
              _this3.audio = audio;
            }
          }),
          _react2.default.createElement(
            'div',
            { className: 'captcha-fallback-controls' },
            _react2.default.createElement('input', {
              type: 'text',
              className: 'form-control',
              onChange: this.handleChange,
              ref: function ref(input) {
                _this3.input = input;
              }
            }),
            _react2.default.createElement(
              'span',
              { className: 'req', 'aria-label': 'required' },
              '*'
            ),
            _react2.default.createElement(
              _uxcore.Button,
              {
                disabled: !isRefreshed,
                design: 'tertiaryInline',
                className: 'captcha-fallback-play text-primary',
                onClick: this.handlePlayClick
              },
              _react2.default.createElement('span', { className: 'uxicon uxicon-volume-up', title: playTitle })
            ),
            _react2.default.createElement(
              _uxcore.Button,
              {
                disabled: isRefreshing,
                design: 'tertiaryInline',
                className: 'captcha-fallback-refresh text-primary',
                onClick: this.handleRefreshClick
              },
              _react2.default.createElement('span', { className: 'uxicon uxicon-refresh', title: refreshTitle })
            )
          )
        )
      );
    }
  }]);

  return Fallback;
}(_react.Component);

exports.default = Fallback;


Fallback.propTypes = {
  siteKey: _propTypes2.default.string.isRequired,
  market: _propTypes2.default.string.isRequired,
  envPrefix: _propTypes2.default.string.isRequired,
  onChange: _propTypes2.default.func.isRequired,
  className: _propTypes2.default.string
};

/***/ }),

/***/ "./src/components/Captcha/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.types = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _recaptcha = __webpack_require__("./src/components/Captcha/recaptcha.js");

var _recaptcha2 = _interopRequireDefault(_recaptcha);

var _fallback = __webpack_require__("./src/components/Captcha/fallback.js");

var _fallback2 = _interopRequireDefault(_fallback);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var types = exports.types = {
  RECAPTCHA: 'RECAPTCHA',
  FALLBACK: 'FALLBACK'
};

var Captcha = function (_Component) {
  _inherits(Captcha, _Component);

  function Captcha(props) {
    _classCallCheck(this, Captcha);

    var _this = _possibleConstructorReturn(this, (Captcha.__proto__ || Object.getPrototypeOf(Captcha)).call(this, props));

    var settings = props.settings;
    var useRecaptcha = settings.useRecaptcha;


    _this.state = {
      useRecaptcha: useRecaptcha,
      key: useRecaptcha ? settings.recaptcha.siteKey : settings.captcha.siteKey,
      market: settings.market
    };

    _this.setRef = _this.setRef.bind(_this);
    return _this;
  }

  _createClass(Captcha, [{
    key: 'setRef',
    value: function setRef(element) {
      if (element) {
        this.element = element;
      }
    }
  }, {
    key: 'execute',
    value: function execute() {
      if (this.state.useRecaptcha && this.element) {
        this.element.execute();
      }
    }
  }, {
    key: 'refresh',
    value: function refresh() {
      if (!this.state.useRecaptcha && this.element) {
        this.element.refresh();
      }
    }
  }, {
    key: 'getType',
    value: function getType() {
      return this.state.useRecaptcha ? types.RECAPTCHA : types.FALLBACK;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _state = this.state,
          useRecaptcha = _state.useRecaptcha,
          market = _state.market;
      var _props = this.props,
          onChange = _props.onChange,
          className = _props.className,
          settings = _props.settings;
      var envPrefix = settings.envPrefix;


      var handleChange = function handleChange(values) {
        onChange(_this2.getType(), values);
      };

      var props = {
        envPrefix: envPrefix,
        market: market,
        onChange: handleChange,
        className: className,
        siteKey: useRecaptcha ? settings.recaptcha.siteKey : settings.captcha.siteKey,
        ref: this.setRef
      };

      var Element = useRecaptcha ? _recaptcha2.default : _fallback2.default;

      return _react2.default.createElement(Element, props);
    }
  }]);

  return Captcha;
}(_react.Component);

exports.default = Captcha;


Captcha.propTypes = {
  settings: _propTypes2.default.object.isRequired,
  onChange: _propTypes2.default.func.isRequired,
  className: _propTypes2.default.string
};

/***/ }),

/***/ "./src/components/Captcha/recaptcha.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactGoogleRecaptcha = __webpack_require__("./node_modules/react-google-recaptcha/lib/es/recaptcha-wrapper.js");

var _reactGoogleRecaptcha2 = _interopRequireDefault(_reactGoogleRecaptcha);

var _classnames = __webpack_require__("./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Recaptcha = function (_Component) {
  _inherits(Recaptcha, _Component);

  function Recaptcha() {
    _classCallCheck(this, Recaptcha);

    return _possibleConstructorReturn(this, (Recaptcha.__proto__ || Object.getPrototypeOf(Recaptcha)).apply(this, arguments));
  }

  _createClass(Recaptcha, [{
    key: 'execute',
    value: function execute() {
      this.element.reset();
      this.element.execute();
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          className = _props.className,
          siteKey = _props.siteKey,
          _onChange = _props.onChange;


      return _react2.default.createElement(
        'div',
        { className: (0, _classnames2.default)('captcha-recaptcha', className) },
        _react2.default.createElement(_reactGoogleRecaptcha2.default, {
          sitekey: siteKey,
          onChange: function onChange(recaptcha) {
            return _onChange({ recaptcha: recaptcha });
          },
          size: 'invisible',
          ref: function ref(element) {
            _this2.element = element;
          }
        })
      );
    }
  }]);

  return Recaptcha;
}(_react.Component);

exports.default = Recaptcha;


Recaptcha.propTypes = {
  className: _propTypes2.default.string,
  siteKey: _propTypes2.default.string.isRequired,
  onChange: _propTypes2.default.func.isRequired
};

/***/ }),

/***/ "./src/components/Card/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__("./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Card = function Card(_ref) {
  var className = _ref.className,
      children = _ref.children;
  return _react2.default.createElement(
    'div',
    { className: (0, _classnames2.default)('card ux-card', className) },
    _react2.default.createElement(
      'div',
      { className: 'card-block' },
      children
    )
  );
};

Card.propTypes = {
  className: _propTypes2.default.string,
  children: _propTypes2.default.node
};

exports.default = Card;

/***/ }),

/***/ "./src/components/ContactUs/form.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = __webpack_require__("./node_modules/react-redux/es/index.js");

var _formik = __webpack_require__("./node_modules/formik/dist/formik.es6.js");

var _reactIntl = __webpack_require__("react-intl");

var _classnames = __webpack_require__("./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _uxcore = __webpack_require__("@ux/uxcore2");

var _Captcha = __webpack_require__("./src/components/Captcha/index.js");

var _Captcha2 = _interopRequireDefault(_Captcha);

var _mapSettings = __webpack_require__("./src/components/map-settings.js");

var _validate = __webpack_require__("./src/components/ContactUs/validate.js");

var _validate2 = _interopRequireDefault(_validate);

var _util = __webpack_require__("./src/util.js");

var _util2 = _interopRequireDefault(_util);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DropdownItem = _uxcore.Dropdown.DropdownItem;

var InnerForm = function (_Component) {
  _inherits(InnerForm, _Component);

  function InnerForm(props) {
    _classCallCheck(this, InnerForm);

    var _this = _possibleConstructorReturn(this, (InnerForm.__proto__ || Object.getPrototypeOf(InnerForm)).call(this, props));

    _this.state = {
      canInteract: false
    };

    _this.products = props.settings.env !== 'production' ? [0] : [15, 7, 56, 43, 3, 9, 11, 6, 25, 18, 8, 148, 349, 57, 24, 16, 190, 0];

    _this.handleFormSubmit = _this.handleFormSubmit.bind(_this);
    return _this;
  }

  _createClass(InnerForm, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.props.setFieldValue('captchaType', this.captcha.getType());

      this.setState({
        canInteract: true
      });
    }
  }, {
    key: 'handleFormSubmit',
    value: function handleFormSubmit(e) {
      e.preventDefault();

      if (this.captcha.getType() === _Captcha.types.RECAPTCHA) {
        return void this.captcha.execute();
      }

      this.captcha.refresh();
      this.props.submitForm();
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          values = _props.values,
          touched = _props.touched,
          dirty = _props.dirty,
          errors = _props.errors,
          handleChange = _props.handleChange,
          handleBlur = _props.handleBlur,
          isSubmitting = _props.isSubmitting,
          setFieldValue = _props.setFieldValue,
          setFieldTouched = _props.setFieldTouched,
          submitForm = _props.submitForm,
          settings = _props.settings,
          intl = _props.intl;
      var canInteract = this.state.canInteract;


      var disabled = !canInteract || isSubmitting;
      var hasErrors = !!Object.keys(errors).length;
      var hasProductError = touched.productId && !!errors.productId;
      var hasDescriptionError = touched.description && !!errors.description;

      var handleProductChange = function handleProductChange(e) {
        setFieldValue('productId', e.value);
      };

      var handleProductToggle = function handleProductToggle(open) {
        if (!open) {
          setFieldTouched('productId');
        }
      };

      return _react2.default.createElement(
        'form',
        { onSubmit: this.handleFormSubmit },
        _util2.default.shouldRenderEnglishOnlySupportDisclaimer(settings) && _react2.default.createElement(
          'p',
          { className: 'contactUs-englishOnly' },
          _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'contactUs.englishOnly' })
        ),
        _react2.default.createElement(
          'fieldset',
          {
            className: (0, _classnames2.default)('form-group', { 'has-danger': hasProductError }),
            'data-eid': 'storefront.contact_us.form.product.wrapper'
          },
          _react2.default.createElement(
            'label',
            { htmlFor: 'contactUs-form-product', id: 'label-contactUs-form-product' },
            _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'contactUs.form.labels.product' }),
            '\xA0',
            _react2.default.createElement(
              'span',
              { className: 'req', 'aria-label': 'required' },
              '*'
            )
          ),
          _react2.default.createElement(
            _uxcore.Dropdown,
            {
              id: 'contactUs-form-product',
              type: 'select',
              onChange: handleProductChange,
              onToggle: handleProductToggle,
              selected: values.productId != null ? [this.products.indexOf(values.productId)] : [],
              disabled: disabled,
              required: true
            },
            this.products.map(function (key) {
              return _react2.default.createElement(
                DropdownItem,
                { key: key, value: key },
                _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'contactUs.form.products.' + key })
              );
            })
          ),
          hasProductError && _react2.default.createElement(
            'span',
            { className: 'form-error' },
            _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'contactUs.form.' + errors.productId })
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-md-6' },
            _react2.default.createElement(_uxcore.FormElement, {
              id: 'contactUs-form-name',
              name: 'name',
              label: intl.formatMessage({ id: 'contactUs.form.labels.name' }),
              onBlur: handleBlur,
              onChange: handleChange,
              value: values.name,
              disabled: disabled,
              required: true,
              'data-eid': 'storefront.contact_us.form.name.input',
              validates: touched.name && !errors.name,
              failure: touched.name && errors.name && intl.formatMessage({ id: 'contactUs.form.' + errors.name })
            })
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-6' },
            _react2.default.createElement(_uxcore.FormElement, {
              id: 'contactUs-form-email',
              name: 'email',
              type: 'email',
              label: intl.formatMessage({ id: 'contactUs.form.labels.emailAddress' }),
              onBlur: handleBlur,
              onChange: handleChange,
              value: values.email,
              disabled: disabled,
              required: true,
              'data-eid': 'storefront.contact_us.form.email.input',
              validates: touched.email && !errors.email,
              failure: touched.email && errors.email && intl.formatMessage({ id: 'contactUs.form.' + errors.email })
            })
          )
        ),
        _react2.default.createElement(_uxcore.FormElement, {
          id: 'contactUs-form-subject',
          name: 'subject',
          label: intl.formatMessage({ id: 'contactUs.form.labels.subject' }),
          onBlur: handleBlur,
          onChange: handleChange,
          value: values.subject,
          disabled: disabled,
          required: true,
          'data-eid': 'storefront.contact_us.form.subject.input',
          validates: touched.subject && !errors.subject,
          failure: touched.subject && errors.subject && intl.formatMessage({ id: 'contactUs.form.' + errors.subject })
        }),
        _react2.default.createElement(
          'fieldset',
          { className: (0, _classnames2.default)('form-group', { 'has-danger': hasDescriptionError }) },
          _react2.default.createElement(
            'label',
            { htmlFor: 'contactUs-form-description', id: 'label-contactUs-form-description' },
            _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'contactUs.form.labels.description' }),
            '\xA0',
            _react2.default.createElement(
              'span',
              { className: 'req', 'aria-label': 'required' },
              '*'
            )
          ),
          _react2.default.createElement('textarea', {
            id: 'contactUs-form-description',
            name: 'description',
            'aria-labelledby': 'label-contactUs-form-description',
            'aria-required': 'true',
            className: (0, _classnames2.default)('form-control', { 'form-control-danger': hasDescriptionError }),
            onBlur: handleBlur,
            onChange: handleChange,
            value: values.description,
            disabled: disabled,
            'data-eid': 'storefront.contact_us.form.description.input'
          }),
          hasDescriptionError && _react2.default.createElement(
            'span',
            { className: 'form-error' },
            _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'contactUs.form.' + errors.description })
          )
        ),
        _react2.default.createElement(_Captcha2.default, {
          settings: settings,
          ref: function ref(captcha) {
            _this2.captcha = captcha;
          },
          onChange: function onChange(type, _ref) {
            var recaptcha = _ref.recaptcha,
                challenge = _ref.challenge,
                response = _ref.response;

            setFieldValue('recaptcha', recaptcha);
            setFieldValue('challenge', challenge);
            setFieldValue('response', response);

            if (type === _Captcha.types.RECAPTCHA) {
              submitForm();
            }
          }
        }),
        _react2.default.createElement('input', { type: 'hidden', name: 'captchaType', value: values.captchaType || '' }),
        _react2.default.createElement(
          _uxcore.Button,
          {
            design: 'primary',
            type: 'submit',
            disabled: disabled || hasErrors || !dirty,
            'data-eid': 'storefront.contact_us.form.submit.click'
          },
          isSubmitting && _react2.default.createElement(_uxcore.Spinner, { size: 'sm', inline: true, shade: 'dark' }),
          _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'contactUs.form.button' })
        )
      );
    }
  }]);

  return InnerForm;
}(_react.Component);

InnerForm.propTypes = _extends({}, _util2.default.formikPropTypes, {
  intl: _reactIntl.intlShape.isRequired,
  settings: _propTypes2.default.object.isRequired
});

var Form = function (_Component2) {
  _inherits(Form, _Component2);

  function Form() {
    _classCallCheck(this, Form);

    return _possibleConstructorReturn(this, (Form.__proto__ || Object.getPrototypeOf(Form)).apply(this, arguments));
  }

  _createClass(Form, [{
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          intl = _props2.intl,
          settings = _props2.settings,
          handleSubmit = _props2.handleSubmit,
          shopper = _props2.shopper;


      var initialValues = {};

      if (shopper) {
        initialValues.name = [shopper.firstName, shopper.lastName].join(' ').trim();
      }

      var formProps = {
        onSubmit: handleSubmit,
        initialValues: initialValues,
        validate: _validate2.default,
        render: function render(formikProps) {
          return _react2.default.createElement(InnerForm, _extends({ intl: intl, settings: settings }, formikProps));
        }
      };

      return _react2.default.createElement(
        'div',
        { className: 'contactUs-form' },
        _react2.default.createElement(_formik.Formik, formProps)
      );
    }
  }]);

  return Form;
}(_react.Component);

Form.propTypes = {
  intl: _reactIntl.intlShape.isRequired,
  settings: _propTypes2.default.object.isRequired,
  handleSubmit: _propTypes2.default.func.isRequired,
  shopper: _propTypes2.default.object
};

exports.default = (0, _reactRedux.connect)(_mapSettings.mapStateToProps)((0, _reactIntl.injectIntl)(Form));

/***/ }),

/***/ "./src/components/ContactUs/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = __webpack_require__("./node_modules/react-redux/es/index.js");

var _reactIntl = __webpack_require__("react-intl");

var _uxcore = __webpack_require__("@ux/uxcore2");

var _PageTitle = __webpack_require__("./src/components/PageTitle/index.js");

var _PageTitle2 = _interopRequireDefault(_PageTitle);

var _Card = __webpack_require__("./src/components/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _sidebar = __webpack_require__("./src/components/ContactUs/sidebar.js");

var _sidebar2 = _interopRequireDefault(_sidebar);

var _interstitial = __webpack_require__("./src/components/ContactUs/interstitial.js");

var _interstitial2 = _interopRequireDefault(_interstitial);

var _form = __webpack_require__("./src/components/ContactUs/form.js");

var _form2 = _interopRequireDefault(_form);

var _mapSettings = __webpack_require__("./src/components/map-settings.js");

var _util = __webpack_require__("./src/util.js");

var _util2 = _interopRequireDefault(_util);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var addGrowlMessage = _uxcore.Growl.addGrowlMessage;

var ContactUs = function (_Component) {
  _inherits(ContactUs, _Component);

  function ContactUs() {
    _classCallCheck(this, ContactUs);

    var _this = _possibleConstructorReturn(this, (ContactUs.__proto__ || Object.getPrototypeOf(ContactUs)).apply(this, arguments));

    _this.state = {
      continueAsGuest: false,
      isShopperLoaded: false
    };

    _this.handleFormSubmit = _this.handleFormSubmit.bind(_this);
    _this.handleContinueAsGuest = _this.handleContinueAsGuest.bind(_this);
    return _this;
  }

  _createClass(ContactUs, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var _this2 = this;

      var _props$settings = this.props.settings,
          env = _props$settings.env,
          envPrefix = _props$settings.envPrefix,
          privateLabelId = _props$settings.privateLabelId;

      // Ignore errors from GUI and treat as signed-out.

      _util2.default.getShopper({ env: env, envPrefix: envPrefix, privateLabelId: privateLabelId }, function (__, shopper) {
        _this2.setState({
          isShopperLoaded: true,
          shopper: shopper || {}
        });
      });
    }
  }, {
    key: 'handleFormSubmit',
    value: function handleFormSubmit(values, _ref) {
      var _this3 = this;

      var setSubmitting = _ref.setSubmitting;
      var _props = this.props,
          intl = _props.intl,
          settings = _props.settings;
      var envPrefix = settings.envPrefix,
          privateLabelId = settings.privateLabelId;


      var url = 'https://www.' + envPrefix + 'secureserver.net/api/v1/contact/' + privateLabelId;

      var options = {
        headers: {
          'Content-Type': 'application/json'
        }
      };

      _uxcore.utils.request.post(url, values, options, function (error, response) {
        if (error) {
          addGrowlMessage({
            title: intl.formatMessage({ id: 'contactUs.growls.error.title' }),
            content: intl.formatMessage({ id: 'contactUs.growls.error.content' }),
            icon: 'error',
            fadeTime: 5000
          });

          return void setSubmitting(false);
        }

        _this3.setState({
          incidentId: response.incidentId
        });
      });
    }
  }, {
    key: 'handleContinueAsGuest',
    value: function handleContinueAsGuest() {
      this.setState({
        continueAsGuest: true
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var initialValues = this.props.initialValues;
      var _state = this.state,
          continueAsGuest = _state.continueAsGuest,
          incidentId = _state.incidentId,
          isShopperLoaded = _state.isShopperLoaded,
          shopper = _state.shopper;

      var isSignedIn = isShopperLoaded && !!shopper.shopperId;
      var body = void 0;

      if (!isShopperLoaded) {
        body = _react2.default.createElement(
          'div',
          { className: 'text-center' },
          _react2.default.createElement(_uxcore.Spinner, { inline: true, size: 'lg' })
        );
      } else if (incidentId) {
        body = _react2.default.createElement(_uxcore.Alert, {
          icon: 'success',
          header: _react2.default.createElement(_reactIntl.FormattedHTMLMessage, { id: 'contactUs.confirmation.header' }),
          message: _react2.default.createElement(_reactIntl.FormattedHTMLMessage, { id: 'contactUs.confirmation.message', values: { incidentId: incidentId } }),
          dismissible: false
        });
      } else if (isSignedIn || continueAsGuest) {
        body = _react2.default.createElement(_form2.default, { shopper: shopper, initialValues: initialValues, handleSubmit: this.handleFormSubmit });
      } else {
        body = _react2.default.createElement(_interstitial2.default, { handleContinueAsGuest: this.handleContinueAsGuest });
      }

      return _react2.default.createElement(
        'div',
        { className: 'contactUs' },
        _react2.default.createElement(
          _PageTitle2.default,
          null,
          _react2.default.createElement(
            'h1',
            { className: 'headline-brand' },
            _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'contactUs.title' })
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'container row' },
          _react2.default.createElement(_sidebar2.default, null),
          _react2.default.createElement(
            'div',
            { className: 'col-lg-8' },
            _react2.default.createElement(
              _Card2.default,
              { className: 'contactUs-main' },
              _react2.default.createElement(
                'h2',
                null,
                _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'contactUs.form.title' })
              ),
              body
            )
          )
        ),
        _react2.default.createElement(_uxcore.Growl, null)
      );
    }
  }]);

  return ContactUs;
}(_react.Component);

ContactUs.propTypes = {
  intl: _reactIntl.intlShape.isRequired,
  settings: _propTypes2.default.object.isRequired,
  initialValues: _propTypes2.default.object
};

exports.default = (0, _reactRedux.connect)(_mapSettings.mapStateToProps)((0, _reactIntl.injectIntl)(ContactUs));

/***/ }),

/***/ "./src/components/ContactUs/interstitial.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = __webpack_require__("./node_modules/react-redux/es/index.js");

var _reactIntl = __webpack_require__("react-intl");

var _uxcore = __webpack_require__("@ux/uxcore2");

var _mapSettings = __webpack_require__("./src/components/map-settings.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Interstitial = function (_Component) {
  _inherits(Interstitial, _Component);

  function Interstitial() {
    _classCallCheck(this, Interstitial);

    var _this = _possibleConstructorReturn(this, (Interstitial.__proto__ || Object.getPrototypeOf(Interstitial)).apply(this, arguments));

    _this.state = {
      canInteract: false
    };
    return _this;
  }

  _createClass(Interstitial, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.setState({
        canInteract: true
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          settings = _props.settings,
          handleContinueAsGuest = _props.handleContinueAsGuest;
      var canInteract = this.state.canInteract;
      var hostname = settings.hostname,
          envPrefix = settings.envPrefix,
          privateLabelId = settings.privateLabelId;


      var app = hostname === 'm.' + envPrefix + 'secureserver.net' ? 'm' : 'www';
      var queryString = '?pl_id=' + privateLabelId + '&path=%2fcontact-us&app=' + app;

      return _react2.default.createElement(
        'div',
        { className: 'contactUs-interstitial' },
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-sm-8 offset-sm-2 col-md-6 offset-md-3' },
            _react2.default.createElement(
              'p',
              null,
              _react2.default.createElement(
                _uxcore.Button,
                {
                  design: 'primary',
                  href: 'https://sso.' + envPrefix + 'secureserver.net/' + queryString,
                  'data-eid': 'storefront.contact_us.interstitial.sign_in.click'
                },
                _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'contactUs.interstitial.signIn' })
              )
            ),
            _react2.default.createElement(
              'p',
              null,
              _react2.default.createElement(_reactIntl.FormattedHTMLMessage, {
                id: 'contactUs.interstitial.createPrompt',
                values: { href: 'https://sso.' + envPrefix + 'secureserver.net/v1/account/create' + queryString }
              })
            ),
            _react2.default.createElement(
              'p',
              null,
              _react2.default.createElement(
                _uxcore.Button,
                {
                  design: 'default',
                  onClick: handleContinueAsGuest,
                  'data-eid': 'storefront.contact_us.interstitial.continue_as_guest.click',
                  disabled: !canInteract
                },
                _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'contactUs.interstitial.continueAsGuest' })
              )
            )
          )
        )
      );
    }
  }]);

  return Interstitial;
}(_react.Component);

Interstitial.propTypes = {
  settings: _propTypes2.default.object.isRequired,
  handleContinueAsGuest: _propTypes2.default.func.isRequired
};

exports.default = (0, _reactRedux.connect)(_mapSettings.mapStateToProps)(Interstitial);

/***/ }),

/***/ "./src/components/ContactUs/sidebar.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = __webpack_require__("./node_modules/react-redux/es/index.js");

var _reactIntl = __webpack_require__("react-intl");

var _uxcore = __webpack_require__("@ux/uxcore2");

var _Card = __webpack_require__("./src/components/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _mapSettings = __webpack_require__("./src/components/map-settings.js");

var _util = __webpack_require__("./src/util.js");

var _util2 = _interopRequireDefault(_util);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SideBar = function SideBar(_ref) {
  var settings = _ref.settings;
  var support = settings.support,
      envPrefix = settings.envPrefix,
      privateLabelId = settings.privateLabelId;


  return _react2.default.createElement(
    'div',
    { className: 'contactUs-sidebar col-lg-4' },
    _react2.default.createElement(
      'div',
      { className: 'row' },
      _react2.default.createElement(
        'div',
        { className: 'col-sm-6 col-lg-12' },
        !!support.technical && _react2.default.createElement(
          _Card2.default,
          { className: 'contactUs-sidebar-support' },
          _react2.default.createElement(
            'h2',
            null,
            _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'contactUs.sidebar.support.title' })
          ),
          _util2.default.shouldRenderEnglishOnlySupportDisclaimer(settings) && _react2.default.createElement(
            'p',
            { className: 'contactUs-englishOnly' },
            _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'contactUs.englishOnly' })
          ),
          _react2.default.createElement(
            'p',
            null,
            _react2.default.createElement(
              'span',
              { className: 'contactUs-sidebar-support-label' },
              _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'contactUs.sidebar.support.technical' })
            ),
            _react2.default.createElement(
              'strong',
              { className: 'contactUs-sidebar-support-value' },
              _react2.default.createElement(
                'a',
                { href: 'tel:' + _util2.default.stripNonNumerals(support.technical) },
                support.technical
              )
            )
          )
        )
      ),
      _react2.default.createElement(
        'div',
        { className: 'col-sm-6 col-lg-12' },
        _react2.default.createElement(
          _Card2.default,
          { className: 'contactUs-sidebar-help' },
          _react2.default.createElement(
            'h2',
            null,
            _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'contactUs.sidebar.help.title' })
          ),
          _react2.default.createElement(
            'p',
            null,
            _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'contactUs.sidebar.help.cta' })
          ),
          _react2.default.createElement(
            'p',
            null,
            _react2.default.createElement(
              _uxcore.Button,
              { href: 'https://www.' + envPrefix + 'secureserver.net/help/?pl_id=' + privateLabelId },
              _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'contactUs.sidebar.help.button' })
            )
          )
        )
      )
    )
  );
};

SideBar.propTypes = {
  settings: _propTypes2.default.object.isRequired
};

exports.default = (0, _reactRedux.connect)(_mapSettings.mapStateToProps)(SideBar);

/***/ }),

/***/ "./src/components/ContactUs/validate.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _rccHelpers = __webpack_require__("./node_modules/rcc-helpers/lib/index.js");

var _Captcha = __webpack_require__("./src/components/Captcha/index.js");

exports.default = function (values) {
  var errors = {};

  if (!values.name || values.name.trim() === '') {
    errors.name = 'required';
  }

  if (!values.email || values.email.trim() === '') {
    errors.email = 'required';
  } else if (!_rccHelpers.validation.emailAddress(values.email)) {
    errors.email = 'invalid';
  }

  if (values.productId == null) {
    errors.productId = 'required';
  }

  if (!values.subject || values.subject.trim() === '') {
    errors.subject = 'required';
  }

  if (!values.description || values.description.trim() === '') {
    errors.description = 'required';
  }

  if (values.captchaType === _Captcha.types.FALLBACK && !values.response) {
    errors.response = 'required';
  }

  return errors;
};

/***/ }),

/***/ "./src/components/DomainRegistration/find.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = __webpack_require__("./node_modules/react-redux/es/index.js");

var _domainIqUi = __webpack_require__("./node_modules/@reseller/domain-iq-ui/dist/index.js");

var _domainIqUi2 = _interopRequireDefault(_domainIqUi);

var _uxcore = __webpack_require__("@ux/uxcore2");

var _mapSettings = __webpack_require__("./src/components/map-settings.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var getDppLanguageStrings = function getDppLanguageStrings(messages) {
  var prefix = 'dpp.';
  var dppLanguageStrings = Object.keys(messages).filter(function (key) {
    return key.includes(prefix);
  }).reduce(function (dppMessages, key) {
    var genericKey = key.replace(prefix, '');

    dppMessages[genericKey] = messages[key];

    return dppMessages;
  }, {});

  return dppLanguageStrings;
};

var getInitProps = function getInitProps(settings) {
  var defaultProps = {
    appName: 'findSearch',
    apiKey: 'dpp_search',
    numberOfSpins: '20',
    urlOverride: settings.envPrefix + 'secureserver.net',
    privateLabelId: settings.privateLabelId,
    trackingCode: 'itc_test',
    excludePromo: false,
    eid: 'none',
    redirectToVCart: false,
    showFilters: true,
    showBundles: false,
    showCrossSell: true
  };

  var market = settings.market;
  var currency = settings.currency;
  var language = getDppLanguageStrings(settings.messages);

  var domainToCheck = void 0;

  // istanbul ignore else
  if (settings.isBrowser) {
    var url = new _uxcore.utils.URL(window.location, true);

    domainToCheck = url.query.domainToCheck;
  }

  return _extends({}, defaultProps, {
    language: language,
    market: market,
    currency: currency,
    domainToCheck: domainToCheck
  });
};

var DomainRegistration = function (_Component) {
  _inherits(DomainRegistration, _Component);

  function DomainRegistration() {
    _classCallCheck(this, DomainRegistration);

    return _possibleConstructorReturn(this, (DomainRegistration.__proto__ || Object.getPrototypeOf(DomainRegistration)).apply(this, arguments));
  }

  _createClass(DomainRegistration, [{
    key: 'render',
    value: function render() {
      var settings = this.props.settings;


      settings.isBrowser = this.isBrowser;

      return _react2.default.createElement(_domainIqUi2.default, { initProps: getInitProps(settings) });
    }
  }]);

  return DomainRegistration;
}(_uxcore.Component);

DomainRegistration.propTypes = {
  settings: _propTypes2.default.object.isRequired
};

exports.default = (0, _reactRedux.connect)(_mapSettings.mapStateToProps)(DomainRegistration);

/***/ }),

/***/ "./src/components/DomainRegistration/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DomainRegistration = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = __webpack_require__("./node_modules/react-redux/es/index.js");

var _uxcore = __webpack_require__("@ux/uxcore2");

var _Card = __webpack_require__("./src/components/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _DomainSearch = __webpack_require__("./src/components/DomainSearch/index.js");

var _DomainSearch2 = _interopRequireDefault(_DomainSearch);

var _ProductIcon = __webpack_require__("./src/components/ProductIcon/index.js");

var _ProductIcon2 = _interopRequireDefault(_ProductIcon);

var _selectors = __webpack_require__("./src/components/DomainRegistration/selectors.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DomainRegistration = exports.DomainRegistration = function (_Component) {
  _inherits(DomainRegistration, _Component);

  function DomainRegistration() {
    _classCallCheck(this, DomainRegistration);

    return _possibleConstructorReturn(this, (DomainRegistration.__proto__ || Object.getPrototypeOf(DomainRegistration)).apply(this, arguments));
  }

  _createClass(DomainRegistration, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.breakpoint('mobile', 'phablet', 'tablet', 'desktop', 'large');
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.breakup();
    }
  }, {
    key: 'render',
    value: function render() {
      var isMobile = this.isMobile;
      var product = this.props.product;


      return _react2.default.createElement(
        _Card2.default,
        { className: 'product-card' },
        _react2.default.createElement(
          'div',
          { className: 'row' },
          isMobile && _react2.default.createElement(_DomainSearch2.default, null),
          _react2.default.createElement(
            'div',
            { className: 'col-xs-3' },
            _react2.default.createElement(_ProductIcon2.default, { product: 'domains' })
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-xs-9' },
            _react2.default.createElement(
              'div',
              { className: 'product-details' },
              !isMobile && _react2.default.createElement(_DomainSearch2.default, { className: 'inline' }),
              _react2.default.createElement(
                'h4',
                { className: 'product-title' },
                product.title
              ),
              _react2.default.createElement('div', {
                className: 'product-description',
                dangerouslySetInnerHTML: { __html: product.content }
              })
            )
          )
        )
      );
    }
  }]);

  return DomainRegistration;
}(_uxcore.Component);

DomainRegistration.propTypes = {
  product: _propTypes2.default.shape({
    id: _propTypes2.default.string.isRequired,
    imageId: _propTypes2.default.string.isRequired,
    title: _propTypes2.default.string.isRequired,
    content: _propTypes2.default.string.isRequired,
    salePrice: _propTypes2.default.oneOfType([_propTypes2.default.bool, _propTypes2.default.string]),
    listPrice: _propTypes2.default.string.isRequired,
    term: _propTypes2.default.string.isRequired
  }).isRequired
};

exports.default = (0, _reactRedux.connect)(_selectors.mapStateToProps)(DomainRegistration);

/***/ }),

/***/ "./src/components/DomainRegistration/selectors.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var mapStateToProps = exports.mapStateToProps = function mapStateToProps(state) {
  return {
    product: state.products.find(function (p) {
      return p.id === 'domain';
    })
  };
};

/***/ }),

/***/ "./src/components/DomainRegistration/wrapper.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DomainRegistrationWrapper = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _reactIntl = __webpack_require__("react-intl");

var _PageTitle = __webpack_require__("./src/components/PageTitle/index.js");

var _PageTitle2 = _interopRequireDefault(_PageTitle);

var _ = __webpack_require__("./src/components/DomainRegistration/index.js");

var _2 = _interopRequireDefault(_);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DomainRegistrationWrapper = exports.DomainRegistrationWrapper = function (_Component) {
  _inherits(DomainRegistrationWrapper, _Component);

  function DomainRegistrationWrapper() {
    _classCallCheck(this, DomainRegistrationWrapper);

    return _possibleConstructorReturn(this, (DomainRegistrationWrapper.__proto__ || Object.getPrototypeOf(DomainRegistrationWrapper)).apply(this, arguments));
  }

  _createClass(DomainRegistrationWrapper, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'products domain-registration' },
        _react2.default.createElement(
          _PageTitle2.default,
          null,
          _react2.default.createElement(
            'h1',
            { className: 'headline-brand' },
            _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'products.domain-registration.title' })
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'container row' },
          _react2.default.createElement(_2.default, null),
          _react2.default.createElement('br', null)
        )
      );
    }
  }]);

  return DomainRegistrationWrapper;
}(_react.Component);

exports.default = DomainRegistrationWrapper;

/***/ }),

/***/ "./src/components/DomainSearch/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactIntl = __webpack_require__("react-intl");

var _classnames = __webpack_require__("./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _reactRedux = __webpack_require__("./node_modules/react-redux/es/index.js");

var _uxcore = __webpack_require__("@ux/uxcore2");

var _mapSettings = __webpack_require__("./src/components/map-settings.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DomainSearch = function (_Component) {
  _inherits(DomainSearch, _Component);

  function DomainSearch() {
    _classCallCheck(this, DomainSearch);

    return _possibleConstructorReturn(this, (DomainSearch.__proto__ || Object.getPrototypeOf(DomainSearch)).apply(this, arguments));
  }

  _createClass(DomainSearch, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          intl = _props.intl,
          className = _props.className,
          eidContext = _props.eidContext,
          buttonDesign = _props.buttonDesign,
          settings = _props.settings;
      var envPrefix = settings.envPrefix,
          privateLabelId = settings.privateLabelId,
          useFind = settings.useFind;


      var domainSearchAction = useFind ? 'https://www.' + envPrefix + 'secureserver.net/products/domain-registration/find' : 'https://www.' + envPrefix + 'secureserver.net/domains/search.aspx?checkAvail=1&pl_id=' + privateLabelId + '&iphoneview=1';

      return _react2.default.createElement(
        'form',
        {
          className: (0, _classnames2.default)('search-form', className),
          method: useFind ? 'GET' : 'POST',
          action: domainSearchAction
        },
        useFind && _react2.default.createElement('input', { name: 'plid', type: 'hidden', value: privateLabelId }),
        _react2.default.createElement('input', {
          className: 'form-control search-form-input',
          name: 'domainToCheck',
          placeholder: intl.formatMessage({ id: 'domainSearch.cta' }),
          'data-eid': 'storefront.' + eidContext + '.domain.search.input'
        }),
        _react2.default.createElement(
          _uxcore.Button,
          {
            design: buttonDesign,
            type: 'submit',
            className: 'search-form-button',
            'data-eid': 'storefront.' + eidContext + '.domain.search.button.click'
          },
          _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'domainSearch.label' })
        )
      );
    }
  }]);

  return DomainSearch;
}(_react.Component);

DomainSearch.propTypes = {
  intl: _reactIntl.intlShape.isRequired,
  className: _propTypes2.default.string,
  eidContext: _propTypes2.default.string,
  buttonDesign: _propTypes2.default.string,
  settings: _propTypes2.default.object.isRequired
};

DomainSearch.defaultProps = {
  eidContext: 'product',
  buttonDesign: 'primary'
};

exports.default = (0, _reactRedux.connect)(_mapSettings.mapStateToProps)((0, _reactIntl.injectIntl)(DomainSearch));

/***/ }),

/***/ "./src/components/DomainTransfer/form.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = __webpack_require__("./node_modules/react-redux/es/index.js");

var _reactIntl = __webpack_require__("react-intl");

var _classnames = __webpack_require__("./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _uxcore = __webpack_require__("@ux/uxcore2");

var _mapSettings = __webpack_require__("./src/components/map-settings.js");

var _util = __webpack_require__("./src/util.js");

var _util2 = _interopRequireDefault(_util);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Form = function (_Component) {
  _inherits(Form, _Component);

  function Form() {
    _classCallCheck(this, Form);

    var _this = _possibleConstructorReturn(this, (Form.__proto__ || Object.getPrototypeOf(Form)).apply(this, arguments));

    _this.handleSearchClick = _this.handleSearchClick.bind(_this);
    _this.state = {};
    return _this;
  }

  _createClass(Form, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.setState({
        canInteract: true
      });
    }
  }, {
    key: 'handleSearchClick',
    value: function handleSearchClick(e) {
      e.preventDefault();

      var _props$settings = this.props.settings,
          envPrefix = _props$settings.envPrefix,
          privateLabelId = _props$settings.privateLabelId,
          progId = _props$settings.progId,
          itc = _props$settings.itc;


      global.location = _util2.default.buildDomainTransferUrl({
        envPrefix: envPrefix,
        domain: this.domain.value,
        privateLabelId: privateLabelId,
        progId: progId,
        itc: itc
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          intl = _props.intl,
          className = _props.className;
      var canInteract = this.state.canInteract;


      return _react2.default.createElement(
        'form',
        { className: (0, _classnames2.default)(className, 'search-form'), onSubmit: this.handleSearchClick },
        _react2.default.createElement('input', {
          className: 'form-control search-form-input',
          name: 'domainToCheck',
          placeholder: intl.formatMessage({ id: 'domainTransfer.cta' }),
          'data-eid': 'storefront.product.domain.transfer.input',
          ref: function ref(i) {
            _this2.domain = i;
          }
        }),
        _react2.default.createElement(
          _uxcore.Button,
          {
            design: 'primary',
            type: 'submit',
            className: 'search-form-button',
            'data-eid': 'storefront.product.domain.transfer.button.click',
            disabled: !canInteract
          },
          _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'domainTransfer.label' })
        )
      );
    }
  }]);

  return Form;
}(_react.Component);

Form.propTypes = {
  intl: _reactIntl.intlShape.isRequired,
  settings: _propTypes2.default.object.isRequired,
  className: _propTypes2.default.string
};

exports.default = (0, _reactRedux.connect)(_mapSettings.mapStateToProps)((0, _reactIntl.injectIntl)(Form));
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./src/components/DomainTransfer/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _uxcore = __webpack_require__("@ux/uxcore2");

var _reactIntl = __webpack_require__("react-intl");

var _Card = __webpack_require__("./src/components/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _form = __webpack_require__("./src/components/DomainTransfer/form.js");

var _form2 = _interopRequireDefault(_form);

var _ProductIcon = __webpack_require__("./src/components/ProductIcon/index.js");

var _ProductIcon2 = _interopRequireDefault(_ProductIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DomainTransfer = function (_Component) {
  _inherits(DomainTransfer, _Component);

  function DomainTransfer() {
    _classCallCheck(this, DomainTransfer);

    return _possibleConstructorReturn(this, (DomainTransfer.__proto__ || Object.getPrototypeOf(DomainTransfer)).apply(this, arguments));
  }

  _createClass(DomainTransfer, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.breakpoint('mobile', 'phablet', 'tablet', 'desktop', 'large');

      this.setState({
        canInteract: true
      });
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.breakup();
    }
  }, {
    key: 'render',
    value: function render() {
      var isMobile = this.isMobile;


      return _react2.default.createElement(
        _Card2.default,
        { className: 'product-card domain-transfer' },
        _react2.default.createElement(
          'div',
          { className: 'row' },
          isMobile && _react2.default.createElement(_form2.default, null),
          _react2.default.createElement(
            'div',
            { className: 'col-xs-3' },
            _react2.default.createElement(_ProductIcon2.default, { product: 'domains' })
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-xs-9' },
            !isMobile && _react2.default.createElement(_form2.default, { className: 'inline' }),
            _react2.default.createElement(
              'h4',
              { className: 'product-title' },
              _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'domainTransfer.subtitle' })
            ),
            _react2.default.createElement(
              'div',
              { className: 'product-description' },
              _react2.default.createElement(
                'ul',
                null,
                ['forwarding', 'locking', 'control', 'registration', 'alerts'].map(function (key) {
                  return _react2.default.createElement(
                    'li',
                    { key: key },
                    _react2.default.createElement(
                      'h5',
                      null,
                      _react2.default.createElement(
                        'strong',
                        null,
                        _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'domainTransfer.features.' + key + '.header' })
                      )
                    ),
                    _react2.default.createElement(
                      'p',
                      null,
                      _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'domainTransfer.features.' + key + '.body' })
                    )
                  );
                })
              ),
              _react2.default.createElement(
                'div',
                { className: 'text-muted' },
                _react2.default.createElement(
                  'h5',
                  null,
                  _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'domainTransfer.steps.subtitle' })
                ),
                _react2.default.createElement(
                  'ul',
                  null,
                  ['unlock', 'authorize', 'verify', 'important', 'cancel'].map(function (key) {
                    return _react2.default.createElement(
                      'li',
                      { key: key },
                      _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'domainTransfer.steps.' + key })
                    );
                  })
                )
              )
            )
          )
        )
      );
    }
  }]);

  return DomainTransfer;
}(_uxcore.Component);

exports.default = DomainTransfer;

/***/ }),

/***/ "./src/components/DomainTransfer/wrapper.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DomainTransferWrapper = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _reactIntl = __webpack_require__("react-intl");

var _PageTitle = __webpack_require__("./src/components/PageTitle/index.js");

var _PageTitle2 = _interopRequireDefault(_PageTitle);

var _ = __webpack_require__("./src/components/DomainTransfer/index.js");

var _2 = _interopRequireDefault(_);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DomainTransferWrapper = exports.DomainTransferWrapper = function (_Component) {
  _inherits(DomainTransferWrapper, _Component);

  function DomainTransferWrapper() {
    _classCallCheck(this, DomainTransferWrapper);

    return _possibleConstructorReturn(this, (DomainTransferWrapper.__proto__ || Object.getPrototypeOf(DomainTransferWrapper)).apply(this, arguments));
  }

  _createClass(DomainTransferWrapper, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'products domain-transfer' },
        _react2.default.createElement(
          _PageTitle2.default,
          null,
          _react2.default.createElement(
            'h1',
            { className: 'headline-brand' },
            _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'products.domain-transfer.title' })
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'container row' },
          _react2.default.createElement(_2.default, null)
        )
      );
    }
  }]);

  return DomainTransferWrapper;
}(_react.Component);

exports.default = DomainTransferWrapper;

/***/ }),

/***/ "./src/components/FormattedMarkdownMessage.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _reactRemarkable = __webpack_require__("./node_modules/react-remarkable/dist/index.js");

var _reactRemarkable2 = _interopRequireDefault(_reactRemarkable);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

var FormattedMarkdownMessage = function FormattedMarkdownMessage(props, context) {
  var formatMessage = context.intl.formatMessage;

  var id = props.id,
      values = props.values,
      options = _objectWithoutProperties(props, ['id', 'values']);

  var message = formatMessage({ id: id }, values);

  return _react2.default.createElement(_reactRemarkable2.default, { source: message, options: options });
};

FormattedMarkdownMessage.propTypes = {
  id: _propTypes.string.isRequired,
  values: _propTypes.object
};

FormattedMarkdownMessage.contextTypes = {
  intl: _propTypes.object.isRequired
};

exports.default = FormattedMarkdownMessage;

/***/ }),

/***/ "./src/components/Home/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Home = undefined;

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = __webpack_require__("./node_modules/react-redux/es/index.js");

var _rccHelpers = __webpack_require__("./node_modules/rcc-helpers/lib/index.js");

var _turnkey = __webpack_require__("./src/components/Home/turnkey.js");

var _turnkey2 = _interopRequireDefault(_turnkey);

var _super = __webpack_require__("./src/components/Home/super/index.js");

var _super2 = _interopRequireDefault(_super);

var _mapSettings = __webpack_require__("./src/components/map-settings.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Home = exports.Home = function Home(_ref) {
  var settings = _ref.settings;

  return settings.resellerTypeId === _rccHelpers.enums.resellerType.SUPER ? _react2.default.createElement(_super2.default, null) : _react2.default.createElement(_turnkey2.default, null);
};

Home.propTypes = {
  settings: _propTypes2.default.object.isRequired
};

exports.default = (0, _reactRedux.connect)(_mapSettings.mapStateToProps)(Home);

/***/ }),

/***/ "./src/components/Home/super/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Super = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactIntl = __webpack_require__("react-intl");

var _reactRedux = __webpack_require__("./node_modules/react-redux/es/index.js");

var _uxcore = __webpack_require__("@ux/uxcore2");

var _ProductCard = __webpack_require__("./src/components/ProductCard/index.js");

var _ProductCard2 = _interopRequireDefault(_ProductCard);

var _selectors = __webpack_require__("./src/components/Home/super/selectors.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Super = exports.Super = function (_Component) {
  _inherits(Super, _Component);

  function Super() {
    _classCallCheck(this, Super);

    return _possibleConstructorReturn(this, (Super.__proto__ || Object.getPrototypeOf(Super)).apply(this, arguments));
  }

  _createClass(Super, [{
    key: 'render',
    value: function render() {
      var products = this.props.products;


      return _react2.default.createElement(
        'div',
        { className: 'home super' },
        _react2.default.createElement(
          'div',
          { className: 'home-hero bg-primary-o text-center' },
          _react2.default.createElement(
            'div',
            { className: 'container' },
            _react2.default.createElement(
              'h1',
              { className: 'headline-brand' },
              _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'superHome.hero.title' })
            ),
            _react2.default.createElement(
              'p',
              null,
              _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'superHome.hero.subtitle' })
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'home-products' },
          _react2.default.createElement(
            'div',
            { className: 'container' },
            _react2.default.createElement(
              'div',
              { className: 'row' },
              products.map(function (product) {
                return _react2.default.createElement(
                  'div',
                  { key: product.id, className: 'col-lg-6' },
                  _react2.default.createElement(_ProductCard2.default, { product: product })
                );
              })
            )
          )
        ),
        _react2.default.createElement(_uxcore.Growl, null)
      );
    }
  }]);

  return Super;
}(_react.Component);

Super.propTypes = {
  products: _propTypes2.default.array.isRequired
};

exports.default = (0, _reactRedux.connect)(_selectors.mapStateToProps)(Super);

/***/ }),

/***/ "./src/components/Home/super/selectors.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var mapStateToProps = exports.mapStateToProps = function mapStateToProps(state) {
  return {
    settings: state.settings,
    products: state.products
  };
};

/***/ }),

/***/ "./src/components/Home/turnkey.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Turnkey = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _reactIntl = __webpack_require__("react-intl");

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = __webpack_require__("./node_modules/react-redux/es/index.js");

var _DomainSearch = __webpack_require__("./src/components/DomainSearch/index.js");

var _DomainSearch2 = _interopRequireDefault(_DomainSearch);

var _ProductIcon = __webpack_require__("./src/components/ProductIcon/index.js");

var _ProductIcon2 = _interopRequireDefault(_ProductIcon);

var _mapSettings = __webpack_require__("./src/components/map-settings.js");

var _util = __webpack_require__("./src/util.js");

var _util2 = _interopRequireDefault(_util);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var productCallouts = [{
  key: 'hosting'
}, {
  key: 'email'
}, {
  key: 'domains',
  productId: 'domain-registration'
}];

var Turnkey = exports.Turnkey = function (_Component) {
  _inherits(Turnkey, _Component);

  function Turnkey() {
    _classCallCheck(this, Turnkey);

    return _possibleConstructorReturn(this, (Turnkey.__proto__ || Object.getPrototypeOf(Turnkey)).apply(this, arguments));
  }

  _createClass(Turnkey, [{
    key: 'render',
    value: function render() {
      var settings = this.props.settings;
      var displayName = settings.displayName,
          privateLabelId = settings.privateLabelId,
          support = settings.support;


      return _react2.default.createElement(
        'div',
        { className: 'home' },
        _react2.default.createElement(
          'div',
          { className: 'home-hero bg-primary-o text-center' },
          _react2.default.createElement(
            'div',
            { className: 'container' },
            _react2.default.createElement(
              'h1',
              { className: 'headline-brand' },
              _react2.default.createElement(_reactIntl.FormattedMessage, {
                id: 'home.hero.title',
                values: { displayName: displayName }
              })
            ),
            _react2.default.createElement(
              'p',
              null,
              _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'home.hero.subtitle' })
            ),
            _react2.default.createElement(_DomainSearch2.default, { eidContext: 'home', buttonDesign: 'purchase' })
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'home-products' },
          _react2.default.createElement(
            'div',
            { className: 'container' },
            _react2.default.createElement(
              'h2',
              { className: 'sr-only' },
              _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'home.products.title' })
            ),
            _react2.default.createElement(
              'div',
              { className: 'row' },
              productCallouts.map(function (productCallout) {
                return _react2.default.createElement(
                  'div',
                  { key: productCallout.key, className: 'col-md-4 text-center home-products-product' },
                  _react2.default.createElement(_ProductIcon2.default, { product: productCallout.key }),
                  _react2.default.createElement(
                    'h3',
                    null,
                    _react2.default.createElement(
                      'a',
                      {
                        href: '/products/' + (productCallout.productId || productCallout.key) + '?plid=' + privateLabelId,
                        'data-eid': 'storefront.home.product_highlights.' + productCallout.key + '.link.click'
                      },
                      _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'products.' + productCallout.key + '.title' })
                    )
                  ),
                  _react2.default.createElement(
                    'p',
                    null,
                    _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'products.' + productCallout.key + '.description' })
                  )
                );
              })
            )
          )
        ),
        !!support.technical && _react2.default.createElement(
          'div',
          { className: 'home-contact bg-faded text-center' },
          _react2.default.createElement(
            'div',
            { className: 'container' },
            _react2.default.createElement(
              'div',
              { className: 'home-contact-info' },
              _react2.default.createElement(
                'h2',
                null,
                _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'home.contact.title' })
              ),
              _react2.default.createElement(
                'p',
                null,
                _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'home.contact.body' })
              ),
              _react2.default.createElement(
                'p',
                { className: 'lead' },
                _react2.default.createElement(_reactIntl.FormattedHTMLMessage, {
                  id: 'home.contact.cta',
                  values: {
                    supportPhoneNumber: support.technical,
                    clickableSupportPhoneNumber: _util2.default.stripNonNumerals(support.technical)
                  }
                })
              ),
              _util2.default.shouldRenderEnglishOnlySupportDisclaimer(settings) && _react2.default.createElement(
                'p',
                { className: 'contactUs-englishOnly' },
                _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'contactUs.englishOnly' })
              )
            )
          )
        )
      );
    }
  }]);

  return Turnkey;
}(_react.Component);

Turnkey.propTypes = {
  settings: _propTypes2.default.object.isRequired
};

exports.default = (0, _reactRedux.connect)(_mapSettings.mapStateToProps)(Turnkey);

/***/ }),

/***/ "./src/components/LegalAgreements/agreement.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LegalAgreement = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactIntl = __webpack_require__("react-intl");

var _reactRedux = __webpack_require__("./node_modules/react-redux/es/index.js");

var _reactRouter = __webpack_require__("./node_modules/react-router/es/index.js");

var _uxcore = __webpack_require__("@ux/uxcore2");

var _PageTitle = __webpack_require__("./src/components/PageTitle/index.js");

var _PageTitle2 = _interopRequireDefault(_PageTitle);

var _Card = __webpack_require__("./src/components/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _selectors = __webpack_require__("./src/components/LegalAgreements/selectors.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var LegalAgreement = exports.LegalAgreement = function (_Component) {
  _inherits(LegalAgreement, _Component);

  function LegalAgreement(props) {
    _classCallCheck(this, LegalAgreement);

    var _this = _possibleConstructorReturn(this, (LegalAgreement.__proto__ || Object.getPrototypeOf(LegalAgreement)).call(this, props));

    _this.handleCloseDisclaimer = _this.handleCloseDisclaimer.bind(_this);

    _this.state = {
      showDisclaimer: !props.settings.market.startsWith('en-')
    };
    return _this;
  }

  _createClass(LegalAgreement, [{
    key: 'handleCloseDisclaimer',
    value: function handleCloseDisclaimer() {
      this.setState({
        showDisclaimer: false
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          settings = _props.settings,
          agreement = _props.agreement;


      if (!agreement) {
        return _react2.default.createElement(_reactRouter.Redirect, { to: '/not-found?plid=' + settings.privateLabelId });
      }

      return _react2.default.createElement(
        'div',
        { className: 'legalAgreements' },
        _react2.default.createElement(
          _PageTitle2.default,
          null,
          _react2.default.createElement(
            'h1',
            { className: 'headline-brand' },
            _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'legalAgreements.title' })
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'container row' },
          _react2.default.createElement(
            _Card2.default,
            null,
            _react2.default.createElement('div', { dangerouslySetInnerHTML: { __html: agreement.body } })
          )
        ),
        this.state.showDisclaimer && _react2.default.createElement(
          _uxcore.Modal,
          { onClose: this.handleCloseDisclaimer },
          _react2.default.createElement(
            'p',
            null,
            _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'legalAgreements.translationDisclaimer.body' })
          ),
          _react2.default.createElement(
            _uxcore.Button,
            { onClick: this.handleCloseDisclaimer },
            _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'legalAgreements.translationDisclaimer.action' })
          )
        )
      );
    }
  }]);

  return LegalAgreement;
}(_react.Component);

LegalAgreement.propTypes = {
  settings: _propTypes2.default.object.isRequired,
  agreement: _propTypes2.default.object
};

exports.default = (0, _reactRedux.connect)(_selectors.mapStateToProps)(LegalAgreement);

/***/ }),

/***/ "./src/components/LegalAgreements/data.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var china = ['zh-CN'];

var chinaAndOtherAsiaMarkets = ['zh-CN', 'zh-HK', 'en-HK', 'id-ID', 'ja-JP', 'ms-MY', 'fil-PH', 'ko-KR', 'zh-TW', 'th-TH', 'vi-VN', 'zh-SG'];

var agreement = 'agreement';
var policy = 'policy';

exports.default = [{
  key: 'universal-terms-of-service-agreement',
  id: 'utos'
}, {
  key: 'certificate-services-agreement',
  type: agreement,
  id: '26864'
}, {
  key: 'change-of-registrant-agreement',
  type: agreement,
  id: 'domain_nc'
}, {
  key: 'domain-name-appraisal-agreement',
  type: agreement,
  id: 'appraisal_sa'
}, {
  key: 'domain-name-proxy-agreement',
  type: agreement,
  id: 'domain_nameproxy'
}, {
  key: 'domain-name-transfer-agreement',
  type: agreement,
  id: 'transfer_sa'
}, {
  key: 'domain-name-registration-agreement',
  type: agreement,
  id: 'reg_sa'
}, {
  key: 'email-marketing-services',
  type: agreement,
  id: '15991',
  suppress: function suppress(market) {
    return chinaAndOtherAsiaMarkets.indexOf(market) > -1;
  }
}, {
  key: 'hosting-agreement',
  type: agreement,
  id: 'hosting_sa',
  suppress: function suppress(market) {
    return china.indexOf(market) > -1;
  }
}, {
  key: 'marketing-applications-agreement',
  type: agreement,
  id: '8310',
  suppress: function suppress(market) {
    return chinaAndOtherAsiaMarkets.indexOf(market) > -1;
  }
}, {
  key: 'online-store-quick-shopping-cart-agreement',
  type: agreement,
  id: 'qsc_eula',
  suppress: function suppress(market) {
    return chinaAndOtherAsiaMarkets.indexOf(market) > -1;
  }
}, {
  key: 'professional-web-services-agreement',
  type: agreement,
  id: '7911',
  suppress: function suppress(market) {
    return chinaAndOtherAsiaMarkets.indexOf(market) > -1;
  }
}, {
  key: 'website-builder-v7-service-agreement',
  type: agreement,
  id: 'wst_eula',
  suppress: function suppress(market) {
    return china.indexOf(market) > -1;
  }
}, {
  key: 'website-security-terms-of-use',
  type: agreement,
  id: '26464'
}, {
  key: 'workspace-service-agreement',
  type: agreement,
  id: '7973',
  suppress: function suppress(market) {
    return chinaAndOtherAsiaMarkets.indexOf(market) > -1;
  }
}, {
  key: 'privacy-policy',
  type: policy,
  id: 'privacy'
}, {
  key: 'subpoena-policy-attorney-tips',
  type: policy,
  id: '5505'
}, {
  key: 'dispute-on-transfer-away-form',
  type: policy,
  id: 'dispute_on_transfer_away_form'
}, {
  key: 'uniform-domain-name-dispute-resolution-policy',
  type: policy,
  id: 'uniform_domain'
}, {
  key: 'registrant-rights',
  type: policy,
  href: 'http://www.icann.org/en/resources/registrars/registrant-rights/'
}, {
  key: 'registrar-transfer-dispute-resolution-policy',
  type: policy,
  href: 'http://www.icann.org/transfers/dispute-policy-12jul04.htm'
}, {
  key: 'trademark-copyright-infringement',
  type: policy,
  id: 'tradmark_copy'
}, {
  key: 'refund-policy',
  type: policy,
  id: '19963'
}, {
  key: 'statement-of-support',
  type: policy,
  id: '20240'
}, {
  key: 'data-processing-addendum',
  type: policy,
  id: '27912'
}];

/***/ }),

/***/ "./src/components/LegalAgreements/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _reactIntl = __webpack_require__("react-intl");

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = __webpack_require__("./node_modules/react-redux/es/index.js");

var _PageTitle = __webpack_require__("./src/components/PageTitle/index.js");

var _PageTitle2 = _interopRequireDefault(_PageTitle);

var _Card = __webpack_require__("./src/components/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _data = __webpack_require__("./src/components/LegalAgreements/data.js");

var _data2 = _interopRequireDefault(_data);

var _mapSettings = __webpack_require__("./src/components/map-settings.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var generateLink = function generateLink(key, privateLabelId) {
  var props = {
    className: 'legalAgreements-link'
  };

  var token = _data2.default.find(function (d) {
    return d.key === key;
  });

  if (token.href) {
    props.href = token.href;
    props.target = '_blank';
  } else {
    props.href = '/legal-agreement?id=' + token.id + '&pl_id=' + privateLabelId;
  }

  return _react2.default.createElement(
    'a',
    props,
    _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'legalAgreements.documentTitles.' + key })
  );
};

var LegalAgreements = function LegalAgreements(_ref) {
  var settings = _ref.settings;
  return _react2.default.createElement(
    'div',
    { className: 'legalAgreements' },
    _react2.default.createElement(
      _PageTitle2.default,
      null,
      _react2.default.createElement(
        'h1',
        { className: 'headline-brand' },
        _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'legalAgreements.title' })
      )
    ),
    _react2.default.createElement(
      'div',
      { className: 'container row' },
      _react2.default.createElement(
        'div',
        { className: 'col-md-12 legalAgreements-main' },
        _react2.default.createElement(
          _Card2.default,
          null,
          _react2.default.createElement(
            'h2',
            null,
            _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'legalAgreements.fullTitle', values: { displayName: settings.displayName } })
          ),
          _react2.default.createElement(
            'p',
            null,
            _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'legalAgreements.description', values: { displayName: settings.displayName } })
          ),
          _react2.default.createElement(
            'p',
            { className: 'legalAgreements-utos' },
            generateLink('universal-terms-of-service-agreement', settings.privateLabelId)
          )
        )
      ),
      ['agreement', 'policy'].map(function (type) {
        return _react2.default.createElement(
          'div',
          { key: type, className: 'col-md-6 legalAgreements-' + type },
          _react2.default.createElement(
            _Card2.default,
            null,
            _react2.default.createElement(
              'h2',
              null,
              _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'legalAgreements.types.' + type + '.title' })
            ),
            _react2.default.createElement(
              'ul',
              null,
              _data2.default.filter(function (d) {
                return d.type === type && !(d.suppress && d.suppress(settings.market));
              }).map(function (document) {
                return _react2.default.createElement(
                  'li',
                  { key: document.key },
                  generateLink(document.key, settings.privateLabelId)
                );
              })
            )
          )
        );
      })
    )
  );
};

LegalAgreements.propTypes = {
  settings: _propTypes2.default.object.isRequired
};

exports.default = (0, _reactRedux.connect)(_mapSettings.mapStateToProps)(LegalAgreements);

/***/ }),

/***/ "./src/components/LegalAgreements/selectors.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var mapStateToProps = exports.mapStateToProps = function mapStateToProps(state) {
  return {
    settings: state.settings,
    agreement: state.legalAgreement
  };
};

/***/ }),

/***/ "./src/components/NotFound/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactIntl = __webpack_require__("react-intl");

var _reactRedux = __webpack_require__("./node_modules/react-redux/es/index.js");

var _PageTitle = __webpack_require__("./src/components/PageTitle/index.js");

var _PageTitle2 = _interopRequireDefault(_PageTitle);

var _Card = __webpack_require__("./src/components/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _mapSettings = __webpack_require__("./src/components/map-settings.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NotFound = function NotFound(_ref) {
  var settings = _ref.settings;
  return _react2.default.createElement(
    'div',
    { className: 'notFound' },
    _react2.default.createElement(
      _PageTitle2.default,
      null,
      _react2.default.createElement(
        'h1',
        { className: 'headline-brand' },
        _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'notFound.header' })
      )
    ),
    _react2.default.createElement(
      'div',
      { className: 'container row' },
      _react2.default.createElement(
        _Card2.default,
        null,
        _react2.default.createElement(
          'p',
          null,
          _react2.default.createElement(_reactIntl.FormattedHTMLMessage, {
            id: 'notFound.message',
            values: { href: '/?plid=' + settings.privateLabelId }
          })
        )
      )
    )
  );
};

NotFound.propTypes = {
  settings: _propTypes2.default.object.isRequired
};

exports.default = (0, _reactRedux.connect)(_mapSettings.mapStateToProps)(NotFound);

/***/ }),

/***/ "./src/components/PageTitle/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PageTitle = function PageTitle(_ref) {
  var children = _ref.children;
  return _react2.default.createElement(
    'div',
    { className: 'ux-pagetitle' },
    _react2.default.createElement(
      'div',
      { className: 'container' },
      children
    )
  );
};

PageTitle.propTypes = {
  children: _propTypes2.default.node
};

exports.default = PageTitle;

/***/ }),

/***/ "./src/components/ProductCard/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ProductCard = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__("./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _camelcase = __webpack_require__("./node_modules/camelcase/index.js");

var _camelcase2 = _interopRequireDefault(_camelcase);

var _reactIntl = __webpack_require__("react-intl");

var _reactRedux = __webpack_require__("./node_modules/react-redux/es/index.js");

var _uxcore = __webpack_require__("@ux/uxcore2");

var _rccHelpers = __webpack_require__("./node_modules/rcc-helpers/lib/index.js");

var _Card = __webpack_require__("./src/components/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _ProductIcon = __webpack_require__("./src/components/ProductIcon/index.js");

var _ProductIcon2 = _interopRequireDefault(_ProductIcon);

var _util = __webpack_require__("./src/util.js");

var _util2 = _interopRequireDefault(_util);

var _mapSettings = __webpack_require__("./src/components/map-settings.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var addGrowlMessage = _uxcore.Growl.addGrowlMessage;


var taxAndFeeKeys = Object.keys(_rccHelpers.enums.taxesAndFees);

var iconIdMap = {
  1: 'domains',
  3: 'hosting',
  4: 'email',
  6: 'websiteBuilder',
  7: 'seo',
  33: 'ssl',
  46: 'wordpress'
};

var mapImageId = function mapImageId(imageId) {
  return iconIdMap[imageId] || (0, _camelcase2.default)(imageId);
};

var ProductCard = exports.ProductCard = function (_Component) {
  _inherits(ProductCard, _Component);

  function ProductCard() {
    _classCallCheck(this, ProductCard);

    var _this = _possibleConstructorReturn(this, (ProductCard.__proto__ || Object.getPrototypeOf(ProductCard)).apply(this, arguments));

    _this.postToCart = function () {
      var _this$props$settings = _this.props.settings,
          envPrefix = _this$props$settings.envPrefix,
          privateLabelId = _this$props$settings.privateLabelId,
          itc = _this$props$settings.itc;
      var _this$props = _this.props,
          intl = _this$props.intl,
          product = _this$props.product;

      var url = 'https://www.' + envPrefix + 'secureserver.net/api/v1/cart/' + privateLabelId + '?itc=' + itc;

      var data = {
        items: [{
          id: product.id,
          quantity: 1
        }]
      };

      var options = {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Credentials': true
        }
      };

      _uxcore.utils.request.post(url, data, options, function (error, response) {
        if (error) {
          return void _this.setState({
            isPosting: false
          }, function () {
            addGrowlMessage({
              title: intl.formatMessage({ id: 'products.growls.errors.addToCart.title' }),
              content: intl.formatMessage({ id: 'products.growls.errors.addToCart.content' }),
              icon: 'error',
              fadeTime: 5000
            });
          });
        }

        global.location.assign(response.nextStepUrl);
      });
    };

    _this.handleAddClick = function () {
      _this.setState({ isPosting: true }, _this.postToCart);
    };

    _this.state = {
      canInteract: false,
      heightHasBeenCalculated: false
    };
    return _this;
  }

  _createClass(ProductCard, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.breakpoint('mobile', 'phablet', 'tablet', 'desktop', 'large');

      this.setState({
        canInteract: true
      });
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.breakup();
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          product = _props.product,
          showMore = _props.showMore;
      var _state = this.state,
          canInteract = _state.canInteract,
          isPosting = _state.isPosting,
          shouldShowMoreModal = _state.shouldShowMoreModal;


      var canCalculateHeight = showMore && !!this.details && !!this.details.scrollHeight;

      // The explicit 500 check prevents `More` link from appearing when it
      // shouldn't during add-to-cart.
      var shouldShowMoreButton = canCalculateHeight && this.details.scrollHeight > 468 && this.details.scrollHeight !== 500;

      var details = _react2.default.createElement('div', {
        className: 'product-description',
        dangerouslySetInnerHTML: { __html: product.content }
      });

      var price = _react2.default.createElement(
        'p',
        { className: 'product-price' },
        product.salePrice && _react2.default.createElement(
          'span',
          null,
          _react2.default.createElement(
            'strike',
            { className: 'product-amount text-muted' },
            product.listPrice
          ),
          '\xA0'
        ),
        _react2.default.createElement(
          'strong',
          { className: 'product-amount text-primary' },
          product.salePrice || product.listPrice
        ),
        ' ',
        _react2.default.createElement(
          _reactIntl.FormattedMessage,
          { id: 'products.terms.' + (product.oneTimeCharge ? 'each' : product.term) },
          function (text) {
            return _react2.default.createElement(
              'span',
              { className: 'product-term' },
              text
            );
          }
        )
      );

      var taxesAndFeesDisclaimer = product.displayTaxesAndFees && taxAndFeeKeys.includes(product.displayTaxesAndFees) && _react2.default.createElement(
        'p',
        {
          className: 'product-taxAndFeeDisclaimer',
          'data-eid': 'storefront.product.taxAndFeeDisclaimer.' + product.displayTaxesAndFees.toLowerCase()
        },
        _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'products.taxAndFeeDisclaimers.' + product.displayTaxesAndFees })
      );

      var button = _react2.default.createElement(
        'p',
        null,
        _react2.default.createElement(
          _uxcore.Button,
          {
            className: 'product-addToCart',
            onClick: this.handleAddClick,
            disabled: !canInteract || isPosting,
            'data-eid': 'storefront.product.' + _util2.default.snakeCase(product.id) + '.add_to_cart.button.click'
          },
          isPosting && _react2.default.createElement(_uxcore.Spinner, { size: 'sm', inline: true, shade: 'dark' }),
          _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'products.addToCart' })
        )
      );

      var productBody = _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'h4',
          { className: 'product-title' },
          product.title
        ),
        price,
        taxesAndFeesDisclaimer,
        button,
        details
      );

      var moreModal = _react2.default.createElement(
        _uxcore.Modal,
        { onClose: function onClose() {
            return _this2.setState({ shouldShowMoreModal: false });
          } },
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-xs-3 product-details' },
            _react2.default.createElement(_ProductIcon2.default, { product: mapImageId(product.imageId) })
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-xs-9' },
            productBody
          )
        )
      );

      var moreButton = _react2.default.createElement(
        'p',
        { className: 'product-more-wrapper text-center' },
        _react2.default.createElement(
          _uxcore.Button,
          { design: 'link', size: 'small', onClick: function onClick() {
              return _this2.setState({ shouldShowMoreModal: true });
            } },
          _react2.default.createElement(
            _reactIntl.FormattedMessage,
            { id: 'products.details.more' },
            function (text) {
              return _react2.default.createElement(
                'span',
                null,
                text,
                '\xA0',
                _react2.default.createElement('span', { className: 'uxicon uxicon-chevron-down-lt' })
              );
            }
          )
        )
      );

      var detailsClassNames = (0, _classnames2.default)('product-details', canCalculateHeight && 'product-details--calculated', shouldShowMoreButton && 'product-details--more');

      return _react2.default.createElement(
        _Card2.default,
        { className: 'product-card' },
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-xs-3' },
            _react2.default.createElement(_ProductIcon2.default, { product: mapImageId(product.imageId) })
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-xs-9' },
            _react2.default.createElement(
              'div',
              { className: detailsClassNames, ref: function ref(d) {
                  _this2.details = d;
                } },
              productBody
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-xs-12' },
            shouldShowMoreButton && moreButton,
            shouldShowMoreModal && moreModal
          )
        )
      );
    }
  }]);

  return ProductCard;
}(_uxcore.Component);

ProductCard.propTypes = {
  intl: _reactIntl.intlShape.isRequired,
  product: _propTypes2.default.shape({
    id: _propTypes2.default.string.isRequired,
    imageId: _propTypes2.default.string.isRequired,
    title: _propTypes2.default.string.isRequired,
    content: _propTypes2.default.string.isRequired,
    salePrice: _propTypes2.default.oneOfType([_propTypes2.default.bool, _propTypes2.default.string]),
    listPrice: _propTypes2.default.string.isRequired,
    term: _propTypes2.default.string.isRequired
  }).isRequired,
  showMore: _propTypes2.default.bool,
  settings: _propTypes2.default.object.isRequired
};

exports.default = (0, _reactRedux.connect)(_mapSettings.mapStateToProps)((0, _reactIntl.injectIntl)(ProductCard));
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./src/components/ProductIcon/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _svgs = __webpack_require__("./src/components/ProductIcon/svgs/index.js");

var svgs = _interopRequireWildcard(_svgs);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var validProducts = Object.keys(svgs);

var ProductIcon = function ProductIcon(_ref) {
  var product = _ref.product;

  var props = {
    className: 'product-icon'
  };

  if (validProducts.indexOf(product) > -1) {
    props.dangerouslySetInnerHTML = {
      __html: svgs[product]
    };
  }

  return _react2.default.createElement('div', props);
};

ProductIcon.propTypes = {
  product: _propTypes2.default.oneOf(validProducts).isRequired
};

exports.default = ProductIcon;

/***/ }),

/***/ "./src/components/ProductIcon/svgs/domains.svg":
/***/ (function(module, exports) {

module.exports = "<svg viewBox=\"0 0 88 88\"><defs><style>#aeb04e .cls-2,#aeb04e .cls-3{stroke-miterlimit:10;stroke-width:2px;}#aeb04e .cls-2{stroke-linecap:round;}</style></defs><g id=\"aeb04e\"><circle class=\"cls-1 svg-fill-primary-o\" cx=\"44\" cy=\"44\" r=\"37\"/><path class=\"cls-2 svg-fill-none svg-stroke-black-dark\" d=\"M44 7c12.26 0 22.2 16.57 22.2 37S56.26 81 44 81\"/><path class=\"cls-3 svg-fill-none svg-stroke-black-dark\" d=\"M44 7v74\"/><path class=\"cls-2 svg-fill-none svg-stroke-black-dark\" d=\"M35 44h46M7 44h22M13 64h62M13 24h62M68.7 16.45a37 37 0 1 1-6.31-4.56\"/><path class=\"cls-2 svg-fill-none svg-stroke-black-dark\" d=\"M44 7C31.74 7 21.8 23.57 21.8 44S31.74 81 44 81\"/></g></svg>"

/***/ }),

/***/ "./src/components/ProductIcon/svgs/email.svg":
/***/ (function(module, exports) {

module.exports = "<svg viewBox=\"0 0 88 88\"><defs><style>#ad56e7 .cls-3,#ad56e7 .cls-4,#ad56e7 .cls-5{stroke-width:2px;}#ad56e7 .cls-3,#ad56e7 .cls-5{stroke-linejoin:round;}#ad56e7 .cls-4,#ad56e7 .cls-5{stroke-linecap:round;}#ad56e7 .cls-4{stroke-miterlimit:10;}</style></defs><g id=\"ad56e7\"><g><rect class=\"cls-1 svg-fill-white\" x=\"1\" y=\"13\" width=\"86\" height=\"62\" rx=\"2\" ry=\"2\"/><path class=\"cls-2 svg-fill-primary-o\" d=\"M80.63 13A6.89 6.89 0 0 0 77 14.22L45.22 38.65a2 2 0 0 1-2.44 0L11 14.22A6.89 6.89 0 0 0 7.37 13H3.62l1.59 1.22 37.57 28.85a2 2 0 0 0 2.44 0L82.8 14.22 84.38 13h-3.75z\"/><path class=\"cls-3 svg-fill-none svg-stroke-black\" d=\"M86.5 74.5L51.5 39M1.5 74.5l35-35.5\"/><path class=\"cls-4 svg-fill-none svg-stroke-black\" d=\"M67.5 75H3a2 2 0 0 1-2-2V15a2 2 0 0 1 2-2h82a2 2 0 0 1 2 2v58a2 2 0 0 1-2 2H73.5\"/><path class=\"cls-5 svg-fill-none svg-stroke-black\" d=\"M3.62 13l39.16 30.07a2 2 0 0 0 2.44 0L84.38 13\"/></g></g></svg>"

/***/ }),

/***/ "./src/components/ProductIcon/svgs/hosting.svg":
/***/ (function(module, exports) {

module.exports = "<svg viewBox=\"0 0 88 88\"><defs><style>#a1fd51 .cls-2,#a1fd51 .cls-3,#a1fd51 .cls-4{stroke-linecap:round;stroke-linejoin:round;}#a1fd51 .cls-2,#a1fd51 .cls-3,#a1fd51 .cls-4,#a1fd51 .cls-6{stroke-width:2px;}#a1fd51 .cls-6{stroke-miterlimit:10;}</style></defs><g id=\"a1fd51\"><g><rect class=\"cls-1 svg-fill-white\" x=\"7\" y=\"59\" width=\"74\" height=\"22\" rx=\"4\" ry=\"4\"/><path class=\"cls-2 svg-fill-primary-o svg-stroke-black\" d=\"M57 65h16M57 70h16M57 75h16\"/><circle class=\"cls-3 svg-fill-primary-o svg-stroke-primary-o\" cx=\"16\" cy=\"70\" r=\"1\"/><circle class=\"cls-3 svg-fill-primary-o svg-stroke-primary-o\" cx=\"24\" cy=\"70\" r=\"1\"/><circle class=\"cls-3 svg-fill-primary-o svg-stroke-primary-o\" cx=\"32\" cy=\"70\" r=\"1\"/><path class=\"cls-4 svg-fill-none svg-stroke-black\" d=\"M18 81h-7a4 4 0 0 1-4-4V63a4 4 0 0 1 4-4h66a4 4 0 0 1 4 4v14a4 4 0 0 1-4 4H24\"/><rect class=\"cls-1 svg-fill-white\" x=\"7\" y=\"7\" width=\"74\" height=\"22\" rx=\"4\" ry=\"4\"/><path class=\"cls-2 svg-fill-primary-o svg-stroke-black\" d=\"M57 13h16M57 18h16M57 23h16\"/><circle class=\"cls-3 svg-fill-primary-o svg-stroke-primary-o\" cx=\"16\" cy=\"18\" r=\"1\"/><circle class=\"cls-3 svg-fill-primary-o svg-stroke-primary-o\" cx=\"24\" cy=\"18\" r=\"1\"/><circle class=\"cls-3 svg-fill-primary-o svg-stroke-primary-o\" cx=\"32\" cy=\"18\" r=\"1\"/><path class=\"cls-4 svg-fill-none svg-stroke-black\" d=\"M70 29h7a4 4 0 0 0 4-4V11a4 4 0 0 0-4-4H11a4 4 0 0 0-4 4v14a4 4 0 0 0 4 4h53\"/><rect class=\"cls-5 svg-fill-primary-o\" x=\"7\" y=\"33\" width=\"74\" height=\"22\" rx=\"2\" ry=\"2\"/><path class=\"cls-2 svg-fill-primary-o svg-stroke-black\" d=\"M57 39h16M57 44h16M57 49h16\"/><circle class=\"cls-6 svg-fill-white svg-stroke-white\" cx=\"16\" cy=\"44\" r=\"1\"/><circle class=\"cls-6 svg-fill-white svg-stroke-white\" cx=\"24\" cy=\"44\" r=\"1\"/><circle class=\"cls-6 svg-fill-white svg-stroke-white\" cx=\"32\" cy=\"44\" r=\"1\"/><rect class=\"cls-4 svg-fill-none svg-stroke-black\" x=\"7\" y=\"33\" width=\"74\" height=\"22\" rx=\"4\" ry=\"4\"/></g></g></svg>"

/***/ }),

/***/ "./src/components/ProductIcon/svgs/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.wordpress = exports.websiteSecurity = exports.websiteBuilder = exports.websiteBackup = exports.ssl = exports.seo = exports.reseller = exports.premiumDns = exports.hosting = exports.email = exports.domains = undefined;

var _domains = __webpack_require__("./src/components/ProductIcon/svgs/domains.svg");

var _domains2 = _interopRequireDefault(_domains);

var _email = __webpack_require__("./src/components/ProductIcon/svgs/email.svg");

var _email2 = _interopRequireDefault(_email);

var _hosting = __webpack_require__("./src/components/ProductIcon/svgs/hosting.svg");

var _hosting2 = _interopRequireDefault(_hosting);

var _premiumDns = __webpack_require__("./src/components/ProductIcon/svgs/premium-dns.svg");

var _premiumDns2 = _interopRequireDefault(_premiumDns);

var _reseller = __webpack_require__("./src/components/ProductIcon/svgs/reseller.svg");

var _reseller2 = _interopRequireDefault(_reseller);

var _seo = __webpack_require__("./src/components/ProductIcon/svgs/seo.svg");

var _seo2 = _interopRequireDefault(_seo);

var _ssl = __webpack_require__("./src/components/ProductIcon/svgs/ssl.svg");

var _ssl2 = _interopRequireDefault(_ssl);

var _websiteBackup = __webpack_require__("./src/components/ProductIcon/svgs/website-backup.svg");

var _websiteBackup2 = _interopRequireDefault(_websiteBackup);

var _websiteBuilder = __webpack_require__("./src/components/ProductIcon/svgs/website-builder.svg");

var _websiteBuilder2 = _interopRequireDefault(_websiteBuilder);

var _websiteSecurity = __webpack_require__("./src/components/ProductIcon/svgs/website-security.svg");

var _websiteSecurity2 = _interopRequireDefault(_websiteSecurity);

var _wordpress = __webpack_require__("./src/components/ProductIcon/svgs/wordpress.svg");

var _wordpress2 = _interopRequireDefault(_wordpress);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.domains = _domains2.default;
exports.email = _email2.default;
exports.hosting = _hosting2.default;
exports.premiumDns = _premiumDns2.default;
exports.reseller = _reseller2.default;
exports.seo = _seo2.default;
exports.ssl = _ssl2.default;
exports.websiteBackup = _websiteBackup2.default;
exports.websiteBuilder = _websiteBuilder2.default;
exports.websiteSecurity = _websiteSecurity2.default;
exports.wordpress = _wordpress2.default;

/***/ }),

/***/ "./src/components/ProductIcon/svgs/premium-dns.svg":
/***/ (function(module, exports) {

module.exports = "<svg viewBox=\"0 0 100 100\"><g id=\"a773bc\"><g fill-rule=\"nonzero\" class=\"svg-fill-none\"><path d=\"M31.62 61.511l-2.818 4.853-11.257 20H28.91l6.818 11.363 15.91-27.272 15.808 27.272 6.919-11.363h11.363l-11.895-20-2.818-4.853\" class=\"svg-fill-primary-o\"/><path d=\"M53.095 2.795l8.598 7.091c.36.3.802.478 1.273.519l11.504.947a2.273 2.273 0 0 1 2.085 2.064l.959 11.39c.039.465.222.905.522 1.26l7.164 8.509a2.241 2.241 0 0 1 0 2.89l-7.166 8.512c-.3.356-.483.796-.523 1.26l-.954 11.393a2.273 2.273 0 0 1-2.087 2.06l-11.506.95c-.469.04-.91.22-1.273.517l-10.055 8.298-10.059-8.296c-.36-.3-.805-.48-1.272-.518L28.8 60.69a2.266 2.266 0 0 1-2.082-2.061l-.959-11.394a2.243 2.243 0 0 0-.523-1.256l-7.159-8.51a2.24 2.24 0 0 1 0-2.89l7.16-8.512c.3-.356.483-.797.522-1.261l.96-11.391a2.268 2.268 0 0 1 2.081-2.064l11.507-.947c.468-.04.909-.219 1.273-.516l8.595-7.091a2.3 2.3 0 0 1 2.92 0v-.003z\" class=\"svg-fill-white\"/><path d=\"M75.795 65.223l-1.304-2.255.166-.013a4.524 4.524 0 0 0 4.159-4.137l.954-11.377 7.166-8.514a4.51 4.51 0 0 0 0-5.818l-7.159-8.5-.961-11.386a4.539 4.539 0 0 0-4.164-4.137l-11.516-.95-8.59-7.09a4.577 4.577 0 0 0-5.814 0l-8.614 7.09-11.511.955a4.523 4.523 0 0 0-4.155 4.136l-.954 11.375-7.164 8.512a4.505 4.505 0 0 0 0 5.818l7.16 8.495.958 11.387a4.5 4.5 0 0 0 3.773 4.075l-12.66 22.363a2.273 2.273 0 0 0 1.98 3.384h10.078l6.159 10.262A2.273 2.273 0 0 0 35.727 100h.014a2.268 2.268 0 0 0 1.948-1.127l13.938-23.898 13.85 23.889A2.273 2.273 0 0 0 67.418 100h.023c.794 0 1.53-.413 1.943-1.09l6.252-10.274h10.091a2.273 2.273 0 0 0 1.955-3.431L75.795 65.223zM26.977 44.518l-7.163-8.477 7.163-8.511a4.487 4.487 0 0 0 1.046-2.532l.961-11.382 11.51-.948a4.512 4.512 0 0 0 2.526-1.027l8.598-7.091c.007-.005.012-.005.018-.005.01 0 .019.005.014.005l8.595 7.09c.72.596 1.596.955 2.532 1.03l11.512.937L75.25 25a4.49 4.49 0 0 0 1.045 2.532l7.16 8.477-7.169 8.514a4.493 4.493 0 0 0-1.045 2.527l-.961 11.38-11.507.947a4.499 4.499 0 0 0-2.53 1.028l-8.607 7.102-8.604-7.098a4.523 4.523 0 0 0-2.539-1.032l-11.509-.938-.961-11.394a4.477 4.477 0 0 0-1.046-2.53v.003zM35.7 93.264l-4.84-8.069a2.273 2.273 0 0 0-1.95-1.104h-7.478l9.336-16.586 2.418-4.17 6.941.574 8.568 7.068L35.7 93.264zm38.664-9.173c-.793 0-1.529.413-1.941 1.09l-4.932 8.096-12.923-22.295 8.582-7.075 6.332-.523 12.25 20.705h-7.368v.002z\" class=\"svg-fill-black\"/><path d=\"M65.87 32.666c.478-.491.444-.916.378-1.125-.068-.21-.273-.577-.939-.677l-7.623-1.16c-.631-.1-1.404-.688-1.686-1.288l-3.41-7.241c-.295-.63-.69-.727-.901-.727-.21 0-.607.097-.905.727l-3.409 7.24c-.282.6-1.055 1.19-1.686 1.287l-7.623 1.16c-.666.1-.873.468-.939.677-.063.209-.102.636.378 1.125L43.02 38.3c.455.468.75 1.42.646 2.08l-1.302 7.96c-.084.535.056.842.19 1.012.273.341.762.387 1.273.105l6.818-3.757c.537-.295 1.55-.295 2.087 0l6.818 3.76c.227.126.443.19.645.19.25 0 .478-.105.628-.295.136-.166.277-.478.19-1.007l-1.302-7.96c-.109-.658.187-1.613.644-2.079l5.515-5.636v-.007z\" class=\"svg-fill-primary-o\"/></g></g></svg>"

/***/ }),

/***/ "./src/components/ProductIcon/svgs/reseller.svg":
/***/ (function(module, exports) {

module.exports = "<svg viewBox=\"0 0 96 70\"><g id=\"ad345e\"><g fill-rule=\"nonzero\" class=\"svg-fill-none\"><path d=\"M93.674 19.765l.109 43.616c0 2.438-1.957 4.397-4.348 4.397H6.717c-2.39 0-4.347-1.96-4.347-4.397l-.11-43.616h91.414z\" class=\"svg-fill-white\"/><path d=\"M93.63 6.619l.044 13.146H2.26L2.217 6.619c0-2.438 1.957-4.397 4.348-4.397h82.718c2.39 0 4.347 1.96 4.347 4.397z\" class=\"svg-fill-primary-o\"/><path d=\"M95.848 19.76L95.804 6.62c0-3.624-2.926-6.573-6.521-6.573H6.565C2.97.046.043 2.995.043 6.626L.196 63.38c0 3.624 2.926 6.573 6.521 6.573h82.718c3.595 0 6.522-2.949 6.522-6.577l-.11-43.617zM6.565 4.4h82.718c1.195 0 2.174.997 2.174 2.226l.028 10.963H4.565c-.052 0-.093.026-.145.029L4.39 6.619c0-1.226.979-2.22 2.174-2.22zM89.435 65.6H6.717c-1.195 0-2.174-.997-2.174-2.224l-.108-41.462c.043 0 .082.026.13.026h86.913l.022-.004.109 41.444c0 1.226-.979 2.22-2.174 2.22z\" class=\"svg-fill-black\"/><path d=\"M56.696 11.059a2.175 2.175 0 0 1 4.348 0 2.175 2.175 0 0 1-2.174 2.176c-1.201 0-2.174-.974-2.174-2.176m8.695 0a2.175 2.175 0 0 1 4.348 0 2.175 2.175 0 0 1-2.174 2.176c-1.2 0-2.174-.974-2.174-2.176m8.696 0a2.175 2.175 0 0 1 4.348 0 2.175 2.175 0 0 1-2.174 2.176c-1.2 0-2.174-.974-2.174-2.176\" class=\"svg-fill-black\"/><path d=\"M63.217 50.235h-2.174v-6.607a2.175 2.175 0 0 0-2.173-2.177h-8.696v-4.275h2.174c1.2 0 2.174-.974 2.174-2.176v-4.353a2.175 2.175 0 0 0-2.174-2.176h-8.696c-1.2 0-2.174.974-2.174 2.176V35c0 1.202.974 2.176 2.174 2.176h2.174v4.275H37.13c-1.2 0-2.173.974-2.173 2.177v6.607h-2.174c-1.201 0-2.174.975-2.174 2.177v4.353c0 1.202.973 2.176 2.174 2.176h8.695c1.2 0 2.174-.974 2.174-2.176v-4.353a2.175 2.175 0 0 0-2.174-2.177h-2.174v-4.431h17.392v4.431h-2.174c-1.2 0-2.174.975-2.174 2.177v4.353c0 1.202.973 2.176 2.174 2.176h8.695c1.201 0 2.174-.974 2.174-2.176v-4.353a2.175 2.175 0 0 0-2.174-2.177z\" class=\"svg-fill-primary-o\"/></g></g></svg>"

/***/ }),

/***/ "./src/components/ProductIcon/svgs/seo.svg":
/***/ (function(module, exports) {

module.exports = "<svg viewBox=\"0 0 100 100\"><g id=\"a0b653\"><g fill-rule=\"nonzero\" class=\"svg-fill-none\"><circle cx=\"39.474\" cy=\"39.474\" r=\"36.842\" class=\"svg-fill-white\"/><path d=\"M98.458 91.016L85.3 77.858a5.237 5.237 0 0 0-5.066-1.342L69.142 65.424c6.087-6.95 9.805-16.01 9.805-25.95C78.947 17.71 61.237 0 39.474 0 17.71 0 0 17.71 0 39.474c0 21.763 17.71 39.473 39.474 39.473 9.937 0 19-3.718 25.947-9.802l11.092 11.09a5.237 5.237 0 0 0 1.342 5.062l13.158 13.158a5.255 5.255 0 0 0 3.724 1.542 5.263 5.263 0 0 0 3.721-8.984v.003zM5.263 39.474c0-18.863 15.348-34.21 34.21-34.21 18.864 0 34.211 15.347 34.211 34.21s-15.347 34.21-34.21 34.21-34.21-15.347-34.21-34.21z\" class=\"svg-fill-black\"/><path d=\"M59.868 39.474c0 8.358-12.036 15.131-20.394 15.131-8.358 0-20.395-6.773-20.395-15.131s12.037-15.132 20.395-15.132c8.358 0 20.394 6.774 20.394 15.132z\" class=\"svg-fill-primary-o\"/><circle cx=\"39.474\" cy=\"39.474\" r=\"7.895\" class=\"svg-fill-black\"/></g></g></svg>"

/***/ }),

/***/ "./src/components/ProductIcon/svgs/ssl.svg":
/***/ (function(module, exports) {

module.exports = "<svg viewBox=\"0 0 88 88\"><defs><style>#a0e0b0 .cls-1,#a0e0b0 .cls-2{stroke-miterlimit:10;}#a0e0b0 .cls-1,#a0e0b0 .cls-2,#a0e0b0 .cls-3{stroke-linecap:round;stroke-width:2px;}#a0e0b0 .cls-3{stroke-linejoin:round;}</style></defs><g id=\"a0e0b0\"><g><path class=\"cls-1 svg-fill-white svg-stroke-gray-dark\" d=\"M67 75H3a2 2 0 0 1-2-2V15a2 2 0 0 1 2-2h82a2 2 0 0 1 2 2v58a2 2 0 0 1-2 2H73\"/><path class=\"cls-2 svg-fill-white svg-stroke-black\" d=\"M18 58h31M43 62h6M18 62h19M18 66h31\"/><circle class=\"cls-3 svg-fill-none svg-stroke-primary-o\" cx=\"66\" cy=\"60\" r=\"4\"/><path class=\"cls-3 svg-fill-none svg-stroke-primary-o\" d=\"M69 68l-3-3-3 3\"/><path class=\"cls-4 svg-fill-primary-o\" d=\"M27 31.36a3.81 3.81 0 0 1 1.56-3.13 6.21 6.21 0 0 1 4-1.23 8.62 8.62 0 0 1 2.7.4 5.81 5.81 0 0 1 2.06 1.14 1.15 1.15 0 0 1 .42.69.72.72 0 0 1-.23.61l-1.29 1.33a.66.66 0 0 1-.54.2 1.23 1.23 0 0 1-.65-.3 4.65 4.65 0 0 0-1.24-.68 3.87 3.87 0 0 0-1.33-.24 2.27 2.27 0 0 0-1.16.26.78.78 0 0 0-.44.7.69.69 0 0 0 .26.55 2.1 2.1 0 0 0 .81.37l2.44.63a6.07 6.07 0 0 1 3 1.52 3.61 3.61 0 0 1 1 2.6A4 4 0 0 1 36.68 40a6.29 6.29 0 0 1-4.13 1.29 9.41 9.41 0 0 1-3.13-.5 6.11 6.11 0 0 1-2.32-1.38 1.18 1.18 0 0 1-.4-.76.77.77 0 0 1 .3-.65l1.54-1.22a.74.74 0 0 1 .55-.18 1.14 1.14 0 0 1 .6.35 4.23 4.23 0 0 0 3 1.21 2.23 2.23 0 0 0 1.22-.3.89.89 0 0 0 .46-.77.81.81 0 0 0-.34-.67 3.15 3.15 0 0 0-1.15-.45l-2.46-.59a4.78 4.78 0 0 1-2.57-1.44 3.75 3.75 0 0 1-.85-2.58zM39.22 31.36a3.81 3.81 0 0 1 1.56-3.13 6.21 6.21 0 0 1 4-1.23 8.62 8.62 0 0 1 2.7.4 5.81 5.81 0 0 1 2.06 1.14 1.15 1.15 0 0 1 .42.69.72.72 0 0 1-.23.61l-1.33 1.33a.66.66 0 0 1-.54.2 1.23 1.23 0 0 1-.65-.3A4.65 4.65 0 0 0 46 30.4a3.87 3.87 0 0 0-1.33-.24 2.27 2.27 0 0 0-1.16.26.78.78 0 0 0-.44.7.69.69 0 0 0 .26.55 2.1 2.1 0 0 0 .81.37l2.44.63a6.07 6.07 0 0 1 3 1.52 3.61 3.61 0 0 1 1 2.6A4 4 0 0 1 48.94 40a6.29 6.29 0 0 1-4.13 1.29 9.41 9.41 0 0 1-3.13-.5 6.11 6.11 0 0 1-2.32-1.38 1.18 1.18 0 0 1-.4-.76.77.77 0 0 1 .32-.68l1.54-1.22a.74.74 0 0 1 .55-.18 1.14 1.14 0 0 1 .6.35 4.23 4.23 0 0 0 3 1.21 2.23 2.23 0 0 0 1.22-.3.89.89 0 0 0 .46-.77.81.81 0 0 0-.34-.67 3.15 3.15 0 0 0-1.15-.45l-2.46-.59a4.78 4.78 0 0 1-2.57-1.44 3.75 3.75 0 0 1-.91-2.55zM61.21 38.26v2.25c0 .14.15.51.06.6s0 .39-.15.39h-9c-.13 0-.45-.3-.55-.39a1.55 1.55 0 0 1-.35-.58V27.8c0-.14.25-.16.35-.25s.41 0 .55 0h2.55c.13 0 .29-.05.38 0s.18.12.18.25v9.7h5.91c.14 0 .06.18.15.27s-.08.35-.08.49z\"/></g></g></svg>"

/***/ }),

/***/ "./src/components/ProductIcon/svgs/website-backup.svg":
/***/ (function(module, exports) {

module.exports = "<svg data-name=\"Layer 1\" viewBox=\"0 0 80 88\"><defs><style>#a4ed4b .cls-3{fill:#ecfff3;}#a4ed4b .cls-4{fill:#aab7c2;}</style></defs><g id=\"a4ed4b\"><path class=\"cls-1 svg-fill-white\" d=\"M30.15.65h-9.38a.64.64 0 0 0-.64.64v46.26a.64.64 0 0 0 .64.64h31.7a.65.65 0 0 0 .65-.64V7.12L46.32.65h-12\" transform=\"translate(-4.41)\"/><path class=\"cls-2 svg-fill-black\" d=\"M52.47 48.84h-31.7a1.29 1.29 0 0 1-1.29-1.29V1.29A1.29 1.29 0 0 1 20.77 0h9.38a.65.65 0 1 1 0 1.29h-9.38v46.26h31.7V7.39l-6.4-6.1H34.36a.65.65 0 1 1 0-1.29h12a.63.63 0 0 1 .45.18l6.79 6.47a.63.63 0 0 1 .2.47v40.43a1.29 1.29 0 0 1-1.33 1.29z\" transform=\"translate(-4.41)\"/><path class=\"cls-3\" d=\"M48.71 7.12h-6.47V.65l6.47 6.47z\"/><path class=\"cls-2 svg-fill-black\" d=\"M53.12 7.76h-6.47a.65.65 0 0 1-.65-.64V.65a.64.64 0 0 1 .4-.6.63.63 0 0 1 .7.14l6.47 6.47a.63.63 0 0 1 .14.7.64.64 0 0 1-.59.4zm-5.83-1.29h4.26l-4.26-4.26z\" transform=\"translate(-4.41)\"/><path class=\"cls-4\" d=\"M46.65 12.61H26.59a.65.65 0 0 1 0-1.3h20.06a.65.65 0 0 1 0 1.3zm0 3.9H26.59a.65.65 0 0 1 0-1.29h20.06a.65.65 0 0 1 0 1.29zm0 3.88H26.59a.65.65 0 0 1 0-1.29h20.06a.65.65 0 0 1 0 1.29zm0 3.87H27.24a.65.65 0 1 1 0-1.29h19.41a.65.65 0 0 1 0 1.29z\" transform=\"translate(-4.41)\"/><path class=\"cls-1 svg-fill-white\" d=\"M64.41 25.23h-6.35a.44.44 0 0 0-.44.44V57a.44.44 0 0 0 .44.44h21.46A.44.44 0 0 0 80 57V29.61l-4.6-4.38h-8.1\" transform=\"translate(-4.41)\"/><path class=\"cls-2 svg-fill-black\" d=\"M79.52 58.07H58.06A1.08 1.08 0 0 1 57 57V25.67a1.08 1.08 0 0 1 1.08-1.09h6.35a.65.65 0 1 1 0 1.29h-6.16v30.9h21V29.88l-4.21-4h-7.8a.65.65 0 1 1 0-1.29h8.1a.63.63 0 0 1 .45.18l4.6 4.38a.63.63 0 0 1 .2.47V57a1.09 1.09 0 0 1-1.09 1.07z\" transform=\"translate(-4.41)\"/><path class=\"cls-3\" d=\"M75.55 29.76h-4.53v-4.53l4.53 4.53z\"/><path class=\"cls-2 svg-fill-black\" d=\"M80 30.4h-4.57a.64.64 0 0 1-.64-.64v-4.53a.64.64 0 0 1 .4-.6.65.65 0 0 1 .7.14l4.53 4.53a.65.65 0 0 1 .14.7.64.64 0 0 1-.56.4zm-3.88-1.29h2.28l-2.32-2.32z\" transform=\"translate(-4.41)\"/><path class=\"cls-4\" d=\"M75.58 33.64H62a.65.65 0 0 1 0-1.3h13.58a.65.65 0 0 1 0 1.3zm0 3.88H62a.65.65 0 0 1 0-1.3h13.58a.65.65 0 0 1 0 1.3zm0 3.87H62a.65.65 0 0 1 0-1.29h13.58a.65.65 0 1 1 0 1.29zm0 3.92H62.44a.65.65 0 1 1 0-1.29h13.14a.65.65 0 1 1 0 1.29z\" transform=\"translate(-4.41)\"/><path d=\"M52.79 57.25A2.27 2.27 0 0 1 50.53 55V30.7a2.6 2.6 0 0 0-2.59-2.59H29.83a.65.65 0 0 1-.65-.64v-3.72a2.1 2.1 0 0 0-1.94-2.05H9.49a1.87 1.87 0 0 0-1.66 2.05v30.91a2.6 2.6 0 0 0 2.59 2.59h42.37z\" transform=\"translate(-4.41)\" fill=\"#fee782\"/><path d=\"M50.53 55a2.27 2.27 0 1 0 4.53 0V33.31h-4.53V55\" transform=\"translate(-4.41)\" class=\"svg-fill-primary-o-dark\"/><path class=\"cls-2 svg-fill-black\" d=\"M52.79 57.9a2.92 2.92 0 0 1-2.91-2.9V33.31a.65.65 0 0 1 .65-.64h4.53a.64.64 0 0 1 .64.64V55a2.91 2.91 0 0 1-2.91 2.9zM51.18 34v21a1.62 1.62 0 1 0 3.23 0V34z\" transform=\"translate(-4.41)\"/><path class=\"cls-2 svg-fill-black\" d=\"M52.79 57.9H10.43a3.25 3.25 0 0 1-3.24-3.24V36.22a.65.65 0 0 1 1.29 0v18.44a1.94 1.94 0 0 0 1.95 1.94h39.94a2.89 2.89 0 0 1-.49-1.6V30.7a1.94 1.94 0 0 0-1.94-1.94H29.83a1.29 1.29 0 0 1-1.29-1.29v-3.72a1.46 1.46 0 0 0-1.3-1.41H9.49c-.63 0-1 .72-1 1.41v7a.65.65 0 0 1-1.29 0v-7a2.51 2.51 0 0 1 2.3-2.7h17.74a2.75 2.75 0 0 1 2.59 2.7v3.72h18.11a3.24 3.24 0 0 1 3.24 3.23V55a1.61 1.61 0 0 0 1.61 1.62.65.65 0 0 1 0 1.3z\" transform=\"translate(-4.41)\"/><path class=\"cls-1 svg-fill-white\" d=\"M74.54 51.31v-1a18.31 18.31 0 0 0-33.76-9.84A14.22 14.22 0 0 0 19.6 51.05a12.59 12.59 0 0 0-2.32-.22c-6.74 0-12.21 6.65-12.21 13.4s5.47 13.39 12.21 13.39h53.86c6.75 0 12.21-6.65 12.21-13.39 0-5.57-3.72-11.44-8.81-12.92z\" transform=\"translate(-4.41)\"/><path class=\"cls-2 svg-fill-black\" d=\"M71.14 78.27H53.76a.65.65 0 0 1 0-1.29h17.38c6 0 11.57-6.08 11.57-12.75 0-5.45-3.75-11-8.35-12.29a.67.67 0 0 1-.47-.66V50.36a17.66 17.66 0 0 0-32.57-9.49.64.64 0 0 1-.86.22 13.56 13.56 0 0 0-6.79-1.83 14.65 14.65 0 0 0-3.56.44.65.65 0 1 1-.33-1.25 15.88 15.88 0 0 1 3.94-.45 14.83 14.83 0 0 1 6.9 1.7 18.95 18.95 0 0 1 34.61 10.66v.49c5 1.73 8.79 7.49 8.79 13.38C84 71.71 78 78.27 71.14 78.27zm-35.14 0H17.28c-6.85 0-12.87-6.56-12.87-14s6-14.05 12.84-14.05a13.09 13.09 0 0 1 1.75.08 14.74 14.74 0 0 1 6.84-10.16.65.65 0 0 1 .68 1.11 13.5 13.5 0 0 0-6.34 9.88.67.67 0 0 1-.26.45.69.69 0 0 1-.5.11 11 11 0 0 0-2.17-.22c-6 0-11.55 6.08-11.55 12.76S11.22 77 17.28 77H36a.65.65 0 1 1 0 1.29z\" transform=\"translate(-4.41)\"/><path class=\"cls-2 svg-fill-black\" d=\"M38 51a.65.65 0 0 1-.65-.64 19 19 0 0 1 19-19 .64.64 0 0 1 .64.65.63.63 0 0 1-.64.64A17.69 17.69 0 0 0 38.6 50.36a.65.65 0 0 1-.6.64z\" transform=\"translate(-4.41)\"/><path d=\"M46.37 87h3.23c.46 0 .63-.25.63-.74v-12a1.81 1.81 0 0 1 1.4-1.86 2.69 2.69 0 0 1 .57-.05h2.06a.65.65 0 0 0 .67-.33.69.69 0 0 0-.14-.78Q50 65.35 45.3 59.42a.7.7 0 0 0-1.18 0q-4.73 5.94-9.48 11.85c-.32.41-.27.86.14 1a1.18 1.18 0 0 0 .41.07h2.36a1.78 1.78 0 0 1 1.66 1.74V86.2c0 .57.22.79.79.79h2.67\" transform=\"translate(-4.41)\" class=\"svg-fill-primary\"/><path class=\"cls-2 svg-fill-black\" d=\"M40 87.69a1.29 1.29 0 0 1-1.44-1.43V74.14a1.14 1.14 0 0 0-1-1.1h-2.33a1.67 1.67 0 0 1-.67-.12 1.2 1.2 0 0 1-.7-.82 1.4 1.4 0 0 1 .32-1.21l2.42-3L43.62 59a1.39 1.39 0 0 1 1.09-.57 1.41 1.41 0 0 1 1.1.56c2.74 3.44 5.53 6.93 8.23 10.3l1.25 1.56a1.28 1.28 0 0 1 .22 1.46 1.26 1.26 0 0 1-1.24.7H52.2a2 2 0 0 0-.43 0 1.18 1.18 0 0 0-.9 1.23v12a1.25 1.25 0 0 1-1.21 1.39h-3.3a.65.65 0 0 1 0-1.3h3.21a.22.22 0 0 0 0-.09V74.29a2.48 2.48 0 0 1 1.9-2.49 3 3 0 0 1 .71-.07h2.13L53 70.12c-2.7-3.37-5.49-6.86-8.23-10.3a.38.38 0 0 0-.09-.08l-.08.08q-3.52 4.43-7.06 8.83l-2.42 3h2.46a2.42 2.42 0 0 1 2.28 2.38v12.26h2.81a.65.65 0 0 1 0 1.3z\" transform=\"translate(-4.41)\"/></g></svg>"

/***/ }),

/***/ "./src/components/ProductIcon/svgs/website-builder.svg":
/***/ (function(module, exports) {

module.exports = "<svg viewBox=\"0 0 88 88\"><defs><style>#acef38 .cls-1,#acef38 .cls-2,#acef38 .cls-3,#acef38 .cls-4,#acef38 .cls-5{stroke-linecap:round;stroke-linejoin:round;stroke-width:2px;}#acef38 .cls-2{stroke-dasharray:6.44 6.44;}#acef38 .cls-3{stroke-dasharray:5.43 5.43;}</style></defs><g id=\"acef38\"><g><path class=\"cls-1 svg-fill-none svg-stroke-black\" d=\"M87 72v3h-3\"/><path class=\"cls-2 svg-fill-none svg-stroke-black\" d=\"M77.56 75H29.22\"/><path class=\"cls-1 svg-fill-none svg-stroke-black\" d=\"M26 75h-3v-3\"/><path class=\"cls-3 svg-fill-none svg-stroke-black\" d=\"M23 66.57V36.71\"/><path class=\"cls-1 svg-fill-none svg-stroke-black\" d=\"M23 34v-3h3\"/><path class=\"cls-2 svg-fill-none svg-stroke-black\" d=\"M32.44 31h48.34\"/><path class=\"cls-1 svg-fill-none svg-stroke-black\" d=\"M84 31h3v3\"/><path class=\"cls-3 svg-fill-none svg-stroke-black\" d=\"M87 39.43v29.86\"/><path class=\"cls-4 svg-fill-primary-o svg-stroke-black\" d=\"M25 13h38a2 2 0 0 1 2 2v40a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V15a2 2 0 0 1 2-2h16\"/><path class=\"cls-5 svg-fill-white svg-stroke-black\" d=\"M71 58.47a1.53 1.53 0 0 1 0-2.17l1.89-1.89a1.51 1.51 0 0 0-.69-2.56l-13.72-3.2a1.58 1.58 0 0 0-1.88 1.88l3.2 13.72a1.51 1.51 0 0 0 2.56.69l1.84-1.84a1.53 1.53 0 0 1 2.17 0l1.41 1.41a1.53 1.53 0 0 0 2.17 0l1.23-1.23 1.06-1.06a1.68 1.68 0 0 0 .14-2.28z\"/></g></g></svg>"

/***/ }),

/***/ "./src/components/ProductIcon/svgs/website-security.svg":
/***/ (function(module, exports) {

module.exports = "<svg viewBox=\"0 0 88 88\"><defs><style>#abee73 .cls-3,#abee73 .cls-4,#abee73 .cls-6{stroke-linecap:round;}#abee73 .cls-3,#abee73 .cls-4,#abee73 .cls-5,#abee73 .cls-6{stroke-linejoin:round;stroke-width:2px;}</style></defs><g id=\"abee73\"><g><path class=\"cls-1 svg-fill-white\" d=\"M69 39.13c0-2.32-1.43-4.13-3.72-4.13H11.4A4.33 4.33 0 0 0 7 39.13v33.66A4.4 4.4 0 0 0 11.4 77h53.47c2.32 0 4.13-1.43 4.13-3.72z\"/><path class=\"cls-2 svg-fill-primary-o\" d=\"M7 42v-2.87A4.33 4.33 0 0 1 11.4 35h53.88c2.29 0 3.72 1.81 3.72 4.13V42z\"/><path class=\"cls-3 svg-fill-primary-o svg-stroke-black\" d=\"M53 21a15 15 0 0 0-30 0v14h6V22a9 9 0 1 1 18 0v13h6V21z\"/><path class=\"cls-4 svg-fill-none svg-stroke-black\" d=\"M46 56a8 8 0 0 0-16 0 7.49 7.49 0 0 0 3 6.24V67a5 5 0 1 0 10 0v-4.76A7.49 7.49 0 0 0 46 56zM69 42H7\"/><path class=\"cls-4 svg-fill-none svg-stroke-black\" d=\"M24 77h40.87c2.32 0 4.13-1.43 4.13-3.72V39.13c0-2.32-1.43-4.13-3.72-4.13H11.4A4.33 4.33 0 0 0 7 39.13v33.66A4.4 4.4 0 0 0 11.4 77H18\"/><circle class=\"cls-2 svg-fill-primary-o\" cx=\"69\" cy=\"69\" r=\"12\"/><circle class=\"cls-5 svg-fill-none svg-stroke-black\" cx=\"69\" cy=\"69\" r=\"12\"/><path class=\"cls-6 svg-fill-none svg-stroke-white\" d=\"M63.5 69.5L67 73l7.5-8\"/></g></g></svg>"

/***/ }),

/***/ "./src/components/ProductIcon/svgs/wordpress.svg":
/***/ (function(module, exports) {

module.exports = "<svg viewBox=\"0 0 88 88\"><defs><style>#a17f78 .cls-2,#a17f78 .cls-3,#a17f78 .cls-4{stroke-linecap:round;stroke-linejoin:round;stroke-width:2px;}#a17f78 .cls-6{fill:#00749a;}</style></defs><g id=\"a17f78\"><g><rect class=\"cls-1 svg-fill-primary-o\" x=\"7\" y=\"59\" width=\"74\" height=\"22\" rx=\"2\" ry=\"2\"/><path class=\"cls-2 svg-fill-primary-o svg-stroke-black\" d=\"M57 65h16M57 70h16M57 75h16\"/><circle class=\"cls-3 svg-fill-white svg-stroke-white\" cx=\"16\" cy=\"70\" r=\"1\"/><circle class=\"cls-3 svg-fill-white svg-stroke-white\" cx=\"24\" cy=\"70\" r=\"1\"/><circle class=\"cls-3 svg-fill-white svg-stroke-white\" cx=\"32\" cy=\"70\" r=\"1\"/><path class=\"cls-4 svg-fill-none svg-stroke-black\" d=\"M18 81H9a2 2 0 0 1-2-2V61a2 2 0 0 1 2-2h70a2 2 0 0 1 2 2v18a2 2 0 0 1-2 2H24\"/><circle class=\"cls-5 svg-fill-white\" cx=\"44\" cy=\"35\" r=\"28\"/><path class=\"cls-6\" d=\"M57.9 34a10.84 10.84 0 0 0-1.7-5.68c-1-1.7-2-3.13-2-4.83a3.56 3.56 0 0 1 3.46-3.65h.27a20.58 20.58 0 0 0-31.13 3.85h1.33c2.15 0 5.49-.26 5.49-.26a.85.85 0 0 1 .13 1.7s-1.12.13-2.36.2l7.5 22.3 4.51-13.51-3.21-8.79c-1.11-.06-2.16-.2-2.16-.2a.85.85 0 0 1 .13-1.7s3.4.26 5.42.26 5.49-.26 5.49-.26a.85.85 0 0 1 .13 1.7s-1.12.13-2.36.2l7.44 22.13 2.05-6.86A19.17 19.17 0 0 0 57.9 34z\"/><path class=\"cls-6\" d=\"M44 11a24 24 0 1 0 24 24 24 24 0 0 0-24-24zm0 46.9A22.9 22.9 0 1 1 66.9 35 22.93 22.93 0 0 1 44 57.9z\"/><path class=\"cls-6\" d=\"M23.41 35A20.59 20.59 0 0 0 35 53.53l-9.81-26.91A20.51 20.51 0 0 0 23.41 35zM44.36 36.8l-6.18 17.95a20.6 20.6 0 0 0 12.65-.33 2 2 0 0 1-.15-.28zM62.07 25.12a15.72 15.72 0 0 1 .14 2.12 19.44 19.44 0 0 1-1.57 7.37l-6.29 18.18a20.59 20.59 0 0 0 7.72-27.67z\"/><circle class=\"cls-4 svg-fill-none svg-stroke-black\" cx=\"44\" cy=\"35\" r=\"28\"/></g></g></svg>"

/***/ }),

/***/ "./src/components/Products/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Products = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactIntl = __webpack_require__("react-intl");

var _reactRouter = __webpack_require__("./node_modules/react-router/es/index.js");

var _classnames = __webpack_require__("./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _reactRedux = __webpack_require__("./node_modules/react-redux/es/index.js");

var _uxcore = __webpack_require__("@ux/uxcore2");

var _PageTitle = __webpack_require__("./src/components/PageTitle/index.js");

var _PageTitle2 = _interopRequireDefault(_PageTitle);

var _ProductCard = __webpack_require__("./src/components/ProductCard/index.js");

var _ProductCard2 = _interopRequireDefault(_ProductCard);

var _selectors = __webpack_require__("./src/components/Products/selectors.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Products = exports.Products = function (_Component) {
  _inherits(Products, _Component);

  function Products() {
    _classCallCheck(this, Products);

    var _this = _possibleConstructorReturn(this, (Products.__proto__ || Object.getPrototypeOf(Products)).apply(this, arguments));

    _this.getProductClassNames = _this.getProductClassNames.bind(_this);

    var tag = _this.props.match.params.tag;

    _this.state = {
      canInteract: false,
      tag: tag,
      products: _this.props.products.filter(function (p) {
        return p.tags && p.tags.includes(tag);
      })
    };
    return _this;
  }

  _createClass(Products, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.breakpoint('mobile', 'phablet', 'tablet', 'desktop', 'large');

      this.setState({
        canInteract: true
      });
    }
  }, {
    key: 'getProductClassNames',
    value: function getProductClassNames(hasMultipleProducts, n) {
      return (0, _classnames2.default)({
        'col-md-6': hasMultipleProducts,
        'clear-left': hasMultipleProducts && n % 2 === 0
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var settings = this.props.settings;
      var _state = this.state,
          canInteract = _state.canInteract,
          tag = _state.tag,
          products = _state.products,
          breakpoint = _state.breakpoint;


      if (products.length === 0) {
        return _react2.default.createElement(_reactRouter.Redirect, { to: '/not-found?plid=' + settings.privateLabelId });
      }

      var hasMultipleProducts = products.length > 1;
      var isMobile = this.isBrowser && ['mobile', 'phablet'].includes(breakpoint);
      var isEligibleForMore = !isMobile && hasMultipleProducts;

      return _react2.default.createElement(
        'div',
        { className: (0, _classnames2.default)('products', tag) },
        _react2.default.createElement(
          _PageTitle2.default,
          null,
          _react2.default.createElement(
            'h1',
            { className: 'headline-brand' },
            _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'products.' + tag + '.title' })
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'container row' },
          !canInteract ? _react2.default.createElement(
            'div',
            { className: 'text-center products--spinner' },
            _react2.default.createElement(_uxcore.Spinner, { inline: true, size: 'lg' })
          ) : products.map(function (item, n) {
            return _react2.default.createElement(
              'div',
              { key: item.id, className: _this2.getProductClassNames(hasMultipleProducts, n) },
              _react2.default.createElement(_ProductCard2.default, { product: item, showMore: isEligibleForMore })
            );
          })
        ),
        _react2.default.createElement(_uxcore.Growl, null)
      );
    }
  }]);

  return Products;
}(_uxcore.Component);

Products.propTypes = {
  match: _propTypes2.default.object.isRequired,
  settings: _propTypes2.default.object.isRequired,
  products: _propTypes2.default.array.isRequired
};

exports.default = (0, _reactRedux.connect)(_selectors.mapStateToProps)(Products);

/***/ }),

/***/ "./src/components/Products/selectors.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var mapStateToProps = exports.mapStateToProps = function mapStateToProps(state) {
  return {
    settings: state.settings,
    products: state.products
  };
};

/***/ }),

/***/ "./src/components/WhoIs/ContactForm/form.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InnerForm = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _formik = __webpack_require__("./node_modules/formik/dist/formik.es6.js");

var _reactRedux = __webpack_require__("./node_modules/react-redux/es/index.js");

var _reactIntl = __webpack_require__("react-intl");

var _uxcore = __webpack_require__("@ux/uxcore2");

var _validate = __webpack_require__("./src/components/WhoIs/ContactForm/validate.js");

var _validate2 = _interopRequireDefault(_validate);

var _mapSettings = __webpack_require__("./src/components/map-settings.js");

var _FormattedMarkdownMessage = __webpack_require__("./src/components/FormattedMarkdownMessage.js");

var _FormattedMarkdownMessage2 = _interopRequireDefault(_FormattedMarkdownMessage);

var _Captcha = __webpack_require__("./src/components/Captcha/index.js");

var _Captcha2 = _interopRequireDefault(_Captcha);

var _util = __webpack_require__("./src/util.js");

var _util2 = _interopRequireDefault(_util);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var contactTypeValues = {
  registrant: 'Registrant',
  technical: 'Technical',
  admin: 'Administrative'
};

var InnerForm = exports.InnerForm = function (_Component) {
  _inherits(InnerForm, _Component);

  function InnerForm() {
    _classCallCheck(this, InnerForm);

    var _this = _possibleConstructorReturn(this, (InnerForm.__proto__ || Object.getPrototypeOf(InnerForm)).apply(this, arguments));

    _this.handleFormSubmit = _this.handleFormSubmit.bind(_this);
    return _this;
  }

  _createClass(InnerForm, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.props.setFieldValue('captchaType', this.captcha.getType());
    }
  }, {
    key: 'handleFormSubmit',
    value: function handleFormSubmit(e) {
      e.preventDefault();

      if (this.captcha.getType() === _Captcha.types.RECAPTCHA) {
        return void this.captcha.execute();
      }

      this.props.submitForm();
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          intl = _props.intl,
          settings = _props.settings,
          handleClose = _props.handleClose,
          values = _props.values,
          touched = _props.touched,
          dirty = _props.dirty,
          errors = _props.errors,
          handleBlur = _props.handleBlur,
          handleChange = _props.handleChange,
          isSubmitting = _props.isSubmitting,
          setFieldValue = _props.setFieldValue,
          submitForm = _props.submitForm;


      var disabled = isSubmitting;
      var hasErrors = !!Object.keys(errors).length;

      var reasonLength = values.reason ? values.reason.trim().length : 0;
      var reasonLimitColor = 'success';

      if (reasonLength === 0 || reasonLength > _validate.REASON_LIMIT) {
        reasonLimitColor = 'danger';
      } else if (reasonLength > _validate.REASON_LIMIT * 0.75) {
        reasonLimitColor = 'warning';
      }

      var contactTypes = Object.keys(contactTypeValues).map(function (type) {
        return {
          label: intl.formatMessage({ id: 'whois.contact.form.labels.types.' + type }),
          id: 'type-' + type,
          name: 'type',
          value: contactTypeValues[type],
          inline: true,
          defaultChecked: contactTypeValues[type] === values.type,
          onChange: handleChange
        };
      });

      return _react2.default.createElement(
        'form',
        { onSubmit: this.handleFormSubmit },
        _react2.default.createElement(_FormattedMarkdownMessage2.default, { id: 'whois.contact.body' }),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-md-6' },
            _react2.default.createElement(_uxcore.FormElement, {
              id: 'whois-contact-domain',
              name: 'domain',
              value: values.domain,
              label: intl.formatMessage({ id: 'whois.contact.form.labels.domain' }),
              disabled: true,
              required: true
            })
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-6' },
            _react2.default.createElement(_uxcore.FormElement, {
              id: 'whois-contact-emailAddress',
              name: 'emailAddress',
              type: 'email',
              value: values.emailAddress,
              label: intl.formatMessage({ id: 'whois.contact.form.labels.emailAddress' }),
              onChange: handleChange,
              onBlur: handleBlur,
              disabled: disabled,
              required: true,
              validates: touched.emailAddress && !errors.emailAddress,
              failure: touched.emailAddress && errors.emailAddress && intl.formatMessage({ id: 'whois.contact.form.errors.' + errors.emailAddress })
            })
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-12' },
            _react2.default.createElement(
              'label',
              { htmlFor: 'type' },
              _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'whois.contact.form.labels.type' }),
              '\xA0',
              _react2.default.createElement(
                'span',
                { className: 'req', 'aria-label': 'required' },
                '*'
              )
            ),
            _react2.default.createElement(_uxcore.RadioGroup, { inputs: contactTypes }),
            touched.type && errors.type && _react2.default.createElement(
              'div',
              { className: 'form-error', role: 'alert' },
              _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'whois.contact.form.errors.required' })
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-12 whois-contact-reason' },
            _react2.default.createElement(_uxcore.FormElement, {
              id: 'whois-contact-reason',
              name: 'reason',
              type: 'textarea',
              value: values.reason,
              label: intl.formatMessage({ id: 'whois.contact.form.labels.reason' }),
              disabled: disabled,
              required: true,
              onChange: handleChange,
              onBlur: handleBlur,
              validates: touched.reason && !errors.reason
            }),
            _react2.default.createElement(
              'p',
              { className: 'whois-contact-reason-limit form-indicator' },
              _react2.default.createElement(
                'span',
                { className: 'text-' + reasonLimitColor },
                reasonLength
              ),
              ' / ',
              _validate.REASON_LIMIT
            )
          ),
          _react2.default.createElement(_Captcha2.default, {
            settings: settings,
            className: 'col-md-12 form-group',
            ref: function ref(captcha) {
              _this2.captcha = captcha;
            },
            onChange: function onChange(type, _ref) {
              var recaptcha = _ref.recaptcha,
                  challenge = _ref.challenge,
                  response = _ref.response;

              setFieldValue('recaptcha', recaptcha);
              setFieldValue('challenge', challenge);
              setFieldValue('response', response);
              setFieldValue('siteKey', settings.captcha.siteKey);

              if (type === _Captcha.types.RECAPTCHA) {
                submitForm();
              }
            }
          }),
          _react2.default.createElement('input', { type: 'hidden', name: 'captchaType', value: values.captchaType || '' }),
          _react2.default.createElement(
            'div',
            { className: 'col-md-12' },
            _react2.default.createElement(_FormattedMarkdownMessage2.default, {
              id: 'whois.contact.disclaimer',
              values: { privateLabelId: settings.privateLabelId },
              html: true
            })
          )
        ),
        _react2.default.createElement(
          _uxcore.ButtonSet,
          null,
          _react2.default.createElement(
            _uxcore.Button,
            {
              design: 'primary',
              type: 'submit',
              'data-eid': 'storefront.whois.contact.submit.click',
              disabled: disabled || hasErrors || !dirty
            },
            isSubmitting && _react2.default.createElement(_uxcore.Spinner, { size: 'sm', inline: true, shade: 'dark' }),
            _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'whois.contact.form.actions.submit' })
          ),
          _react2.default.createElement(
            _uxcore.Button,
            {
              design: 'default',
              'data-eid': 'storefront.whois.contact.cancel.click',
              disabled: disabled,
              onClick: handleClose
            },
            _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'whois.contact.form.actions.cancel' })
          )
        )
      );
    }
  }]);

  return InnerForm;
}(_react.Component);

InnerForm.propTypes = _extends({}, _util2.default.formikPropTypes, {
  intl: _propTypes2.default.object.isRequired,
  settings: _propTypes2.default.object.isRequired,
  handleClose: _propTypes2.default.func.isRequired
});

var Form = function (_Component2) {
  _inherits(Form, _Component2);

  function Form() {
    _classCallCheck(this, Form);

    return _possibleConstructorReturn(this, (Form.__proto__ || Object.getPrototypeOf(Form)).apply(this, arguments));
  }

  _createClass(Form, [{
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          intl = _props2.intl,
          settings = _props2.settings,
          initialValues = _props2.initialValues,
          handleClose = _props2.handleClose,
          onSubmit = _props2.handleSubmit;


      var formProps = {
        validate: _validate2.default,
        onSubmit: onSubmit,
        initialValues: _extends({ type: 'Registrant' }, initialValues),
        render: function render(additionalProps) {
          return _react2.default.createElement(InnerForm, _extends({ intl: intl, settings: settings, handleClose: handleClose }, additionalProps));
        }
      };

      return _react2.default.createElement(_formik.Formik, formProps);
    }
  }]);

  return Form;
}(_react.Component);

Form.propTypes = {
  intl: _reactIntl.intlShape.isRequired,
  settings: _propTypes2.default.object.isRequired,
  initialValues: _propTypes2.default.object.isRequired,
  handleClose: _propTypes2.default.func.isRequired,
  handleSubmit: _propTypes2.default.func.isRequired
};

exports.default = (0, _reactRedux.connect)(_mapSettings.mapStateToProps)((0, _reactIntl.injectIntl)(Form));

/***/ }),

/***/ "./src/components/WhoIs/ContactForm/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = __webpack_require__("./node_modules/react-redux/es/index.js");

var _querystring = __webpack_require__("./node_modules/querystring-es3/index.js");

var _querystring2 = _interopRequireDefault(_querystring);

var _reactIntl = __webpack_require__("react-intl");

var _uxcore = __webpack_require__("@ux/uxcore2");

var _form = __webpack_require__("./src/components/WhoIs/ContactForm/form.js");

var _form2 = _interopRequireDefault(_form);

var _mapSettings = __webpack_require__("./src/components/map-settings.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var addGrowlMessage = _uxcore.Growl.addGrowlMessage;

var ContactForm = function (_Component) {
  _inherits(ContactForm, _Component);

  function ContactForm() {
    _classCallCheck(this, ContactForm);

    var _this = _possibleConstructorReturn(this, (ContactForm.__proto__ || Object.getPrototypeOf(ContactForm)).apply(this, arguments));

    _this.handleFormSubmit = _this.handleFormSubmit.bind(_this);
    return _this;
  }

  _createClass(ContactForm, [{
    key: 'handleFormSubmit',
    value: function handleFormSubmit(values, _ref) {
      var setSubmitting = _ref.setSubmitting;
      var _props = this.props,
          intl = _props.intl,
          settings = _props.settings,
          handleClose = _props.handleClose;
      var envPrefix = settings.envPrefix,
          privateLabelId = settings.privateLabelId;


      var url = 'https://www.' + envPrefix + 'secureserver.net/whois/contact.aspx?plid=' + privateLabelId;

      var data = {
        domainName: values.domain,
        requesterEmailAddress: values.emailAddress,
        contactReasonMessage: values.reason.trim(),
        contactType: values.type,
        privateLabelId: privateLabelId,
        isSecureserver: true
      };

      if (values.recaptcha) {
        data['g-Recaptcha-Response'] = values.recaptcha;
        data.UseReCaptcha = true;
      } else {
        data.UseReCaptcha = false;
        data.captcha_code = values.response;
        data.captcha_ch = values.challenge;
        data.captcha_c = values.siteKey;
      }

      var options = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        // Disable credentials inclusion by providing a `mode` with a value other
        // than 'cors'.
        mode: ''
      };

      _uxcore.utils.request.post(url, _querystring2.default.stringify(data), options, function (error) {
        var isError = !!error;
        var key = isError ? 'error' : 'success';

        addGrowlMessage({
          title: intl.formatMessage({ id: 'whois.contact.form.growls.' + key + '.title' }),
          content: intl.formatMessage({ id: 'whois.contact.form.growls.' + key + '.content' }),
          icon: key,
          fadeTime: 5000
        });

        setSubmitting(false);

        if (!isError) {
          handleClose();
        }
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          initialValues = _props2.initialValues,
          handleClose = _props2.handleClose,
          intl = _props2.intl;


      return _react2.default.createElement(
        'div',
        { className: 'whois-contact' },
        _react2.default.createElement(
          _uxcore.Modal,
          { title: intl.formatMessage({ id: 'whois.contact.title' }), onClose: handleClose },
          _react2.default.createElement(_form2.default, { initialValues: initialValues, handleClose: handleClose, handleSubmit: this.handleFormSubmit })
        ),
        _react2.default.createElement(_uxcore.Growl, null)
      );
    }
  }]);

  return ContactForm;
}(_react.Component);

ContactForm.propTypes = {
  intl: _reactIntl.intlShape.isRequired,
  settings: _propTypes2.default.object.isRequired,
  initialValues: _propTypes2.default.object,
  handleClose: _propTypes2.default.func.isRequired
};

exports.default = (0, _reactRedux.connect)(_mapSettings.mapStateToProps)((0, _reactIntl.injectIntl)(ContactForm));

/***/ }),

/***/ "./src/components/WhoIs/ContactForm/validate.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.REASON_LIMIT = undefined;

var _rccHelpers = __webpack_require__("./node_modules/rcc-helpers/lib/index.js");

var _Captcha = __webpack_require__("./src/components/Captcha/index.js");

var REASON_LIMIT = 240;

exports.default = function (values) {
  var errors = {};

  if (!values.emailAddress || values.emailAddress.trim() === '') {
    errors.emailAddress = 'required';
  } else if (!_rccHelpers.validation.emailAddress(values.emailAddress)) {
    errors.emailAddress = 'invalid';
  }

  if (!values.type) {
    errors.type = 'required';
  }

  if (!values.reason || values.reason.trim() === '') {
    errors.reason = 'required';
  } else if (values.reason.trim().length > REASON_LIMIT) {
    errors.reason = 'tooLong';
  }

  if (values.captchaType === _Captcha.types.FALLBACK && !values.response) {
    errors.response = 'required';
  }

  return errors;
};

exports.REASON_LIMIT = REASON_LIMIT;

/***/ }),

/***/ "./src/components/WhoIs/form.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _formik = __webpack_require__("./node_modules/formik/dist/formik.es6.js");

var _reactRedux = __webpack_require__("./node_modules/react-redux/es/index.js");

var _reactIntl = __webpack_require__("react-intl");

var _uxcore = __webpack_require__("@ux/uxcore2");

var _validate = __webpack_require__("./src/components/WhoIs/validate.js");

var _validate2 = _interopRequireDefault(_validate);

var _mapSettings = __webpack_require__("./src/components/map-settings.js");

var _Captcha = __webpack_require__("./src/components/Captcha/index.js");

var _Captcha2 = _interopRequireDefault(_Captcha);

var _util = __webpack_require__("./src/util.js");

var _util2 = _interopRequireDefault(_util);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var InnerForm = function (_Component) {
  _inherits(InnerForm, _Component);

  function InnerForm() {
    _classCallCheck(this, InnerForm);

    var _this = _possibleConstructorReturn(this, (InnerForm.__proto__ || Object.getPrototypeOf(InnerForm)).apply(this, arguments));

    _this.handleFormSubmit = _this.handleFormSubmit.bind(_this);
    return _this;
  }

  _createClass(InnerForm, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var _props = this.props,
          setFieldValue = _props.setFieldValue,
          initialValues = _props.initialValues;

      var captchaType = this.captcha.getType();

      setFieldValue('captchaType', captchaType);

      // Fallback captcha must be manually entered so only auto-search if using Recaptcha.
      if (initialValues.domain && captchaType === _Captcha.types.RECAPTCHA) {
        this.handleFormSubmit();
      }
    }
  }, {
    key: 'handleFormSubmit',
    value: function handleFormSubmit(e) {
      if (e) {
        e.preventDefault();
      }

      if (this.captcha.getType() === _Captcha.types.RECAPTCHA) {
        return void this.captcha.execute();
      }

      this.captcha.refresh();
      this.props.submitForm();
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props2 = this.props,
          settings = _props2.settings,
          canInteract = _props2.canInteract,
          values = _props2.values,
          dirty = _props2.dirty,
          errors = _props2.errors,
          handleBlur = _props2.handleBlur,
          handleChange = _props2.handleChange,
          isSubmitting = _props2.isSubmitting,
          setFieldValue = _props2.setFieldValue,
          setFieldTouched = _props2.setFieldTouched,
          submitForm = _props2.submitForm;


      var disabled = !canInteract || isSubmitting;
      var hasErrors = !!Object.keys(errors).length;

      return _react2.default.createElement(
        'form',
        { onSubmit: this.handleFormSubmit },
        _react2.default.createElement(_Captcha2.default, {
          settings: settings,
          ref: function ref(captcha) {
            _this2.captcha = captcha;
          },
          onChange: function onChange(type, _ref) {
            var recaptcha = _ref.recaptcha,
                challenge = _ref.challenge,
                response = _ref.response;

            setFieldValue('recaptcha', recaptcha);
            setFieldValue('challenge', challenge);
            setFieldValue('response', response);

            if (type === _Captcha.types.RECAPTCHA) {
              submitForm();
            }
          }
        }),
        _react2.default.createElement('input', { type: 'hidden', name: 'captchaType', value: values.captchaType || '' }),
        _react2.default.createElement(
          'div',
          { className: 'search-form' },
          _react2.default.createElement('input', {
            className: 'form-control search-form-input',
            'data-eid': 'storefront.whois.search.input',
            disabled: disabled,
            name: 'domain',
            value: values.domain || '',
            onBlur: handleBlur,
            onChange: handleChange,
            onKeyDown: function onKeyDown() {
              return setFieldTouched('domain');
            }
          }),
          _react2.default.createElement(
            _uxcore.Button,
            {
              design: 'primary',
              type: 'submit',
              'data-eid': 'storefront.whois.search.button.click',
              disabled: disabled || hasErrors || !dirty,
              className: 'search-form-button'
            },
            isSubmitting && _react2.default.createElement(_uxcore.Spinner, { size: 'sm', inline: true, shade: 'dark' }),
            _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'whois.form.button' })
          )
        )
      );
    }
  }]);

  return InnerForm;
}(_react.Component);

InnerForm.propTypes = _extends({}, _util2.default.formikPropTypes, {
  settings: _propTypes2.default.object.isRequired,
  canInteract: _propTypes2.default.bool.isRequired
});

var Form = function (_Component2) {
  _inherits(Form, _Component2);

  function Form(props) {
    _classCallCheck(this, Form);

    var _this3 = _possibleConstructorReturn(this, (Form.__proto__ || Object.getPrototypeOf(Form)).call(this, props));

    _this3.state = {
      domain: props.settings.location.query.domain
    };
    return _this3;
  }

  _createClass(Form, [{
    key: 'render',
    value: function render() {
      var _props3 = this.props,
          settings = _props3.settings,
          handleSubmit = _props3.handleSubmit,
          canInteract = _props3.canInteract;
      var domain = this.state.domain;


      var formProps = {
        validate: _validate2.default,
        onSubmit: handleSubmit,
        initialValues: { domain: domain },
        render: function render(formikProps) {
          return _react2.default.createElement(InnerForm, _extends({ settings: settings, canInteract: canInteract }, formikProps));
        }
      };

      return _react2.default.createElement(_formik.Formik, formProps);
    }
  }]);

  return Form;
}(_react.Component);

Form.propTypes = {
  settings: _propTypes2.default.object.isRequired,
  canInteract: _propTypes2.default.bool.isRequired,
  handleSubmit: _propTypes2.default.func.isRequired
};

exports.default = (0, _reactRedux.connect)(_mapSettings.mapStateToProps)(Form);

/***/ }),

/***/ "./src/components/WhoIs/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _reactIntl = __webpack_require__("react-intl");

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = __webpack_require__("./node_modules/react-redux/es/index.js");

var _uxcore = __webpack_require__("@ux/uxcore2");

var _PageTitle = __webpack_require__("./src/components/PageTitle/index.js");

var _PageTitle2 = _interopRequireDefault(_PageTitle);

var _Card = __webpack_require__("./src/components/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _mapSettings = __webpack_require__("./src/components/map-settings.js");

var _form = __webpack_require__("./src/components/WhoIs/form.js");

var _form2 = _interopRequireDefault(_form);

var _ContactForm = __webpack_require__("./src/components/WhoIs/ContactForm/index.js");

var _ContactForm2 = _interopRequireDefault(_ContactForm);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var addGrowlMessage = _uxcore.Growl.addGrowlMessage;

var WhoIs = function (_Component) {
  _inherits(WhoIs, _Component);

  function WhoIs() {
    _classCallCheck(this, WhoIs);

    var _this = _possibleConstructorReturn(this, (WhoIs.__proto__ || Object.getPrototypeOf(WhoIs)).apply(this, arguments));

    _this.handleSubmit = _this.handleSubmit.bind(_this);
    _this.handleContactOpenClick = _this.handleContactOpenClick.bind(_this);
    _this.handleContactCloseClick = _this.handleContactCloseClick.bind(_this);

    _this.state = {
      canInteract: false,
      showContactModalOnSearch: _this.props.settings.location.query.showContact === 'true'
    };
    return _this;
  }

  _createClass(WhoIs, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.setState({
        canInteract: true
      });
    }
  }, {
    key: 'handleContactOpenClick',
    value: function handleContactOpenClick(e) {
      if (e) {
        e.preventDefault();
      }

      this.setState({ isContactModalOpen: true });
    }
  }, {
    key: 'handleContactCloseClick',
    value: function handleContactCloseClick() {
      this.setState({ isContactModalOpen: false });
    }
  }, {
    key: 'handleSubmit',
    value: function handleSubmit(values, _ref) {
      var _this2 = this;

      var setSubmitting = _ref.setSubmitting;
      var domain = values.domain,
          recaptcha = values.recaptcha,
          challenge = values.challenge,
          response = values.response;
      var _props = this.props,
          intl = _props.intl,
          settings = _props.settings;
      var envPrefix = settings.envPrefix,
          privateLabelId = settings.privateLabelId;


      var query = { privateLabelId: privateLabelId };

      if (recaptcha) {
        query.recaptcha = recaptcha;
      } else {
        query.challenge = challenge;
        query.response = response;
      }

      var options = {
        headers: {
          'Content-Type': 'application/json'
        },
        query: query
      };

      var url = 'https://www.' + envPrefix + 'secureserver.net/api/v1/whois/' + domain.toLowerCase();

      _uxcore.utils.request.get(url, options, function (error, whoisResponse) {
        setSubmitting(false);

        if (error) {
          if (error.status === 404) {
            return void _this2.setState({
              domain: domain,
              isAvailable: true,
              record: null
            });
          } else if (error.status === 403) {
            return void _this2.setState({
              domain: domain,
              isAvailable: false,
              record: null
            });
          }

          return void addGrowlMessage({
            title: intl.formatMessage({ id: 'whois.growls.error.title' }),
            content: intl.formatMessage({ id: 'whois.growls.error.content' }),
            icon: 'error',
            fadeTime: 5000
          });
        }

        _this2.setState({
          domain: domain,
          isAvailable: false,
          isManaged: whoisResponse.isManaged,
          record: whoisResponse.record.trim(),
          isContactModalOpen: whoisResponse.isManaged && _this2.state.showContactModalOnSearch,
          showContactModalOnSearch: false
        });
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          intl = _props2.intl,
          settings = _props2.settings;
      var _state = this.state,
          canInteract = _state.canInteract,
          domain = _state.domain,
          isAvailable = _state.isAvailable,
          isManaged = _state.isManaged,
          record = _state.record,
          isContactModalOpen = _state.isContactModalOpen;


      var body = void 0;

      var envPrefix = settings.envPrefix,
          privateLabelId = settings.privateLabelId;


      if (domain) {
        if (record) {
          body = _react2.default.createElement(
            'div',
            { className: 'whois-record row' },
            _react2.default.createElement(
              'div',
              { className: 'col-md-4 whois-record-sidebar' },
              _react2.default.createElement(
                'h2',
                null,
                _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'whois.sidebar.header' })
              ),
              _react2.default.createElement(
                'p',
                null,
                _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'whois.sidebar.cta' })
              ),
              _react2.default.createElement(
                _uxcore.Button,
                { design: 'primary', href: 'https://mya.' + envPrefix + 'secureserver.net/?pl_id=' + privateLabelId },
                _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'whois.sidebar.action' })
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'col-md-8' },
              _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                  'h2',
                  null,
                  _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'whois.record.header' })
                ),
                _react2.default.createElement(
                  'pre',
                  null,
                  record
                )
              ),
              isManaged && _react2.default.createElement(
                'p',
                null,
                _react2.default.createElement(
                  'a',
                  { href: '#', 'data-eid': 'storefront.whois.contact.open.click', onClick: this.handleContactOpenClick },
                  _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'whois.contactRegistrant' })
                )
              )
            )
          );
        } else if (isAvailable) {
          var action = 'https://www.' + envPrefix + 'secureserver.net/domains/search.aspx?checkAvail=1&pl_id=' + privateLabelId + '&iphoneview=1';

          body = _react2.default.createElement(
            'div',
            { className: 'whois-available row' },
            _react2.default.createElement(
              'div',
              { className: 'col-md-8 offset-md-2' },
              _react2.default.createElement(
                'h2',
                null,
                _react2.default.createElement(_reactIntl.FormattedHTMLMessage, { id: 'whois.available.header', values: { domain: domain } })
              ),
              _react2.default.createElement(
                'form',
                { action: action, method: 'POST' },
                _react2.default.createElement('input', { type: 'hidden', name: 'domainToCheck', value: domain }),
                _react2.default.createElement(
                  _uxcore.Button,
                  { design: 'primary', type: 'submit' },
                  _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'whois.available.action' })
                )
              )
            )
          );
        } else {
          var lastIndexOfDot = domain.lastIndexOf('.');
          var extension = domain.substring(lastIndexOfDot);

          body = _react2.default.createElement(
            'div',
            { className: 'whois-notAvailable row' },
            _react2.default.createElement(
              'div',
              { className: 'col-md-8 offset-md-2' },
              _react2.default.createElement(_uxcore.Alert, {
                icon: 'warning',
                header: intl.formatMessage({ id: 'whois.notAvailable.header' }),
                message: intl.formatMessage({ id: 'whois.notAvailable.body' }, { extension: extension }),
                dismissible: false
              })
            )
          );
        }
      } else {
        body = _react2.default.createElement(
          'div',
          { className: 'whois-interstitial' },
          _react2.default.createElement(
            'h2',
            null,
            _react2.default.createElement(_reactIntl.FormattedHTMLMessage, { id: 'whois.interstitial.title' })
          ),
          _react2.default.createElement(_reactIntl.FormattedHTMLMessage, { id: 'whois.interstitial.gdprBody' })
        );
      }

      return _react2.default.createElement(
        'div',
        { className: 'whois' },
        _react2.default.createElement(
          _PageTitle2.default,
          null,
          _react2.default.createElement(
            'h1',
            { className: 'headline-brand' },
            _react2.default.createElement(_reactIntl.FormattedMessage, { id: 'whois.title' })
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'container row' },
          _react2.default.createElement(
            _Card2.default,
            null,
            _react2.default.createElement(
              'div',
              { className: 'row' },
              _react2.default.createElement(
                'div',
                { className: 'col-md-8 offset-md-2' },
                _react2.default.createElement(_form2.default, {
                  onClose: this.handleContactCloseClick,
                  canInteract: canInteract,
                  handleSubmit: this.handleSubmit
                })
              )
            ),
            body,
            _react2.default.createElement('hr', null),
            _react2.default.createElement(_reactIntl.FormattedHTMLMessage, { id: 'whois.disclaimer' })
          ),
          isContactModalOpen && _react2.default.createElement(_ContactForm2.default, { initialValues: { domain: domain }, handleClose: this.handleContactCloseClick }),
          _react2.default.createElement(_uxcore.Growl, null)
        )
      );
    }
  }]);

  return WhoIs;
}(_react.Component);

WhoIs.propTypes = {
  intl: _reactIntl.intlShape.isRequired,
  settings: _propTypes2.default.object.isRequired
};

exports.default = (0, _reactRedux.connect)(_mapSettings.mapStateToProps)((0, _reactIntl.injectIntl)(WhoIs));

/***/ }),

/***/ "./src/components/WhoIs/validate.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _rccHelpers = __webpack_require__("./node_modules/rcc-helpers/lib/index.js");

var _Captcha = __webpack_require__("./src/components/Captcha/index.js");

exports.default = function (values) {
  var errors = {};

  if (!values.domain) {
    errors.domain = 'required';
  } else if (!_rccHelpers.validation.hostname(values.domain)) {
    errors.domain = 'invalid';
  }

  if (values.captchaType === _Captcha.types.FALLBACK && !values.response) {
    errors.response = 'required';
  }

  return errors;
};

/***/ }),

/***/ "./src/components/map-settings.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var mapStateToProps = exports.mapStateToProps = function mapStateToProps(state) {
  return {
    settings: state.settings
  };
};

/***/ }),

/***/ "./src/data-loaders/agreement.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.load = load;
function load(req, res, _ref, callback) {
  var context = _ref.context,
      params = _ref.params,
      cache = _ref.cache;
  var privateLabelId = context.privateLabelId,
      envPrefix = context.envPrefix,
      market = context.market;
  var id = params.id;


  var url = "https://www." + envPrefix + "secureserver.net/api/v1/agreements/" + privateLabelId + "/" + id + "?marketId=" + market;

  cache.wrapHttp(url, function (error, agreement) {
    if (error) {
      error.url = url;

      return void callback(error);
    }

    callback(null, { legalAgreement: agreement });
  });
}

/***/ }),

/***/ "./src/data-loaders/products.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.load = load;
function load(req, res, _ref, callback) {
  var context = _ref.context,
      cache = _ref.cache;
  var privateLabelId = context.privateLabelId,
      envPrefix = context.envPrefix,
      currency = context.currency,
      market = context.market;


  var url = "https://www." + envPrefix + "secureserver.net/api/v1/catalog/" + privateLabelId + "/products?currencyType=" + currency + "&marketId=" + market;

  cache.wrapHttp(url, function (error, products) {
    if (error) {
      error.url = url;

      return void callback(error);
    }

    callback(null, { products: products });
  });
}

/***/ }),

/***/ "./src/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _reactRouterDom = __webpack_require__("./node_modules/react-router-dom/es/index.js");

var _reactDom = __webpack_require__("react-dom");

var _redux = __webpack_require__("./node_modules/redux/es/redux.js");

var _storefront = __webpack_require__("./src/storefront.js");

var _storefront2 = _interopRequireDefault(_storefront);

var _reducers = __webpack_require__("./src/reducers/index.js");

var _reducers2 = _interopRequireDefault(_reducers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _global$_STORE$settin = global._STORE.settings,
    locale = _global$_STORE$settin.locale,
    messages = _global$_STORE$settin.messages;


var props = {
  store: (0, _redux.createStore)(_reducers2.default, global._STORE),
  locale: locale,
  messages: messages
};

delete global._STORE;

(0, _reactDom.hydrate)

(_react2.default.createElement(
  _reactRouterDom.BrowserRouter,
  null,
  _react2.default.createElement(_storefront2.default, props)
), document.getElementById('body'));

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./src/reducers/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _redux = __webpack_require__("./node_modules/redux/es/redux.js");

var passthrough = function passthrough() {
  var s = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  return s;
};

var storefront = (0, _redux.combineReducers)({
  products: passthrough,
  legalAgreement: passthrough,
  settings: passthrough
});

exports.default = storefront;

/***/ }),

/***/ "./src/route-data.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _enums = __webpack_require__("./node_modules/rcc-helpers/lib/enums/index.js");

var _querystring = __webpack_require__("./node_modules/querystring-es3/index.js");

var _querystring2 = _interopRequireDefault(_querystring);

var _Home = __webpack_require__("./src/components/Home/index.js");

var _Home2 = _interopRequireDefault(_Home);

var _Products = __webpack_require__("./src/components/Products/index.js");

var _Products2 = _interopRequireDefault(_Products);

var _LegalAgreements = __webpack_require__("./src/components/LegalAgreements/index.js");

var _LegalAgreements2 = _interopRequireDefault(_LegalAgreements);

var _agreement = __webpack_require__("./src/components/LegalAgreements/agreement.js");

var _agreement2 = _interopRequireDefault(_agreement);

var _ContactUs = __webpack_require__("./src/components/ContactUs/index.js");

var _ContactUs2 = _interopRequireDefault(_ContactUs);

var _WhoIs = __webpack_require__("./src/components/WhoIs/index.js");

var _WhoIs2 = _interopRequireDefault(_WhoIs);

var _wrapper = __webpack_require__("./src/components/DomainRegistration/wrapper.js");

var _wrapper2 = _interopRequireDefault(_wrapper);

var _find = __webpack_require__("./src/components/DomainRegistration/find.js");

var _find2 = _interopRequireDefault(_find);

var _wrapper3 = __webpack_require__("./src/components/DomainTransfer/wrapper.js");

var _wrapper4 = _interopRequireDefault(_wrapper3);

var _NotFound = __webpack_require__("./src/components/NotFound/index.js");

var _NotFound2 = _interopRequireDefault(_NotFound);

var _products = __webpack_require__("./src/data-loaders/products.js");

var _agreement3 = __webpack_require__("./src/data-loaders/agreement.js");

var _util = __webpack_require__("./src/util.js");

var _util2 = _interopRequireDefault(_util);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

var TURNKEY = [_enums.resellerType.BASIC, _enums.resellerType.PRO];
var TURNKEY_AND_SUPER = TURNKEY.concat(_enums.resellerType.SUPER);
var TURNKEY_AND_API = TURNKEY.concat([_enums.resellerType.API]);

var ALL = TURNKEY_AND_API.concat(_enums.resellerType.SUPER)
// Wild West Domains one-off type.
.concat([7]);

var redirectToSecureserver = function redirectToSecureserver(req, res) {
  var _req$context = req.context,
      privateLabelId = _req$context.privateLabelId,
      envPrefix = _req$context.envPrefix;

  var _req$query = req.query,
      pl_id = _req$query.pl_id,
      plid = _req$query.plid,
      prog_id = _req$query.prog_id,
      progId = _req$query.progId,
      progid = _req$query.progid,
      query = _objectWithoutProperties(_req$query, ['pl_id', 'plid', 'prog_id', 'progId', 'progid']);

  query.plid = privateLabelId;

  var url = 'https://www.' + envPrefix + 'secureserver.net' + req.path + '?' + _querystring2.default.stringify(query);

  res.redirect(url);
};

exports.default = [{
  key: 'home',
  path: '/',
  exact: true,
  component: _Home2.default,
  preRender: _products.load,
  isAvailableToReseller: function isAvailableToReseller(typeId) {
    return TURNKEY_AND_SUPER.includes(typeId);
  }
}, {
  key: 'products/domain-transfer',
  path: '/products/domain-transfer',
  component: _wrapper4.default,
  isAvailableToReseller: function isAvailableToReseller(typeId) {
    return TURNKEY.includes(typeId);
  },
  preRender: function preRender(req, res, _ref, next) {
    var context = _ref.context,
        params = _ref.params;

    if (!params.domainToCheck) {
      return void next();
    }

    var envPrefix = context.envPrefix,
        privateLabelId = context.privateLabelId,
        progId = context.progId,
        itc = context.itc;


    res.redirect(_util2.default.buildDomainTransferUrl({
      envPrefix: envPrefix,
      domain: params.domainToCheck,
      privateLabelId: privateLabelId,
      progId: progId,
      itc: itc
    }));
  }
}, {
  key: 'products/domain-registration',
  path: '/products/domain-registration',
  exact: true,
  component: _wrapper2.default,
  preRender: function preRender(req, res, options, next) {
    var _req$context2 = req.context,
        privateLabelId = _req$context2.privateLabelId,
        useFind = _req$context2.useFind;


    if (useFind) {
      var domainToCheck = req.query.domainToCheck;

      if (domainToCheck) {
        var url = '/products/domain-registration/find?domainToCheck=' + domainToCheck;

        if (!req.isOnCustomDomain) {
          url += '&plid=' + privateLabelId;
        }

        return void res.redirect(url);
      }
    }

    (0, _products.load)(req, res, options, function (error, data) {
      next(error, data);
    });
  },
  isAvailableToReseller: function isAvailableToReseller(typeId) {
    return TURNKEY.includes(typeId);
  }
}, {
  key: 'products/domain-registration/find',
  path: '/products/domain-registration/find',
  component: _find2.default,
  preRender: function preRender(req, res, options, next) {
    if (req.isOnCustomDomain) {
      return void redirectToSecureserver(req, res);
    }

    options.context.domainToCheck = req.query.domainToCheck;

    (0, _products.load)(req, res, options, function (error, data) {
      next(error, data);
    });
  },
  isAvailableToReseller: function isAvailableToReseller(typeId) {
    return TURNKEY.includes(typeId);
  }
}, {
  key: 'products',
  path: '/products/:tag',
  component: _Products2.default,
  preRender: _products.load,
  isAvailableToReseller: function isAvailableToReseller(typeId) {
    return TURNKEY.includes(typeId);
  }
}, {
  key: 'legalAgreements',
  path: '/legal-agreements',
  exact: true,
  component: _LegalAgreements2.default,
  isAvailableToReseller: function isAvailableToReseller(typeId) {
    return ALL.includes(typeId);
  }
}, {
  key: 'legalAgreement',
  path: '/legal-agreement',
  component: _agreement2.default,
  preRender: _agreement3.load,
  isAvailableToReseller: function isAvailableToReseller(typeId) {
    return ALL.includes(typeId);
  }
}, {
  key: 'contactUs',
  path: '/contact-us',
  component: _ContactUs2.default,
  isAvailableToReseller: function isAvailableToReseller(typeId) {
    return TURNKEY_AND_SUPER.includes(typeId);
  }
}, {
  key: 'whois',
  path: '/whois',
  component: _WhoIs2.default,
  isAvailableToReseller: function isAvailableToReseller(typeId) {
    return ALL.includes(typeId);
  }
}, {
  key: 'notFound',
  component: _NotFound2.default,
  isAvailableToReseller: function isAvailableToReseller(typeId) {
    return TURNKEY.includes(typeId);
  }
}];

/***/ }),

/***/ "./src/storefront.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = __webpack_require__("./node_modules/react-redux/es/index.js");

var _reactIntl = __webpack_require__("react-intl");

var _reactRouter = __webpack_require__("./node_modules/react-router/es/index.js");

var _reactRouterDom = __webpack_require__("./node_modules/react-router-dom/es/index.js");

var _routeData = __webpack_require__("./src/route-data.js");

var _routeData2 = _interopRequireDefault(_routeData);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _reactIntl.addLocaleData)({
  locale: 'qa',
  // Locale data requires `pluralRuleFunction`, so provide a stub.
  pluralRuleFunction: function pluralRuleFunction() {
    return 'one';
  }
});

var Storefront = function Storefront(_ref) {
  var store = _ref.store,
      locale = _ref.locale,
      messages = _ref.messages;
  return _react2.default.createElement(
    _reactRedux.Provider,
    { store: store },
    _react2.default.createElement(
      _reactIntl.IntlProvider,
      { locale: locale, messages: messages },
      _react2.default.createElement(
        _reactRouter.Switch,
        null,
        _routeData2.default.map(function (props) {
          var key = props.key,
              path = props.path,
              exact = props.exact,
              component = props.component;


          return _react2.default.createElement(_reactRouterDom.Route, _extends({ key: key }, { path: path, exact: exact, component: component }));
        })
      )
    )
  );
};

Storefront.propTypes = {
  store: _propTypes2.default.object.isRequired,
  locale: _propTypes2.default.string.isRequired,
  messages: _propTypes2.default.object.isRequired
};

exports.default = Storefront;

/***/ }),

/***/ "./src/util.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _uxcore = __webpack_require__("@ux/uxcore2");

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  snakeCase: function snakeCase(value) {
    return value.replace(/-/g, '_');
  },
  stripNonNumerals: function stripNonNumerals(value) {
    return value && value.replace ? value.replace(/[^0-9]/g, '') : value;
  },
  buildDomainTransferUrl: function buildDomainTransferUrl(_ref) {
    var envPrefix = _ref.envPrefix,
        domain = _ref.domain,
        privateLabelId = _ref.privateLabelId,
        progId = _ref.progId,
        itc = _ref.itc;

    return 'https://dcc.' + envPrefix + 'secureserver.net/domains/transfer-in/' + encodeURIComponent(domain) + '?plid=' + privateLabelId + '&prog_id=' + progId + '&itc=' + itc;
  },
  getShopper: function getShopper(_ref2, callback) {
    var env = _ref2.env,
        envPrefix = _ref2.envPrefix,
        privateLabelId = _ref2.privateLabelId;

    var cookieEnvPrefix = env === 'production' ? '' : env;
    var sid = _uxcore.utils.cookie.get(cookieEnvPrefix + 'ShopperId' + privateLabelId) || '';
    var guiUrl = 'https://gui.' + envPrefix + 'secureserver.net/pcjson/salesheader?plId=' + privateLabelId + '&sid=' + sid;

    _uxcore.utils.request.get(guiUrl, { jsonp: true }, function (error, token) {
      if (error) {
        return callback(error);
      } else if (!token || !token.shopperid) {
        return callback();
      }

      var firstName = token.name,
          lastName = token.lastname,
          shopperId = token.shopperid;


      return callback(null, {
        firstName: firstName,
        lastName: lastName,
        shopperId: shopperId
      });
    });
  },
  shouldRenderEnglishOnlySupportDisclaimer: function shouldRenderEnglishOnlySupportDisclaimer(settings) {
    return !settings.providesOwnSupport && !settings.locale.startsWith('en-');
  },


  formikPropTypes: {
    resetForm: _propTypes2.default.func.isRequired,
    submitForm: _propTypes2.default.func.isRequired,
    validateForm: _propTypes2.default.func.isRequired,
    setError: _propTypes2.default.func.isRequired,
    setErrors: _propTypes2.default.func.isRequired,
    setFieldError: _propTypes2.default.func.isRequired,
    setFieldTouched: _propTypes2.default.func.isRequired,
    setFieldValue: _propTypes2.default.func.isRequired,
    setSubmitting: _propTypes2.default.func.isRequired,
    setTouched: _propTypes2.default.func.isRequired,
    handleChange: _propTypes2.default.func.isRequired,
    handleBlur: _propTypes2.default.func.isRequired,
    values: _propTypes2.default.object.isRequired,
    touched: _propTypes2.default.object.isRequired,
    dirty: _propTypes2.default.bool.isRequired,
    errors: _propTypes2.default.object.isRequired,
    isSubmitting: _propTypes2.default.bool.isRequired,
    initialValues: _propTypes2.default.object.isRequired
  }
};

/***/ }),

/***/ "@ux/button":
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__ux_button__;

/***/ }),

/***/ "@ux/checkbox":
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__ux_checkbox__;

/***/ }),

/***/ "@ux/checkbox-group":
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__ux_checkbox_group__;

/***/ }),

/***/ "@ux/dropdown":
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__ux_dropdown__;

/***/ }),

/***/ "@ux/form":
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__ux_form__;

/***/ }),

/***/ "@ux/form-element":
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__ux_form_element__;

/***/ }),

/***/ "@ux/icon":
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__ux_icon__;

/***/ }),

/***/ "@ux/modal":
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__ux_modal__;

/***/ }),

/***/ "@ux/quantity-selector":
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__ux_quantity_selector__;

/***/ }),

/***/ "@ux/radio":
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__ux_radio__;

/***/ }),

/***/ "@ux/tooltip":
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__ux_tooltip__;

/***/ }),

/***/ "@ux/uxcore2":
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__ux_uxcore2__;

/***/ }),

/***/ "react-dom":
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_react_dom__;

/***/ }),

/***/ "react-intl":
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_react_intl__;

/***/ })

},["./src/index.js"]);
});
//# sourceMappingURL=main.1bb02e72.js.map