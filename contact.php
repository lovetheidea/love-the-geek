<?php

	/**
	 * Configure contact form
	 */
	$contact_form_config = array
	(
		/**
		 * Email address to receive messages
		 */
		'recipient' => '',

		/**
		 * Email address to send the messages to your recipient
		 * Requires account permissions when used with an SMTP server
		 *
		 * Uses 'recipient' if not defined
		 */
		'sender' => '',

		/**
		 * Configure subject field
		 * The prefix is prepended to the user subject
		 */
		'subject' => array
		(
			'visible' => true,
			'required' => true,
			'prefix' => '[Contact Form]'
		),

		/**
		 * SMTP connection configuration
		 * Local mail server is used if not configured
		 *
		 * Setting 'secure' supports false, 'tls' or 'ssl'
		 * Setting 'port' supports 25, 465 or 587
		 */
		'smtp' => array
		(
			'hostname' => '',
			'username' => '',
			'password' => '',
			'secure' => 'tls',
			'port' => 587
		),

		/**
		 * Configure ReCaptcha protection
		 * Requires an API v2 key pair to function
		 *
		 * @see https://developers.google.com/recaptcha
		 */
		'recaptcha' => array
		(
			'site_key' => '',
			'secret_key' => ''
		),

		/**
		 * Configure labels and messages
		 * Can be used to customize or translate items
		 */
		'translate' => array
		(
			'labels' => array
			(
				'name' => 'Full Name',
				'email' => 'Email Address',
				'subject' => 'Subject',
				'message' => 'Message'
			),
			'errors' => array
			(
				'required' => '%s is required',
				'invalid' => '%s is invalid'
			),
			'messages' => array
			(
				'error' => 'We have encountered an unknown error, please try again.',
				'success' => 'We have received your message and will get back to you shortly.'
			)
		)
	);

	/**
	 * Load contact form class and its dependencies
	 * Create new class object with given configuration
	 */
	require __DIR__ . DIRECTORY_SEPARATOR . 'php' . DIRECTORY_SEPARATOR . 'contact-form.php';
	$contact_form = new Contact_Form($contact_form_config);

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Love the Geek - Hosting and Technology</title>
		<meta name="description" content="Love the Geek is perfectly suited for app and web technology companies.">
		<meta name="keywords" content="html template, responsive, retina, cloud hosting, technology, startup">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<!-- Icons -->
		<link rel="apple-touch-icon-precomposed" href="img/icons/apple-touch-icon.png">
		<link rel="icon" href="img/icons/favicon.ico">
		<!-- Stylesheets -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/main.min.css">
	</head>
	<body class="footer-dark">
		<!-- Header -->
		<header id="header" class="header-dynamic header-shadow-scroll">
			<div class="container">
				<a class="logo" href="index.html">
					<img src="img/logos/header-light.png" alt="">
				</a>
				<nav>
					<ul class="nav-primary">
						<li>
							<a href="index.html">Products</a>
							<ul>
								<li>
									<a href="products-domain-names.html">Domain Names</a>
								</li>
								<li>
									<a href="products-anycast-dns.html">Website Builder</a>
								</li>
								<li style="color: #ddd;margin-left: 10px">----------------</li>
								<li>
									<a href="products-cloud-hosting.html">Business Hosting</a>
								</li>
								<li>
									<a href="products-dedicated-cloud.html">Dedicated Hosting</a>
								</li>
								<li>
									<a href="products-cloud-servers.html">CRM Hosting</a>

								</li>
								<li style="color: #ddd;margin-left: 10px">----------------</li>
								<li>
									<a href="products-block-storage.html">Email Delivery</a>
								</li>
								<li>
									<a href="products-ssl-certificates.html">SSL Certificates</a>
								</li>

							</ul>
						</li>
						<li>
							<a href="home-custom-color.html">Backup</a>

						</li>
						<li>
							<a href="home-product-slider.html">Security</a>
						</li>
						<li>
							<a href="about.html">Company</a>
							<ul>
								<li>
									<a href="about.html">News</a>
								</li>
								<li>
									<a href="contact.html">Contact</a>
								</li>
							</ul>
						</li>
						<li>
							<a class="button button-secondary" href="https://sso.secureserver.net/?plid=539528&prog_id=539528&realm=idp&path=%2Fproducts&app=account">
								<i class="fa fa-lock icon-left"></i>Login
							</a>
						</li>
						<li>
							<a class="button btn-basket button-secondary" href="https://lovetheidea.co.uk">
								Custom Dev/Design
							</a>
						</li>
					</ul>
					<ul class="nav-secondary">
						<li>
							<a href="contact.html"><i class="fa fa-phone icon-left"></i>+44 (0)20 3014 1446</a>
						</li>
						<li>
							<a href="contact.html"><i class="fa fa-comment icon-left"></i>Support Ticket</a>
						</li>
						<li>
							<a href="https://www.secureserver.net/help?pl_id=539528&prog_id=539528"><i class="fa fa-question-circle icon-left"></i>Knowledge Base</a>
						</li>
						<li>
							<a href="service-status.html"><i class="fa fa-check icon-left"></i>Service Status</a>
						</li>
					</ul>
				</nav>
			</div>
		</header>
		<!-- Content -->
		<section id="content">
			<!-- Content Row -->
			<section class="content-row content-row-color content-row-clouds">
				<div class="container">
					<header class="content-header content-header-small content-header-uppercase">
						<h1>
							Contact Us
						</h1>
						<p>
							Send us a message and our team will be in touch very soon.
						</p>
					</header>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row">
				<div class="container">
					<div class="column-row align-center">
						<div class="column-50">
							<form class="form-full-width" method="post" action="contact.php">
								<?php if ($contact_form->success): ?>
								<div class="form-row form-success">
									<ul>
										<li><?php echo $contact_form->config['translate']['messages']['success'] ?></li>
									</ul>
								</div>
								<div class="form-row">
									<div class="feature-box">
										<div class="feature-content">
											<dl>
												<dt><?php echo $contact_form->config['translate']['labels']['name'] ?></dt>
												<dd><?php if (isset($contact_form->sanitize['name'])) echo $contact_form->sanitize['name'] ?></dd>
											</dl>
											<dl>
												<dt><?php echo $contact_form->config['translate']['labels']['email'] ?></dt>
												<dd><?php if (isset($contact_form->sanitize['email'])) echo $contact_form->sanitize['email'] ?></dd>
											</dl>
											<?php if (empty($contact_form->sanitize['subject']) === false): ?>
											<dl>
												<dt><?php echo $contact_form->config['translate']['labels']['subject'] ?></dt>
												<dd><?php if (isset($contact_form->sanitize['subject'])) echo $contact_form->sanitize['subject'] ?></dd>
											</dl>
											<?php endif; ?>
											<dl>
												<dt><?php echo $contact_form->config['translate']['labels']['message'] ?></dt>
												<dd><?php if (isset($contact_form->sanitize['message'])) echo nl2br($contact_form->sanitize['message']) ?></dd>
											</dl>
										</div>
									</div>
								</div>
								<?php else: ?>
								<?php if ($contact_form->errors): ?>
								<div class="form-row form-error">
									<ul>
										<?php foreach ($contact_form->errors as $error): ?>
										<li><?php echo $error ?></li>
										<?php endforeach; ?>
									</ul>
								</div>
								<?php endif; ?>
								<div class="form-row">
									<div class="column-row">
										<div class="column-50">
											<label for="form-name"><?php echo $contact_form->config['translate']['labels']['name'] ?></label>
											<input id="form-name" type="text" name="name" value="<?php if (isset($contact_form->sanitize['name'])) echo $contact_form->sanitize['name'] ?>" required>
										</div>
										<div class="column-50">
											<label for="form-email"><?php echo $contact_form->config['translate']['labels']['email'] ?></label>
											<input id="form-email" type="email" name="email" value="<?php if (isset($contact_form->sanitize['email'])) echo $contact_form->sanitize['email'] ?>" required>
										</div>
									</div>
								</div>
								<?php if ($contact_form->config['subject']['visible']): ?>
								<div class="form-row">
									<label for="form-subject"><?php echo $contact_form->config['translate']['labels']['subject'] ?></label>
									<input id="form-subject" type="text" name="subject" value="<?php if (isset($contact_form->sanitize['subject'])) echo $contact_form->sanitize['subject'] ?>"<?php if ($contact_form->config['subject']['required']) echo ' required' ?>>
								</div>
								<?php endif; ?>
								<div class="form-row">
									<label for="form-message"><?php echo $contact_form->config['translate']['labels']['message'] ?></label>
									<textarea id="form-message" name="message" cols="10" rows="10" required><?php if (isset($contact_form->sanitize['message'])) echo $contact_form->sanitize['message'] ?></textarea>
								</div>
								<?php if ($contact_form->config['recaptcha']['site_key'] && $contact_form->config['recaptcha']['secret_key']): ?>
								<div class="form-row">
									<div class="g-recaptcha" data-sitekey="<?php echo $contact_form->config['recaptcha']['site_key'] ?>"></div>
									<script src="https://www.google.com/recaptcha/api.js"></script>
								</div>
								<?php endif; ?>
								<div class="form-row">
									<button class="button-secondary"><i class="fa fa-envelope icon-left"></i>Send Message</button>
								</div>
								<?php endif; ?>
								<?php if ($contact_form->debug && is_object($contact_form->debug)): ?>
								<div class="form-row">
									<pre><?php print_r($contact_form->debug) ?></pre>
								</div>
								<?php endif; ?>
							</form>
						</div>
					</div>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row content-row-gray">
				<div class="container">
					<div class="column-row align-center text-align-center">
						<div class="column-33">
							<i class="fa fa-support icon-feature"></i>
							<h3>
								Customer Support
							</h3>
							<p>
								Please submit a ticket through our client portal to contact us if you are already a customer.
							</p>
							<p>
								<a href="#customer-support">Open a Ticket<i class="fa fa-angle-right icon-right"></i></a>
							</p>
						</div>
						<div class="column-33">
							<i class="fa fa-comments icon-feature"></i>
							<h3>
								IRC Community
							</h3>
							<p>
								Chat with our IRC community to learn more about cloud hosting, management and networking.
							</p>
							<p>
								<a href="#irc-community">Connect to IRC<i class="fa fa-angle-right icon-right"></i></a>
							</p>
						</div>
						<div class="column-33">
							<i class="fa fa-exclamation-triangle icon-feature"></i>
							<h3>
								Bounty Program
							</h3>
							<p>
								We are dedicated to keep our platform secure and offer a bounty for every reported security issue.
							</p>
							<p>
								<a href="#bounty-program">Report a Bug<i class="fa fa-angle-right icon-right"></i></a>
							</p>
						</div>
					</div>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row content-row-color">
				<div class="container">
					<header class="content-header">
						<h2>
							Interested in visiting us?
						</h2>
						<p>
							Please schedule an appointment with our sales team if you're interested in visiting one of our branches, datacenters or meeting with our team of technicians in person.<br><br>
							<a class="button button-secondary" href="about.html">
								<i class="fa fa-globe icon-left"></i>Global Branches
							</a>
						</p>
					</header>
				</div>
			</section>
		</section>
		<!-- Footer -->
		<footer id="footer">
			<section class="footer-primary">
				<div class="container">
					<div class="column-row">
						<div class="column-33">
							<h5>
								About Love the Geek
							</h5>
							<p>
								We provide cloud based enterprise hosting, server and storage solutions of unmatched quality. Feel free to email or chat to us for a custom quote.<br><br>
								HQ: London, United Kingdom<br>
								+44 (0)20 3014 1446
							</p>
						</div>
						<div class="column-66">
							<div class="column-row align-right-top">
								<div class="column-25">
									<h5>
										Connect
									</h5>
									<ul class="list-style-icon">
										<li>
											<a href="#facebook"><i class="fa fa-facebook"></i>Facebook</a>
										</li>
										<li>
											<a href="https://twitter.com/lovethegeek"><i class="fa fa-twitter"></i>Twitter</a>
										</li>
										<li>
											<a href="#instagram"><i class="fa fa-instagram"></i>Instagram</a>
										</li>
										<li>
											<a href="#xing"><i class="fa fa-xing"></i>Xing</a>
										</li>
									</ul>
								</div>
								<div class="column-25">
									<h5>
										Products
									</h5>
									<ul>
										<li>
											<a href="products-cloud-hosting.html">Business Hosting</a>
										</li>
										<li>
											<a href="products-cloud-servers.html">CRM Hosting</a>
										</li>
										<li>
											<a href="products-dedicated-cloud.html">Dedicated Hosting</a>
										</li>
										<li>
											<a href="products-block-storage.html">Email Delivery</a>
										</li>
										<li>
											<a href="products-anycast-dns.html">Website Builder</a>
										</li>
									</ul>
								</div>
								<div class="column-25">
									<h5>
										Resources
									</h5>
									<ul>
										<li>
											<a href="home-custom-color.html">Backup</a>
										</li>
										<li>
											<a href="home-product-slider.html">Security</a>
										</li>
										<li>
											<a href="https://www.secureserver.net/legal-agreement?id=privacy&pl_id=539528">Privacy Policy</a>
										</li>
										<li>
											<a href="https://www.secureserver.net/legal-agreements?plid=539528">Terms of Service</a>
										</li>
									</ul>
								</div>
								<div class="column-flex">
									<h5>
										Company
									</h5>
									<ul>
										<li><a href="about.html">News</a></li>
										<li><a href="https://lovetheidea.co.uk">Custom Developers</a></li>
										<li><a href="contact.html">Contact</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="footer-secondary">
				<div class="container">
					<p>
						Copyright 2018 &copy; Love the Geek - Hosting Services. All rights reserved.<br>
						<a href="https://www.secureserver.net/legal-agreements?plid=539528">Legal</a> | <a href="https://www.secureserver.net/legal-agreement?plid=539528&id=privacy">Privacy</a>
					</p>
				</div>
			</section>
		</footer>
		<!-- Scripts -->
		<script src="js/jquery.min.js"></script>
		<script src="js/headroom.min.js"></script>
		<script src="js/js.cookie.min.js"></script>
		<script src="js/imagesloaded.min.js"></script>
		<script src="js/bricks.min.js"></script>
		<script src="js/main.min.js"></script>
	</body>
</html>